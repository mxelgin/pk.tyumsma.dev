ALTER TABLE `pk_entrant`  ADD `achv` TINYINT(1) NOT NULL;

ALTER TABLE `pk_entrant` ADD `medblock` TINYINT( 1 ) NOT NULL;

CREATE TABLE IF NOT EXISTS `direct_priority_pravo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `direct_priority_pravo_others` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `link_entrant_achv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entrantid` int(11) NOT NULL,
  `achvid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

CREATE TABLE IF NOT EXISTS `link_entrant_pravo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entrantid` int(11) NOT NULL,
  `pravoid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

CREATE TABLE IF NOT EXISTS `direct_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `direct_grade` (`id`, `name`) VALUES
(1, 'Химия'),
(2, 'Биология'),
(3, 'Русский язык');

CREATE TABLE IF NOT EXISTS `pk_config_ppm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specid` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `pk_config_gm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specid` int(11) NOT NULL,
  `gradeid` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;