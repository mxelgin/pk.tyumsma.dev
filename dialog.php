<!doctype html>
 
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>jQuery UI Dialog - Modal form</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
 
  <style>
    body { font-size: 62.5%; }
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 350px; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>

  <script>
  $(function() {
    var marks = $( "#marks" ),
      email = $( "#email" ),
      password = $( "#password" ),
      allFields = $( [] ).add( name ).add( email ).add( password ),
      tips = $( ".validateTips" );
      $.mask.definitions['5']='[345]';
      $( "#marks" ).mask("5,? 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5");
 
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 200,
      width: 400,
      modal: true,
      buttons: {
        "Рассчитать": function() {
          var bValid = true;
          allFields.removeClass( "ui-state-error" );
 
          //console.debug(marks.val());
          //bValid = bValid && checkLength( marks, "marks", 1, 256 );
          //bValid = bValid && checkRegexp( marks, /^([3-5,_])+$/i, "Оценки должны быть записаны цифрами через запятую." );

          var n = marks.val().split(",");
          var k = 0;
          var j = 0;
          for (i = 0; i < n.length; i++) {
              m = n[i];
              if (m != 3 && m != 4 && m != 5) continue;
              //else console.debug(m + ' ' + i);
              m = parseInt(m);
              k += m;
              j++;
          }
          k = (k / j);
          k = k.toFixed(2);
        
 
          if ( bValid ) {
              /*
            $( "#users tbody" ).append( "<tr>" +
              "<td>" + marks.val() + "</td>" +
              "<td>" + k + "</td>" +
            "</tr>" );
            */
            $( "#mid-mark" ).text( k );
            $( this ).dialog( "close" );
          }
        },
        "Отмена": function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        allFields.val( "" ).removeClass( "ui-state-error" );
      }
    });
 
    $( "#create-user" )
      .button()
      .click(function() {
        $( "#dialog-form" ).dialog( "open" );
      });
  });
  </script>
</head>
<body>
 
<div id="dialog-form" title="Рассчитать средний балл">
  <p class="validateTips">Введите оценки через запятую.</p>
 
  <form>
  <fieldset>
    <label for="name">Оценки</label>
    <input type="text" name="marks" id="marks" class="text ui-widget-content ui-corner-all" />
  </fieldset>
  </form>
</div>
 
 
<div id="users-contain" class="ui-widget">
  <h1>Средний балл</h1>
  <div id="mid-mark"></div>
  <!--
  <table id="users" class="ui-widget ui-widget-content">
    <thead>
      <tr class="ui-widget-header ">
        <th>Оценки</th>
        <th>Средний балл</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>3, 4, 5</td>
        <td>4</td>
      </tr>
    </tbody>
  </table>
  -->
</div>
<button id="create-user">Рассчитать</button>
 
 
</body>
</html>
