    function getKladr( type, _parent, request, response, more, sfx ) {
        more = typeof more !== 'undefined' ? more : '';
        sfx = typeof sfx !== 'undefined' ? true : false;
        /*
        console.debug(request.term);
        console.debug(type);
        console.debug(_parent);
        console.debug(more);
        */
        $.ajax({
                url: "/pkentrant/getKladr",
                dataType: "jsonp",
                data: {
                        query: request.term,
                        type: type,
                        _parent: _parent,
                        more: more
                },
                success: function( data ) {
                    console.debug(data);
                    response( $.map( data.data, function( item ) {
                        if (sfx) name = item.sfx + ' ' + item.name;
                        else name = item.name;
                            return {
                                label: name,
                                id: item.id,
                                code: item.code
                            }
                    }));
                }
        });
    }
    function initField( type, id ) {
        $.ajax({
                url: "/pkentrant/getKladr",
                dataType: "jsonp",
                data: {
                        query: '',
                        type: type,
                        _parent: id,
                        more: ''
                },
                success: function( data ) {
                    $.map( data.data, function( item ) {
                        console.debug(item);
                        $( "#" + type ).val( item.name );
                        $( "#" + type + "-id" ).val( item.id );
                    });
                }
        });
        console.debug(type + ' ' + id);
    }
    $(document).ready(function() {

        code = $( "#Entrant_kladr_street" ).val();
        if (typeof code === 'undefined') {
            initField( 'parent', $("#parent-id").val());
            return;
        }
        //if (code.length < 17) code = '0' + code;

        $( "#region-id" ).attr( "disabled", true );
        $( "#raion-id" ).attr( "disabled", true );
        $( "#city-id" ).attr( "disabled", true );
    //    $( "#Entrant_kladr_street" ).attr( "disabled", true );

        if (code == 0) {
            $( "#raion" ).attr( "disabled", true );
            $( "#city" ).attr( "disabled", true );
            $( "#street" ).attr( "disabled", true );
            $( "#Entrant_home_build" ).attr( "disabled", true );
            $( "#Entrant_home_kv" ).attr( "disabled", true );
        } else {
            initField( 'region', code.substr(0, 2) );
            initField( 'raion', code.substr(0, 5) );
            initField( 'city', code.substr(0, 11) );
            initField( 'street', code );
        }
    });

    $(function() {
        /*
        $('#region, #raion, #city, #street, #Entrant_home_build').keydown( function(e) {
            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            if(key == 13) {
                e.preventDefault();
                var inputs = $(this).closest('form').find(':input:visible');
                inputs.eq( inputs.index(this)+ 1 ).focus();
            }
        });
        */

        $( "#region" ).autocomplete({
            minLength: 3,
            delay: 500,
            source: function( request, response ) { getKladr( 'region', "", request, response ); },
            select: function (data, value) {
                //console.debug(value);
                $( "#region-id" ).val( value.item.id );
                if ( value.item != null ) {
                    $( "#raion" ).attr( "disabled", false );
                    $( "#city" ).attr( "disabled", false );
                }
            },
            change: function (data, value) {
                $( "#raion" ).val( "" );
                $( "#raion-id" ).val( "" );
                $( "#city" ).val( "" );
                $( "#city-id" ).val( "" );
                $( "#street" ).val( "" );
                $( "#street-id" ).val( "" );

                if ( value.item == null ) {
                    $( "#region" ).val( "" );
                    $( "#region-id" ).val( "" );

                    $( "#raion" ).attr( "disabled", true );
                    $( "#city" ).attr( "disabled", true );
                    $( "#street" ).attr( "disabled", true);
                    $( "#Entrant_home_build" ).attr( "disabled", true );
                    $( "#Entrant_home_kv" ).attr( "disabled", true );
                } else {
                    $( "#raion" ).attr( "disabled", false );
                    $( "#city" ).attr( "disabled", false );
                }
            },
        });

        $( "#raion" ).autocomplete({
            minLength: 3,
            delay: 500,
            source: function( request, response ) { getKladr( 'raion', $("#region-id").val(), request, response ); },
            select: function (data, value) {
                //console.debug(value);
                $( "#raion-id" ).val( value.item.id );
            },
            change: function (data, value) {
                if ( value.item == null ) {
                    $( "#raion" ).val( "" );
                    $( "#raion-id" ).val( "" );
                }
                $( "#city" ).val( "" );
                $( "#city-id" ).val( "" );
                $( "#street" ).val( "" );
                $( "#street-id" ).val( "" );
            },
        });

        $( "#city" ).autocomplete({
            minLength: 3,
            delay: 500,
            source: function( request, response ) { 
                if ($("#raion-id").val() == "") _parent = $("#region-id").val();
                else _parent = $("#raion-id").val();
                getKladr( 'city', _parent, request, response, 'and id_status in (0, 1) and id_type not in (101, 102, 104, 105, 106, 107, 201, 205, 445, 526)', true); 
            },
            select: function (data, value) {
                //console.debug(value);
                $( "#city-id" ).val( value.item.code );
                if ( value.item != null ) {
                    $( "#street" ).attr( "disabled", false );
                }
            },
            change: function (data, value) {
                $( "#street" ).val( "" );
                $( "#street-id" ).val( "" );

                if ( value.item == null ) {
                    $( "#city" ).val( "" );
                    $( "#city-id" ).val( "" );

                    $( "#street" ).attr( "disabled", true );
                    $( "#Entrant_home_build" ).attr( "disabled", true );
                    $( "#Entrant_home_kv" ).attr( "disabled", true );
                } else {
                    $( "#street" ).attr( "disabled", false );
                }
            },
        });

        $( "#street" ).autocomplete({
            minLength: 3,
            delay: 500,
            source: function( request, response ) { getKladr( 'street', $("#city-id").val(), request, response, 'and id_status = 0' ); },
            select: function (data, value) {
                //console.debug(value);
                $( "#Entrant_kladr_street" ).val( value.item.id );
                if ( value.item != null ) {
                    $( "#Entrant_home_build" ).attr( "disabled", false );
                    $( "#Entrant_home_kv" ).attr( "disabled", false );
                }
            },
            change: function (data, value) {
                if ( value.item == null ) {
                    $( "#street" ).val( "" );
                    $( "#Entrant_kladr_street" ).val( "" );
                    $( "#Entrant_home_build" ).attr( "disabled", true );
                    $( "#Entrant_home_kv" ).attr( "disabled", true );
                } else {
                    $( "#Entrant_home_build" ).attr( "disabled", false );
                    $( "#Entrant_home_kv" ).attr( "disabled", false );
                }
            },
        });

        $( "#parent" ).autocomplete({
            minLength: 3,
            delay: 500,
            source: function( request, response ) { 
                getKladr( 'parent', "", request, response, 'and id_status in (0, 1) and id_type not in (101, 102, 104, 105, 106, 107, 201, 205, 445, 526)', true); 
            },
            select: function (data, value) {
                console.debug(value);
                $( "#id_parent" ).val( value.item.code );
                $( "#parent-id" ).val( value.item.code );
                initField( 'parent', value.item.code );
            },
            change: function (data, value) {
                if ( value.item == null ) {
                    $( "#id_parent" ).val( "" );
                    $( "#parent-id" ).val( "" );
                    $( "#parent" ).val( "" );
                }
            },
        });

    });

