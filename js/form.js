function dublicaterow($what, $to){
    //counter ++;
    cloned = $($what).last().clone();
    cloned.find('input[type=text], select').each(function(){
        iname = $(this).attr('name').split('[');
        id = $(this).attr('id').split('_');
        counter=parseInt(id[id.length-1], 10)+1;
        $(this).attr('name', iname[0] + '[' + counter + ']');
        $(this).attr('id', id[0] + '_' + counter);
        $(this).attr('value', '');
    });
    var radiocounter=0;
    cloned.find('input[type=checkbox], input[type=radio]').each(function(){
        iname = $(this).attr('name').split('[');
        id = $(this).attr('id').split('_');
        $(this).attr('name', iname[0] + '[' + counter + ']');
        $(this).attr('id', id[0] + '_' + counter + '_' + radiocounter);
        $(this).removeAttr('checked');
        radiocounter++;
    });
    cloned.find('label').each(function(){
        forid = $(this).attr('for').split('_');
        $(this).attr('for', forid[0] + '_' + counter);
    });
    cloned.find('option').each(function(){
        $(this).removeAttr('selected');
    });
    number= parseInt($($what+'number').last().html(), 10)+1;
    cloned.find($what+'number').html(number+'.');
    cloned.appendTo($to);
}

function removerow(selector){
    $(selector+":last").remove();
    var countrow=0;
    $('body').find(selector).each(function(){
        countrow++;
    });
    if (countrow==1){
        $('#removebuttom').addClass('hide');
    }
}

function inputformat(selector){
/*
*/
    $(selector+' input:text').setMask();
    $(selector+' input[placeholder], textarea[placeholder]').placeholder();
    $(".datepicker").datepicker({dateFormat: "dd.mm.yy", changeMonth: true, changeYear: true});
}

function enableorphan(id){
    //console.debug($('#orphan' + id).attr('checked'));
    if ($('#orphan' + id).attr('checked') != 'checked'){
        $('#orphan' + id + '_type').attr('disabled', 'disabled');
        $(":radio[name='orphan"+id+"_type']").attr('disabled', 'disabled');

        $('#parent' + id).removeAttr('disabled');
        $('#parent_fio_' + id).removeAttr('disabled');
        $('#parent_addr_' + id).removeAttr('disabled');
        $('#parent_work_' + id).removeAttr('disabled');
        $('#parent_phone_' + id).removeAttr('disabled');
    }else{
        $('#orphan' + id + '_type').removeAttr('disabled');
        $(":radio[name='orphan"+id+"_type']").removeAttr('disabled');

        if ( $('#orphan' + id + '_type input:checked').length == 0) {
            $('#orphan' + id + '_type_0').attr('checked', 'checked');
        }

        if ( $('#orphan' + id + '_type input:checked').val() == 3) {
            $('#parent' + id).removeAttr('disabled');
            $('#parent_fio_' + id).removeAttr('disabled');
            $('#parent_addr_' + id).removeAttr('disabled');
            $('#parent_work_' + id).removeAttr('disabled');
            $('#parent_phone_' + id).removeAttr('disabled');
        } else {
            $('#parent' + id).attr('disabled', 'disabled');
            $('#parent_fio_' + id).attr('disabled', 'disabled');
            $('#parent_addr_' + id).attr('disabled', 'disabled');
            $('#parent_work_' + id).attr('disabled', 'disabled');
            $('#parent_phone_' + id).attr('disabled', 'disabled');
        }
    }
    //console.debug($('#orphan' + id + '_type').attr('disabled'));
}

function enablemailnum(obj){
    if ($('#Entrant_is_mail').attr('checked') != 'checked'){
        $('#Entrant_mail_num').attr('disabled', 'disabled');
    }else{
        $('#Entrant_mail_num').removeAttr('disabled');
    }
}

function getCurrentDate() {
    var now = new Date();
    d = now.getDate();
    m = now.getMonth() + 1;
    y = now.getFullYear();

    if (d < 10) d = '0' + d;
    if (m < 10) m = '0' + m;

    return d + '.' + m + '.' + y;
}

function enableorig(obj){
    if ($('#Entrant_orig').attr('checked') != 'checked'){
        $('#Entrant_orig_date').attr('disabled', 'disabled');
    }else{
        $('#Entrant_orig_date').removeAttr('disabled');
        $('#Entrant_orig_date').val(getCurrentDate());
        //if ($('#Entrant_orig_date').val() == '0000-00-00') { }
    }
}

function enabletook(obj){
    if ($('#Entrant_took').attr('checked') != 'checked'){
        $('#Entrant_took_date').attr('disabled', 'disabled');
    }else{
        $('#Entrant_took_date').removeAttr('disabled');
        $('#Entrant_took_date').val(getCurrentDate());
        //if ($('#Entrant_took_date').val() == '0000-00-00') { }
    }
}

function hidemilitary(obj){
    if (obj.value==2){
        $('.military').addClass('hide');
    }else{
        $('.military').removeClass('hide');
    }
    
}

$.fn.focusNextInputField = function() {
    return this.each(function() {
        var fields = $(this).parents('form:eq(0),body').find('button,input,textarea,select');
        var index = fields.index( this );
        if ( index > -1 && ( index + 1 ) < fields.length ) {
            fields.eq( index + 1 ).focus();
        }
        return false;
    });
};

function clearcityinput(obj, selectorspan){
    contentspan=$(selectorspan).html();
    if (contentspan=='') $(obj).attr('value', '');
}

function intval( mixed_var, base ) {
	var tmp;

	if( typeof( mixed_var ) == 'string' ){
		tmp = parseInt(mixed_var);
		if(isNaN(tmp)){
			return 0;
		} else{
			return tmp.toString(base || 10);
		}
	} else if( typeof( mixed_var ) == 'number' ){
		return Math.floor(mixed_var);
	} else{
		return 0;
	}
}

function beforesendform(){
    $('input[placeholder], textarea[placeholder]').each(function(){
        if ($(this).val()==$(this).attr('placeholder')){
            $(this).val('');
        }
    });
    if ($("#maindocinput").hasClass('loading')){
        return false;
    }
    $('#entrant-form').submit();
}

function budjet_disable(){
    $('#type_b_0').removeAttr('checked');
    $('#type_b_0').attr('disabled', 'disabled');
    $('#type_b_1').removeAttr('checked');
    $('#type_b_1').attr('disabled', 'disabled');
    $('#type_b_2').removeAttr('checked');
    $('#type_b_2').attr('disabled', 'disabled');
    $('#type_b_3').removeAttr('checked');
    $('#type_b_3').attr('disabled', 'disabled');
}

function budjet_enable(){
    $('#type_b_0').removeAttr('disabled');
    $('#type_b_1').removeAttr('disabled');
    $('#type_b_2').removeAttr('disabled');
    $('#type_b_3').removeAttr('disabled');
}
