<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'ТюмГМА Приемная комиссия',

    //Год постпления
    //'yearReceipts' => '2014',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.controllers.LogController',
        'application.controllers.PkexportController',
                //'application.modules.rights.*',
                //'application.modules.rights.components.*',
                'ext.EDataTables.*',
                'ext.MPDF56.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'f,bnehf',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('*','::1'),
		),
                /*'rights'=>array(
                    'userClass' => 'Auth', 
                    'superuserName'=>'admin',                       //Name of the role with super user privileges.
                    'authenticatedName'=>'guest',                   //Name of the authenticated user role.
                    'userIdColumn'=>'userid',                       //Name of the user id column in the database.
                    'userNameColumn'=>'login',                      //Name of the user name column in the database.
                    'enableBizRule'=>true,                          //Whether to enable authorization item business rules.
                    'enableBizRuleData'=>false,                     //Whether to enable data for business rules.
                    'displayDescription'=>true,                     //Whether to use item description instead of name.
                    'flashSuccessKey'=>'RightsSuccess',             //Key to use for setting success flash messages.
                    'flashErrorKey'=>'RightsError',                 //Key to use for setting error flash messages.
                    'install'=>false,                               //Whether to install rights.
                    'baseUrl'=>'/rights',                           //Base URL for Rights. Change if module is nested.
                    'layout'=>'rights.views.layouts.main',          //Layout to use for displaying Rights.
                    'appLayout'=>'application.views.layouts.main',  //Application layout.
                    'cssFile'=>'rights.css',                        //Style sheet file to use for Rights.    
                    'debug'=>true,                                  //Whether to enable debug mode.
                ),*/
	),

	// application components
	'components'=>array(
		'user'=>array(
                        'class'=>'WebUser',
                        'loginUrl'=>array('site/index'),
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
                'authManager'=>array(
                    'class'=>'PhpAuthManager',
                    //'connectionID'=>'db',
                    //'itemTable'=>'authitem',
                    //'itemChildTable'=>'authitemchild',
                    //'assignmentTable'=>'authassignment',
                    //'rightsTable'=>'rights',
                    'defaultRoles' => array('guest'),
                ),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
//			'rules'=>array(
//				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
//				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
//				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
//			),
		),
		
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
            'enableProfiling'=>true,
            'enableParamLogging' => true,

			'connectionString' => 'mysql:host=localhost;dbname=pk_2014',
			'emulatePrepare' => true,
			'username' => 'pk',
			'password' => 'azMUjuBXJHE5AWBv',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',

//                    'class'=>'CProfileLogRoute',
//                    'levels'=>'profile',
//                    'enabled'=>true,
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
    
        'sourceLanguage' => 'en',
        'language' => 'ru',
    
);
