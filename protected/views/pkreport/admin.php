<h1>Отчеты для министерств</h1>

<ul>
    <li>Отчет о выполнении государственного задания на подготовку специалистов с высшим профессиональным образованием (очная форма). 
        <a href="/pkreport/report1" target="_blank">Сформировать</a>.
        <a href="/pkreport/report1?type=pdf" target="_blank">Скачать PDF</a>.
        <a href="/pkreport/report1?type=xls" target="_blank">Скачать XLS</a>.
    </li>
    <li>Отчет о выполнении государственного задания на подготовку специалистов с высшим профессиональным образованием (заочная форма).
        <a href="/pkreport/report2" target="_blank">Сформировать</a>.
        <a href="/pkreport/report2?type=pdf" target="_blank">Скачать PDF</a>.
        <a href="/pkreport/report2?type=xls" target="_blank">Скачать XLS</a>.
    </li>
    <li>Форма 76 КД (очная форма).
        <a href="/pkreport/report3" target="_blank">Сформировать</a>.
        <a href="/pkreport/report3?type=pdf" target="_blank">Скачать PDF</a>.
        <a href="/pkreport/report3?type=xls" target="_blank">Скачать XLS</a>.
    </li>
    <li>Форма 76 КД (заочная форма).
        <a href="/pkreport/report4" target="_blank">Сформировать</a>.
        <a href="/pkreport/report4?type=pdf" target="_blank">Скачать PDF</a>.
        <a href="/pkreport/report4?type=xls" target="_blank">Скачать XLS</a>.
    </li>
    <li>Информация об объемах и динамике целевого приема в вузы по направлениям органов государственной власти (очная форма).
        <a href="/pkreport/report5" target="_blank">Сформировать</a>.
        <a href="/pkreport/report5?type=pdf" target="_blank">Скачать PDF</a>.
        <a href="/pkreport/report5?type=xls" target="_blank">Скачать XLS</a>.
    </li>
    <li>Информация об объемах и динамике целевого приема в вузы по направлениям органов государственной власти (заочная форма).
        <a href="/pkreport/report6" target="_blank">Сформировать</a>.
        <a href="/pkreport/report6?type=pdf" target="_blank">Скачать PDF</a>.
        <a href="/pkreport/report6?type=xls" target="_blank">Скачать XLS</a>.
    </li>
</ul>
