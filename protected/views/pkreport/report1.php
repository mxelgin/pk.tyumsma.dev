<?php
$pdf = false;
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mpdf('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Отчет о выполнении государственного задания на подготовку специалистов с высшим профессиональным образованием (очная форма)</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:9pt;
}
table td.center{
    text-align: center;
}
.rotate{
	-moz-transform: rotate(-90deg);
	-webkit-transform: rotate(-90deg);
	-o-transform: rotate(-90deg);
	-ms-transform: rotate(-90deg); /* для IE9 */
	transform: rotate(-90deg); /* на будущее */
	text-align: left; line-height:0.9em; /*было text-align: top*/
}
</style>
</head>

<body>
<div class="section"><center><b>
<p class="title">Государственное  образовательное учреждение высшего профессионального образования  &quot;Тюменская государственная медицинская академия&quot; <br />
  Министерства здравоохранения  Российской Федерации (ГОУ ВПО ТюмГМА Минздрава России) <br />
  в <?php echo date('Y') ?> году <br />очная форма </p></b></center>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="14%" rowspan="2" align="center" valign="top">Наименование специальности по Общероссийскому<br />
      классификатору специальностей по образованию</td>
    <td width="12%" rowspan="2" align="center" valign="top">Всего</td>
    <td colspan="12" align="center">В том числе</td>
    </tr>
  <tr>
    <td valign="top">на обучение за счет средств федераль-ного бюджета</td>
    <td valign="top">обучение на основе договоров с оплатой стоимости обучения</td>
    <td valign="top">на обучение в рамках целевого приема**</td>
    <td valign="top">лица с ограничен-ными возможно-стями здоровья</td>
    <td valign="top">лица, имеющие право поступления без вступитель-ных испытаний</td>
    <td valign="top">лица, имеющие право поступления вне конкурса при условии успешного прохождения вступитель-ных испытаний</td>
    <td valign="top">лица, имеющие преиму-щественное  право на поступлени</td>
    <td valign="top">лица, поступающие по результатам единого государствен-ного экзамена (ЕГЭ)</td>
    <td valign="top">результатам вступитель-ных испытаний, форма которых определяется вузом самостоя-тельно</td>
    <td valign="top">лица, имеющие высшее профес-сиональное образование</td>
    <td valign="top">лица, имеющие среднее профес-сиональное образование</td>
    <td valign="top">баллов по результатам ЕГЭ, вступительных испытаний, проводимых вузом самостоятельно, подтверждающее успешное прохождение вступительных испытаний по</td>
  </tr>
  <tr>
    <td class="center">1</td>
    <td class="center">2</td>
    <td class="center">3</td>
    <td class="center">4</td>
    <td class="center">5</td>
    <td class="center">6</td>
    <td class="center">7</td>
    <td class="center">8</td>
    <td class="center">9</td>
    <td class="center">10</td>
    <td class="center">11</td>
    <td class="center">12</td>
    <td class="center">13</td>
    <td class="center">14</td>
  </tr>
<?php
$i=1;
foreach ($bm as $b){
?>
  <tr>
    <td colspan="13"><?php echo $b->spec->specname;?></td>
    <?php if ($i==1){ ?>
    <td rowspan="16" valign="top">Химия (все факульт кроме бюджетных м стоматологического факультета) - 32 бал Химия (стоматологи бюджет) - 50 баллов приказу №442 от 21.10.09 Минобрнау РФ), Биология - 36 баллов. Русский язык 36 баллов</td>
    <?php } ?>
  </tr>
  <tr>
    <td>Подано заявлений</td>
    <td class="center"><?php echo $counts['all'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['bm'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['dm'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['rcm'][$b->specid]; ?></td>
    <td class="center">0</td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['vk'][$b->specid]; ?></td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['ege'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['exam'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['vuz'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['spo'][$b->specid]; ?></td>
    </tr>
  <tr>
    <td>Зачислено</td>
    <td class="center"><?php echo $counts['all_ent'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['bm_ent'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['dm_ent'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['rcm_ent'][$b->specid]; ?></td>
    <td class="center">0</td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['vk_ent'][$b->specid]; ?></td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['ege_ent'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['exam_ent'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['vuz_ent'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['spo_ent'][$b->specid]; ?></td>
    </tr>
  <tr>
    <td>План приема</td>
    <td class="center"><?php echo $b->value; ?></td>
    <td class="center"><?php echo $b->value; ?></td>
    <td class="center"><?php echo $dmvalues[$b->specid]; ?></td>
    <td class="center"><?php echo $rcmvalues[$b->specid]; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
<?php
$i++;
}
?>
  <tr>
    <td colspan="14">ВСЕГО:</td>
    </tr>
  <tr>
    <td>Подано заявлений</td>
    <td class="center"><?php echo $counts['all_sum']; ?></td>
    <td class="center"><?php echo $counts['bm_sum']; ?></td>
    <td class="center"><?php echo $counts['dm_sum']; ?></td>
    <td class="center"><?php echo $counts['rcm_sum']; ?></td>
    <td class="center">0</td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['vk_sum']; ?></td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['ege_sum']; ?></td>
    <td class="center"><?php echo $counts['exam_sum']; ?></td>
    <td class="center"><?php echo $counts['vuz_sum']; ?></td>
    <td class="center"><?php echo $counts['spo_sum']; ?></td>
  </tr>
  <tr>
    <td>Зачислено</td>
    <td class="center"><?php echo $counts['all_ent_sum']; ?></td>
    <td class="center"><?php echo $counts['bm_ent_sum']; ?></td>
    <td class="center"><?php echo $counts['dm_ent_sum']; ?></td>
    <td class="center"><?php echo $counts['rcm_ent_sum']; ?></td>
    <td class="center">0</td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['vk_ent_sum']; ?></td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['ege_ent_sum']; ?></td>
    <td class="center"><?php echo $counts['exam_ent_sum']; ?></td>
    <td class="center"><?php echo $counts['vuz_ent_sum']; ?></td>
    <td class="center"><?php echo $counts['spo_ent_sum']; ?></td>
  </tr>
</table>
<br>
<table width="400px" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td colspan="2" align="center">Целевой прием</td>
    <td width="54" align="center">Подано заявлений</td>
    <td width="63" align="center"><p>Заявленных мест</p></td>
    <td width="58" align="center">Зачисленно</td>
  </tr>
    <?php
    $oldreg='';
    foreach ($rcmReg as $rr){
        echo '<tr>';
        if ($oldreg!=$rr->regionid){
            $countreg=ConfigRcm::model()->count('regionid='.$rr->regionid);
            echo '
            <td width="100" rowspan="'.$countreg.'" valign="top">Главное<br />
            Управление<br />
            здравоохранения<br />
            '.$rr->region->name.'
            </td>';
            $oldreg=$rr->regionid;
        }
        $count_z=Entrant::model()->count(array('with'=>'rcm','condition'=>'deleted=0 and rcm.entrantid>0 and rcm.regionid='.$rr->regionid.' and rcm.specid='.$rr->specid));
        $count_ent=Entrant::model()->count(array('with'=>'rcm','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and rcm.entrantid>0 and rcm.regionid='.$rr->regionid.' and rcm.specid='.$rr->specid));
    ?>
    <td width="73"><?php echo $rr->spec->name; ?></td>
    <td class="center"><?php echo $rr->value; ?></td>
    <td class="center"><?php echo $count_z; ?></td>
    <td class="center"><?php echo $count_ent; ?></td>
  </tr>
<?php
    }
?>
  </table>
<br>
<p>И.О. директора _________________________________________ Л.А. Суплотова
  </div>
</body>
</html>
<?php
if ($_GET['type'] == 'pdf') {
    $mpdf->WriteHTML(ob_get_contents());
    ob_end_clean();
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
