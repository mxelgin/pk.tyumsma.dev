<?php
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mpdf('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Форма 76 КД (очная форма)</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:500px;
	margin: 0;
	padding: 0 5px 0 5px;
}
table td.center{
    text-align: center;
}
.title{
	text-align:center;
}
table{
	font-size:9pt;
}
</style>
</head>

<body>
<div class="section"><center><b>
<p class="title">Сведения о результатах приема студентов в государственное высшее учебное заведение по очной форме обучения в <?php echo date('Y') ?> году<br />
  Тюменская государственная медицинская академия</p></b></center>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td colspan="2">&nbsp;</td>
    <td width="19%">Подано заявлений</td>
    <td width="17%">Зачислено</td>
  </tr>
  <tr>
    <td colspan="2">Всего</td>
    <td class="center"><?php echo $count['all']; ?></td>
    <td class="center"><?php echo $count['all_ent']; ?></td>
  </tr>
  <tr>
    <td width="39%" rowspan="2" valign="top">В т.ч. по договорам с юридическими и физическими лицами</td>
    <td width="25%">Всего</td>
    <td class="center"><?php echo $count['dm']; ?></td>
    <td class="center"><?php echo $count['dm_ent']; ?></td>
  </tr>
  <tr>
    <td>Из них с возмещением стоимости обучения (сверх контрольных цифр)</td>
    <td class="center"><?php echo $count['dm']; ?></td>
    <td class="center"><?php echo $count['dm_ent']; ?></td>
  </tr>
  <tr>
    <td rowspan="2" valign="top">Граждан, постоянно проживающих в государствах СНГ и других государствах на территории бышего СССР(кроме РФ) </td>
    <td>Всего</td>
    <td class="center"><?php echo $count['sng']; ?></td>
    <td class="center"><?php echo $count['sng_ent']; ?></td>
  </tr>
  <tr>
    <td>Из них с возмещением стоимости обучения (сверх контрольных цифр)</td>
    <td class="center"><?php echo $count['sng']; ?></td>
    <td class="center"><?php echo $count['sng_ent']; ?></td>
  </tr>
</table>

Из общего числа:
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td>Мужчин</td>
    <td class="center"><?php echo $count['male']; ?></td>
    <td class="center"><?php echo $count['male_ent']; ?></td>
  </tr>
</table>
<br>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="64%">Выдержали вступительные экзамены</td>
    <td class="center" colspan="2"><?php echo $count['exam']; ?></td>
  </tr>
  <tr>
    <td>Контрольные цифры приема</td>
    <td class="center" colspan="2"><?php echo $count['summ_b']; ?></td>
  </tr>
</table>
<br>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="39%" rowspan="2">Кроме того, принято  иностранных граждан (не считая граждан, постоянно проживающих в государствах СНГ и других государствах на  территории бывшего СССР</td>
    <td width="25%">По государственной линии</td>
    <td width="19%" class="center">0</td>
    <td width="17%" class="center">0</td>
  </tr>
  <tr>
    <td valign="top">на коммерческой основе</td>
    <td class="center">0</td>
    <td class="center">0</td>
  </tr>
</table>

<p>Примечание: сведения предоставляются вцелом по учебному заведению, включая филиалы</p>


<p><span style="float:right;">_____________________ 201_ года.</span><br><br><br><br>
<span style="float:right;">  _____________________ Э.А. Кашуба</span></p>
</div>
</body>
</html>
<?php
if ($_GET['type'] == 'pdf') {
    $mpdf->WriteHTML(ob_get_contents());
    ob_end_clean();
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
