<?php
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mpdf('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Информация об объемах и динамике целевого приема в вузы по направлениям органов государственной власти</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:9pt;
}
table td.center{
    text-align: center;
}
.rotate{
	-moz-transform: rotate(-90deg);
	-webkit-transform: rotate(-90deg);
	-o-transform: rotate(-90deg);
	-ms-transform: rotate(-90deg); /* для IE9 */
	transform: rotate(-90deg); /* на будущее */
	text-align:top; line-height:0.9em;
}
.left{
	float:left;
}
.right{
	float:right;
}
.clear{
	overflow:hidden;
}
</style>
</head>

<body>
<div class="section">
<span class="right">Приложение №1</span>
<div class="clear"></div>
<p class="title"><span class="left">Субъект Российской Федерации ___________________________________________________________________</span><br />
  Информация об объемах и динамике целевого приема в вузы по направлениям органов государственной власти субъекта Российской Федерациии органов местного самоуправления, действующих на территории субъекта Российской Федерации</p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td rowspan="2">Укрупненные группы направлений подготовки (специальностей)</td>
<?php
$y = date('Y') - 5;
?>
    <td colspan="3">Прием <?php echo ($y++) . '/' . $y ?> учебного года</td>
    <td colspan="3">Прием <?php echo ($y++) . '/' . $y ?> учебного года</td>
    <td colspan="3">Прием <?php echo ($y++) . '/' . $y ?> учебного года</td>
    <td colspan="3">Прием <?php echo ($y++) . '/' . $y ?> учебного года</td>
    <td colspan="3">Прием <?php echo ($y++) . '/' . $y ?> учебного года</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">4</td>
    <td align="center">5</td>
    <td align="center">6</td>
    <td align="center">7</td>
    <td align="center">8</td>
    <td align="center">9</td>
    <td align="center">10</td>
    <td align="center">11</td>
    <td align="center">12</td>
    <td align="center">13</td>
    <td align="center">14</td>
    <td align="center">15</td>
    <td align="center">16</td>
  </tr>
  <tr>
    <td>ВСЕГО</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Очная форма обучсния</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>010000 ФИЗИКО-МАТЕМАТИЧЕСКИЕ НАУКИ</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>020000 ЕСТЕСТВЕННЫЕ НАУКИ</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>030000 ГУМАНИТАРНЫЕ НАУКИ</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>040000 СОЦИАЛЬНЫЕ НАУКИ</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>050000 ОБРАЗОВАНИЕ И ПЕДАГОГИКА</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>060000 ЗДРАВООХРАНЕНИЕ</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p class="title">Сведения о приеме в Тюменскую государственную медицинскую академию в <?php echo date('Y') ?> году (без учета филиалов)<br />
(очная форма обучения)</p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="2%" rowspan="2" valign="bottom">№</td>
    <td width="33%" rowspan="2" valign="bottom">Направление</td>
    <td width="4%" rowspan="2" valign="bottom">План приема<br />
      на бюджет</td>
    <td width="4%" rowspan="2" valign="bottom">План прием<br />
      на договор<br /></td>
    <td colspan="3" align="center">Подано заявлений</td>
    <td colspan="3" align="center">Принято</td>
    <td colspan="5" align="center">Принято в разрезе территорий</td>
    <td colspan="2" align="center">Принято в разрезе финансирования</td>
    <td width="5%" rowspan="2">Конкурс (человек на место)</td>
  </tr>
  <tr>
    <td width="4%">Всего, в<br />
      т.ч.:</td>
    <td width="4%">Победители, призеры олимпиад</td>
    <td width="4%">Льготники</td>
    <td width="4%">Всего, в<br />
т.ч.:</td>
    <td width="4%">Победители, призеры олимпиад</td>
    <td width="4%">Льготники</td>
    <td width="4%">г. Тюмень</td>
    <td width="4%">ХМАО</td>
    <td width="4%">ЯНАО</td>
    <td width="4%">Юг области</td>
    <td width="4%">Другие<br />
      территории</td>
    <td width="4%">Бюджет</td>
    <td width="4%">Договор</td>
    </tr>
    <?php 
    $i=1;
    foreach ($bm as $b){
    ?>
  <tr>
    <td class="center"><?php echo $i++; ?></td>
    <td class="center"><?php echo $b->spec->name; ?></td>
    <td class="center"><?php echo $b->value; ?></td>
    <td class="center"><?php echo $dmvalues[$b->specid]; ?></td>
    <td class="center"><?php echo $counts['all'][$b->specid]; ?></td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['vk'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['all_ent'][$b->specid]; ?></td>
    <td class="center">0</td>
    <td class="center"><?php echo $counts['vk_ent'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['all_ent_tmn'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['all_ent_hmao'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['all_ent_yanao'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['all_ent_ug'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['all_ent_other'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['bm_ent'][$b->specid]; ?></td>
    <td class="center"><?php echo $counts['dm_ent'][$b->specid]; ?></td>
    <td class="center"><?php
    if ($b->value!=0) echo round($counts['all'][$b->specid]/$b->value,1); 
    else echo '0';
    ?></td>
  </tr>
<?php
    }
?>
</table>
</div>
</body>
</html>
<?php
if ($_GET['type'] == 'pdf') {
    $mpdf->WriteHTML(ob_get_contents());
    ob_end_clean();
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
