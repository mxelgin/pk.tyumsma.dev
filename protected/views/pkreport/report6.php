<?php
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mpdf('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Информация об объемах и динамике целевого приема в вузы по направлениям органов государственной власти</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:9pt;
}
table td.center{
    text-align: center;
}
.rotate{
	-moz-transform: rotate(-90deg);
	-webkit-transform: rotate(-90deg);
	-o-transform: rotate(-90deg);
	-ms-transform: rotate(-90deg); /* для IE9 */
	transform: rotate(-90deg); /* на будущее */
	text-align:top; line-height:0.9em;
}
.left{
	float:left;
}
.right{
	float:right;
}
.clear{
	overflow:hidden;
}
</style>
</head>

<body>
<div class="section">
<span class="right">Приложение №1</span>
<div class="clear"></div>
<p class="title">Сведения о приеме в Тюменскую государственную медицинскую академию в <?php echo date('Y') ?> году (без учета филиалов)<br>
(заочная форма обучения)</p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="2%" rowspan="2" valign="bottom">№</td>
    <td width="14%" rowspan="2" valign="bottom">Направление</td>
    <td width="6%" rowspan="2" valign="bottom">План приема<br />
на бюджет</td>
    <td width="6%" rowspan="2" valign="bottom">План прием<br />
на договор</td>
    <td colspan="2" align="center">Подано заявлений</td>
    <td colspan="2" align="center">Принято</td>
    <td colspan="5" align="center">Принято в разрезе территорий</td>
    <td colspan="2" align="center">Принято в разрезе финансирования</td>
    <td width="6%" rowspan="2">Конкурс (человек на место)</td>
  </tr>
  <tr>
    <td width="6%">Всего, в<br />
т.ч.:</td>
    <td width="6%">Льготники</td>
    <td width="6%">Всего, в<br />
т.ч.:</td>
    <td width="6%">Льготники</td>
    <td width="6%">г. Тюмень</td>
    <td width="6%">ХМАО</td>
    <td width="6%">ЯНАО</td>
    <td width="6%">Юг области</td>
    <td width="6%">Другие<br />
территории</td>
    <td width="6%">Бюджет</td>
    <td width="6%">Договор</td>
    </tr>
    <?php 
    $i=1;
    foreach ($bm as $b){
    ?>
  <tr>
    <td style="text-align: center;"><?php echo $i++; ?></td>
    <td style="text-align: center;"><?php echo $b->spec->name; ?></td>
    <td style="text-align: center;"><?php echo $b->value; ?></td>
    <td style="text-align: center;"><?php echo $dmvalues[$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['all'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['vk'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['all_ent'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['vk_ent'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['all_ent_tmn'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['all_ent_hmao'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['all_ent_yanao'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['all_ent_ug'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['all_ent_other'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['bm_ent'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php echo $counts['dm_ent'][$b->specid]; ?></td>
    <td style="text-align: center;"><?php
    if ($b->value!=0) echo round($counts['all'][$b->specid]/$b->value,1); 
    else echo '0';
    ?></td>
  </tr>
<?php
    }
?>
</table>

</div>
</body>
</html>
<?php
if ($_GET['type'] == 'pdf') {
    $mpdf->WriteHTML(ob_get_contents());
    ob_end_clean();
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
