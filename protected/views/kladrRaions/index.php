<?php
$this->breadcrumbs=array(
	'Kladr Raions',
);

$this->menu=array(
	array('label'=>'Create KladrRaions', 'url'=>array('create')),
	array('label'=>'Manage KladrRaions', 'url'=>array('admin')),
);
?>

<h1>Kladr Raions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
