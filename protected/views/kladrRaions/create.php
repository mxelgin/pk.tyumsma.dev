<?php
/*
$this->breadcrumbs=array(
	'Kladr Raions'=>array('index'),
	'Create',
);
*/

$this->menu=array(
	//array('label'=>'KladrRaions', 'url'=>array('index')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h1>Добавить район</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
