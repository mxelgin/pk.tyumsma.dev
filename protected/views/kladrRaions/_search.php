<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'index'); ?>
		<?php echo $form->textField($model,'index',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_type'); ?>
		<?php //echo $form->textField($model,'id_type'); ?>
		<?php echo $form->dropDownList($model,'id_type',
            CHtml::ListData(KladrType::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите тип')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_region'); ?>
		<?php //echo $form->textField($model,'id_region'); ?>
		<?php echo $form->dropDownList($model,'id_region',
            CHtml::ListData(KladrRegions::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите регион')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Поиск'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
