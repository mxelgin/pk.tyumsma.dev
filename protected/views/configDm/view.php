<?php
$this->breadcrumbs=array(
	'Config Rcms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ConfigRcm', 'url'=>array('index')),
	array('label'=>'Create ConfigRcm', 'url'=>array('create')),
	array('label'=>'Update ConfigRcm', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ConfigRcm', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ConfigRcm', 'url'=>array('admin')),
);
?>

<h1>View ConfigRcm #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'specid',
		'regionid',
		'value',
	),
)); ?>
