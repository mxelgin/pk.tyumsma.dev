<?php
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=windows-1251');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mpdf('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Сводная ведомость</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","Arial";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
        border-collapse: collapse; 
}
table td.center{
    text-align: center;
}
table td.border{
    border: 1px solid black;
}
td.big0{
    font-size: 30pt;
}
td.big1{
    font-size: 45pt;
}
td.big2{
    font-size: 48pt;
}
td.big3{
    font-size: 50pt;
}
td.small{
    font-size: 14px;
}
</style>
</head>

<body>
<div class="section">
<table width="1000" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td height="100" width="600" rowspan="2" align="center" valign="middle" class="big3"><?php echo date('d.m.Y'); ?></td>
    <td colspan="2" width="400" class="big1">ВСЕГО: <?php echo $countall; ?></td>
  </tr>
  <tr>
      <td width="200">Подано заявлений</td>
      <td width="200">сумма баллов ЕГЭ у абитуриентов*</td>
  </tr>
<?php
    foreach ($specs as $spec){
        echo '<tr><td rowspan="4" vlaign="middle" class=" border big0">'.$spec->name.'</td><td class="border big1 center" rowspan="4">'.$count[$spec->id]['all'].' </td><td class="border small center">max='.intval($count[$spec->id]['max']).'</td><tr>';
        $all=$count[$spec->id]['count'];
        if ($all==0) $all=1;
        $mid=round($count[$spec->id]['sum']/$all);
        echo '<tr><td class="border small center">среднее='.$mid.'</td></tr>';
        echo '<tr><td class="border small center">min='.intval($count[$spec->id]['min']).'</td></tr>';
        //echo '<tr><td colspan="3" height="15"></td></tr>';
    }
?>
</table>
<p>*учтены баллы только у абитуриентов, уже подавших документы и имеющих баллы за все экзамены ЕГЭ (химия, русский язык и биология). По мере подачи новых заявлений и сдачи 2 волны ЕГЭ данные могут изменяться.</p>
</body>
</html>
<?php
if ($_GET['type'] == 'pdf') {
    $mpdf->WriteHTML(ob_get_contents());
    ob_end_clean();
    $mpdf->Output('mpdf.pdf', 'I');
}
?>
