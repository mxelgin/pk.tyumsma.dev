<h1>Ежедневные отчеты</h1>

<ul>
    <li>Сводная ведомость.
        <a href="/pkdayreport/report1" target="_blank">Сформировать</a>.
        <a href="/pkdayreport/report1?type=pdf" target="_blank">Скачать PDF</a>.
        <a href="/pkdayreport/report1?type=xls" target="_blank">Скачать XLS</a>.
    </li>
    <li>Отчеты по специальностям.
        <a href="/pkdayreport/report2" target="_blank">Сформировать</a>.
        <a href="/pkdayreport/report2?type=pdf" target="_blank">Скачать PDF</a>.
        <a href="/pkdayreport/report2?type=xls" target="_blank">Скачать XLS</a>.
    </li>
</ul>
