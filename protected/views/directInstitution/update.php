<?php
$this->menu=array(
	array('label'=>'Добавить уч. заведение', 'url'=>array('create')),
	array('label'=>'Управление уч. заведениями', 'url'=>array('admin')),
);
?>

<h1>Изменение уч. заведения</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>