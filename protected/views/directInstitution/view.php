<?php
$this->breadcrumbs=array(
	'Direct Institutions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectInstitution', 'url'=>array('index')),
	array('label'=>'Create DirectInstitution', 'url'=>array('create')),
	array('label'=>'Update DirectInstitution', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectInstitution', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectInstitution', 'url'=>array('admin')),
);
?>

<h1>View DirectInstitution #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cityid',
		'name',
	),
)); ?>
