<script>
$(document).ready(function($) {
    $('#DirectInstitution_cityid').autocomplete({
        serviceUrl: '<?php echo $this->createUrl('pkentrant/getKladrItems'); ?>',
        minChars: 2,
        onSelect: function(data, value){
            $('#DirectInstitution_cityid').attr('value', value);
            $('#DirectInstitution_cityid').hide();
            $('.city-text').html(data+' <a href="#" class="change-city">Изменить</a>');
            $('#DirectInstitution_cityid').focusNextInputField();
            $("a.change-city").click(function(){
                $('.city-text').html('');
                $('#DirectInstitution_cityid').attr('value', '');
                $('#DirectInstitution_cityid').show();
                return false;
            });
        },
        deferRequestBy: 200
    });
    
    $('#DirectInstitution_cityid').blur(function(){clearcityinput(this, '.city-text')});

    $("a.change-city").click(function(){
        $('.city-text').html('');
        $('#DirectInstitution_cityid').attr('value', '');
        $('#DirectInstitution_cityid').show();
        return false;
    });
});
</script>    


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'direct-institution-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cityid'); ?>
                <?php
                if ($model->cityid){ 
                    echo $form->textField($model,'cityid',array('size'=>60,'maxlength'=>100, 'class'=>'hide'));
                    echo '<span class="city-text">'.$model->city->name.' <a href="#" class="change-city">Изменить</a></span>';
                }else{
                    echo $form->textField($model,'cityid',array('size'=>60,'maxlength'=>100));
                    echo '<span class="city-text"></span>';                    
                } 
                ?>
		<?php echo $form->error($model,'cityid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->