<?php
$this->breadcrumbs=array(
	'Direct Institutions',
);

$this->menu=array(
	array('label'=>'Create DirectInstitution', 'url'=>array('create')),
	array('label'=>'Manage DirectInstitution', 'url'=>array('admin')),
);
?>

<h1>Direct Institutions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
