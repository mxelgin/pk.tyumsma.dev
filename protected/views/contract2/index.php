<?php
$this->breadcrumbs=array(
	'Contract2s',
);

$this->menu=array(
	array('label'=>'Create Contract2', 'url'=>array('create')),
	array('label'=>'Manage Contract2', 'url'=>array('admin')),
);
?>

<h1>Contract2s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
