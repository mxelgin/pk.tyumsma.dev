<?php
$this->breadcrumbs=array(
	'Contract2s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Contract2', 'url'=>array('index')),
	array('label'=>'Create Contract2', 'url'=>array('create')),
	array('label'=>'Update Contract2', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Contract2', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Contract2', 'url'=>array('admin')),
);
?>

<h1>View Contract2 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'entrant_id',
		'fio',
		'pass_number',
		'pass_date',
		'pass_place',
		'addr',
		'phone',
	),
)); ?>
