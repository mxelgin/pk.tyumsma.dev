<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contract2-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php if ($entrantid) $model->entrantid=$entrantid;
        echo $form->hiddenField($model,'entrantid'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fio'); ?>
		<?php echo $form->textField($model,'fio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pass_number'); ?>
		<?php echo $form->textField($model,'pass_number',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'pass_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pass_date'); ?>
		<?php echo $form->textField($model,'pass_date'); ?>
		<?php echo $form->error($model,'pass_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pass_place'); ?>
		<?php echo $form->textArea($model,'pass_place',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'pass_place'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'addr'); ?>
		<?php echo $form->textArea($model,'addr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->