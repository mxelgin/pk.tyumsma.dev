<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrant_id')); ?>:</b>
	<?php echo CHtml::encode($data->entrant_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fio')); ?>:</b>
	<?php echo CHtml::encode($data->fio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pass_number')); ?>:</b>
	<?php echo CHtml::encode($data->pass_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pass_date')); ?>:</b>
	<?php echo CHtml::encode($data->pass_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pass_place')); ?>:</b>
	<?php echo CHtml::encode($data->pass_place); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addr')); ?>:</b>
	<?php echo CHtml::encode($data->addr); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	*/ ?>

</div>