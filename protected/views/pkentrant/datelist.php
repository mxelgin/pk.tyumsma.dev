<?php
$this->breadcrumbs=array(
	'Абитуриенты, подавшие заявление '.date('Y-m-d'),
);

$this->menu=array(
	array('label'=>'База абитуриентов', 'url'=>array('admin')),
        array('label'=>'База РЦМ', 'url'=>'/rcm'),
);
?>

<h1>Абитуриенты, подавшие заявление <?php echo date('Y-m-d'); ?></h1>

<form action="/pkentrant/dateList" method="post">
    <p>День приема 
    <select name="day">
        <?php
        foreach ($days as $day){
            echo '<option value="'.$day->dateadd.'">'.$day->dateadd.'</option>';
        }
        ?>
    </select>
        <input type="submit" value="Показать">
    </p>
</form>
<?php
if ($entrants){
?>
<table align="center" class="bordertable tablecentertd">
    <tr>
        <th>#</th>
        <th>ФИО</th>
        <th>Рейтинг</th>
    </tr>
    <?php
    $i=1;
    foreach ($entrants as $ent){
        echo '<tr>';
        echo '<td>'.$i++.'</td>';
        echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
        echo '<td>'.$ent->rating.'</td>';
        echo '</tr>';
    }    
    ?>
</table>
<?php
}
?>
