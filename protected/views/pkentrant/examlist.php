<?php
$this->breadcrumbs=array(
	'Список на экзамен',
);

$this->menu=array(
	array('label'=>'База абитуриентов', 'url'=>array('admin')),
        array('label'=>'База РЦМ', 'url'=>'/rcm'),
);
?>

<h1>Примерный список абитуриентов на экзамен</h1>

<table align="center" class="bordertable tablecentertd">
    <tr>
        <th>#</th>
        <th>ФИО</th>
        <th>Секретарь</th>
    </tr>
    <?php
    $i=1;
    foreach ($entrants as $ent){
        echo '<tr>';
        echo '<td>'.$i++.'</td>';
        echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
        echo '<td>'.$ent->checked->last_name.' '.$ent->checked->first_name.' '.$ent->checked->middle_name.'</td>';
        echo '</tr>';
    }    
    ?>
</table>
