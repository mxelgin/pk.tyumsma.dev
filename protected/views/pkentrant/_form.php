<script>
$(document).ready(function($) {
    selected=$('#Entrant_maindoc').attr('value');
    $.ajax({'url':'<?php echo $this->createUrl('pkentrant/getDocFields'); ?>','data':'id='+selected+'&entrantid=<?php echo $model->id; ?>&data=<?php echo CJSON::encode($docsdata); ?>&docserrors=<?php echo CJSON::encode($entrantDocErrors->getErrors()); ?>','cache':false,'beforeSend':function(){$("#maindocinput").addClass("loading");},'complete':function(){$("#maindocinput").removeClass("loading");},'success':function(html){$("#maindocinput").html(html);  inputformat("#maindocinput");}});

    inputformat('');

    $('#Entrant_birth_place_id').autocomplete({
            source: function( request, response ) {
                $.ajax({
                        url: "<?php echo $this->createUrl('pkentrant/getKladrItems'); ?>",
                        dataType: "jsonp",
                        data: {
                                query: request.term
                        },
                        success: function( data ) {
                            response( $.map( data.city, function( item ) {
                                    return {label: item.name}
                            }));
                        }
                });
            },
            minLength: 1,
            delay: 200,
            open: function() {
                    $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
            },
            close: function() {
                    $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
            }
        });

    $('#Entrant_citizenship').change(function(){
        var val = $('#Entrant_citizenship').val();
        $.ajax({
            url: '<?php echo $this->createUrl('directCitizenship/isBudget'); ?>',
            data: {
                id: val
            },
            success: function (data) {
                console.debug(data);
                if (data == true) budjet_enable();
                else budjet_disable();
            }
        });
    });
    
    $('#Entrant_service').change(function(){
        if ($('#Entrant_service_0').attr("checked")){
            $('#service_year_row').removeClass('hidden');
        }else{
            $('#service_year_row').addClass('hidden');
        }
    });

    $("#Entrant_home_region").combobox();
    $('#gradeduble').click(function(){
        var gradedoc=$('#Entrant_grade_rus_doc').val();
        var gradedoctype=$('#Entrant_grade_rus_doctype').val();
        $('#Entrant_grade_him_doc').val(gradedoc);
        $('#Entrant_grade_bio_doc').val(gradedoc);        
        $('#Entrant_grade_him_doctype').val(gradedoctype);
        $('#Entrant_grade_bio_doctype').val(gradedoctype);
        return false;
    });
    $('#copy_home_fact').click(function(){
        var region=$('#region').val();
        var city=$('#city').val();
        var street=$('#street').val();
        var id=$('#Entrant_kladr_street').val();
        var build=$('#Entrant_home_build').val();
        var kv=$('#Entrant_home_kv').val();
        var val='';
        if (region) val=val+', '+region;
        if (city) val=val+', '+city;
        if (street) val=val+', '+street;
        if (build) val=val+', '+build;
        if (kv) val=val+', '+kv;
        if (val) $('#Entrant_home_fact').val(val);
        $.ajax({
            url: '<?php echo $this->createUrl('kladrStreet/getIndex'); ?>',
            data: {
                id: id,
            },
            success: function (data) {
                //console.debug(data);
                var val = data + $('#Entrant_home_fact').val();
                $('#Entrant_home_fact').val(val);
                }
        });
        return false;
    });
    $('#copy_parents_addr_home').click(function(){
        //var addr=$('#Entrant_home_fact').val();
        //if (addr) $('#parent_addr_1').val(addr);
        //return;
        var region=$('#region').val();
        var city=$('#city').val();
        var street=$('#street').val();
        var id=$('#Entrant_kladr_street').val();
        var build=$('#Entrant_home_build').val();
        var kv=$('#Entrant_home_kv').val();
        var val='';
        if (region) val=val+', '+region;
        if (city) val=val+', '+city;
        if (street) val=val+', '+street;
        if (build) val=val+', '+build;
        if (kv) val=val+', '+kv;
        if (val) $('#parent_addr_1').val(val);
        $.ajax({
            url: '<?php echo $this->createUrl('kladrStreet/getIndex'); ?>',
            data: {
                id: id,
            },
            success: function (data) {
                //console.debug(data);
                var val = data + $('#parent_addr_1').val();
                $('#parent_addr_1').val(val);
                }
        });
        return false;
    });
    $('#copy_parents_addr').click(function(){
        var addr=$('#parent_addr_1').val();
        if (addr) $('#parent_addr_2').val(addr);
        else{
            var region=$('#region').val();
            var city=$('#city').val();
            var street=$('#street').val();
            var id=$('#Entrant_kladr_street').val();
            var build=$('#Entrant_home_build').val();
            var kv=$('#Entrant_home_kv').val();
            var val='';
            if (region) val=val+', '+region;
            if (city) val=val+', '+city;
            if (street) val=val+', '+street;
            if (build) val=val+', '+build;
            if (kv) val=val+', '+kv;
            if (val) $('#parent_addr_2').val(val);
            $.ajax({
                url: '<?php echo $this->createUrl('kladrStreet/getIndex'); ?>',
                data: {
                    id: id,
                },
                success: function (data) {
                    //console.debug(data);
                    var val = data + $('#parent_addr_2').val();
                    $('#parent_addr_2').val(val);
                    }
            });
        }
        //console.debug(addr);
        return false;
    });
    $('#Entrant_vk').change(function(){
       if ($('#Entrant_vk').attr('checked'))
           $('#vk_notice_div').removeClass('hidden');
       else{
           $('#Entrant_vk_notice').val('');
           $('#vk_notice_div').addClass('hidden');
        }
    });
    $('#Entrant_achv').change(function(){
       if ($('#Entrant_achv').attr('checked'))
           $('#achv_notice_div').removeClass('hidden');
       else{
           $('#achv_notice_div').addClass('hidden');
        }
    });
    $('.speclist').change(function(){
        <?php
        $vls=ConfigBm::model()->findAll('value=0');
        $txt='';
        foreach($vls as $vl){
            $txt.='"'.$vl->specid.'",';
        }
        if ($txt){
            $txt=substr($txt, 0, -1);
            echo 'var arr=[ '.$txt.' ];'."\n";
        }else{
            echo 'var arr=[];'."\n";
        }
        ?>
        var exp = $(this).attr('id').split('_');
        if ($.inArray($(this).val(), arr)!=-1){
            $('#type_b_'+exp[1]).removeAttr('checked');
            $('#type_b_'+exp[1]).attr('disabled', 'disabled');
        }else{
            if ($('#type_b_'+exp[1]).attr('disabled'))
                $('#type_b_'+exp[1]).removeAttr('disabled');
        }
    });
    
    <?php 
    $errors=$model->getErrors();
    foreach ($errors as $field=>$error){
        echo '$("#Entrant_'.$field.'").addClass("error");'."\n";
    }
    $errors=$entrantSpecErrors->getErrors();
    foreach ($errors as $field=>$error){
        echo '$("#'.$field.'").addClass("error");'."\n";
    }
    if ($model->sex==2)
        echo '$(".military").addClass("hide");';
    ?>
    
});

  $(function() {
    var marks = $( "#marks" );
    var allFields = $( [] ).add( marks );
      /*
      tips = $( ".validateTips" );
      $.mask.definitions['5']='[345]';
      $( "#marks" ).mask("5,? 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5");
      */
    $.mask.rules = {
      '0': /[0]/,
      '1': /[0-1]/,
      '2': /[0-2]/,
      '3': /[0-3]/,
      '4': /[0-4]/,
      '5': /[3-5]/,
      '6': /[0-6]/,
      '7': /[0-7]/,
      '8': /[0-8]/,
      '9': /[0-9]/
    };
 
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
    $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 200,
      width: 550,
      modal: true,
      buttons: {
        "Рассчитать": function() {
          var bValid = true;
          allFields.removeClass( "ui-state-error" );
 
          var n = marks.val().split(",");
          var k = 0;
          var j = 0;
          for (i = 0; i < n.length; i++) {
              m = n[i];
              if (m != 3 && m != 4 && m != 5) continue;
              //else console.debug(m + ' ' + i);
              m = parseInt(m);
              k += m;
              j++;
          }
          k = (k / j);
          k = k.toFixed(2);

          if (isNaN(k)) {
            //$( this ).dialog( "close" );
            return;
          }
 
          if ( bValid ) {
            $( "#Entrant_edu_grade" ).val( k );
            $( this ).dialog( "close" );
          }
        },
        "Отмена": function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        allFields.val( "" ).removeClass( "ui-state-error" );
      }
    });
 
    $( "#calculate-edu-grade" )
      .button()
      .click(function() {
        $( "#dialog-form" ).dialog( "open" );
      });
    $('#marks').bind('input', function() { 
        var n = marks.val();
        n = n.replace(", , ", ", ");
        if (n.substring(n.length - 2) == ', ') n =  n.slice(0, -2);
        if (n.length != 0) n = n.split(", ");
        $('#marks-count').html(n.length);
        //console.debug(n.length + ' # ' + n);
    });
  });
</script>
<style>
.ui-autocomplete-loading{
    background: white url('/images/ui-anim_basic_16x16.gif') right center no-repeat;
}
</style>

<div id="dialog-form" title="Рассчитать средний балл">
  <p class="validateTips">Введите оценки через запятую.</p>
 
  <form>
  <fieldset>
    <label for="name">Оценки</label>
    <?php //<input type="text" name="marks" size="40" id="marks" class="text ui-widget-content ui-corner-all" /> ?>
    <?php echo CHtml::textField('marks', '', array('size'=>50, 'placeholder'=>'3, 4, 5', 'alt'=>'5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ')); ?>
    <label id="marks-count">0</label>
  </fieldset>
  </form>
</div>
 
<div id="entrantform">
    <?php
    if ($model->id) $action='/pkentrant/update/id/'.$model->id;
    else $action='/pkentrant/create';
    ?>
    <form id="entrant-form" action="<?php echo $action; ?>" method="post">
    <div class="errorSummary">
        <?php echo CHtml::errorSummary(array($model,$entrantDocErrors,$entrantSpecErrors)); ?>
    </div>
    <div class="leftblock">
        <!--<div class="title"><h1>Добавление нового абитуриента</h1></div>-->

	<div class="maindoc block">
            <?php 
            $options['ajax']=array('update'=>'#maindocinput', 'url' => $this->createUrl('pkentrant/getDocFields'),'data' => 'js:\'id=\'+this.value+\'&entrantid='.$model->id.'&data='.CJSON::encode($docsdata).'&docserrors='.CJSON::encode($entrantDocErrors->getErrors()).'\'', 'cache' => false, 'beforeSend'=>'function(){$("#maindocinput").addClass("loading");}', 'complete'=>'function(){$("#maindocinput").removeClass("loading");}', 'success'=>'function(html){$("#maindocinput").html(html); inputformat("#maindocinput");}'); 
            if (empty($model->maindoc)) $model->maindoc=21;
            ?>
            <div class="blockinfo"><h2><label for="Entrant_maindoc">Основной документ</label></h2>
            <?php echo CHtml::dropDownList('Entrant[maindoc]',$model->maindoc, CHtml::listData($docs->findAll('categoryid=1'), 'id', 'name'), $options); ?></div>
            <div class="row">
                <label for="fam" class="required">ФИО <span class="required">*</span></label>
                <?php echo CHtml::textField('Entrant[fam]', $model->fam,array('size'=>60, 'maxlength'=>250, 'placeholder'=>'Фамилия', 'class'=>'s-input')); ?>
		<?php echo CHtml::textField('Entrant[name]', $model->name,array('size'=>60,'maxlength'=>250, 'placeholder'=>'Имя', 'class'=>'s-input')); ?>
		<?php echo CHtml::textField('Entrant[m_name]', $model->m_name,array('size'=>60,'maxlength'=>250, 'placeholder'=>'Отчество', 'class'=>'s-input')); ?>
            </div>
            <div id="maindocinput">
            </div>
            <div class="row">
                <label for="Entrant_birth_date" class="required">Родился <span class="required">*</span></label>
                <?php echo CHtml::textField('Entrant[birth_date]', $model->birth_date,array('placeholder'=>'__.__.____', 'alt' => '39.19.2999', 'class'=>'datepicker')); ?>
                <?php 
                if ($model->id) $birthplace=$model->birth_city->name;
                else $birthplace=$model->birth_place_id;
                ?>
                <?php echo CHtml::textField('Entrant[birth_place_id]', $birthplace,array('size'=>60,'placeholder'=>'Населенный пункт')); ?>
            </div>
            <div class="row">
                <label for="Entrant_citizenship" class="required">Гражданство <span class="required">*</span></label>
                <?php
                $citizenshipdef=$model->citizenship;
                if (empty($citizenshipdef)) $citizenshipdef=1;
                echo CHtml::dropDownList('Entrant[citizenship]', $citizenshipdef, CHtml::listData($citizenship, 'id', 'name'));
                ?>
                <label for="Entrant_sex" class="required">Пол <span class="required">*</span></label>
                <?php echo CHtml::radioButtonList('Entrant[sex]', $model->sex, array(1=>'Муж', 2=>'Жен'), array('separator'=>'', 'onclick'=>'js:hidemilitary(this);')); ?>
            </div>
	</div>
	<div class="spec block">
            <div class="blockinfo"><h2>Приоритет специальностей</h2></div>
            <div class="row">
                <?php
                $departmentlist=CHtml::listData($department, 'id', 'name');
                array_unshift($departmentlist, '');
                if (count($model->specs)>0){
                    $i=1;
                    foreach ($model->specs as $key=>$spec){
                        //if (empty($spec->specid)) continue;
                        $specid=$spec->id;
                        if (empty($specid)) $specid=$key;
                ?>
                        <div class="spec-item">
                            <span class="spec-itemnumber rownumber"><?php echo $i; ?>.</span>
                            <label for="specid_<?php echo $specid; ?>">Факультет
                            <?php if ($i==1) echo '<span class="required">*</span>'; ?>
                            </label>
                            <?php echo CHtml::dropDownList('specid['.$specid.']', $spec->specid, $departmentlist, array('class'=>'speclist')); ?>
                            Условия
                            <?php if ($i==1) echo '<span class="required">*</span>'; ?>
                            <?php 
                            if (ConfigBm::model()->exists('specid='.$spec->specid.' and value=0'))
                                $options=array('disabled'=>true);
                            else
                                $options=array();
                            echo CHtml::checkBox('type_b['.$specid.']', $spec->type_b, $options).' <label for="type_b_'.$specid.'">Бюджет</label>'; ?>
                            <?php echo CHtml::checkBox('type_d['.$specid.']', $spec->type_d).' <label for="type_d_'.$specid.'">Договор</label>'; ?>
                        </div>
                        <?php 
                        $i++;
                    }
                }else{
                ?>
                        <div class="spec-item">
                            <span class="spec-itemnumber rownumber">1.</span>
                            <label for="specid_0" class="required">Факультет <span class="required">*</span></label>
                            <?php echo CHtml::dropDownList('specid[0]', '', $departmentlist, array('class'=>'speclist')); ?>
                            Условия <span class="required">*</span>
                            <?php echo CHtml::checkBox('type_b[0]', true).' <label for="type_b_0">Бюджет</label>'; ?>
                            <?php echo CHtml::checkBox('type_d[0]', false).' <label for="type_d_0">Договор</label>'; ?>
                        </div>
                        <div class="spec-item">
                            <span class="spec-itemnumber rownumber">2.</span>
                            <label for="specid_1">Факультет</label>
                            <?php echo CHtml::dropDownList('specid[1]', '', $departmentlist, array('class'=>'speclist')); ?>
                            Условия
                            <?php echo CHtml::checkBox('type_b[1]', false).' <label for="type_b_1">Бюджет</label>'; ?>
                            <?php echo CHtml::checkBox('type_d[1]', false).' <label for="type_d_1">Договор</label>'; ?>
                        </div>
                        <div class="spec-item">
                            <span class="spec-itemnumber rownumber">3.</span>
                            <label for="specid_2">Факультет</label>
                            <?php echo CHtml::dropDownList('specid[2]', '', $departmentlist, array('class'=>'speclist')); ?>
                            Условия
                            <?php echo CHtml::checkBox('type_b[2]', false).' <label for="type_b_2">Бюджет</label>'; ?>
                            <?php echo CHtml::checkBox('type_d[2]', false).' <label for="type_d_2">Договор</label>'; ?>
                        </div>
                <?php } ?>
            </div>
	</div>
	<div class="home block">
            <div class="blockinfo"><h2>Контактные данные, проживание</h2></div>
            <div class="row">
                <label for="Entrant_phone">Мобильный телефон</label>
                <?php echo CHtml::textField('Entrant[phone]', $model->phone,array('placeholder'=>'8 123 456 7890', 'alt'=>'8 999 999 9999')); ?>
                <label for="Entrant_need_hostel">Нуждается в общежитии</label>
                <?php echo CHtml::checkBox('Entrant[need_hostel]', $model->need_hostel); ?>
            </div>
            <div class="row">
                <label>Адрес по прописке (регистрация)</label>
            </div>
            <div class="row">
                <?php echo CHtml::textField('region', '', array('placeholder'=>'Регион')); ?>
                <?php echo CHtml::hiddenField('region-id'); ?>

                <?php echo CHtml::textField('raion', '', array('placeholder'=>'Район')); ?>
                <?php echo CHtml::hiddenField('raion-id'); ?>

                <?php echo CHtml::textField('city', '', array('placeholder'=>'Город')); ?>
                <?php echo CHtml::hiddenField('city-id'); ?>

            </div>
            <div class="row">
                <?php echo CHtml::textField('street', '', array('size'=>40, 'placeholder'=>'Улица')); ?>
                <?php echo CHtml::hiddenField('Entrant[kladr_street]', $model->kladr_street, array('size'=>20, 'placeholder'=>'КЛАДР')); ?>

                <?php echo CHtml::textField('Entrant[home_build]', $model->home_build,array('size'=>10,'placeholder'=>'Дом/Корпус', 'class'=>'home-st')); ?>
                <?php echo CHtml::textField('Entrant[home_kv]', $model->home_kv,array('size'=>10,'placeholder'=>'Квартира', 'class'=>'home-st')); ?>
            </div>
            <?php /*
            <div class="row">

                <?php echo CHtml::dropDownList('Entrant[home_type]', $model->home_type, $model->hometypes); ?>
                <?php
                if ($model->id) $homecityplace=$model->home_city_db->name;
                else $homecityplace=$model->home_city;
                ?>
                <?php echo CHtml::textField('Entrant[home_city]', $homecityplace, array('size'=>30,'placeholder'=>'Населенный пункт')); ?>
                <?php 
                $array0=array(0=>'');
                $array=CHtml::listData(DirectRegions::model()->findAll(), 'id', 'name');
                //array_unshift($array, '');
                $array=$array0+$array;
                echo CHtml::dropDownList('Entrant[home_region]', $model->home_region, $array);
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::textField('Entrant[home_street]', $model->home_street,array('size'=>43,'placeholder'=>'Улица', 'class'=>'home-st')); ?>
            </div>
            */ ?>
            <div class="row">
                <label for="Entrant_home_fact">Фактическое проживание</label> <a href="#" id="copy_home_fact">Копировать &uarr;</a>
            </div>
            <div class="row">
                <?php echo CHtml::textField('Entrant[home_fact]', $model->home_fact, array('size'=>80,'placeholder'=>'Индекс, Город, улица, дом/корпус, квартира')); ?>
            </div>
	</div>
	<div class="education block">
            <div class="blockinfo"><h2>Окончил учебное заведение</h2></div>
                <div class="row">
                    <label for="Entrant_edu_type">Что окончил</label>
                    <?php 
                    $data=CHtml::listData(DirectEducation::model()->findAll(), 'id', 'name');
                    array_unshift($data, '');
                    echo CHtml::dropDownList('Entrant[edu_type]', $model->edu_type, $data);
                    ?>
                    <label for="Entrant_edu_date">Дата окончания</label>
                    <?php echo CHtml::textField('Entrant[edu_date]', $model->edu_date,array('placeholder'=>'__.__.____', 'alt' => '39.19.2999', 'class'=>'datepicker')); ?>
                </div>
                <div class="row">
                    <label for="Entrant_edu_grade">Средний балл</label>
                    <?php echo CHtml::textField('Entrant[edu_grade]', $model->edu_grade,array('placeholder'=>'_.__', 'alt'=>'5.99', 'size'=>2)); ?>
                    <button type="button" id="calculate-edu-grade" style="background: url(/images/calculator.png) no-repeat; height: 32px; width: 32px;">#</button>
                    <label for="Entrant_edu_spec">Отличие</label>
                    <?php 
                    $data=CHtml::listData(DirectMedal::model()->findAll(), 'id', 'name');
                    array_unshift($data, '');
                    echo CHtml::dropDownList('Entrant[edu_spec]', $model->edu_spec, $data);
                    ?>
                </div>
	</div>
	<div class="military block">
            <div class="blockinfo">
                <h2>Отношение к военной службе</h2>
                <?php
                if ($model->service) $service=$model->service;
                else $service=2;
                echo CHtml::radioButtonList('Entrant[service]', $service, array(1=>'Служил', 2=>'Не служил'), array('separator'=>' '));
                echo ' <span id="service_year_row" ';
                if ($service==2)
                    echo 'class="hidden"';
                echo '>Год демобилизации: '.CHtml::textField('Entrant[service_year]', $model->service_year,array('placeholder'=>'____', 'alt'=>'2999', 'size'=>'2')).'</span>';
                ?>
            </div>
	</div>
	<div class="parents block">
            <div class="blockinfo"><h2>Родители</h2></div>
            <?php
            if (count($model->parents)>0){
                foreach ($model->parents as $key=>$parent){
                    if (empty($parent->id)) $parentid=$key;
                    else {
                        $parentid=$parent->id;
                        $key++;
                    }
                    /*
echo '<pre>'; print_r($model->attributes); 
echo $key;
echo '<pre>'; print_r($parent->attributes); die;
                    if (empty($create)) $key++;
*/
            ?>
                <div class="parants-row">
                    <div class="row">
                        <label for="orphan<?php echo $key; ?>">Отсутствует</label>
                        <?php echo CHtml::checkBox('orphan'.$key, $model['orphan'.$key], array('onclick'=>'js:enableorphan('.$key.');')); ?>
                        <label for="orphan<?php echo $key; ?>_type">Причина</label>
                        <?php
                        if (!$model['orphan'.$key])
                            $options=array(
                                'disabled'=>true,
                                'separator' => ' ',
                                'onclick'=>'js:enableorphan('.$key.');',
                            );
                        else
                            $options=array(
                                'separator' => ' ',
                                'onclick'=>'js:enableorphan('.$key.');',
                            );
                        ?>
                        <?php echo CHtml::radioButtonList('orphan'.$key.'_type', $model['orphan'.$key.'_type'], array(1 => 'Умер', 2=> 'Не проживает с семьей', 3 => 'Опекун'), $options); ?>
                    </div>
                    <?php
                    if ($model['orphan'.$key] && $model['orphan'.$key.'_type'] != 3) $options=array( 'disabled'=>true,);
                    else $options=array();
                    ?>
                    <div class="row">
                        <label for="parent_fio">ФИО</label>
                        <?php echo CHtml::textField('parent_fio['.$parentid.']', $parent->fio, array_merge($options, array('id'=>'parent_fio_'.$key, 'placeholder'=>'Фамилия Имя Отчество', 'size'=>'60'))); ?>
                    </div>
                    <div class="row">
                        <label for="uparent_addr">Адрес родителя</label>
                        <?php echo CHtml::textField('parent_addr['.$parentid.']', $parent->addr, array_merge($options, array('id'=>'parent_addr_'.$key, 'placeholder'=>'Город, улица, дом/корпус, квартира', 'size'=>'60'))); ?>
                    </div>
                    <div class="row">
                        <?php echo CHtml::textField('parent_work['.$parentid.']', $parent->work, array_merge($options, array('id'=>'parent_work_'.$key, 'placeholder'=>'Место работы и должность', 'size'=>'50'))); ?>
                        <?php echo CHtml::textField('parent_phone['.$parentid.']', $parent->phone, array_merge($options, array('id'=>'parent_phone_'.$key, 'placeholder'=>'8 123 456 7890', 'alt'=>'8 999 999 9999'))); ?>
                    </div>
                </div>
            <?php
                }
            }else{
                for ($i = 0; $i < 2; $i++) {
                    $key = $i + 1;
            ?>
                <div class="parants-row">
                    <div class="row">
                        <label for="orphan<?php echo $key; ?>">Отсутствует</label>
                        <?php echo CHtml::checkBox('orphan'.$key, $model['orphan'.$key], array('onclick'=>'js:enableorphan('.$key.');')); ?>
                        <label for="orphan<?php echo $key; ?>_type">Причина</label>
                        <?php
                        if (!$model['orphan'.$key])
                            $options=array(
                                'disabled'=>true,
                                'separator' => ' ',
                                'onclick'=>'js:enableorphan('.$key.');',
                            );
                        else
                            $options=array(
                                'separator' => ' ',
                                'onclick'=>'js:enableorphan('.$key.');',
                            );
                        ?>
                        <?php echo CHtml::radioButtonList('orphan'.$key.'_type', $model['orphan'.$key.'_type'], array(1 => 'Умер', 2=> 'Не проживает с семьей', 3 => 'Опекун'), $options); ?>
                    </div>
                    <?php
                    if ($model['orphan'.$key] && $model['orphan'.$key.'_type'] != 3) $options=array( 'disabled'=>true,);
                    else $options=array();
                    ?>
                    <div class="row" id="parent<?php echo $key ?>">
                        <label for="parent_fio_<?php echo $key ?>">ФИО</label>
                        <?php echo CHtml::textField('parent_fio['.$key.']', '',array('id'=>'parent_fio_'.$key, 'placeholder'=>'Фамилия Имя Отчество', 'size'=>'60')); ?>
                    </div>
                    <div class="row">
                        <label for="parent_addr_<?php echo $key ?>">Адрес родителя</label>
                        <?php echo CHtml::textField('parent_addr['.$key.']', '',array('id'=>'parent_addr_'.$key, 'placeholder'=>'Город, улица, дом/корпус, квартира', 'size'=>'60')); ?>
                        <?php if ($key == 1) { ?>
                        <a href="#" id="copy_parents_addr_home">Копировать</a>
                        <?php } else { ?>
                        <a href="#" id="copy_parents_addr">Копировать</a>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <?php echo CHtml::textField('parent_work['.$key.']', '',array('id'=>'parent_work_'.$key, 'placeholder'=>'Место работы и должность', 'size'=>'50')); ?>
                        <?php echo CHtml::textField('parent_phone['.$key.']', '',array('id'=>'parent_phone_'.$key, 'placeholder'=>'8 123 456 7890', 'alt'=>'8 999 999 9999')); ?>
                    </div>
                </div>
            <?php
                }
            }
            ?>
	</div>
    </div>
    <div class="rightblock">
	<div class="dopdocs block">
            <div class="blockinfo"><h2>Информация о сданных документах</h2></div>
            <?php
            $dopdocs=$docs->findAll(array('condition'=>'categoryid=2', 'order'=>'action ASC, id ASC'));
            $action='';
            foreach ($dopdocs as $docinfo){ 
                if ($action!='' && $action!=$docinfo->action) echo '<div style="width: 100%; overflow: hidden; padding:5px;"></div>';
                $action=$docinfo->action;
                if ($docinfo->action=='input'){
                    $docfields= new DirectDocsFields;
                    $fields=$docfields->findAll(array('condition'=>'docid='.$docinfo->id, 'order'=>'id ASC'));
                    $id=$docinfo->id;
                    echo '<div class="row"><b>'.$docinfo->name.'</b></div>';
                    include Yii::app()->getBasePath().'/views/pkentrant/doc-with-fields.php';
                }else{
                    include Yii::app()->getBasePath().'/views/pkentrant/doc-without-fields.php';
                }
            }
            ?>
	</div>
	<div class="grades block">
            <div class="blockinfo"><h2>Данные об экзаменах и ЕГЭ</h2></div>
            <div class="row"><b>Русский язык</b></div>
            <div class="row">
                <label for="Entrant_grade_rus">Оценка</label>
                <?php echo CHtml::textField('Entrant[grade_rus]', $model->grade_rus,array('alt'=>'999', 'size'=>'2')); ?>
                <label for="Entrant_grade_rus_doctype">Документ</label> <?php echo CHtml::dropDownList('Entrant[grade_rus_doctype]',$model->grade_rus_doctype, $model->gradedoctypes); ?>
            </div>
            <div class="row">
                <label for="Entrant_grade_rus_doc">Номер</label>
                <?php echo CHtml::textField('Entrant[grade_rus_doc]', $model->grade_rus_doc,array('alt'=>'99-999999999-99', 'placeholder'=>'__-_________-__')); ?>
                <a href="#" id="gradeduble">Продублировать</a>
            </div>
            <div class="row"><b>Химия</b></div>
            <div class="row">
                <label for="Entrant_grade_him">Оценка</label>
                <?php echo CHtml::textField('Entrant[grade_him]', $model->grade_him,array('alt'=>'999', 'size'=>'2')); ?>
                <label for="Entrant_grade_him_doctype">Документ</label> <?php echo CHtml::dropDownList('Entrant[grade_him_doctype]',$model->grade_him_doctype, $model->gradedoctypes); ?>
            </div>
            <div class="row">
                <label for="Entrant_grade_him_doc">Номер</label>
                <?php echo CHtml::textField('Entrant[grade_him_doc]', $model->grade_him_doc,array('alt'=>'99-999999999-99', 'placeholder'=>'__-_________-__')); ?>
            </div>
            <div class="row"><b>Биология</b></div>
            <div class="row">
                <label for="Entrant_grade_bio">Оценка</label>
                <?php echo CHtml::textField('Entrant[grade_bio]', $model->grade_bio,array('alt'=>'999', 'size'=>'2')); ?>
                <label for="Entrant_grade_bio_doctype">Документ</label> <?php echo CHtml::dropDownList('Entrant[grade_bio_doctype]',$model->grade_bio_doctype, $model->gradedoctypes); ?>
            </div>
            <div class="row">
                <label for="Entrant_grade_bio_doc">Номер</label>
                <?php echo CHtml::textField('Entrant[grade_bio_doc]', $model->grade_bio_doc,array('alt'=>'99-999999999-99', 'placeholder'=>'__-_________-__')); ?>
            </div>
	</div>
        
	<div class="lang block">
            <div class="blockinfo"><h2>Иностранный язык</h2></div>
            <div class="row">
                <label for="Entrant_foreignid">Язык</label>
                <?php 
                $lang=CHtml::listData(DirectForeign::model()->findAll(), 'id', 'name');
                array_unshift($lang, '');
                echo CHtml::dropDownList('Entrant[foreignid]',$model->foreignid, $lang);
                ?>
            </div>
            <div class="row">
                <label for="Entrant_foreign_exp">Изучал</label>
                <?php echo CHtml::textField('Entrant[foreign_exp]', $model->foreign_exp,array('alt'=>'99', 'placeholder'=>'Лет', 'size'=>'2')); ?>
                <label for="Entrant_foreign_grade">Оценка в аттестате/дипломе</label>
                <?php echo CHtml::textField('Entrant[foreign_grade]', $model->foreign_grade,array('alt'=>'5', 'size'=>'1')); ?>
            </div>
	</div>

        <div class="dop block">
            <div class="blockinfo"><h2>Дополнительно</h2></div>
            <div class="row"><label for="Entrant_social_work">Общественная работа до поступления в ВУЗ</label></div>
            <div class="row"><?php echo CHtml::textArea('Entrant[social_work]', $model->social_work, array('class'=>'doptextarea')); ?></div>
            <div class="row"><label for="Entrant_talent">Участие в художественное самодеятельности</label></div>
            <div class="row"><?php echo CHtml::textArea('Entrant[talent]', $model->talent, array('class'=>'doptextarea')); ?></div>
            <div class="row"><label for="Entrant_sport">Занятие спортом</label></div>
            <div class="row"><?php echo CHtml::textArea('Entrant[sport]', $model->sport, array('class'=>'doptextarea')); ?></div>
            <div class="row"><label for="Entrant_comment">Комментарий</label></div>
            <div class="row"><?php echo CHtml::textArea('Entrant[comment]', $model->comment, array('class'=>'doptextarea')); ?></div>
            <!--<div class="row"><label for="Entrant_ege_dop">Место сдачи ЕГЭ в доп. сроки</label></div>
            <div class="row"><?php // echo CHtml::textField('Entrant[ege_dop]', $model->ege_dop,array('size'=>'50')); ?></div>-->
	</div>

    </div>
    <br class="clear">
    <div class="row">
        <b><label for="Entrant_vk">Вне конкурса</label></b>
        <?php echo CHtml::checkBox('Entrant[vk]', $model->vk); ?><br />
        <div id="vk_notice_div"
        <?php 
        if ($model->vk) echo 'class="row">';
        else echo 'class="row hidden">';
//        echo CHtml::textField('Entrant[vk_notice]', $model->vk_notice, array('size'=>'50', 'placeholder'=>'Примечание'));
        ?>
        <div class="form">
            <?php
                $priorityPravo = CHtml::listData(DirectPriorityPravo::model()->findAll(), 'id', 'name');
                foreach ($priorityPravo as $id => $value){
            ?>
            <div class="row">
                <?php

                    echo CHtml::checkBox('Entrant[pravos]['.$id.']',
                        isset($model->id) ?
                            (LinkEntrantPravo::model()->find('entrantid='.$model->id.' and pravoid='.$id) == null ?
                            false : true): false);
                    echo $value;
                ?>
            </div>
            <?php
                }
            ?>
        </div>
    </div>
    <div class="row">
        <b><label for="Entrant_achv">Личные достижения</label></b>
        <?php echo CHtml::checkBox('Entrant[achv]', $model->achv); ?><br />
        <div id="achv_notice_div"
        <?php
            if ($model->achv == true) echo 'class="row">';
            else echo 'class="row hidden">';
        ?>
        <div class="form">
            <?php
                $priorityPravoOthers = CHtml::listData(DirectPriorityPravoOthers::model()->findAll(), 'id', 'name');
                foreach ($priorityPravoOthers as $id => $value){
            ?>
            <div class="row">
                <?php echo CHtml::checkBox('Entrant[achvs]['.$id.']',
                    isset($model->id) ?
                        (LinkEntrantAchv::model()->find('entrantid='.$model->id.' and achvid='.$id) == null ?
                            false : true) : false);
                    echo $value;
                ?>
            </div>
            <?php
                }
            ?>
        </div>
    </div>
    <div class="row">
        <b><label for="Entrant_medblock">Наличие медицинских противопоказаний</label></b>
        <?php echo CHtml::checkBox('Entrant[medblock]', $model->medblock); ?><br />
    </div>

        <?php
        if (!$model->took) $options=array('disabled'=>true);
        else $options=array();
        ?>
        <b><label for="Entrant_took">Забор</label></b>
        <?php echo CHtml::checkBox('Entrant[took]', $model->took, array('onclick' => 'js:enabletook(this)')); ?>
        <b><label for="Entrant_took_date">Дата забора документов</label></b>
        <?php echo CHtml::textField('Entrant[took_date]', $model->took_date, array_merge($options, array('placeholder'=>'__.__.____', 'alt' => '39.19.2999', 'class'=>'datepicker'))); ?><br />

        <?php
        if (!$model->orig) $options=array('disabled'=>true);
        else $options=array();
        ?>
        <b><label for="Entrant_orig">Оригиналы</label></b>
        <?php echo CHtml::checkBox('Entrant[orig]', $model->orig, array('onclick' => 'js:enableorig(this)')); ?>
        <b><label for="Entrant_orig_date">Дата подачи документов</label></b>
        <?php echo CHtml::textField('Entrant[orig_date]', $model->orig_date, array_merge($options, array('placeholder'=>'__.__.____', 'alt' => '39.19.2999', 'class'=>'datepicker'))); ?><br />

        <b><label for="Entrant_is_mail">Дело оформлено по почте</label></b>
        <?php echo CHtml::checkBox('Entrant[is_mail]', $model->is_mail, array('onclick'=>'js:enablemailnum(this);')); ?>
        <b><label for="Entrant_mail_num">Номер почтового дела</label></b>
        <?php
        if (!$model->is_mail) $options=array('disabled'=>true);
        else $options=array();
        ?>
        <?php echo CHtml::textField('Entrant[mail_num]', $model->mail_num, $options); ?>
        <p><b><label for="Entrant_crimea">Крым</label></b>
        <?php echo CHtml::checkBox('Entrant[crimea]', $model->crimea); ?></p>
    </div>

    <div class="row buttons">
            <?php echo CHtml::button($model->isNewRecord ? 'Добавить' : 'Сохранить', array('onclick'=>'js:beforesendform();')); ?>
    </div>
</form>
</div><!-- entantform -->
