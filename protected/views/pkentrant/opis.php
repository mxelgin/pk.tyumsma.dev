<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Опись документов</title>
<style>
.section2 {
	width:733px;
	font-size:12pt;
	font-family:"Times New Roman","serif";
	line-height:1.0em;
}
.head_title {
	text-align:center;
	font-size:14pt;
}
.head_title1 {
	text-align:justify;
}
.head_title span{
	text-transform:uppercase;
}
.small {
	font-size:10pt;
}
p {
	text-align:justify;
	line-height:1.0em;
}
.just {
	text-align:justify;
}
.right{
	line-height:1.5em;
}
</style>
<script>
window.onload = function () {
    window.print();
};
</script>
</head>

<body>
<div class="section2">
<p class="head_title"><strong>Опись документов</strong></p>
<table border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td width="480" valign="top"><br />
      <p class="right"><strong>Факультет (специальность) по выбору:</strong></p>
<?php
if ($model->specs){
    $i=1;
    foreach ($model->specs as $spec){
        echo '<p>'.$i++.' <u>'.$spec->spec->name.' ';
        if ($spec->type_b) echo 'бюджет ';
        if ($spec->type_d) echo 'договор ';
        echo '</u></p>';
    }
}else{
?>
    <p>1</p>
    <p>2</p>
    <p>3</p>
<?php
}
?>
<p><strong>Ф.И.О.</strong> <u><?php echo $model->fam.' '.$model->name.' '.$model->m_name; ?></u>
<p><strong>Домашний адрес</strong> <u> <?php echo $address.' '.$model->home_build.', '.$model->home_kv; ?></u></p>
<p><strong>Дата рождения</strong> (д. м. г.) <u><?php echo $model->birth_date; ?></u> <strong>Пол </strong>(обвести)
<?php
if ($model->sex==1)
    echo '<u>М.</u>      Ж.';
else
    echo 'М.      <u>Ж.</u>';
?>
</p>
<p><strong>Что окончил</strong> <u><?php echo $model->education->name; ?></u></p>
<p><strong>в каком году</strong> <u><?php if ($model->edu_date!='01.01.1970') echo date('Y',  strtotime($model->edu_date)); ?></u></p>
<p><strong>Производственный стаж:</strong>  общий _________ мед. _______________</p>
<p><strong>Иностранный язык</strong> (подчеркнуть): 
<?php
if ($model->foreignid==1)
    echo '<u>английский</u>, ';
else
    echo 'английский, ';
if ($model->foreignid==2)
    echo '<u>немецкий</u>, ';
else
    echo 'немецкий, ';
if ($model->foreignid==4)
    echo '<u>французский</u>, ';
else
    echo 'французский, ';
if ($model->foreignid==3)
    echo '<u>испанский</u>, ';
else
    echo 'испанский, ';
echo '<br />другой ';
if ($model->foreignid!=1 && $model->foreignid!=2 && $model->foreignid!=3 && $model->foreignid!=4)
    echo '<u>'.$model->foreign->name.'</u>';
else
    echo '_______________________________';
?>    
<br />
</p>
<!--<p><strong>Отношение к    военной службе</strong> (подчеркнуть):<br />
<?php
if ($model->service==1)
    echo '    <u>военнообязанный</u>                                          невоеннообязанный';
else
    echo '    военнообязанный                                          <u>невоеннообязанный</u>';
?>
-->
</p>
<p><strong>Житель</strong> (подчеркнуть):
<?php
if ($model->home_type==1)
    echo '    <u>города</u>                           села';
else
    echo '    города                           <u>села</u>';
?>

</p>
<!--      <p align="left"><strong>Родители</strong> (подчеркнуть) служащий(е), рабочий(е),<br />
      медработник(и), пенсионер(ы)</p>-->
    </td>
      
    <td width="253" rowspan="2" valign="bottom" style="border-left:1px solid;">
      <p>&nbsp;</p>
      <?php
      if ($model->orig) echo '<center><u><b>Сдал оригиналы</b></u></center>';
      else echo '<center><u><b>Сдал копии</b></u></center>';
      ?>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p class="right just">1.    Заявление     <u>есть</u>         нет<br />
        2. Документ    об обр.
<?php
$doc=Docs::model()->find('entrantid='.$model->id.' and docid=29 and fieldid=19');
if ($doc->value)
    echo '<u>'.$doc->value.'</u> ';
else
    echo '___________';
echo '<br />';
$doc=Docs::model()->find('entrantid='.$model->id.' and docid=29 and fieldid=20');
if ($doc->value)
    echo 'серия <u>'.$doc->value.'</u> ';
else
    echo 'серия______ № _________________ ';
$doc=Docs::model()->find('entrantid='.$model->id.' and docid=29 and fieldid=21');
if ($doc->value)
    echo 'от <u>'.$doc->value.'</u> ';
else
    echo 'от_____________________________';

?>
<br />
        
        3. Фото 3х4    4 штуки
<?php
$doc=Docs::model()->find('entrantid='.$model->id.' and docid=20');
if ($doc->value)
    echo '<u>есть</u>      нет';
else
    echo 'есть      <u>нет</u>';
?>
        <br />
        4. Копия    трудовой кн.
<?php
$doc=Docs::model()->find('entrantid='.$model->id.' and docid=27');
if ($doc->value)
    echo '<u>есть</u>      нет';
else
    echo 'есть      <u>нет</u>';
?>
        <br />
        5. Мед.    справка ф. 086у
<?php
$doc=Docs::model()->find('entrantid='.$model->id.' and docid=28');
if ($doc->value)
    echo '<u>есть</u>      нет';
else
    echo 'есть      <u>нет</u>';
?>
        <br />
        6.    Свидетельства ЕГЭ 
<?php
if ($model->grade_rus_doc)
    echo '<u>'.$model->grade_rus_doc.'</u> ';
if ($model->grade_him_doc && $model->grade_him_doc!=$model->grade_rus_doc)
    echo '<u>'.$model->grade_him_doc.'</u> ';
if ($model->grade_bio_doc && $model->grade_bio_doc!=$model->grade_rus_doc && $model->grade_bio_doc!=$model->grade_him_doc)
    echo '<u>'.$model->grade_bio_doc.'</u> ';
if (empty($model->grade_rus_doc) && empty($model->grade_him_doc) && empty($model->grade_bio_doc))
    echo ' ___________________________________________';
?>      
</p>
        <p class="right">
        <strong><center>Другие документы</center></strong></p>
        <p class="right just">
<?php
$docs=Docs::model()->findAll(array('with'=>'doc', 'condition'=>"t.entrantid=".$model->id." and t.docid>30 and t.value>0 and doc.action='checkbox'"));
$i=7;
if ($docs){
    foreach ($docs as $doc){
        echo $i++.' <u>'.$doc->doc->name.'</u><br />';
    }
}else{
?>
        7. _____________________________<br />
        8. _____________________________<br />
        9. _____________________________</p>
<?php } ?>
    </td>
  </tr>
</table>
</div>
</body>
</html>
