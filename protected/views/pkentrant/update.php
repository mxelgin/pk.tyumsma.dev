<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'config-form',
	'enableAjaxValidation'=>false,
)); ?>
<h1>Основные настройки</h1>
<?php foreach ($records as $model){ ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
        <?php
        if ($model->name!='description'){
        ?>
	<div class="row">
		<?php
                echo $form->labelEx($model,'value');
                echo $form->textField($model,'value',array('size'=>60,'maxlength'=>255));
		echo $form->error($model,'value');
        }else{
                echo $form->labelEx($model, 'value');
                $this->widget('application.extensions.tinymce.ETinyMce', array(
                    'model'=>$model,
                    'attribute'=>'value',
                    'language'=>'ru',
                    'htmlOptions'=>array('style'=>'width: 600px; height: 200px;')
                ));
        }
        ?>

	</div>

<?php } ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить'); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->