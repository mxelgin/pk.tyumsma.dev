<div class="view">
    <p align="center">
        <?php echo CHtml::button('Распечатать заявление', array('onclick'=>'js: window.open("'.$this->createUrl('pkentrant/printStatement', array('id'=>$data->id)).'", "Statement", "menubar=no,location=no,resizable=no,status=no,width=710")')); ?>
        <?php echo CHtml::button('Распечатать форму №9', array('onclick'=>'js: window.open("'.$this->createUrl('pkentrant/printForm9', array('id'=>$data->id)).'", "Form9", "menubar=no,location=no,resizable=no,status=no,width=710")')); ?>
        <?php echo CHtml::button('Распечатать опись документов', array('onclick'=>'js: window.open("'.$this->createUrl('pkentrant/printOpis', array('id'=>$data->id)).'", "Opis", "menubar=no,location=no,resizable=no,status=no,width=710")')); ?>
    </p>
    <?php if (!$data->rcm->id && $data->maindoc==21){
        $pass_number=Docs::model()->find('entrantid='.$data->id.' and docid=21 and fieldid=13');
        $rcm=Rcm::model()->find(array('with'=>'spec','condition'=>"t.pass_number like '".$pass_number->value."'"));
        if ($rcm){
    ?>
    <div class="view">
        Указанные паспортные данные присутствуют в базе РЦМ. 
        <?php 
        $rcmok=true;
        if (empty($data->orig)){
            echo '<br /> <b>Абитуриенты, которые не подали оригиналы документов не могут участвовать в рейтинге РЦМ.</b>';
            $rcmok=false;
        }
        if ($data->specs[0]->specid!=$rcm->specid){
            echo '<br /><b>В качестве приоритетной специальности необходимо выбрать '.$rcm->spec->name.' факультет</b>';
            $rcmok=false;
        }
        if (empty($data->specs[0]->type_b)){
            echo '<br /><b>Приоритетная специальность должна быть бюджетной</b>';
            $rcmok=false;            
        }
        if ($rcmok && Yii::app()->user->checkAccess('secretary')) echo '<a href="'.$this->createUrl('/rcm/setrcm', array('id'=>$data->id, 'rcm'=>$rcm->id)).'">Отметить абитуриента как целевика</a>.';
        ?>
    </div>
    <?php
        }
    }
    if (Yii::app()->user->checkAccess('secretary')){
        if (!$data->checkedbyuser)
            echo '<p><form action="/pkentrant/check/id/'.$data->id.'" method="post"><b>'.CHtml::checkBox('checked', $data->checkedbyuser, array('onclick'=>'js:this.form.submit();')).'Подтверждаю, что все данные верны</b></form></p>';
    }
    if ($data->checkedbyuser)
        echo '<p><b>Данные проверены секретарем '.$data->checked->last_name.' '.$data->checked->first_name.' '.$data->checked->middle_name.'</b></p>';
    ?>
    <table width="100%">
        <tr><td width="50%" valign="top">
	<b><?php echo $data->getAttributeLabel('id'); ?>:</b>
	<?php echo CHtml::link($data->id, array('view', 'id'=>$data->id)); ?>
	<br />
        
	<b>ФИО:</b>
	<?php echo $data->fam.' '.$data->name.' '.$data->m_name; ?>
        <?php
        $entrants=Entrant::model()->findAll("fam like '".$data->fam."' and name like '".$data->name."' and m_name like '".$data->m_name."' and id<>".$data->id);
        if ($entrants){
            echo '<p><small>Похожие абитуриенты:</small><br />';
            foreach ($entrants as $ent){
                echo '<small>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.' '.$ent->birth_date.'</small><br>';
            }
            echo '</p>';
        }
        ?>
	<br />
        
        <?php if ($data->phone){ ?>
        <b><?php echo $data->getAttributeLabel('phone'); ?>:</b>
	<?php echo $data->phone; ?>
	<br />
        <?php } ?>

        <?php if ($data->birth_date){ ?>
        <b><?php echo $data->getAttributeLabel('birth_date'); ?>:</b>
	<?php echo $data->birth_date; ?>
	<br />
        <?php } ?>

        <?php if ($data->birth_place_id){ ?>
        <b><?php echo $data->getAttributeLabel('birth_place_id'); ?>:</b>
	<?php echo $data->birth_city->name; ?>
	<br />
        <?php } ?>

        <?php if ($data->citizenship){ ?>
        <b><?php echo $data->getAttributeLabel('citizenship'); ?>:</b>
	<?php echo $data->citizen->name; ?>
	<br />
        <?php } ?>

        <?php if ($address){ ?>
        <b>Домашний адрес:</b><br />
        <?php echo $address.', '.$data->home_build.', '.$data->home_kv;?>
        <?php // echo $data->home_region_db->name.', '.$data->home_city_db->name.', '.$data->home_street.' '.$data->home_build.', '.$data->home_kv;?>
	<br />        
        <?php } ?>

        <?php if ($data->sex){ ?>
        <b><?php echo $data->getAttributeLabel('sex'); ?>:</b>
	<?php echo $data->sextypes[$data->sex]; ?>
	<br />
        <?php } ?>

        <?php if ($data->need_hostel){
            echo '<b>Нуждается в общежитии</b></br>';
        } ?>
        
        <?php if ($data->sex==1){ ?>
        <b><?php echo $data->getAttributeLabel('service'); ?>:</b>
	<?php echo $data->servicetypes[$data->service]; ?>
	<br />
        <?php } ?>
        
        <?php if ($data->foreignid){ ?>
        <b><?php echo $data->getAttributeLabel('foreignid'); ?>:</b>
	<?php echo $data->foreign->name; ?>
	<br />
        <?php } ?>

        <?php if ($data->foreign_exp){ ?>
        <b><?php echo $data->getAttributeLabel('foreign_exp'); ?>:</b>
	<?php echo $data->foreign_exp; ?>
	<br />
        <?php } ?>
        
        <?php if ($data->foreign_grade){ ?>
        <b><?php echo $data->getAttributeLabel('foreign_grade'); ?>:</b>
	<?php echo $data->foreign_grade; ?>
	<br />
        <?php } ?>
                
        <?php if ($data->edu_type){ ?>
        <b>Окончил учебное заведение:</b> <?php echo $data->education->name; ?><br />
        Дата окончания: <?php echo $data->edu_date; ?><br />
        Средний балл: <?php echo $data->edu_grade; ?>
        <br />
        Отличие: <?php echo $data->education_spec->name; ?>
	<br />
        <?php } ?>        

        <?php if ($data->rcm->id){?>
        <b>Регионально-целевое направление (<?php echo $data->rcm->region->name.' '.$data->rcm->spec->name;?>)</b><br />
        <?php } ?>

        <?php if ($data->social_work){ ?>
        <b><?php echo $data->getAttributeLabel('social_work'); ?>:</b>
	<?php echo $data->social_work; ?>
	<br />
        <?php } ?>

        <?php if ($data->talent){ ?>
        <b><?php echo $data->getAttributeLabel('talent'); ?>:</b>
	<?php echo $data->talent; ?>
	<br />
        <?php } ?>

        <?php if ($data->sport){ ?>
        <b><?php echo $data->getAttributeLabel('sport'); ?>:</b>
	<?php echo $data->sport; ?>
	<br />        
        <?php } ?>
        </td>
        <td width="50%" valign="top">
            <table>
                <tr><th>Предмет</th><th>Оцена</th><th>Документ</th><th>Номер</th></tr>
                <tr><td>Русский язык</td><td><?php echo $data->grade_rus; ?></td><td><?php echo $data->gradedoctypes[$data->grade_rus_doctype]; ?></td><td><?php echo $data->grade_rus_doc; ?></td></tr>
                <tr><td>Химия</td><td><?php echo $data->grade_him; ?></td><td><?php echo $data->gradedoctypes[$data->grade_him_doctype]; ?></td><td><?php echo $data->grade_him_doc; ?></td></tr>
                <tr><td>Биология</td><td><?php echo $data->grade_bio; ?></td><td><?php echo $data->gradedoctypes[$data->grade_bio_doctype]; ?></td><td><?php echo $data->grade_bio_doc; ?></td></tr>
            </table>
            <b><?php echo $data->mdoc->name; ?></b><br />
            <?php
            foreach ($mdocfields as $field){
                echo $field->field->name.': '.$field->value.'<br>';
            }
            ?>
            <b>Сдал документы:</b><br />
            <?php
            foreach ($dopdocs as $doc){
                if ($doc->doc->action=='input'){
                    echo $doc->doc->name.' (';
                    echo $doc->field->name.': '.$doc->value.')<br>';
                }else{
                    echo $doc->doc->name.'<br>';
                }
            }
            ?>
        </td></tr>
        <tr>
            <td>
                <?php if ($data->specs){ ?>
                    <b>Приоритет специальностей:</b><br />
                    <?php
                    foreach ($data->specs as $spec){
                        if (empty($spec->specid)) continue;
                        echo $spec->level.' '.$spec->spec->name.' ';
                        if ($spec->type_b) echo 'бюджет ';
                        if ($spec->type_d) echo 'договор ';
                        echo '<br>';
                    }
                }
                ?>
                    <br />
                    <?php
                    if ($data->took){
                        echo '<b>Забрал документы</b><br />';
                    }
                    if ($data->vk){
                        echo '<b>Вне конкурса:</b>';
                        if ($data->vk_notice) echo ' ('.$data->vk_notice.')';

                        echo '<div class="form">';
                            $linkPriorityPravo = CHtml::listData(LinkEntrantPravo::model()->findAll('entrantid='.$data->id),'id','pravoid');
                            foreach ($linkPriorityPravo as $id => $value){
                            $priorityPravo = DirectPriorityPravo::model()->find('id='.$value);
                            echo '<div class="row">'.$priorityPravo->name.'</div>';
                        }
                        echo '</div>';
                        echo '<br />';
                    }
                    if ($data->achv){
                        echo '<b>Личные достижения:</b>';
                        echo '<div class="form">';
                        $linkEntrantAchv = CHtml::listData(LinkEntrantAchv::model()->findAll('entrantid='.$data->id),'id','achvid');
                        foreach ($linkEntrantAchv as $id => $value){
                            $priorityPravoOthers = DirectPriorityPravoOthers::model()->find('id='.$value);
                            echo '<div class="row">'.$priorityPravoOthers->name.'</div>';
                        }
                        echo '</div>';
                        echo '<br />';
                    }
                    if ($data->orig){
                        echo '<b>Сдал оригиналы</b><br />';
                    }
                    if ($data->is_mail){
                        echo '<b>Номер почтового дела:</b> '.$data->mail_num.'<br />';
                    }
                    if ($data->crimea){
                        echo '<b>Крым</b><br />';
                    }
                    ?>
                <br />
                    <?php 
                    if ($data->ege_dop)
                    echo '<b>Место сдачи ЕГЭ в доп. сроки</b>: '.$data->ege_dop.'<br />';
                    ?>
            </td>
            <td>
                <?php if ($data->parents){ ?>
                    <b>Информация о родителях:</b><br />
                    <?php
                    $i = 1;
                    foreach ($data->parents as $parent){
                        $orphan = $data['orphan'.$i];
                        $orphan_type = $data['orphan'.$i.'_type'];
                        if (!$orphan || $orphan_type == 3) {
                            echo 'ФИО: '.$parent->fio;
                            if ($orphan_type == 3) echo ' (опекун)';
                            echo '<br>';
                            echo 'Адрес: '.$parent->addr.'<br>';
                            echo 'Работа: '.$parent->work.'<br>';
                            echo 'Телефон: '.$parent->phone.'<br>';
                        } else {
                            echo '<br>';
                            if ($orphan_type == 1) echo 'Умер';
                            if ($orphan_type == 2) echo 'Не живет с семьей';
                            //echo '<pre>'; print_r($data->attributes); echo '</pre>';
                        }
                        $i++;
                    }
                }
                ?>        
            </td>
        </tr>
    </table>
    <p>Добавил пользователь: <?php echo $data->user->last_name.' '.$data->user->first_name.' '.$data->user->middle_name; ?></p>
    <div class="row"><?php echo CHtml::button('2х сторонний договор', array('onclick'=>'js: window.open("'.$this->createUrl('pkentrant/dog1', array('id'=>$data->id)).'", "Statement", "menubar=no,location=no,resizable=no,status=no,width=710")')); ?></div>
    <div class="row"><?php echo CHtml::button('3х сторонний договор с физ лицом', array('onclick'=>'js: window.open("'.$this->createUrl('pkentrant/dog2', array('id'=>$data->id)).'", "Statement", "menubar=no,location=no,resizable=no,status=no,width=710")'));
        $dog2=Contract2::model()->find('entrantid='.$data->id);
        if ($dog2)
            echo '<a href="/contract2/update/id/'.$dog2->id.'">Изменить данные</a>';
        else
            echo '<a href="/contract2/create/entrantid/'.$data->id.'">Задать данные</a>';
        ?>
    </div>
    <div class="row"><?php echo CHtml::button('3х сторонний договор с юр лицом', array('onclick'=>'js: window.open("'.$this->createUrl('pkentrant/dog3', array('id'=>$data->id)).'", "Statement", "menubar=no,location=no,resizable=no,status=no,width=710")'));
        $dog3=Contract3::model()->find('entrantid='.$data->id);
        if ($dog3)
            echo '<a href="/contract3/update/id/'.$dog3->id.'">Изменить данные</a>';
        else
            echo '<a href="/contract3/create/entrantid/'.$data->id.'">Задать данные</a>';
        ?>
    </div>
<!--    <div class="row">--><?php //echo CHtml::button('4х сторонний договор', array('onclick'=>'js: window.open("'.$this->createUrl('pkentrant/dog4', array('id'=>$data->id)).'", "Statement", "menubar=no,location=no,resizable=no,status=no,width=710")'));
//        $dog4=Contract4::model()->find('entrantid='.$data->id);
//        if ($dog4)
//            echo '<a href="/contract4/update/id/'.$dog4->id.'">Изменить данные</a>';
//        else
//            echo '<a href="/contract4/create/entrantid/'.$data->id.'">Задать данные</a>';
//        ?>
<!--    </div>-->
</div>
