<?php
$this->breadcrumbs=array(
	'Абитуриенты'=>array('admin')
);

$this->menu=array(
	array('label'=>'Добавить абитуриента', 'url'=>array('create')),
);
if (Yii::app()->user->checkAccess('secretary')){
    $this->menu[]=array('label'=>'База РЦМ', 'url'=>'/rcm');
    $this->menu[]=array('label'=>'Список на экзамен', 'url'=>array('examList'));
    $this->menu[]=array('label'=>'Список на ЕГЭ', 'url'=>array('egeList'));
    $this->menu[]=array('label'=>'Список абитуриентов по датам', 'url'=>array('dateList'));
    $this->menu[]=array('label'=>'Список абитуриентов по рейтингу', 'url'=>array('ratingList'));
}


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('entrant-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>База абитуриентов</h1>

<p>
Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) для поиска нужных позиций.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'entrant-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                'id',
                'fam',
		'name',
                'm_name',
                //'birth_date',
                //array('name'=>'sex', 'value'=>'$data->sextypes[$data->sex]', 'filter'=>$model->sextypes),
                'rating',
                'grade_rus',
                'grade_him',
                'grade_bio',
                array('name'=>'specs', 'header'=>'Специальность', 'value'=>'$data->showallspec()', 'filter'=>  CHtml::listData(Departments::model()->findAll(), 'id', 'name')),
                array('name'=>'vk', 'value'=>'$data->vktypes[$data->vk]', 'filter'=>$model->vktypes),
                array('name'=>'took', 'value'=>'$data->tooktypes[$data->took]', 'filter'=>$model->tooktypes),
                array('name'=>'orig', 'value'=>'$data->origtypes[$data->orig]', 'filter'=>$model->origtypes),
                array('name'=>'rcm.entrantid', 'header'=>'РЦМ', 'value'=>'$data->rcm->id?"Да":"Нет"'),
		array(
			'class'=>'CButtonColumn',
		),
	),
));
?>
