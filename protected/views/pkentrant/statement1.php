<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Заявление</title>
<style>
body {
	margin:0;
	padding:0;
        font-size: 12pt;
        font-family:"Times New Roman","serif";
        line-height:1.0em;
}
.section2 {
	width:710px;
        padding-left: 30pt;
}
.head_title {
	text-align:center;
	font-size:14pt;
}
.head_title1 {
	text-align:justify;
}
.head_title span{
	text-transform:uppercase;
}
.small {
	font-size:10pt;
}
/*p {
	text-align:justify;
	line-height:1.0em;
}*/
.just {
	text-align:justify;
}
.right{
	line-height:1.5em;
	text-align:right;
}
.left_float{
	float:left;
	line-height:1.5em;
}
.right_float{
	float:right;
	right:0;
	line-height:1.5em;
}
.nor{
	line-height:1.5em;
}
</style>
<script>
window.onload = function () {
    window.print() ;
};
</script>
</head>

<body>
<div class="section2">
<div class="left_float">
К вступительным экзаменам допущен<br />
протокол № ______________________<br />
от «_____»_______________<?php echo date('Y'); ?> г.<br />
Подпись
</div>
<div class="right_float">

Зачислен на 1 курс<br />
__________________________________факультета<br />
протокол № _____ от «___»_____________<?php echo date('Y'); ?> г.<br />
Подпись
</div>
<div style="overflow:hidden;"></div>
<p>&nbsp;</p>
<div>
<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
<p class="nor">
Ректору ГБОУ ВПО ТюмГМА Минздрава России от гр.<br />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td width="10%" align="left">Фамилия: </td>
        <td width="30%" align="left"><u><?php echo $model->fam; ?></u></td>
        <td width="10%" align="left">&nbsp;</td>
        <td width="50%" align="left">&nbsp;</td>
    </tr>
    <tr>
        <td width="10%" align="left">Имя: </td>
        <td width="30%" align="left"><u><?php echo $model->name; ?></u></td>
        <td width="10%" align="left">Отчество: </td>
        <td width="50%" align="left"><u><?php echo $model->m_name; ?></u></td>
    </tr>
    <tr>
        <td width="50%" colspan="3" align="left">проживающего (ей) (регистрация с почт. индекс): </td>
        <td width="50%" align="left"><u><?php echo $address.' '.$model->home_build.', '.$model->home_kv; ?></u></td>
    </tr>
    <tr>
        <td width="50%" colspan="3" align="left">Фактический адрес проживания (с почт. индекс):  </td>
        <td width="50%" align="left"><u><?php echo $model->home_fact; ?></u></td>
    </tr>
    <tr>
        <td width="10%" align="left">Контактные телефоны: </td>
        <td width="30%" align="left"><u><?php echo $model->phone; ?></u></td>
        <td width="10%" align="left">e-mail: </td>
        <td width="50%" align="left">__________________________</td>
    </tr>
    <tr>
        <td width="40%" colspan="2" align="left">паспортные данные (серия, №, когда и кем выдан): </td>
        <td width="60%" colspan="2" align="left"><?php
            if ($model->maindoc==21)
            echo '<p><u>';
            foreach ($mdocfields as $mfield)
                echo $mfield->value.' ';
            echo '</u></p>';
            ?>
        </td>
    </tr>
</table>
<b><h2><center>заявление</center></h2></b>
<p class="nor">Прошу допустить меня к
<?php
if ($model->grade_rus==0 || $model->grade_him==0 || $model->grade_bio==0)
    echo '<s> участию в конкурсе</s>, вступительным экзаменам ';
else
    echo ' участию в конкурсе, <s>вступительным экзаменам</s>';
?>
&nbsp;для поступления&nbsp;
<?php
if ($model->specs[0]->specid>5) echo 'на <s>дневное</s> заочное обучение<br />';
else echo 'на дневное <s>заочное обучение</s><br />';
?>
На 
<?php
if ($model->rcm->id)
    echo 'целевое место, ';
else{
    echo '<s>целевое место</s>; <s>по квоте лиц имеющих особые права</s>; ';
    if ($specfirst->type_b==1)
        echo 'финансируемое из федерального бюджета, ';
    else
        echo '<s>финансируемое из федерального бюджета</s>; ';
    if ($specfirst->type_d==1)
        echo 'на место с оплатой стоимости обучения ';
    else
        echo '<s>на место с оплатой стоимости обучения</s> ';
}
?>
(<small>ненужное зачеркнуть</small>)<br />
факультета: <u><?php echo $specfirst->spec->name; ?></u><br />
по специальности <u><?php echo $specfirst->spec->specname; ?></u><br />
В случае непрохождения по конкурсу прошу передать мои документы на специальность в соответствии
с приведенным ниже списком приоритетов:</p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="8%" rowspan="2" align="center">№ пп</td>
    <td width="49%" rowspan="2" align="center">Название факультета</td>
    <td colspan="2" align="center">Условия</td>
  </tr>
  <tr>
    <td width="23%" align="center">Бюджет</td>
    <td width="20%" align="center">Внебюджет</td>
  </tr>
<?php
if (count($specother)){
    foreach ($specother as $spec){
        echo '
        <tr>
            <td>'.($spec->level-1).'</td>
            <td>'.$spec->spec->name.'</td>
            <td colspan="2" align="center">';
        if ($spec->type_b) echo 'бюджет ';
        if ($spec->type_d) echo 'договор ';
        echo '</td>
        </tr>';
    }
}else{
    echo '
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
    </tr>';
}
?>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td width="75%" align="left">Перечень специальностей и форм обучения подтверждаю</td>
        <td width="25%" align="center">________________________</td>
    </tr>
    <tr>
        <td width="75%" align="left">&nbsp;</td>
        <td width="25%" align="center">(<small>подпись</small>)</td>
    </tr>
</table>
<p style="font-weight: bold">Если не буду зачислен на бюджет, согласен(а) обучаться по договору возмездного оказания
    образовательных услуг: да [&nbsp;&nbsp;], нет [&nbsp;&nbsp;].</p>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td width="75%" align="left">«_____»_______________<?php echo date('Y'); ?> г.</td>
        <td width="25%" align="center">________________________</td>
    </tr>
    <tr>
        <td width="75%" align="left">&nbsp;</td>
        <td width="25%" align="center">(<small>подпись</small>)</td>
    </tr>
</table>
<p style="font-weight: bold">Медицинские противопоказания в соответствиии с приказом Минздравсоцразвития РФ от
    12.04.2011 №302-н имеются: да [&nbsp;&nbsp;<?php echo $model->medblock == 1 ? '&nbsp;v&nbsp;' :'&nbsp;&nbsp;' ?>], нет [<?php echo $model->medblock == 0 ? '&nbsp;v&nbsp;' :'&nbsp;&nbsp;' ?>].</p>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<p>Прошу засчитать в качестве результатов вступительных испытаний следующее:</p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="32%" height="34" valign="top">Наименование предмета</td>
    <td width="33%" valign="top">Балл</td>
    <td width="35%" valign="top">Подтверждающий документ
      Наименование  &nbsp;&nbsp;&nbsp;&nbsp;  номер</td>
  </tr>
    <tr><td>Русский язык</td><td align="center"><?php echo $model->grade_rus; ?></td><td><?php echo $model->gradedoctypes[$model->grade_rus_doctype]; ?> <?php echo $model->grade_rus_doc; ?></td></tr>
    <tr><td>Химия</td><td align="center"><?php echo $model->grade_him; ?></td><td><?php echo $model->gradedoctypes[$model->grade_him_doctype]; ?> <?php echo $model->grade_him_doc; ?></td></tr>
    <tr><td>Биология</td><td align="center"><?php echo $model->grade_bio; ?></td><td><?php echo $model->gradedoctypes[$model->grade_bio_doctype]; ?> <?php echo $model->grade_bio_doc; ?></td></tr>
</table>
<p class="nor">
Сумма баллов: <?php echo $model->rating;?><br />
<p>
Прошу допустить меня к вступительным испытаниям по материалам ТюмГМА по следующим предметам:
________________________________________________________________________________________
Прошу создать специальные условия при проведени  вступительных испытаний в связи с ограниченными возможностями здоровья
или инвалидностью (с указанием перечня вступительных испытаний и специальных условий)
________________________________________________________________________________________
________________________________________________________________________________________</p>
<p>В период с ________ по _________ июля 2014 года сдаю ЕГЭ по следующим предметам:
________________________________________________________________________________________</p>
<p>Место сдачи ЕГЭ в дополнительные сроки проведения ЕГЭ:</p>
<?php
if ($model->ege_dop)
    echo '<u>'.$model->ege_dop.'</u>';
else
    echo '________________________________________________________________________________________';
echo '<br />';
if ($model->need_hostel)
    echo '<p>В общежитии <u>нуждаюсь</u>, не нуждаюсь(подчеркнуть)</p>';
else
    echo '<p>В общежитии нуждаюсь, <u>не нуждаюсь</u>(подчеркнуть)</p>';
?>

<p>О себе сообщаю следующие сведения:</p>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td width="25%" align="left">Пол</td>
        <td width="25%" align="left"><u><?php echo $model->sextypes[$model->sex]; ?></u></td>
        <td width="25%" align="left">Гражданство</td>
        <td width="25%" align="left"><u><?php echo $model->citizen->name; ?></u></td>
    </tr>
    <tr>
        <td width="25%" align="left">Год и место рождения</td>
        <td width="25%" align="left"><u><?php echo $model->birth_date.' '.$model->birth_city->name; ?></u></td>
        <td width="25%" align="left">&nbsp;</td>
        <td width="25%" align="left">&nbsp;</td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td width="35%" align="left" style="font-weight: bold">Документ об образовании</td>
        <td width="25%" align="left">
            <?php
            if ($model->maindoc==21) {
                echo "Паспорт гражданина РФ";
            }
            ?>
        </td>
    </tr>
    <tr>
        <td width="25%" align="left">серия и номер</td>
        <td width="25%" align="left">
            <?php
            if ($model->maindoc==21)
                foreach ($mdocfields as $mfield)
                    if ($mfield->fieldid == 13) {
                        echo $mfield->value.' ';
                    }
            ?>
        </td>
        <td width="25%" align="left">год выдачи</td>
        <td width="25%" align="left">
            <?php
            if ($model->maindoc==21)
                foreach ($mdocfields as $mfield)
                    if ($mfield->fieldid == 14) {
                        echo $mfield->value.' ';
                    }
            ?>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td><span style="font-weight: bold">Наименование</span> учебного заведения</td>
        <td><u><?php echo $model->education->name;?></u></td>
    </tr>
    <tr>
        <td><span style="font-weight: bold">Местонахождение</span> учебного заведения: (страна)</td>
        <td>__________________________________________________</td>
    </tr>
    <tr>
        <td>населенный пункт</td>
        <td>__________________________________________________</td>
    </tr>
    <tr>
        <td><span style="font-weight: bold">Год окончания учебного заведения:</span></td>
        <td><u><?php echo $model->edu_date?></u></td>
    </tr>
    <tr>
        <td><span style="font-weight: bold">Изучаемый иностранный язык:</span></td>
        <td><u><?php echo $model->foreign->name;?></u></td>
    </tr>
</table>

<b><h4><center>Дополнительная информация</center></h4></b>
<p><span style="font-weight: bold">Имею льготы</span>, дающие право на зачисление вне конкурса в соответствии с
правилами приема: да [<?php echo $model->vk != 0 ? '&nbsp;v&nbsp;' :'&nbsp;&nbsp;' ?>], нет [<?php echo $model->vk == 0 ? '&nbsp;v&nbsp;' :'&nbsp;&nbsp;' ?>]. Наименование льготы:
    <u><?php
       foreach ($model->pravos as $pravo){
        echo $pravo->pravo->name.'; ';
       }

        for ($i = 0; $i <= 50; $i++){
        echo '&nbsp;';
       }
    ?>

    </u>, документ,
подтверждающий льготу __________________________, серия, номер ___________________________________________,
выдан ___________________________________________________________</p>
<p><span style="font-weight: bold">Являюсь призером заключительного этапа олимпиад школьников, дающих право:</span></p>
<p>- на поступление без экзаменов: да [&nbsp;&nbsp;], нет [&nbsp;&nbsp;].</p>
<p>- на зачет 100 баллов по соответствующему предмету: да [&nbsp;&nbsp;], нет [&nbsp;&nbsp;].</p>
<p>Название олимпиады и ее номер в перечне олимпиад школьников
________________________________________________________________________________________,
</p>
<p>Диплом(ы) победителя (призера) серия, номер</p>
<p>предмет(ы)___________________________ степень___________________________________________</p>
<p>При поступлении имею дополнительные сведения о себе при прочих равных условиях:</p>
<u><?php
    foreach ($model->achvs as $achv){
        echo $achv->achv->name.'; ';
    }

    for ($i = 0; $i <= 50; $i++){
        echo '&nbsp;';
    }
    ?>
</u>
<p>________________________________________________________________________________________</p>
<p>Фамилия, имя, отчество родителей, их местожительство, телефон, кем и где работают
    (наименование и местонахождение предприятия, организации, занимаемая должность):</p>
<?php
if ($model->parents){
    foreach ($model->parents as $parent){
        echo '<p><u>';
        if ($parent->fio) echo $parent->fio.', ';
        if ($parent->addr) echo $parent->addr.', ';
        if ($parent->work) echo $parent->work.', ';
        if ($parent->phone) echo $parent->phone;
        echo '</u></p><br />';
    }
}else{
    echo '________________________________________________________________________________________<br />';
}
?>
<p>О себе дополнительно сообщаю ___________________________________________________________</p>
<p>________________________________________________________________________________________</p>
<small>
    <center><p style="font-weight: bold">Для гражданина иностранного государства</p></center>
<p>Встал(а) на миграционный учет да [&nbsp;&nbsp;], нет [&nbsp;&nbsp;] на срок до _____________________</p>
<p style="font-weight: bold">Для обладателя документа об образовании, полученного в иностранном государстве</p>
<p>Предупрежден(а) о необходимости получения свидетельства о признании документа об уровне образования в срок до 1 января следующего календарного года</p>
</small>
<p style="font-weight: bold">Я ознакомлен(а):</p>
<small>
<p style="font-weight: bold">-с лицензией на право ведения образовательной деятельности, свидетельством о государственной акредитации и Уставом
ГБОУ ВПО ТюмГМА Минздрава России;</p>
<p><span style="font-weight: bold">-с Правилами приема в ТюмГМА на 2014/15</span> (в том числе: с порядком и сроками
    приема документов, проведения вступительных экзаменов и апелляций, с правилами участия в конкурсе и порядком
    зачисления в Академию по каждой специальности/направлению; формами заявлений);</p>
<p style="font-weight: bold">- с информацией о количестве бюджетных мест по каждому направлению подготовки (специальности)</p>
<p style="font-weight: bold">- с информацией на обработку персональных данных (моих и родителей) в порядке, предусмотренном Федеральным законом
    от 27.07.2006 г № 152-ФЗ «О персональных данных» ознакомлен и согласен</p>
<p style="font-weight: bold">Я предпрежден(а) о сроках предоставления подлинника докмента об образовании</p>
<p style="font-weight: bold">Подтверждаю, что зачисление меня в высшее учебное заведение не противоречит действующему законодательству РФ</p>
<p style="font-weight: bold">В случае невыполнения или нарушения мною обязательств, предусмотренных указанными правилами и положениями претензий к приемной комиссии ТюмГМА не имею</p>
<p style="font-weight: bold">Расписка в приеме документов мною получена, правильность указанных в ней сведений подтверждаю</p>
</small>
  <br />
  «<?php echo date('d'); ?>» <?php echo date('m'); ?> <?php echo date('Y'); ?> г.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Подпись ____________________
</p>
</div>

</div>


</body>
</html>
