<style>
#wrapper div {
    display:none;
}
#wrapper a.active {
    color: black;
    cursor: default;
    font-weight: bold;
    text-decoration: none;
}
#wrapper div.active {
    display:block;
    width:100%;
}
</style>
<script type="text/javascript" >

$(document).ready(function(){
    $('#wrapper a').click(function() {
        var tab_id=$(this).attr('id');
        tabClick(tab_id);
    });
    
    //tabClick(window.location.hash.substr(1));

    function tabClick(tab_id) {
        if (tab_id != $('#wrapper a.active').attr('id') ) {
            $('#wrapper a').removeClass('active');
            $('#'+tab_id).addClass('active');
            $('#wrapper div').removeClass('active');
           $('#con_' + tab_id).addClass('active');
        }    
    }
});
</script>

<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div id="wrapper">
    <a href="#tab1" id="tab1" class="active">По основному документу</a>
    <a href="#tab2" id="tab2">По приоритету специальностей</a>
    <a href="#tab3" id="tab3">По контактным данным, и месту проживания</a>
    <a href="#tab4" id="tab4">По оконченному учреждению</a>
    <a href="#tab5" id="tab5">По информации о родителях</a>
    <a href="#tab6" id="tab6">По наличию/отсутствию документов</a>
    <a href="#tab7" id="tab7">По документу об образовании</a>
    <a href="#tab8" id="tab8">По данным об экзаменах и ЕГЭ</a>
    <a href="#tab9" id="tab9">По изучаемому иностранному языку</a>
    <a href="#tab10" id="tab10">Дополнительно</a>
<?//    <a href="#tab11" id="tab11">КЛАДР</a> ?>

    <div id="con_tab1" class="active">

        <?php echo $form->label($model,'Тип паспорта'); ?>
        <?php echo CHtml::dropDownList('docType', $docType, CHtml::listData(DirectDocs::model()->findAll('categoryid=1'), 'id', 'name'), array('empty' => '')); ?>
        <br />

        <?php echo $form->label($model,'Серия и номер паспорта'); ?>
        <?php echo CHtml::textField('docNum', $docNum); ?>
        <br />

        <?php echo $form->label($model,'fam'); ?>
        <?php echo $form->textField($model,'fam',array('size'=>60,'maxlength'=>255)); ?>
        <br />

        <?php echo $form->label($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
        <br />

        <?php echo $form->label($model,'m_name'); ?>
        <?php echo $form->textField($model,'m_name',array('size'=>60,'maxlength'=>255)); ?>
        <br />

        <?php echo $form->label($model,'Кем выдан'); ?>
        <?php echo CHtml::textField('docSrc', $docSrc); ?>
        <br />

        <?php echo $form->label($model,'Год рождения'); ?>
        <?php echo CHtml::textField('birthYear', $birthYear); ?>
        <br />

        <?php echo $form->label($model,'Дата выдачи'); ?>
        <?php echo CHtml::textField('docDate', $docDate); ?>
        <br />

        <?php echo $form->label($model,'Где родился'); ?>
        <?php echo CHtml::textField('birthPlace', $birthPlace); ?>
        <br />

        <?php echo $form->label($model,'Гражданство'); ?>
        <?php echo $form->dropDownList($model, 'citizenship', CHtml::listData(DirectCitizenship::model()->findAll(), 'id', 'name'), array('empty' => '')); ?>
        <br />

        <?php echo $form->label($model,'Пол'); ?>
        <?php echo $form->dropDownList($model, 'sex', array(1=>'Мужской', 2=>'Женский'), array('empty' => '')); ?>
        <br />

        <?php /*echo $form->label($model,'Национальность'); ?>
        <?php echo CHtml::dropDownList('citizen', $citizen, CHtml::listData(DirectCitizenship::model()->findAll(), 'id', 'name'), array('empty' => '')); ?>
        <br />

        <?php echo $form->label($model,'Страна'); ?>
        <?php echo CHtml::dropDownList('citizen', $citizen, CHtml::listData(DirectCitizenship::model()->findAll(), 'id', 'name'), array('empty' => '')); ?>
        <?php */?>
        <br />

    </div>

    <div id="con_tab2">
        <?php echo $form->label($model,'Тип факультета'); ?>
        <?php echo CHtml::dropDownList('fakType', $fakType, array(0=>'Очно', 1=>'Заочно'), array('empty' => '')); ?>
        <br />

        <?php echo $form->label($model,'Код факультета'); ?>
        <?php echo CHtml::textField('fakCode', $fakCode); ?>
        <br />

        <?php echo $form->label($model,'Условия (бюджет/договор)'); ?>
        <?php echo CHtml::dropDownList('fakUsl', $fakUsl, array(1=>'Бюджет', 2=>'Договор'), array('empty' => '')); ?>
        <br />

    </div>

    <div id="con_tab3">
        <?php echo $form->label($model,'Мобильный телефон'); ?>
        <?php echo $form->textField($model,'phone'); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Нуждается в общежитии'); ?>
        <?php echo $form->dropDownList($model, 'need_hostel', array(1=>'Нуждается', 0=>'Не нуждается'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Адрес прописки'); ?>
        <?php echo CHtml::textField('region', '', array('placeholder'=>'Регион')); ?>
        <?php echo CHtml::hiddenField('region-id'); ?>

        <?php echo CHtml::textField('raion', '', array('placeholder'=>'Район')); ?>
        <?php echo CHtml::hiddenField('raion-id'); ?>

        <?php echo CHtml::textField('city', '', array('placeholder'=>'Город')); ?>
        <?php echo CHtml::hiddenField('city-id'); ?>

        <?php echo CHtml::textField('street', '', array('size'=>40, 'placeholder'=>'Улица')); ?>
        <?php echo $form->hiddenField($model,'kladr_street'); // TODO ?> 
        <br />

        <?php echo $form->label($model,'Адрес фактический'); ?>
        <?php echo $form->textField($model,'home_fact'); ?>
        <br />

    </div>

    <div id="con_tab4">
        <?php echo $form->label($model,'Учебное заведение'); ?>
        <?php echo $form->dropDownList($model, 'edu_type', CHtml::listData(DirectEducation::model()->findAll(), 'id', 'name'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Год окончания'); ?>
        <?php echo $form->textField($model,'edu_date'); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Средний балл'); ?>
        <?php echo $form->textField($model,'edu_grade'); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие медали'); ?>
        <?php echo $form->dropDownList($model, 'edu_spec', CHtml::listData(DirectMedal::model()->findAll(), 'id', 'name'), array('empty' => '')); ?>
        <br />

    </div>

    <div id="con_tab5">
        <?php echo $form->label($model,'ФИО'); ?>
        <?php echo CHtml::textField('parentFIO', $parentFIO); ?>
        <br />

        <?php echo $form->label($model,'Адрес проживания'); ?>
        <?php echo CHtml::textField('parentAddress', $parentAddress); ?>
        <br />

        <?php echo $form->label($model,'Телефон'); ?>
        <?php echo CHtml::textField('parentPhone', $parentPhone); ?>
        <br />

    </div>

    <div id="con_tab6">
        <table border=1><tr><td>
        <?php echo $form->label($model,'Наличие фото 3х4 4 штуки'); ?>
        <?php echo CHtml::dropDownList('dopPhoto', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие копии трудовой книжки'); ?>
        <?php echo CHtml::dropDownList('dopTK', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие медицинской справки формы 086у'); ?>
        <?php echo CHtml::dropDownList('dop86u', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />
        <br />

        <?php echo $form->label($model,'Наличие сертификата ЕГЭ'); ?>
        <?php echo CHtml::dropDownList('dopEGE', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие военного билета'); ?>
        <?php echo CHtml::dropDownList('dopVB', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие заключения психолого-медико-педагогической комиссии'); ?>
        <?php echo CHtml::dropDownList('dopPMPK', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />
        <br />
        <br />
        <br />
</td><td>

        <?php echo $form->label($model,'Наличие справки об инвалидности'); ?>
        <?php echo CHtml::dropDownList('dopInv', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие справки из органов опеки'); ?>
        <?php echo CHtml::dropDownList('dopOpek', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие диплома участника олимпиады'); ?>
        <?php echo CHtml::dropDownList('dopOlimp', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />
        <br />
        <?php echo $form->label($model,'Наличие справки о материальном доходе'); ?>
        <?php echo CHtml::dropDownList('dopDohod', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />
        <br />

        <?php echo $form->label($model,'Наличие пенсионного удостоверения'); ?>
        <?php echo CHtml::dropDownList('dopPens', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />
</td><td>

        <?php echo $form->label($model,'Наличие пенсионной книжки'); ?>
        <?php echo CHtml::dropDownList('dopPBook', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие страховое свидетельства обязательного пенсионного страхования'); ?>
        <?php echo CHtml::dropDownList('dopSNILS', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />
        <br />
        <br />
        <br />

        <?php echo $form->label($model,'Наличие сберегательной книжки'); ?>
        <?php echo CHtml::dropDownList('dopSber', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Наличие справки о пребывании в организации для детей-сирот'); ?>
        <?php echo CHtml::dropDownList('dopOrphan', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />
        <br />
        <br />

        <?php echo $form->label($model,'Наличие документа подтверждающего смену фамилии'); ?>
        <?php echo CHtml::dropDownList('dopFam', $dopPhoto, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />
</td></tr></table>
    </div>

    <div id="con_tab7">
        <?php echo $form->label($model,'Название документа'); ?>
        <?php echo CHtml::textField('eduName', $eduName); ?>
        <br />

        <?php echo $form->label($model,'Номер'); ?>
        <?php echo CHtml::textField('eduNum', $eduNum); ?>
        <br />

        <?php echo $form->label($model,'Дата выдачи'); ?>
        <?php echo CHtml::textField('eduDate', $eduDate); ?>
        <br />

    </div>

    <div id="con_tab8">
        <?php echo $form->label($model,'Русский язык'); ?>
        <br />
        <?php echo $form->label($model,'Оценка'); ?>
        <?php echo $form->textField($model, 'grade_rus'); ?>
        <br />

        <?php echo $form->label($model,'Документ'); ?>
        <?php echo $form->dropDownList($model, 'grade_rus_doctype', $model->gradedoctypes, array('empty' => '')); ?>
        <br />

        <?php echo $form->label($model,'Номер'); ?>
        <?php echo $form->textField($model, 'grade_rus_doc'); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Химия'); ?>
        <br />
        <?php echo $form->label($model,'Оценка'); ?>
        <?php echo $form->textField($model, 'grade_him'); ?>
        <br />

        <?php echo $form->label($model,'Документ'); ?>
        <?php echo $form->dropDownList($model, 'grade_him_doctype', $model->gradedoctypes, array('empty' => '')); ?>
        <br />

        <?php echo $form->label($model,'Номер'); ?>
        <?php echo $form->textField($model, 'grade_him_doc'); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Биология'); ?>
        <br />
        <?php echo $form->label($model,'Оценка'); ?>
        <?php echo $form->textField($model, 'grade_bio'); ?>
        <br />

        <?php echo $form->label($model,'Документ'); ?>
        <?php echo $form->dropDownList($model, 'grade_bio_doctype', $model->gradedoctypes, array('empty' => '')); ?>
        <br />

        <?php echo $form->label($model,'Номер'); ?>
        <?php echo $form->textField($model, 'grade_bio_doc'); ?>
        <br />

    </div>

    <div id="con_tab9">
        <?php echo $form->label($model,'Язык'); ?>
        <?php echo $form->textField($model,'id'); ?>
        <br />

        <?php echo $form->label($model,'Изучал'); ?>
        <?php echo $form->textField($model,'id'); ?>
        <br />

        <?php echo $form->label($model,'Оценка'); ?>
        <?php echo $form->textField($model,'id'); ?>
        <br />

    </div>

    <div id="con_tab10">
        <?php echo $form->label($model,'Общественная работа до поступления в ВУЗ'); ?>
        <?php echo $form->textField($model,'social_work'); ?>
        <br />
        <br />
        <br />

        <?php echo $form->label($model,'Участие в художественное самодеятельности'); ?>
        <?php echo $form->textField($model,'talent'); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Занятие спортом'); ?>
        <?php echo $form->textField($model,'sport'); ?>
        <br />

        <?php echo $form->label($model,'Вне конкурса'); ?>
        <?php echo $form->dropDownList($model, 'vk', array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />

        <?php echo $form->label($model,'Забрал документы'); ?>
        <?php echo $form->dropDownList($model, 'took', array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Дата подачи заявления'); ?>
        <?php echo $form->textField($model,'dateadd'); ?>
        <br />

        <?php echo $form->label($model,'Сданы оригиналы документов'); ?>
        <?php echo $form->dropDownList($model, 'orig', array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />
        <br />

        <?php echo $form->label($model,'Зачислен'); ?>
        <?php echo CHtml::dropDownList('invited', $invited, array(1=>'Да', 0=>'Нет'), array('empty' => '')); ?>
        <br />

    </div>
    
<?/*
    <div id="con_tab11">
        kladr
    </div>
*/?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton('Поиск'); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
