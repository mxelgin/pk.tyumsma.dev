<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Форма №9</title>
<style>
.section2 {
	width:645px;
	font-size:12pt;
	font-family:"Times New Roman","serif";
	line-height:1.0em;
        padding-left: 30pt;
}
.head_title {
	text-align:center;
	font-size:14pt;
}
.head_title1 {
	text-align:justify;
}
.head_title span{
	text-transform:uppercase;
}
.small {
	font-size:10pt;
}
p {
	text-align:justify;
	line-height:1.0em;
}
.just {
	text-align:justify;
}
.right{
	line-height:1.5em;
	text-align:right;
}
.center{
    text-align: center;
}

</style>
<script>
window.onload = function () {
    window.print() ;
};
</script>
</head>

<body>
<div class="section2">
<p class="center"><strong>Форма №9</strong></p>
  <p><strong>Фамилия</strong> <u><?php echo $model->fam; ?></u></p>
  <p class="just"><strong>Имя, отчество</strong> <u><?php echo $model->name.' '.$model->m_name; ?></u><br><br>
  <strong>Дата рождения </strong><strong>(дд.мм.гг.)</strong> <u><?php echo $model->birth_date; ?></u><br><br>
  <strong>Пол </strong><strong>(обвести)</strong>
  <?php
  if ($model->sex==1)
      echo '<u>М.</u>      Ж.';
  else
      echo 'М.      <u>Ж.</u>';
  ?>
  <br><br>
  <strong>Образование </strong><strong>(когда, где и какую  школу окончил, ср.балл аттестата )</strong>
  <u>
  <?php
  if ($model->edu_date) echo $model->edu_date.' ';
  if ($model->edu_type) echo $model->education->name.' ';
  if ($model->edu_spec) echo $model->education_spec->name.' ';
  if ($model->edu_grade) echo $model->edu_grade.' ';
  ?>
  </u>
  <br /><br />
  <!--<strong>Соцпроисхождение </strong><strong>(обвести)</strong>   из  рабочих      из служащих     другое _____________________<br><br>-->
  <strong>Какой  иностранный язык изучал</strong> <u><?php echo $model->foreign->name; ?></u> <strong>сколько лет</strong> <u><?php echo $model->foreign_exp; ?></u> <br><br>
  <strong>Оценка по ин.  языку в аттестате (дипломе)</strong> <u><?php echo $model->foreign_grade; ?></u></p>
  <p><strong>Адрес родителей,  телефон </strong> <u><?php echo $model->parents[0]->addr.' '.$model->parents[0]->phone; ?></u> </p>
  <p><strong>Общественная  работа до поступления в ВУЗ</strong> <u><?php echo $model->social_work; ?></u></p>
  <p><strong>Участие в художественной самодеятельности</strong> <u><?php echo $model->talent; ?></u></p>
  <p><strong>Занятия спортом  (вид, разряд)</strong> <u><?php echo $model->sport; ?></u></p>
</div>
</body>
</html>