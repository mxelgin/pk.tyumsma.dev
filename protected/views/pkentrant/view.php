<?php
$this->breadcrumbs=array(
	'Абитуриенты'=>array('index'),
	$model->fam.' '.$model->name.' '.$model->m_name,
);

$this->menu=array(
	array('label'=>'Добавить абитуриента', 'url'=>array('create')),
	array('label'=>'Изменить информацию', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить абитуриента', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены, что хотите удалить информацию об абитуриенте?')),
	array('label'=>'База абитуриентов', 'url'=>array('admin')),
);
?>

<h1>Информация об абитуриенте #<?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_view', array('data'=>$model, 'mdocfields'=>$mdocfields, 'dopdocs'=>$dopdocs, 'address' => $address)); ?>
