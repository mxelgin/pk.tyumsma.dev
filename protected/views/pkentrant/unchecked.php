<?php
$this->breadcrumbs=array(
	'Не проверены'=>array('admin')
);

$this->menu=array(
	array('label'=>'Добавить абитуриента', 'url'=>array('create')),
);
if (Yii::app()->user->checkAccess('secretary')){
    $this->menu[]=array('label'=>'База РЦМ', 'url'=>'/rcm');
    $this->menu[]=array('label'=>'Список на экзамен', 'url'=>array('examList'));
    $this->menu[]=array('label'=>'Список на ЕГЭ', 'url'=>array('egeList'));
    $this->menu[]=array('label'=>'Список абитуриентов по датам', 'url'=>array('dateList'));
    $this->menu[]=array('label'=>'Список абитуриентов по рейтингу', 'url'=>array('ratingList'));
}


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('config-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Не проверены</h1>

<p>
Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) для поиска нужных позиций.
</p>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'entrant-grid',
	'dataProvider'=>$model->searchUncheck(),
	'filter'=>$model,
	'columns'=>array(
                'id',
                'fam',
		'name',
                'm_name',
	),
));
?>
