<?php
$this->menu=array(
	array('label'=>'List DirectDocsCategories', 'url'=>array('index')),
	array('label'=>'Create DirectDocsCategories', 'url'=>array('create')),
	array('label'=>'Update DirectDocsCategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectDocsCategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectDocsCategories', 'url'=>array('admin')),
);
?>

<h1>Просмотр категории #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
	),
)); ?>
