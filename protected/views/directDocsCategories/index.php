<?php
$this->breadcrumbs=array(
	'Direct Docs Categories',
);

$this->menu=array(
	array('label'=>'Create DirectDocsCategories', 'url'=>array('create')),
	array('label'=>'Manage DirectDocsCategories', 'url'=>array('admin')),
);
?>

<h1>Direct Docs Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
