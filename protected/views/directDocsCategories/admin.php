<?php
$this->menu=array(
	array('label'=>'Добавить категорию', 'url'=>array('create')),
        array('label'=>'Добавить документ', 'url'=>'directDocs/create'),
        array('label'=>'Управление документами', 'url'=>'directDocs'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('direct-docs-categories-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление категориями документов</h1>

<p>
Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) для поиска нужных позиций.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'direct-docs-categories-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{update}{delete}',
                        'buttons'=>array('delete'=>array('visible'=>'($data->required)?false:true;')),
		),
	),
)); ?>
