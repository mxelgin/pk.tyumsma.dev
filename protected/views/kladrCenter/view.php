<?php
$this->breadcrumbs=array(
	'Kladr Centers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список KladrCenter', 'url'=>array('index')),
	array('label'=>'Создать KladrCenter', 'url'=>array('create')),
	array('label'=>'Изменить KladrCenter', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить KladrCenter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление KladrCenter', 'url'=>array('admin')),
);
?>

<h1>Просмотр #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
