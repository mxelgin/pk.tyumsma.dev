<?php
$this->breadcrumbs=array(
	'Kladr Centers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список KladrCenter', 'url'=>array('index')),
	array('label'=>'Создать KladrCenter', 'url'=>array('create')),
	array('label'=>'Просмотр KladrCenter', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление KladrCenter', 'url'=>array('admin')),
);
?>

<h1>Изменить KladrCenter <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>