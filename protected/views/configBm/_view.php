<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('specid')); ?>:</b>
	<?php echo CHtml::encode($data->specid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('regionid')); ?>:</b>
	<?php echo CHtml::encode($data->regionid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
	<?php echo CHtml::encode($data->value); ?>
	<br />


</div>