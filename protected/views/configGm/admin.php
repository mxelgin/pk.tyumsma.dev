<?php
$this->menu=array(
    array('label'=>'Добавить значение', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('config-gm-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление количеством мест по приоритетам предметов</h1>

<p>
    Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) для поиска нужных позиций.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'config-gm-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array('name'=>'specid', 'value'=>'$data->spec->name', 'filter'=> CHtml::listData(Departments::model()->findAll(), 'id', 'name')),
        array('name'=>'gradeid', 'value'=>'$data->grade->name', 'filter'=> CHtml::listData(DirectGrade::model()->findAll(), 'id', 'name')),
        'value',
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{update}',
		),
	),
)); ?>
