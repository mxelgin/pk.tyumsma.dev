<?php
$this->breadcrumbs=array(
	'Config Gms',
);

$this->menu=array(
	array('label'=>'Create ConfigGm', 'url'=>array('create')),
	array('label'=>'Manage ConfigGm', 'url'=>array('admin')),
);
?>

<h1>Config Gms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
