<?php
$this->breadcrumbs=array(
	'Priority Rights',
);

$this->menu=array(
	array('label'=>'Create PriorityRight', 'url'=>array('create')),
	array('label'=>'Manage PriorityRight', 'url'=>array('admin')),
);
?>

<h1>Priority Rights</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
