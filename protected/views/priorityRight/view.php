<?php
$this->breadcrumbs=array(
	'Priority Rights'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PriorityRight', 'url'=>array('index')),
	array('label'=>'Create PriorityRight', 'url'=>array('create')),
	array('label'=>'Update PriorityRight', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PriorityRight', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PriorityRight', 'url'=>array('admin')),
);
?>

<h1>View PriorityRight #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
