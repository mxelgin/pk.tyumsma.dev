<?php
$this->breadcrumbs=array(
	'Priority Rights'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PriorityRight', 'url'=>array('index')),
	array('label'=>'Manage PriorityRight', 'url'=>array('admin')),
);
?>

<h1>Create PriorityRight</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>