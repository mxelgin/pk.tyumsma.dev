<?php
$this->breadcrumbs=array(
	'Priority Rights'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PriorityRight', 'url'=>array('index')),
	array('label'=>'Create PriorityRight', 'url'=>array('create')),
	array('label'=>'View PriorityRight', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PriorityRight', 'url'=>array('admin')),
);
?>

<h1>Update PriorityRight <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>