<?php
$this->breadcrumbs=array(
	'Direct Medals'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectMedal', 'url'=>array('index')),
	array('label'=>'Create DirectMedal', 'url'=>array('create')),
	array('label'=>'Update DirectMedal', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectMedal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectMedal', 'url'=>array('admin')),
);
?>

<h1>View DirectMedal #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
