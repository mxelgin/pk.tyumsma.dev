<?php
$this->breadcrumbs=array(
	'Direct Medals',
);

$this->menu=array(
	array('label'=>'Create DirectMedal', 'url'=>array('create')),
	array('label'=>'Manage DirectMedal', 'url'=>array('admin')),
);
?>

<h1>Direct Medals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
