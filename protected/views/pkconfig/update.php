<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'config-form',
	'enableAjaxValidation'=>false,
)); ?>
<h1>Основные настройки</h1>
<?php
if ($saved) echo '<p><b>Настройки сохранены</b></p>';
?>
<?php foreach ($records as $model){ ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <?php 
                if ($model->name == 'sessionTime') {
                    echo '<label>Время сессии (в часах)</label>';
                }elseif($model->name=='onlineDataUpload'){
                    echo '<label>Онлайн выгрузка данных в ФИС ЕГЭ (1 - включена, 0 - выключена)</label>';
                }elseif($model->name=='pkNumber'){
                    echo '<label>ID приемной комиссии</label>';
                }else{
                    echo CHtml::label($model->name, 'Config_'.$model->id.'_value');
                }
            ?>
	</div>
    <div class="row">
        <?php
        if ($model->name=='description'){
                /*$this->widget('application.extensions.tinymce.ETinyMce', array(
                    'editorTemplate'=>'full',
                    'name'=>'Config['.$model->id.'][value]',
                    'language'=>'ru',
                    'htmlOptions'=>array('style'=>'width: 600px; height: 200px;')
                ));*/
                //$this->widget('application.extensions.tinymce.ETinyMce', array('name'=>'html'));
                //echo $form->textArea($model, 'value', array('cols'=>100, 'rows'=>10));
        }elseif ($model->name == 'sessionTime') {
            echo CHtml::textField('Config['.$model->id.'][value]', $model->value,array('size'=>60,'maxlength'=>255));
        }elseif($model->name=='onlineDataUpload'){
            echo CHtml::textField('Config['.$model->id.'][value]', $model->value,array('size'=>60,'maxlength'=>255));
            echo '<br /><a href="/pkexport/modulFISnew">Создать приемную компанию</a>';
        }else{
            echo CHtml::textField('Config['.$model->id.'][value]', $model->value,array('size'=>60,'maxlength'=>255));
        }
        //echo CHtml::hiddenField('Config['.$model->id.'][id]', $model->id);
        ?>

	</div>
    <br />
<?php } ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить'); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->
