<?php
$this->menu=array(
	array('label'=>'Добавить специальность', 'url'=>array('create')),
	array('label'=>'Управление специальностями', 'url'=>array('admin')),
);
?>

<h1>Изменение специальности</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>