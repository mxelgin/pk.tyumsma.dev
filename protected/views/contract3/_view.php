<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrantid')); ?>:</b>
	<?php echo CHtml::encode($data->entrantid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('person')); ?>:</b>
	<?php echo CHtml::encode($data->person); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('osn')); ?>:</b>
	<?php echo CHtml::encode($data->osn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addr')); ?>:</b>
	<?php echo CHtml::encode($data->addr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('rekv')); ?>:</b>
	<?php echo CHtml::encode($data->rekv); ?>
	<br />

	*/ ?>

</div>