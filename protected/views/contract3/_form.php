<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contract3-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->errorSummary($model); ?>
        <?php if ($entrantid) $model->entrantid=$entrantid;
        echo $form->hiddenField($model,'entrantid'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'person'); ?>
		<?php echo $form->textField($model,'person',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'person'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'osn'); ?>
		<?php echo $form->textField($model,'osn',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'osn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'addr'); ?>
		<?php echo $form->textArea($model,'addr',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rekv'); ?>
		<?php echo $form->textField($model,'rekv',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'rekv'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->