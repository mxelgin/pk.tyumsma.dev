<?php
$this->breadcrumbs=array(
	'Contract3s',
);

$this->menu=array(
	array('label'=>'Create Contract3', 'url'=>array('create')),
	array('label'=>'Manage Contract3', 'url'=>array('admin')),
);
?>

<h1>Contract3s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
