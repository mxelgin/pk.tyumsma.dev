<?php
$this->breadcrumbs=array(
	'Contract3s'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Contract3', 'url'=>array('index')),
	array('label'=>'Create Contract3', 'url'=>array('create')),
	array('label'=>'Update Contract3', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Contract3', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Contract3', 'url'=>array('admin')),
);
?>

<h1>View Contract3 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'entrantid',
		'name',
		'person',
		'osn',
		'addr',
		'phone',
		'rekv',
	),
)); ?>
