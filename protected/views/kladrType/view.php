<?php
$this->breadcrumbs=array(
	'Kladr Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список KladrType', 'url'=>array('index')),
	array('label'=>'Создать KladrType', 'url'=>array('create')),
	array('label'=>'Изменить KladrType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить KladrType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление KladrType', 'url'=>array('admin')),
);
?>

<h1>Просмотр KladrType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'fullname',
		'level',
	),
)); ?>
