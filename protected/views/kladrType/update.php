<?php
$this->breadcrumbs=array(
	'Kladr Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список KladrType', 'url'=>array('index')),
	array('label'=>'Создать KladrType', 'url'=>array('create')),
	array('label'=>'Просмотр KladrType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление KladrType', 'url'=>array('admin')),
);
?>

<h1>Изменить KladrType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>