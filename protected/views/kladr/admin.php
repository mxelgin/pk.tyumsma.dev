<?php
$this->menu=array(
//	array('label'=>'List Kladr', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kladr-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Города</h1>

<p>
Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) для поиска нужных позиций.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'kladr-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array (
            'name' => 'region.name',
            'value' => '$data->region->name',
        ),
        array (
            'name' => 'raion.name',
            'value' => '$data->raion->name',
        ),
        array (
            'name' => 'type.name',
            'value' => '$data->type->name',
        ),
		'name',
		/*
		'code',
		'index',
		'gninmb',
		'uno',
		'ocatd',
		'id_center',
		'id_type',
		'id_region',
		'id_raion',
		'id_status',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{update} {delete}',
		),
	),
)); ?>
