<?php
/*
$this->breadcrumbs=array(
	'Kladrs'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
*/

$this->menu=array(
//	array('label'=>'List Kladr', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
//	array('label'=>'View Kladr', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h1>Изменить <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
