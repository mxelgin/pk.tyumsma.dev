<?php
/*
$this->breadcrumbs=array(
	'Kladrs'=>array('index'),
	'Create',
);
*/

$this->menu=array(
//	array('label'=>'List Kladr', 'url'=>array('index')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h1>Добавить</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
