<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>13,'maxlength'=>13)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type_text'); ?>
		<?php echo $form->textField($model,'type_text',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'index'); ?>
		<?php echo $form->textField($model,'index',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gninmb'); ?>
		<?php echo $form->textField($model,'gninmb'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uno'); ?>
		<?php echo $form->textField($model,'uno'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ocatd'); ?>
		<?php echo $form->textField($model,'ocatd',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'center'); ?>
		<?php echo $form->dropDownList($model,'id_center',
            CHtml::ListData(KladrCenter::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->dropDownList($model,'id_type',
            CHtml::ListData(KladrType::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'region'); ?>
		<?php echo $form->dropDownList($model,'id_region',
            CHtml::ListData(KladrRegions::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите регион')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'raion'); ?>
		<?php echo $form->dropDownList($model,'id_raion',
            CHtml::ListData(KladrRaions::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->dropDownList($model,'id_status',
            CHtml::ListData(KladrStatus::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Поиск'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
