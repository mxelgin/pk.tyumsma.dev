<?php
$this->breadcrumbs=array(
	'Kladrs'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Kladr', 'url'=>array('index')),
	array('label'=>'Create Kladr', 'url'=>array('create')),
	array('label'=>'Update Kladr', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Kladr', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Kladr', 'url'=>array('admin')),
);
?>

<h1>View Kladr #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'type_text',
		'code',
		'index',
		'gninmb',
		'uno',
		'ocatd',
		'id_center',
		'id_type',
		'id_region',
		'id_raion',
		'id_status',
	),
)); ?>
