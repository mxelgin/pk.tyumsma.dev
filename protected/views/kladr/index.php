<?php
$this->breadcrumbs=array(
	'Kladrs',
);

$this->menu=array(
	array('label'=>'Create Kladr', 'url'=>array('create')),
	array('label'=>'Manage Kladr', 'url'=>array('admin')),
);
?>

<h1>Kladrs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
