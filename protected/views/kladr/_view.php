<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_text')); ?>:</b>
	<?php echo CHtml::encode($data->type_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
	<?php echo CHtml::encode($data->code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('index')); ?>:</b>
	<?php echo CHtml::encode($data->index); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gninmb')); ?>:</b>
	<?php echo CHtml::encode($data->gninmb); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uno')); ?>:</b>
	<?php echo CHtml::encode($data->uno); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ocatd')); ?>:</b>
	<?php echo CHtml::encode($data->ocatd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_center')); ?>:</b>
	<?php echo CHtml::encode($data->id_center); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_type')); ?>:</b>
	<?php echo CHtml::encode($data->id_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_region')); ?>:</b>
	<?php echo CHtml::encode($data->id_region); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_raion')); ?>:</b>
	<?php echo CHtml::encode($data->id_raion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_status')); ?>:</b>
	<?php echo CHtml::encode($data->id_status); ?>
	<br />

	*/ ?>

</div>