<?php
$this->breadcrumbs=array(
	'Kladr Streets'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List KladrStreet', 'url'=>array('index')),
	array('label'=>'Create KladrStreet', 'url'=>array('create')),
	array('label'=>'Update KladrStreet', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KladrStreet', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KladrStreet', 'url'=>array('admin')),
);
?>

<h1>View KladrStreet #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_parent',
		'name',
		'type_text',
		'code',
		'index',
		'gninmb',
		'uno',
		'ocatd',
		'id_type',
		'id_status',
	),
)); ?>
