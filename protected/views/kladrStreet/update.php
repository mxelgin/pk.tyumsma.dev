<?php
/*
$this->breadcrumbs=array(
	'Kladr Streets'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
*/

$this->menu=array(
//	array('label'=>'List KladrStreet', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
//	array('label'=>'View KladrStreet', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h1>Изменить улицу <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
