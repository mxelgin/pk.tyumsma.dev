<?php
$this->breadcrumbs=array(
	'Kladr Streets',
);

$this->menu=array(
	array('label'=>'Create KladrStreet', 'url'=>array('create')),
	array('label'=>'Manage KladrStreet', 'url'=>array('admin')),
);
?>

<h1>Kladr Streets</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
