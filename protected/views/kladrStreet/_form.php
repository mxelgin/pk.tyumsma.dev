<script>
/*
    $(document).ready(function() {
        code = $( "#parent-id" ).val();
        initField( 'parent', code );
    });
    */
</script>
<style>
.ui-autocomplete-loading{
    background: white url('/images/ui-anim_basic_16x16.gif') right center no-repeat;
}
</style>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kladr-street-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'parent'); ?>
		<?php //echo $form->textField($model,'id_parent',array('size'=>20,'maxlength'=>20)); ?>
		<?php /*echo $form->dropDownList($model,'id_parent',
            CHtml::ListData(Kladr::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите тип')); */ ?>
        <?php echo CHtml::textField('parent', '', array('placeholder'=>'Родитель')); ?>
        <?php echo CHtml::hiddenField('parent-id', $model->id_parent); ?>
		<?php echo $form->error($model,'id_parent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type_text'); ?>
		<?php echo $form->textField($model,'type_text',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'type_text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'index'); ?>
		<?php echo $form->textField($model,'index',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'index'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gninmb'); ?>
		<?php echo $form->textField($model,'gninmb'); ?>
		<?php echo $form->error($model,'gninmb'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uno'); ?>
		<?php echo $form->textField($model,'uno'); ?>
		<?php echo $form->error($model,'uno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ocatd'); ?>
		<?php echo $form->textField($model,'ocatd',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ocatd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php //echo $form->textField($model,'id_type'); ?>
		<?php echo $form->dropDownList($model,'id_type',
            CHtml::ListData(KladrType::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите тип')); ?>
		<?php echo $form->error($model,'id_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php //echo $form->textField($model,'id_status'); ?>
		<?php echo $form->dropDownList($model,'id_status',
            CHtml::ListData(KladrStatus::model()->findAll(), 'id', 'name'),
            array('prompt' => 'Выберите статус')); ?>
		<?php echo $form->error($model,'id_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
