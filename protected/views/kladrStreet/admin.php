<?php
$this->menu=array(
//	array('label'=>'List KladrStreet', 'url'=>array('index')),
	array('label'=>'Добавить', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kladr-street-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Улицы</h1>

<p>
Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) для поиска нужных позиций.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'kladr-street-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name' => 'parent.name',
            'value' => '$data->parent->name',
        ),
        array(
            'name' => 'type.name',
            'value' => '$data->type->name',
        ),
		'name',
        array(
            'name' => 'status.name',
            'value' => '$data->status->name',
        ),
		/*
		'code',
		'index',
		'gninmb',
		'uno',
		'ocatd',
		'id_type',
		'id_status',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{update} {delete}',
		),
	),
)); ?>
