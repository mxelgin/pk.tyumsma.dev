<?php
/*
$this->breadcrumbs=array(
	'Kladr Streets'=>array('index'),
	'Create',
);
*/

$this->menu=array(
//	array('label'=>'List KladrStreet', 'url'=>array('index')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h1>Добавить улицу</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
