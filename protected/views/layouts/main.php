<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <meta name="language" content="ru_RU" />
 
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />


        <!-- Подключаем UI CSS -->
	<link rel="stylesheet" type="text/css" href="/css/smoothness/jquery-ui-1.8.17.custom.css" />
        
        
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
                            
                array('label'=>'Абитуриенты', 'url'=>array('/pkentrant'), 'visible'=>Yii::app()->user->checkAccess('operator')),    
                array('label'=>'Справочники', 'url'=>array('/directory'), 'visible'=>Yii::app()->user->checkAccess('operator')),
				array('label'=>'Панель управления', 'url'=>array('/adminPanel'), 'visible'=>Yii::app()->user->checkAccess('secretary')),
                array('label'=>'Экспорт', 'url'=>array('/pkexport'), 'visible'=>Yii::app()->user->checkAccess('operator')),
                array('label'=>'Зачисление', 'url'=>array('/pkenrol'), 'visible'=>Yii::app()->user->checkAccess('administrator')),
                array('label'=>'Вход', 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Выход ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
        <?if (!empty ($this->submenu)):?>
        <div id="submenu">
            <?foreach($this->submenu as $title=>$url):?>
                <a href="<?=$url?>"><?=$title?></a>
            <?endforeach;?>            
        </div>
        <?endif;?>
        
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,'homeLink'=>'<a href="/">Главная</a>'
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> Тюменская медицинская академия<br/>
		Все права защищены.<br/>
		
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
