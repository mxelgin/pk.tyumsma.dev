<?php
$this->breadcrumbs=array(
	'Зачисление'=>array('admin')
);

?>

<h1>Формирование приказов о зачислении</h1>

    <ul>
        <li>Вывести полный список по алфавиту.
	        <a href="/pkenrol/allAlph" target="_blank">Сформировать</a>.
<!-- 	        <a href="/pkenrol/allAlph?type=pdf" target="_blank">Скачать PDF</a>. -->
	        <a href="/pkenrol/allAlph?type=xls" target="_blank">Скачать XLS</a>.
    	</li>
    </ul>	
    </p>
<?php

if ($model->exists('wave_first>0')){
    $first=1;
?>
    <p><b>Нулевая волна. Бюджет.</b> Приказ сформирован.
        <?php echo CHtml::button('Вывести приказ', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/print?wave=1').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
        <?php
            if (!$model->exists('wave_second>0 and enrol_type=1')){
                echo CHtml::button('Отменить приказ', array('submit'=>array('pkenrol/unenrol')));
            }
        ?>
        <li>Вывести список по алфавиту.
	        <a href="/pkenrol/printAlph?wave=1" target="_blank">Сформировать</a>.
<!-- 	        <a href="/pkenrol/printAlph?wave=1&type=pdf" target="_blank">Скачать PDF</a>. -->
	        <a href="/pkenrol/printAlph?wave=1&type=xls" target="_blank">Скачать XLS</a>.
    	</li>
    </ul>	
    </p>  
<?php
}else{
?>
    <p><b>Нулевая волна. Бюджет.</b> Вывести пофамильный перечень лиц, зачисление которых может рассматриваться Приемной комиссией: 
    <?php echo CHtml::button('Вывести список', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/preview1').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
    <ul>
        <li>Вывести список по алфавиту.
	        <a href="/pkenrol/previewAlph" target="_blank">Сформировать</a>.
<!-- 	        <a href="/pkenrol/previewAlph?type=pdf" target="_blank">Скачать PDF</a>. -->
	        <a href="/pkenrol/previewAlph?type=xls" target="_blank">Скачать XLS</a>.
    	</li>
    </ul>	
    </p>
    <p><b>Нулевая волна. Бюджет.</b> Сформировать приказ по абитуриентам поступающим на внеконкурсной основе и по РЦМ 
    <?php echo CHtml::button('Сформировать приказ', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/enrol1').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
    </p>  
<?php
}
if ($model->exists('wave_second>0 and enrol_type=1')){
        $second_b=1;
?>
    <p><b>1 волна. Бюджет.</b> Приказ сформирован. 
        <?php echo CHtml::button('Вывести приказ', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/print?wave=2').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
        <?php
            if (!$model->exists('wave_third>0 and enrol_type=1')){
                echo CHtml::button('Отменить приказ', array('submit'=>array('pkenrol/unenrol')));
            }
        ?>
    </p>
    <ul>
        <li>Вывести список по алфавиту.
	        <a href="/pkenrol/printAlph?wave=2" target="_blank">Сформировать</a>.
<!-- 	        <a href="/pkenrol/printAlph?wave=2&type=pdf" target="_blank">Скачать PDF</a>. -->
	        <a href="/pkenrol/printAlph?wave=2&type=xls" target="_blank">Скачать XLS</a>.
    	</li>
    </ul>	
    </p>	
<?php
}elseif ($first==1){
?>
    <p><b>1 волна. Бюджет.</b> Вывести пофамильный перечень лиц, зачисление которых может рассматриваться Приемной комиссией: 
    <?php echo CHtml::button('Вывести список', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/preview2').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
    </p>
    <p><b>1 волна. Бюджет.</b> Сформировать приказ по абитуриентам поступающим на бюджетной основе
    <?php echo CHtml::button('Сформировать приказ по бюджету', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/enrol2').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
    </p>  
<?php
}
if ($model->exists('wave_third>0 and enrol_type=1')){
    $third_b=1;
?>
    <p><b>2 волна. Бюджет.</b> Приказ сформирован. 
        <?php echo CHtml::button('Вывести приказ', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/print?wave=3').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
        <?php
            if (!$model->exists('wave_third>0 and enrol_type=2')){
                echo CHtml::button('Отменить приказ', array('submit'=>array('pkenrol/unenrol')));
            }
        ?>
    </p>
<?php
}elseif ($first==1 && $second_b==1){
?>
    <p><b>2 волна. Бюджет.</b> Вывести пофамильный перечень лиц, зачисление которых может рассматриваться Приемной комиссией: 
    <?php echo CHtml::button('Вывести список', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/preview3').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
    </p>
    <p><b>2 волна. Бюджет.</b> Сформировать приказ по абитуриентам поступающим на бюджетной основе
    <?php echo CHtml::button('Сформировать приказ по бюджету', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/enrol3').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
    </p>
<?php
}
if ($model->exists('wave_third>0 and enrol_type=2')){
?>
    <p><b>2 волна. Договор.</b> Приказ сформирован. 
        <?php echo CHtml::button('Вывести приказ', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/print?wave=3&type=2').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
        <?php echo CHtml::button('Отменить приказ', array('submit'=>array('pkenrol/unenrol'))); ?>
    </p>
<?php
}elseif($first==1 && $second_b==1 && $third_b==1){
?>
    <p><b>2 волна. Договор.</b> Вывести пофамильный перечень лиц, зачисление которых может рассматриваться Приемной комиссией: 
    <?php echo CHtml::button('Вывести список', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/preview33').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
    </p>
    <p><b>2 волна. Договор.</b> Сформировать приказ по абитуриентам поступающим на договорной основе
    <?php echo CHtml::button('Сформировать приказ по договору', array('onclick'=>'js: window.open("'.$this->createUrl('/pkenrol/enrol33').'", "List", "menubar=no,location=no,status=no,width=710")')); ?>
    </p>      
<?php
}
?>
