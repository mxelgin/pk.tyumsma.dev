<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Зачисление</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
        border-collapse: collapse; 
}
table td.center{
    text-align: center;
}
table td{
    border: 1px solid black;
    padding: 4px;
}
table th{
    border: 1px solid black;
    padding: 4px;
    text-align: center;
}
@media print{
    .break{
        page-break-after: always;
    }
}
</style>
</head>

<body>
    <?php
    //echo '<p align="center"><b><a href="/pkenrol/enrol2?yes=1">Сформировать приказ</a></b></p>';
    echo '
    <p align="center">Показать по специальностям:</p>
    <table border=0 align="center"><tr><td><form action="/pkenrol/enrol33" method="post">
    <select name="spec">
    <option value="0">Все</option>
    ';
    foreach ($specs as $spec){
        echo '<option value="'.$spec->id.'"';
        if ($showspec>0 && $showspec==$spec->id) echo ' selected';
        echo '>'.$spec->name.'</option>';
    }
    echo '</select> <input type="submit" name="submit" value="Показать"></form></td></tr></table>';

    //foreach ($list_b as $key=>$value) echo '<p>'.$key.' '.count($value).'</p>';
    echo '<form action="/pkenrol/enrol33" method="post">';
    foreach ($specs as $spec){
        if ($showspec>0 && $showspec!=$spec->id) continue;
        echo '<p align="center"><b>'.$spec->name.'</b></p>';
        if ($prikaz[$spec->id]){
            echo '<p align="center"><b>Зачислены</b></p>';
            echo '<table align="center" class="bordertable">
                    <tr>
                        <th>ФИО</th>
                        <th>Рейтинг</th>
                    </tr>';
            foreach ($prikaz[$spec->id] as $ent){
                echo '<tr>';
                echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
                echo '<td class="center">';
                    echo $ent->rating;
                echo '</td>';
                echo '</tr>';
            }
            echo '</table><br /> <br />';
        }
        if ($list_d[$spec->id]){
            $i=1;
            echo '<table align="center" class="bordertable">
                    <tr>
                        <th>#</th>
                        <th>Зачислить</th>
                        <th>ФИО</th>
                        <th>Вид конкурса</th>
                        <th>Рейтинг</th>
                        <th>Химия</th>
                        <th>Биология</th>
                        <th>Русский</th>
                        <th>Вес достижений</th>
                        <th>Средний балл</th>
                        <th>Наличие медицинский противопоказаний</th>
                        <th>Сданы документы</th>
                    </tr>';
            foreach ($list_d[$spec->id] as $ent){
                echo '<tr>';
                echo '<td class="center">'.$i++.'</td>';
                echo '<td class="center"><input type="checkbox" value="'.$spec->id.'" name="entrant['.$ent->id.']" checked></td>';
                echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
                echo '<td class="center">'.$ent->vktypes[$ent->vk].'</td>';
                echo '<td class="center">';
                if ($ent->grade_rus==0 || $ent->grade_him==0 || $ent->grade_bio==0)
                    echo 'Экзамен/ЕГЭ';
                else
                    echo $ent->rating;
                echo '</td>';
                echo '<td class="center">'.$ent->grade_him.'</td>';
                echo '<td class="center">'.$ent->grade_bio.'</td>';
                echo '<td class="center">'.$ent->grade_rus.'</td>';
                echo '<td class="center">'.$ent->achv.'</td>';
                echo '<td class="center">'.$ent->edu_grade.'</td>';
                echo '<td class="center">'.$ent->medblock.'</td>';
                echo '<td class="center">'.$ent->origtypes[$ent->orig].'</td>';
                echo '</tr>';
            }
        }

        echo '</table><br />';
    }
    echo '<p align=center><input type="submit" value="Зачислить"></p>';
    echo '</form>';
    ?>
</body>
</html>