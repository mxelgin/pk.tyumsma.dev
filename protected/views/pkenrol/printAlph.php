<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Приказ о зачислении</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
        border-collapse: collapse; 
}
table td.center{
    text-align: center;
}
table td{
    border: 1px solid black;
    padding: 4px;
}
table th{
    border: 1px solid black;
    padding: 4px;
    text-align: center;
}
@media print{
    .break{
        page-break-after: always;
    }
}
</style>
</head>
<?php 
   	$arr[] = array();
    foreach ($specs as $spec){
		foreach ($entrants[$spec->id] as $ent){
			if (!empty($enrol[$ent->id])) continue;
			if (!empty($ent)) $arr[] = $ent;
			$enrol[$ent->id]=$spec->id;
		}
   		foreach ($entrantsrcm[$spec->id] as $entr){
			foreach ($entr as $ent){
				if (!empty($enrol[$ent->id])) continue;
				if (!empty($ent)) $arr[] = $ent;
				$enrol[$ent->id]=$spec->id;
			}
		}
	}
foreach ($arr as $key => $row) {
    $fam[$key]  = $row['fam'];
    $name[$key] = $row['name'];
    $m_name[$key] = $row['m_name'];
}
array_multisort($fam, SORT_ASC, $name, SORT_ASC, $m_name, SORT_ASC, $arr);
?>	
<?php
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mpdf('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
}
?>
<body>
<div class="section">
<span class="right">Приложение №1</span>
<div class="clear"></div>
<p class="title">Результаты приемной кампании <?php echo date('Y') ?> года<br></p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="2%" rowspan="2" valign="bottom">№ п/п</td>
    <td width="12%" rowspan="2" valign="bottom">Фамилия, имя, отчество абитуриента</td>
    <td width="6%" rowspan="2" valign="bottom">Факультет<br /></td>
    <td width="12%" rowspan="2" valign="bottom">Специальность\направление подготовки<br /></td>
    <td width="6%" rowspan="2" valign="bottom">Форма обучения (очная\заочная)</td>
    <td width="6%" rowspan="2" valign="bottom">Место (бюджетное/ с полным возмещением затрат)</td>
    <td colspan="3" align="center">Баллы по предметам, которые учитывались при зачислении на данную специальность /направление подготовки*</td>
    <td width="5%" rowspan="2" valign="bottom">Общая сумма баллов абитуриента</td>
    <td width="12%" rowspan="2">Основание для зачисления (только на бюджетные места )***</td>
    <td width="3%" rowspan="2">Название олимпиады  (только для зачисленных по коду 3)</td>
  </tr>
  <tr>
    <td width="5%">Химия <br /></td>
    <td width="5%">Биология</td>
    <td width="5%">Русский язык</td>
  </tr>
<?php 
	$i = 1;
  	foreach ($arr as $key=>$ent){
		if (!empty($ent)){
?>		
	<tr>
		<td><?php echo $i++; ?></td>
		<td><?php echo "{$ent->fam} {$ent->name} {$ent->m_name}"; ?></td>
		<td><?php 
			switch ($ent->specs[0]->spec->id){
				case 1: echo $ent->faculty[1]; break;
				case 2: echo $ent->faculty[2]; break;		
				case 3: echo $ent->faculty[3]; break;	
				case 4: 		
				case 6: echo $ent->faculty[4]; break;		
				case 5: 		
				case 7: echo $ent->faculty[5]; break;		
			}
			?></td>
		<td><?php echo $ent->specs[0]->spec->specname; ?></td>
		<td><?php 
			switch ($ent->specs[0]->spec->id){
				case 1: 
				case 2: 
				case 3: 
				case 4: 
				case 5: echo "очная"; break;
				case 6:
				case 7: echo "заочная"; break;
			}; ?></td>
		<td><?php 
    		if ($ent->specs[0]->type_b) echo 'бюджет</br>';
        	//if ($ent->specs[0]->type_d) echo 'договор</br>'; ?></td>	
        <td><?php echo $ent->grade_rus; ?></td>
        <td><?php echo $ent->grade_him; ?></td>
        <td><?php echo $ent->grade_bio; ?></td>
        <td><?php echo $ent->rating; ?></td>
        <td><?php 
        	switch($ent->vk){
        		case 0: if (!empty($ent->rcm)) echo "4-({$ent->rcm->region->name})"; break;
        		case 1: if (!empty($ent->rcm)) echo "4-({$ent->rcm->region->name}), "; echo "5"; break;
        		default: echo "&nbsp;";
        }; ?></td>
        <td>&nbsp;</td>
	</tr>
<?php 
	}}
?>
</table>
<br/>
* Баллы согласно свидетельству о ЕГЭ
<br/><br/>
В случае, если абитуриент сдавал не ЕГЭ, а экзамен вуза по соответствующему предмету, указаны баллы за экзамен и в скобках написано (Э)
<br/><br/>
***указывает код:
<br/><br/>
1-зачисление по общему конкурсу
<br/>
2-без экзаменов (льготы для победителей и призеров всероссийской олимпиады школьников)
<br/>
3-без экзаменов (льготы для победителей и призеров других олимпиад школьников)
<br/>
4-целевой набор
<br/>
5-прием вне конкурса
<br/><br/>
</div>
</body>
</html>
<?php
if ($_GET['type'] == 'pdf') {
    $mpdf->WriteHTML(ob_get_contents());
    ob_end_clean();
    $mpdf->Output('mpdf.pdf', 'I');
}
?>