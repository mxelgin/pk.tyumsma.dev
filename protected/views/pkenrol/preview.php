<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Рейтинг</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
        border-collapse: collapse; 
}
table td.center{
    text-align: center;
}
table td{
    border: 1px solid black;
    padding: 4px;
}
table th{
    border: 1px solid black;
    padding: 4px;
    text-align: center;
}
@media print{
    .break{
        page-break-after: always;
    }
}
</style>
</head>

<body>
    <?php
    $enrol=array();
    $regionrcm=ConfigRcm::model()->findAll();
    foreach ($specs as $spec){
        $i=1;
        echo '<p align="center"><b>'.$spec->name.'</b></p>';
        echo '<table align="center" class="bordertable">
                <tr>
                    <th>#</th>
                    <th>ФИО</th>
                    <th>Вид конкурса</th>
                    <th>Рейтинг</th>
                    <th>Сданы документы</th>
                </tr>';
        foreach ($entrantsvkb[$spec->id] as $ent){
            if (!empty($enrol[$ent->id])) continue;
            echo '<tr>';
            echo '<td class="center">'.$i++.'</td>';
            echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
            echo '<td class="center">'.$ent->vktypes[$ent->vk].'</td>';
            echo '<td class="center">';
            if ($ent->grade_rus==0 || $ent->grade_him==0 || $ent->grade_bio==0)
                echo 'Экзамен/ЕГЭ';
            else
                echo $ent->rating;
            echo '</td>';
            echo '<td class="center">'.$ent->origtypes[$ent->orig].'</td>';
            echo '</tr>';
            $enrol[$ent->id]=$spec->id;
        }

        foreach ($entrantsrcm[$spec->id] as $k=>$entr){
            if ($entr){
                echo '<tr><td colspan=5>РЦМ - '.$regname[$k].'</td></tr>';
                foreach ($entr as $ent){
                    if (!empty($enrol[$ent->id])) continue;
                    echo '<tr>';
                    echo '<td class="center">'.$i++.'</td>';
                    echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
                    echo '<td class="center">'.$ent->vktypes[$ent->vk].' (РЦМ)</td>';
                    echo '<td class="center">';
                    if ($ent->grade_rus==0 || $ent->grade_him==0 || $ent->grade_bio==0)
                        echo 'Экзамен/ЕГЭ';
                    else
                        echo $ent->rating;
                    echo '</td>';
                    echo '<td class="center">'.$ent->origtypes[$ent->orig].'</td>';
                    echo '</tr>';
                    $enrol[$ent->id]=$spec->id;
                } 
            }
        }
        if ($entrantsvkd[$spec->id]){
            echo '<tr><td colspan=5>Договорные места</td></tr>';
            foreach ($entrantsvkd[$spec->id] as $ent){
                if (!empty($enrol[$ent->id])) continue;
                echo '<tr>';
                echo '<td class="center">'.$i++.'</td>';
                echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
                echo '<td class="center">'.$ent->vktypes[$ent->vk].'</td>';
                echo '<td class="center">';
                if ($ent->grade_rus==0 || $ent->grade_him==0 || $ent->grade_bio==0)
                    echo 'Экзамен/ЕГЭ';
                else
                    echo $ent->rating;
                echo '</td>';
                echo '<td class="center">'.$ent->origtypes[$ent->orig].'</td>';
                echo '</tr>';
                $enrol[$ent->id]=$spec->id;
            }
        }
        echo '</table><br />';
    }
    ?>
</body>
</html>