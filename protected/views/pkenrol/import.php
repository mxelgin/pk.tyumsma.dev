<?php
$this->breadcrumbs=array(
	'Зачисление'=>array('admin')
);

?>

<h1>Импорт данные о приказе</h1>
<?
if (count($entrants)>0){
    $i=1;
    echo '<table align="center" class="bordertable">
            <tr>
                <th>#</th>
                <th>ФИО</th>
                <th>Вид конкурса</th>
                <th>Рейтинг</th>
                <th>Сданы документы</th>
            </tr>';
    foreach ($entrants as $ent){
        echo '<tr>';
        echo '<td class="center">'.$i++.'</td>';
        echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
        echo '<td class="center">'.$ent->vktypes[$ent->vk].'</td>';
        echo '<td class="center">';
        if ($ent->grade_rus==0 || $ent->grade_him==0 || $ent->grade_bio==0)
            echo 'Экзамен/ЕГЭ';
        else
            echo $ent->rating;
        echo '</td>';
        echo '<td class="center">'.$ent->origtypes[$ent->orig].'</td>';
        echo '</tr>';
    }
    echo '</table><br />';
}
?>
<form action="/pkenrol/import" method="post">
    <select name="wave">
        <option>1</option>
        <option>2</option>
        <option>3</option>
    </select>
    <br />
    <select name="enrol_type">
        <option value="1">Бюджет</option>
        <option value="2">Договор</option>
    </select>
    <select name="spec">
        <?php
        foreach ($specs as $spec) echo '<option value="'.$spec->id.'">'.$spec->name.'</option>';
        ?>
    </select>
    <br />
    <textarea name="importdata"></textarea>
    <br />
    <input type="submit" value="Загнать">
</form>