<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Зачисление</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
        border-collapse: collapse; 
}
table td.center{
    text-align: center;
}
table td{
    border: 1px solid black;
    padding: 4px;
}
table th{
    border: 1px solid black;
    padding: 4px;
    text-align: center;
}
@media print{
    .break{
        page-break-after: always;
    }
}
</style>
</head>

<body>
    <?php
    if ($exist || $yes) {
        echo '<h2>Приказ</h2>';
    }elseif($fail) {
        echo '<h2>Абитуриенты на границе приема</h2>';
    }else{
        echo '<p align="center"><b><a href="/pkenrol/enrol1?yes=1">Сформировать приказ</a></b></p>';
    }
    $enrol=array();
    $regionrcm=ConfigRcm::model()->findAll();
    //echo '<pre>';
    foreach ($specs as $spec){
        $ok = false;
        foreach ($entrantsrcm[$spec->id] as $s) 
            if (!empty($s)) {
                $ok = true;
                break;
            }
        if (empty($entrantsvkb[$spec->id]) && !$ok) continue;
        $i=1;

        echo '<p align="center"><b>'.$spec->name.'</b></p>';
        if ($fail)
            echo '<table align="center" class="bordertable">
                    <tr>
                        <th>#</th>
                        <th>Номер дела</th>
                        <th>ФИО</th>
                        <th>Ведущий балл</th>
                        <th>Средний балл</th>
                        <th>Гражданство</th>
                    </tr>';
        else
            echo '<table align="center" class="bordertable">
                    <tr>
                        <th>#</th>
                        <th>ФИО</th>
                        <th>Вид конкурса</th>
                        <th>Рейтинг</th>
                        <th>Химия</th>
                        <th>Биология</th>
                        <th>Русский</th>
                        <th>Преимщественное право</th>
                        <th>Средний балл</th>
                        <th>Наличие медицинский противопоказаний</th>
                        <th>Сданы документы</th>
                    </tr>';
        if (!empty($entrantsvkb[$spec->id])) {
            foreach ($entrantsvkb[$spec->id] as $ent){
                if (!empty($enrol[$ent->id])) continue;
                echo '<tr>';
                echo '<td class="center">'.$i++.'</td>';
                echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
                echo '<td class="center">'.$ent->vktypes[$ent->vk].'</td>';
                echo '<td class="center">';
                if ($ent->grade_rus==0 || $ent->grade_him==0 || $ent->grade_bio==0)
                    echo 'Экзамен/ЕГЭ';
                else
                    echo $ent->rating;
                echo '</td>';
                echo '<td class="center">&nbsp;</td>';
                echo '<td class="center">&nbsp;</td>';
                echo '<td class="center">&nbsp;</td>';
                echo '<td class="center">&nbsp;</td>';
                echo '<td class="center">&nbsp;</td>';
                echo '<td class="center">&nbsp;</td>';
                echo '<td class="center">'.$ent->origtypes[$ent->orig].'</td>';
                echo '</tr>';
                $enrol[$ent->id]=$spec->id;
            }
        }

        foreach ($entrantsrcm[$spec->id] as $k=>$entr){
            if ($entr){
                if ($fail)
                    echo '<tr><td colspan=6>РЦМ - '.$regname[$k].'</td></tr>';
                else
                    echo '<tr><td colspan=5>РЦМ - '.$regname[$k].'</td></tr>';
                foreach ($entr as $ent){
                    if (!empty($enrol[$ent->id])) continue;
                    if ($fail) {
                        echo '<tr>';
                        echo '<td class="center">'.$i++.'</td>';
                        echo '<td>'.$ent->id.'</td>';
                        echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
                        if ($ent->id % 2)
                            echo '<td class="center">'.$ent->grade_him.' </td>';
                        else
                            echo '<td class="center">'.$ent->grade_bio.' </td>';
                        echo '<td class="center">'.$ent->edu_grade.' </td>';
                        echo '<td class="center">'.$ent->citizen->name.' </td>';
                        echo '</tr>';
                    } else {
                        echo '<tr>';
                        echo '<td class="center">'.$i++.'</td>';
                        echo '<td>'.$ent->fam.' '.$ent->name.' '.$ent->m_name.'</td>';
                        echo '<td class="center">'.$ent->vktypes[$ent->vk].' (РЦМ)</td>';
                        echo '<td class="center">';
                        if ($ent->grade_rus==0 || $ent->grade_him==0 || $ent->grade_bio==0)
                            echo 'Экзамен/ЕГЭ';
                        else
                            echo $ent->rating;
                        echo '</td>';
                        echo '<td class="center">'.$ent->grade_him.'</td>';
                        echo '<td class="center">'.$ent->grade_bio.'</td>';
                        echo '<td class="center">'.$ent->grade_rus.'</td>';
                        echo '<td class="center">'.$ent->achv.'</td>';
                        echo '<td class="center">'.$ent->edu_grade.'</td>';
                        echo '<td class="center">'.$ent->medblock.'</td>';
                        echo '<td class="center">'.$ent->origtypes[$ent->orig].'</td>';
                        echo '</tr>';
                        $enrol[$ent->id]=$spec->id;
                    }
                } 
            }
            foreach ($entrantsrcm_double[$spec->id][$k] as $j=>$entr_double){
                echo '<tr style="color:red">';
                echo '<td class="center">'.($i-1).'</td>';
                echo '<td>'.$entr_double->fam.' '.$entr_double->name.' '.$entr_double->m_name.'</td>';
                echo '<td class="center">'.$entr_double->vktypes[$entr_double->vk].' (РЦМ)</td>';
                echo '<td class="center">';
                if ($entr_double->grade_rus==0 || $entr_double->grade_him==0 || $entr_double->grade_bio==0)
                    echo 'Экзамен/ЕГЭ';
                else
                    echo $entr_double->rating;
                echo '</td>';
                echo '<td class="center">'.$entr_double->grade_him.'</td>';
                echo '<td class="center">'.$entr_double->grade_bio.'</td>';
                echo '<td class="center">'.$entr_double->grade_rus.'</td>';
                echo '<td class="center">'.$entr_double->achv.'</td>';
                echo '<td class="center">'.$entr_double->edu_grade.'</td>';
                echo '<td class="center">'.$entr_double->medblock.'</td>';
                echo '<td class="center">'.$entr_double->origtypes[$entr_double->orig].'</td>';
                echo '</tr>';
            }

        }
        if ($yes && empty($exist)){
            foreach ($enrol as $entrant=>$specid){
                Entrant::model()->updateByPk($entrant, array('wave_first'=>$specid, 'enrol_type'=>1));
                $rcm=Rcm::model()->find(array('condition'=>"entrantid=".$entrant));
                //$rcm->enrol=1;
                //$rcm->save();
                Rcm::model()->updateByPk($rcm->id, array('enrol'=>1));
            }
            /*$rcm=Rcm::model()->findAll(array('condition'=>"entrantid>0"));
            foreach ($rcm as $r){
                $entrant=Entrant::model()->findByPk((int)$r->entrantid);
                if ($entrant->wave_first==0){
                    Rcm::model()->updateByPk($r->id, array('no_enrol'=>1));
                }
            }*/
        }
        echo '</table><br />';
    }
    ?>
</body>
</html>
