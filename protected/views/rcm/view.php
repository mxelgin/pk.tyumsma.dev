<?php
$this->breadcrumbs=array(
	'Rcms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Rcm', 'url'=>array('index')),
	array('label'=>'Create Rcm', 'url'=>array('create')),
	array('label'=>'Update Rcm', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Rcm', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Rcm', 'url'=>array('admin')),
);
?>

<h1>View Rcm #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fio',
		'pass_number',
		'useradd',
	),
)); ?>
