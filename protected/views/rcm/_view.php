<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fio')); ?>:</b>
	<?php echo CHtml::encode($data->fio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pass_number')); ?>:</b>
	<?php echo CHtml::encode($data->pass_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('useradd')); ?>:</b>
	<?php echo CHtml::encode($data->useradd); ?>
	<br />


</div>