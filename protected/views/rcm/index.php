<?php
$this->breadcrumbs=array(
	'Rcms',
);

$this->menu=array(
	array('label'=>'Create Rcm', 'url'=>array('create')),
	array('label'=>'Manage Rcm', 'url'=>array('admin')),
);
?>

<h1>Rcms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
