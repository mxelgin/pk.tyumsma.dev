<?php
$this->breadcrumbs=array(
	'Регионально-целевые места',
);

$this->menu=array(
	array('label'=>'Добавить РЦМ', 'url'=>array('create')),
        array('label'=>'База абитуриентов', 'url'=>'/pkentrant'),
);
?>

<h1>Статистика РЦМ</h1>

<table align="center" class="bordertable tablecentertd">
    <tr>
        <th>Субъект РФ</th>
        <?php
        $departments=Departments::model()->findAll(array('order'=>'id ASC'));
        foreach ($departments as $dp){
            echo '<th colspan=2>'.$dp->name.'</th>';
        }
        ?>
    </tr>
    <?php
    $regions=DirectRcmRegions::model()->findAll(array('order'=>'id ASC'));
    foreach ($regions as $rg){
        echo '<tr>';
        echo '<td>'.$rg->name.'</td>';
        foreach ($departments as $dp){
            $exist=ConfigRcm::model()->find('regionid='.$rg->id.' and specid='.$dp->id);
            $podano=Rcm::model()->count('regionid='.$rg->id.' and entrantid>0 and specid='.$dp->id);
            echo '<td>'.$exist->value.'</td><td>'.$podano.'</td>';
        }
        echo '</tr>';
    }    
    ?>
</table>
