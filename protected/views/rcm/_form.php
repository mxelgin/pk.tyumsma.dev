<script>
    $(document).ready(function($) {
        inputformat('');
    });
</script>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'rcm-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fio'); ?>
		<?php echo $form->textField($model,'fio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pass_number'); ?>
		<?php echo $form->textField($model,'pass_number',array('size'=>20,'maxlength'=>20, 'alt'=>'9999-999999', 'placeholder'=>'____-______')); ?>
		<?php echo $form->error($model,'pass_number'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'regionid'); ?>
		<?php echo $form->dropDownList($model, 'regionid', CHtml::listData(DirectRcmRegions::model()->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'regionid'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'specid'); ?>
		<?php echo $form->dropDownList($model, 'specid', CHtml::listData(Departments::model()->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'specid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->