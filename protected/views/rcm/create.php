<?php
$this->breadcrumbs=array(
    'Регионально-целевые места',
);

$this->menu=array(
	array('label'=>'База РЦМ', 'url'=>array('admin')),
	array('label'=>'База абитуриентов', 'url'=>'/pkentrant'),
);
?>

<h1>Добавление РЦМ</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>