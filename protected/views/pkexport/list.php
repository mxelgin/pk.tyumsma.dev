<?php 
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-type: application/csv; charset=windows-1251');
header('Content-Disposition: attachment; filename="export.csv"');
header('Cache-Control: max-age=0');

$docfields= new Docs();

$s = '"Код ассистента";"Номер дела";"";"1 выбранный бюджетный факультет";"2 выбранный бюджетный факультет";"3 выбранный бюджетный факультет";"1 выбранный ВНЕбюджетный факультет";"2 выбранный ВНЕбюджетный факультет";"3 выбранный ВНЕбюджетный факультет";"ФИО абитуриента";"РЦ,ВК и обяз. фин";"Копия и фин";"пол";"Город/село";"Год рождения";"Гражданство";"Паспорт";"№ сертификата/сертификатов ЕГЭ";"Что закончил";"Год окончания учебного заведения";"Отличия, медали, красные дипломы";"Оценка Аттестата - Химия";"Оценка Аттестата - Русский";"Оценка Аттестата - Биология";"Оценка Аттестата - Средний Балл";"Копия/забор";"Рабочий стаж";"Служба в армии";"Год демобилизации";"Название города/села";"Название области/района";"Адрес поступающего";"Нужно ли общежитие";"Отец. ФИО";"Отец Адрес";"Отец - место работы";"Мать. ФИО";"Мать Адрес";"Мать - место работы";"Телефон для связи";"ЕГ Химия";"ЕГ Русский";"ЕГ Биология";"Сумма"';
$s = iconv("UTF-8", "CP1251", $s);
echo $s;



foreach ($data as $dt){ 
    if($dt->sex==1) $sex = "муж"; else $sex = "жен";
    
    $mdocfield=$docfields->find('entrantid='.$dt->id.' and docid=21 and fieldid=13')->value;
    if (empty($mdocfield)) $mdocfield=$docfields->find('entrantid='.$dt->id.' and docid=47 and fieldid=34')->value;
    if (empty($mdocfield)) $mdocfield=$docfields->find('entrantid='.$dt->id.' and docid=26 and fieldid=18')->value;
    //echo '<pre>'; print_r($mdocfield); die;
    /*$i=0;
    foreach ($mdocfields as $field){
        $mandat[$i] =  $field->value; $i++;
    }*/
    $mdocf=$docfields->findAll('entrantid='.$dt->id.' and docid=29 order by fieldid ASC');
    $i=0;
    foreach ($mdocf as $field){
        $mdat[$i] =  $field->value; $i++;
    }
    if($dt->home_type==1) $ht = "Город"; else $ht = "Село";
    $doco=Docs::model()->find('entrantid='.$dt->id.' and docid=29 and fieldid=20');
    if($dt->education->id == 1) $ed_type = "Дневная";
    if($dt->education->id == 2) $ed_type = "ПрофТехУчилище";
    if($dt->education->id == 3) $ed_type = "Второе высшее";
    if($dt->education->id == 4) $ed_type = "Медучилище";
    $i=0;
    foreach ($dt->specs as $spec){
       if ($spec->type_b) {
           if($spec->spec->id == 1) $spec_i[$i] = "Лечфак";
           if($spec->spec->id == 2) $spec_i[$i] = "Стомфак";
           if($spec->spec->id == 3) $spec_i[$i] = "Педфак";
           if($spec->spec->id == 4) $spec_i[$i] = "Фармфак";
           if($spec->spec->id == 5) $spec_i[$i] = "ВСО";
           if($spec->spec->id == 6) $spec_i[$i] = "ФЗао";
           if($spec->spec->id == 7) $spec_i[$i] = "Взао";
       }
       if ($spec->type_d){
            if($spec->spec->id == 1) $spec_k[$i] = "леч";
           if($spec->spec->id == 2) $spec_k[$i] = "стом";
           if($spec->spec->id == 3) $spec_k[$i] = "пед";
           if($spec->spec->id == 4) $spec_k[$i] = "фарм";
           if($spec->spec->id == 5) $spec_k[$i] = "ВСО";
           if($spec->spec->id == 6) $spec_k[$i] = "фзао";
           if($spec->spec->id == 7) $spec_k[$i] = "взао";
       }
       $i++;
    }
    $i=0;
    foreach ($dt->parents as $parent){
        if ($dt['orphan'.($i+1)])
            switch ($dt['orphan'.($i+1).'_type']) {
                case 1: $par_fio[$i]= 'Умер'; break;
                case 2: $par_fio[$i]= 'Не проживает с семьей'; break;
                case 3: $par_fio[$i]= 'Опекун ' . $parent->fio; break;
            }
        else
            $par_fio[$i]= $parent->fio;

        $par_addr[$i] = $parent->addr;
        $par_work[$i] = $parent->work;
        $par_phone[$i] = $parent->phone;
        $i++;
    }
    $copy = "";
    $grade_sum = $dt->grade_him + $dt->grade_rus + $dt->grade_bio;
    if($dt->need_hostel==1) $nh = "да"; else $nh = "нет";
    if ($dt->rcm->id) { 
            //$rcm = "Регион-Целевой";
            if($dt->rcm->regionid == 1)$rcm ="Курган";
            if($dt->rcm->regionid == 2)$rcm ="Регион.Целевой";
            if($dt->rcm->regionid == 3)$rcm ="ФСИН";
            if($dt->rcm->regionid == 4)$rcm ="Ханты";
            if($dt->rcm->regionid == 5)$rcm ="Ямал";
            if($dt->rcm->regionid == 6)$rcm ="ФМБА";
    }
     
    if ($dt->vk) {$vk = "Вне конкурса";}
    if ($dt->vk && $dt->rcm->id)  {$vk = "Вне конкурса/РЦМ";}
    if ($dt->orig!=1){$copy = "Копия"; $took = "Копия";}
    //if($copy != "") $slash = "/";
    if ($dt->took!=0) { $copy= "забор"; $took = "забор";}
    if ($dt->service==1) $army = "да"; else $army = "нет";
    if($dt->service_year) $army_year = substr($dt->service_year, 2);
    $address = $model->getFullAddressArray($dt->id);
    if ($address['region'] == 'Тюменская обл') $raion = $address['raion'];

    $s = "
    ".$dt->checkedbyuser.";\"".$dt->id."\";\"\";\"".$spec_i[0]."\";\"".$spec_i[1]."\";\"".$spec_i[2]."\";\"".$spec_k[0]."\";\"".$spec_k[1]."\";\"".$spec_k[2]."\";\"".$dt->fam." ".$dt->name." ".$dt->m_name."\";\"".$rcm.$vk."\";\"".$copy."\";\"".$sex."\";\"".$ht."\";\"".date('y', strtotime($dt->birth_date))."\";\"".$dt->citizen->name."\";\"".$mdocfield."\";\"".$doco->value."\";\"".$ed_type."\";\"".date('y', strtotime($dt->edu_date))."\";\"".$dt->education_spec->name."\";\"\";\"\";\"\";\"".str_replace (".", ",", $dt->edu_grade)."\";\"".$took."\";\"\";\"".$army."\";\"".$army_year."\";\"".$address['city']."\";\"".$address['region'] . $raion ."\";\"".$address['street'].", ".$dt->home_build." - ".$dt->home_kv."\";\"".$nh."\";\"".$par_fio[0]."\";\"".$par_addr[0]."\";\"".$par_work[0]."\";\"".$par_fio[1]."\";\"".$par_addr[1]."\";\"".$par_work[1]."\";\"".$par_phone[1]."\";\"".$dt->grade_him."\";\"".$dt->grade_rus."\";\"".$dt->grade_bio."\";\"".$grade_sum."\""; 
    $s = iconv("UTF-8", "CP1251", $s);
    echo $s;
    
    unset($spec_i);  unset($spec_k); unset($rcm); unset($vk); unset($ht); unset($army_year);
    unset($par_fio); unset($par_addr); unset($par_work); unset($par_phone);
    unset($mandat); unset($mdat); unset($copy); unset($took); unset($ed_type);
}
        
?>
