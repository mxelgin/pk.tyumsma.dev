<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Приказ о зачислении</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
        border-collapse: collapse;
}
table td.center{
    text-align: center;
}
table td{
    border: 1px solid black;
    padding: 4px;
}
table th{
    border: 1px solid black;
    padding: 4px;
    text-align: center;
}
@media print{
    .break{
        page-break-after: always;
    }
}
</style>
</head>
<?php
	$arr[] = array();

	$specs = $wave1[specs];
   	$entrants = $wave1[entrants];
   	$entrantsrcm = $wave1[entrantsrcm];
   	$regname = $wave1[regname];

    if ($specs != null) {
        foreach ($specs as $spec){
	   		foreach ($entrants[$spec->id] as $ent){
	   			if (!empty($enrol[$ent->id])) continue;
	   			if (!empty($ent)) $arr[] = $ent;
	   			$enrol[$ent->id]=$spec->id;
	   		}
   	   		foreach ($entrantsrcm[$spec->id] as $entr){
	   			foreach ($entr as $ent){
	   				if (!empty($enrol[$ent->id])) continue;
	   				if (!empty($ent)) $arr[] = $ent;
	   				$enrol[$ent->id]=$spec->id;
	   			}
	   		}
	   	}
    }

	$specs = $wave2[specs];
   	$entrants = $wave2[entrants];
   	$entrantsrcm = $wave2[entrantsrcm];
   	$regname = $wave2[regname];

    if ($specs != null) {
        foreach ($specs as $spec){
	   		foreach ($entrants[$spec->id] as $ent){
	   			if (!empty($enrol[$ent->id])) continue;
	   			if (!empty($ent)) $arr[] = $ent;
	   			$enrol[$ent->id]=$spec->id;
	   		}
   	   		foreach ($entrantsrcm[$spec->id] as $entr){
	   			foreach ($entr as $ent){
	   				if (!empty($enrol[$ent->id])) continue;
	   				if (!empty($ent)) $arr[] = $ent;
	   				$enrol[$ent->id]=$spec->id;
	   			}
	   		}
	   	}
    }

    $specs = $wave3[specs];
    $entrants = $wave3[entrants];
    $entrantsrcm = $wave3[entrantsrcm];
    $regname = $wave3[regname];

    if ($specs != null) {
        foreach ($specs as $spec){
	           foreach ($entrants[$spec->id] as $ent){
	               if (!empty($enrol[$ent->id])) continue;
	               if (!empty($ent)) $arr[] = $ent;
	               $enrol[$ent->id]=$spec->id;
	           }
   	        foreach ($entrantsrcm[$spec->id] as $entr){
	               foreach ($entr as $ent){
	                   if (!empty($enrol[$ent->id])) continue;
	                   if (!empty($ent)) $arr[] = $ent;
	                   $enrol[$ent->id]=$spec->id;
	               }
	           }
	       }
    }

    $specs = $wave4[specs];
    $entrants = $wave4[entrants];
    $entrantsrcm = $wave4[entrantsrcm];
    $regname = $wave4[regname];

    if ($specs != null) {
        foreach ($specs as $spec){
	           foreach ($entrants[$spec->id] as $ent){
	               if (!empty($enrol[$ent->id])) continue;
	               if (!empty($ent)) $arr[] = $ent;
	               $enrol[$ent->id]=$spec->id;
	           }
   	        foreach ($entrantsrcm[$spec->id] as $entr){
	               foreach ($entr as $ent){
	                   if (!empty($enrol[$ent->id])) continue;
	                   if (!empty($ent)) $arr[] = $ent;
	                   $enrol[$ent->id]=$spec->id;
	               }
	           }
	       }
    }

	foreach ($arr as $key => $row) {
	    $fam[$key]  = $row['fam'];
	    $name[$key] = $row['name'];
	    $m_name[$key] = $row['m_name'];
	}
	array_multisort($fam, SORT_ASC, $name, SORT_ASC, $m_name, SORT_ASC, $arr);
?>
<?php
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mpdf('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
}
?>
<body>
<div class="section">
<span class="right">Приложение №1</span>
<div class="clear"></div>
<p class="title">Форма для Газпрома <?php echo date('Y') ?> года<br></p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="100%" valign="bottom">Insert</td>
  </tr>
<?php
	$i = 1;
  	foreach ($arr as $key=>$ent){
		if (!empty($ent)){
//            Education::model()->
?>
    <tr>
        <td>INSERT INTO [Деканат].[dbo].[Все_Студенты]
            (
            [Фамилия]
            ,[Имя]
            ,[Отчество]
            ,[Примечание]
            ,[Год_Поступления]
            )
            VALUES
            (
            <?php echo "'{$ent->fam}'" ?>
            ,
            <?php echo "'{$ent->name}'" ?>
            ,
            <?php echo "'{$ent->m_name}'" ?>
            ,
            <?php echo "'экспорт'" ?>
            ,
            <?php echo "'2014'" ?> <!-- <- Вставить год постпления -->
            );
        </td>
    </tr>
<!--	<tr>-->
<!--		<td>--><?php //echo $i++; ?><!--</td>-->
<!--		<td>--><?php //echo "{$ent->fam}"; ?><!--</td>-->
<!--		<td>--><?php //echo "{$ent->name}"; ?><!--</td>-->
<!--		<td>--><?php //echo "{$ent->m_name}"; ?><!--</td>-->
<!--		<td>--><?php //echo "{$ent->birth_date}"; ?><!--</td>-->
<!--		<td>--><?php //echo "{$ent->birth_city->name}"; ?><!--</td>-->
<!--		<td>--><?php //echo "{$ent->mdoc->name}"; ?><!--</td>-->
<!--        <td>--><?php
//            foreach ($ent->docs as $key=>$value){
//                if (($value->docid == 21) AND ($value->fieldid == 13)){
//                    echo $value->value;
//                }
//                if (($value->docid == 47) AND ($value->fieldid == 34)){
//                    echo $value->value;
//                }
//            }
//            ?>
<!--        </td>-->
<!--        <td>--><?php //foreach ($ent->docs as $key=>$value){
//                if (($value->docid == 21) AND ($value->fieldid == 14)){
//                    echo $value->value;
//                }
//                if (($value->docid == 47) AND ($value->fieldid == 38)){
//                    echo $value->value;
//                }
//
//            }
//            ?>
<!--        </td>-->
<!--        <td>--><?php //foreach ($ent->docs as $key=>$value){
//                if (($value->docid == 21) AND ($value->fieldid == 15)){
//                    echo $value->value;
//                }
//                if (($value->docid == 47) AND ($value->fieldid == 37)){
//                    echo $value->value;
//                }
//
//            }
//            ?>
<!--        </td>-->
<!--		<td>--><?php //echo "{$ent->home_fact}"; ?><!--</td>-->
<!--		<td>--><?php //echo "1"; ?><!--</td>-->
<!--        <td>--><?php //echo $ent->specs[0]->spec->specname; ?><!--</td>-->
<!--        <td>--><?php
//            switch ($ent->specs[0]->spec->id){
//                case 1: echo $ent->faculty[1]; break;
//                case 2: echo $ent->faculty[2]; break;
//                case 3: echo $ent->faculty[3]; break;
//                case 4:
//                case 6: echo $ent->faculty[4]; break;
//                case 5:
//                case 7: echo $ent->faculty[5]; break;
//            }
//            ?>
<!--        </td>-->
<!--    </tr>-->
<?php
	}}
?>
</table>

<table width="100%" border="1" cellspacing="0" cellpadding="4">
    <tr>
      <td width="100%" valign="bottom">Update</td>
    </tr>
    <?php
    $i = 1;
    foreach ($arr as $key=>$ent){
    if (!empty($ent)){
    //            Education::model()->
    ?>
    <tr>
        <td>UPDATE [Деканат].[dbo].[Все_Студенты]
            SET
            <!--            ,[Статус]-->
            [Код_Группы] =
            <?php
                switch ($ent->department->id){
                    case 1: echo "'10068'"; break;//Лечебный
                    case 2: echo "'10072'"; break;//Стоматологический
                    case 3: echo "'10069'"; break;//Педиатрический
                    case 4:
                    case 6: echo "'10070'"; break;//Фармацевтический
                    case 5:
                    case 7: echo "'10071'"; break;//ВСО                }
                }

//                switch ($ent->specs[0]->spec->id){
//              case 1: echo "'10052'"; break;//Лечебный
//              case 2: echo "'10056'"; break;//Стоматологический
//              case 3: echo "'10053'"; break;//Педиатрический
//              case 4:
//              case 6: echo "'10054'"; break;//Фармацевтический
//              case 5:
//              case 7: echo "'10055'"; break;//ВСО
//            }
            ?>
            <!--            ,[Номер_Зачетной_Книжки]-->
            ,[Год_Поступления] = <?php echo "'2014'" ?> <!-- <- Вставить год постпления -->
            <!--            ,[Статус] = --><?php //echo "'1'" ?>
            ,[Пол] =
            <?php
            switch ($ent->sex){
              case 1: echo "'Муж'"; break;
              case 2: echo "'Жен'"; break;
            }
            ?>
            ,[Основания] =
            <?php
               if (isset($ent->rcm)){
                   echo "'ЦН'";
               } else echo "'ОО'";
            ?>
            ,[Изучаемый_Язык] = <?php echo "'{$ent->foreign->name}'" ?>
            <!--            ,[Долг]-->
            <!--            ,[Национальность]-->
            ,[Дата_Рождения] = <?php echo "'{$ent->birth_date}'"; ?>
            ,[Уч_Заведение] = <?php echo "'{$ent->education->name}'"; ?>
            <!--            ,[Где_Находится_УЗ]-->
            ,[Год_Окончания_УЗ] = <?php echo date("Y", strtotime($ent->edu_date)); ?>
            ,[Отличник_УЗ] = <?php echo "'{$ent->education_spec->name}'"; ?>
            <!--            ,[Пособие]-->
            <!--            ,[Номер_Мед_Полиса]-->
            <!--            ,[Серия_Мед_Полиса]-->
            ,[Страна_ПП] = <?php echo "'Россия'" ?>
            ,[Регион_ПП] = <?php echo "'{$ent->street->parent->region->name}'"; ?>
            ,[Город_ПП] = <?php echo "'{$ent->street->parent->name}'"; ?>
            <!--            ,[Индекс_ПП]-->
            ,[Улица_ПП] = <?php echo "'{$ent->street->name}'"; ?>
            ,[Дом_Кв_ПП] = <?php echo "'{$ent->home_build},{$ent->home_kv}'"; ?>
            ,[Телефон_ПП] = <?php echo "'{$ent->phone}'"; ?>
            <!--            ,[E_Mail]-->
            <!--            ,[Общежитие]-->
            <!--            ,[Номер_Общежития]-->
            <!--            ,[Номер_Комнаты]-->
            ,[Кем_Выдан] =
            <?php foreach ($ent->docs as $key=>$value){
                    if (($value->docid == $ent->maindoc) AND ($value->fieldid == 15)){
                        echo "'{$value->value}'";
                    }
                    if (($value->docid == $ent->maindoc) AND ($value->fieldid == 37)){
                        echo "'{$value->value}'";
                    }

                }
            ?>
            ,[Номер_Паспорта] =
            <?php
                foreach ($ent->docs as $key=>$value){
                    if (($value->docid == $ent->maindoc) AND ($value->fieldid == 13)){
                        echo "'{$value->value}'";
                    }
                    if (($value->docid == $ent->maindoc) AND ($value->fieldid == 34)){
                        echo "'{$value->value}'";
                    }
                }
            ?>
            ,[Дата_Выдачи] =
            <?php foreach ($ent->docs as $key=>$value){
                if (($value->docid == $ent->maindoc) AND ($value->fieldid == 14)){
                    echo "'{$value->value}'";
                }
                if (($value->docid == $ent->maindoc) AND ($value->fieldid == 38)){
                    echo "'{$value->value}'";
                }

            }
            ?>

            <!--            ,[Наличие_Матери]-->
            <!--            ,[Наличие_Отца]-->
            <!--            ,[ФИО_Матери]-->
            <!--            ,[Место_Раб_Матери]-->
            <!--            ,[Должность_Матери]-->
            <!--            ,[ФИО_Отца]-->
            <!--            ,[Место_Раб_Отца]-->
            <!--            ,[Должность_Отца]-->
            <!--            ,[Число_Братьев_И_Сестер]-->
            <!--            ,[Страна_Родители]-->
            <!--            ,[Регион_Родители]-->
            <!--            ,[Город_Родители]-->
            <!--            ,[Индекс_Родители]-->
            <!--            ,[Улица_Родители]-->
            <!--            ,[Дом_Кв_Родители]-->
            <!--            ,[Телефон_Родители]-->
            <!--            ,[Опекунство]-->
            <!--            ,[Состоит_В_Браке]-->
            <!--            ,[Число_Детей]-->
            <!--            ,[Примечание]-->
            <!--            ,[НомерЛД]-->
            <!--            ,[Льготы]-->
            ,[Тип_Удостоверения] =
            <?php
            echo $pf = "";
            foreach ($ent->docs as $key=>$value){
                if (($value->docid == 21) AND ($value->fieldid == 13)){
                    $pf =  "Паспорт РФ";
                };
            }
            echo "'{$pf}'";
            ?>
            ,[Серия_Аттестата] =
            <?php
            echo $s_att = "";
            foreach ($ent->docs as $key=>$value){
                if (($value->docid == 29) AND ($value->fieldid == 20)){
                    $s_att = "{$value->value}";
                };
            }
            echo "'{$s_att}'";
            ?>
            ,[Номер_Аттестата] =
            <?php
            echo $n_att = "";
            foreach ($ent->docs as $key=>$value){
                if (($value->docid == 29) AND ($value->fieldid == 20)){
                    $n_att = "{$value->value}";
                };
            }
            echo "'{$n_att}'";
            ?>
            ,[Тип_Образ_Документа] =
            <?php
            echo $o_doc = "";
            foreach ($ent->docs as $key=>$value){
                if (($value->docid == 29) AND ($value->fieldid == 19)){
                    $o_doc = "{$value->value}";
                };
            }
            echo "'{$o_doc}'";
            ?>
            <!--            ,[ДатаПодачиДокументов] =-->
            <!--            ,[ПереводДокументов]-->
            <!--            ,[НеМестный]-->
            ,[ГодРождения] = <?php echo date("Y", strtotime($ent->birth_date)); ?>
            <!--            ,[ФормаОбучения]-->
            <!--            ,[НаучнаяСпециальность]-->
            <!--            ,[ДатаЗачисления]-->
            <!--            ,[НомерПриказаОЗачислении]-->
            <!--            ,[ДатаОкончания]-->
            <!--            ,[МестоРаботы]-->
            <!--            ,[ПрикрепКафедра]-->
            <!--            ,[НаучныйРуководитель]-->
            <!--            ,[КандФилософия]-->
            <!--            ,[КандИностранный]-->
            <!--            ,[КандСпециальность]-->
            <!--            ,[Аттестация1]-->
            <!--            ,[Аттестация2]-->
            <!--            ,[Аттестация3]-->
            <!--            ,[Аттестация4]-->
            <!--            ,[ДополнОтпуск]-->
            <!--            ,[Командировки]-->
            <!--            ,[КодОригинала]-->
            <!--            ,[Село]-->
            <!--            ,[КвалификацияСтудента]-->
            <!--            ,[Стаж]-->
            <!--            ,[Должность]-->
            <!--            ,[ПродлениеСессии]-->
            <!--            ,[УчебныйПлан]-->
            <!--            ,[Кандидатские]-->
            <!--            ,[Соискатель]-->
            <!--            ,[ДопКвалификация]-->
            <!--            ,[ИНН]-->
            <!--            ,[НомерСоцСтрахования]-->
            <!--            ,[Номер_договора]-->
            <!--            ,[Фото]-->
            <!--            ,[Пропуск]-->
            <!--            ,[ЧитательскийБилет]-->
            <!--            ,[СокращенноеОбучение]-->
            <!--            ,[ПродленоОбучение]-->
            <!--            ,[ПриписноеСвидетельство]-->
            ,[ВоенныйБилет] = <?php echo "'{$ent->servicetypes[$ent->service]}'"; ?>
            <!--            ,[ГруппаУчета]-->
            <!--            ,[КатегорияУчета]-->
            <!--            ,[Состав]-->
            <!--            ,[ВоинскоеЗвание]-->
            <!--            ,[ВоеннСпециальность]-->
            <!--            ,[ВоеннГодность]-->
            <!--            ,[Военкомат]-->
            <!--            ,[ВоеннСпецУчет]-->
            ,[Гражданство] = <?php echo "'{$ent->citizen->name}'"; ?>
            ,[Даты_Выдачи_Аттестата] =
            <?php
            echo $d_doc = "";
            foreach ($ent->docs as $key=>$value){
                if (($value->docid == 29) AND ($value->fieldid == 21)){
                    $d_doc = "{$value->value}";
                };
            }
            echo "'{$d_doc}'";
            ?>
        <!--            ,[БывшаяФамилия]-->
            <!--            ,[Иностранец]-->
            <!--            ,[Район_ПП]-->
            <!--            ,[АкадемСправка]-->
            <!--            ,[НомерДиплома]-->
            <!--            ,[ДатаРешения]-->
            <!--            ,[ДатаВыдачи]-->
            <!--            ,[РегистрационныйНомер]-->
            <!--            ,[ТемаДиплома]-->
            <!--            ,[Заблокирован]-->
            <!--            ,[Второе_Образование]-->
            ,[УслОбучения] =
            <?php
                if (isset($ent->rcm)){
                    echo "'2'";
                } else echo "'1'";
            ?>
            <!--            ,[Слушатель]-->
            <!--            ,[СоответсвиеВПО]-->
            ,[Тип_УЗ] = <?php echo "'{$ent->education->name}'"; ?>
            <!--            ,[Район_Родители]-->
            <!--            ,[СелоПоПрописке]-->
            <!--            ,[КодОбщежития]-->
            <!--            ,[ДатаАкадОтпуск]-->
            <!--            ,[ВоеннаяКафедра]-->
            <!--            ,[БывшееИмя]-->
            <!--            ,[КодФакультета]-->
            <!--            ,[АкадемСтипендия]-->
            <!--            ,[АкадемСтипендияС]-->
            <!--            ,[АкадемСтипендияПо]-->
            <!--            ,[СоцСтипендия]-->
            <!--            ,[СоцСтипендияС]-->
            <!--            ,[СоцСтипендияПо]-->
            <!--            ,[ПродленаСессия]-->
            <!--            ,[ДатаНачСессии]-->
            <!--            ,[ДатаКонСессии]-->
            <!--            ,[ОЗО]-->
            <!--            ,[ЛицСчетБанк]-->
            ,[МестоРождения] = <?php echo "'{$ent->birth_city->name}'"; ?>

            <!--            ,[Город]-->
            <!--            ,[ВоенЗвание]-->
            <!--            ,[ВоенОрган]-->
            <!--            ,[ВоенМестоСлужбы]-->
            <!--            ,[ВоенДолжность]-->
            <!--            ,[Логин]-->
            <!--            ,[Пароль]-->
            WHERE
            [Фамилия] =   <?php echo "'{$ent->fam}'" ?>
            AND
            [Имя] =  <?php echo "'{$ent->name}'" ?>
            AND
            [Отчество] = <?php echo "'{$ent->m_name}'" ?>
            AND
            [Год_Поступления] = <?php echo "'2014'" ?> <!-- <- Вставить год постпления -->
            AND
            [Примечание] = 'экспорт';
        </td>
  </tr>
  <?php
  }}
  ?>
</table>
<!--<br/>-->
<!--* Виды документов<br>-->
<!--Паспорт РФ<br>-->
<!--Иностранный паспорт<br>-->
<!--Вид на жительство<br>-->
<!--Миграционная карта<br>-->
<!--<br/><br/>-->
</div>
</body>
</html>
<?php
if ($_GET['type'] == 'pdf') {
    $mpdf->WriteHTML(ob_get_contents());
    ob_end_clean();
    $mpdf->Output('mpdf.pdf', 'I');
}
?>