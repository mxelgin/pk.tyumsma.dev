<script>
$(document).ready(function($) {
    selected=$('#Entrant_maindoc').attr('value');
    $.ajax({'url':'<?php echo $this->createUrl('pkentrant/getDocFields'); ?>','data':'id='+selected+'&entrantid=<?php echo $model->id; ?>&data=<?php echo CJSON::encode($docsdata); ?>&docserrors=<?php echo CJSON::encode($entrantDocErrors->getErrors()); ?>','cache':false,'beforeSend':function(){$("#maindocinput").addClass("loading");},'complete':function(){$("#maindocinput").removeClass("loading");},'success':function(html){$("#maindocinput").html(html);  inputformat("#maindocinput");}});

    inputformat('');
    
    $('#Entrant_birth_place_id, #Entrant_home_city').autocomplete({
            source: function( request, response ) {
                $.ajax({
                        url: "<?php echo $this->createUrl('pkentrant/getKladrItems'); ?>",
                        dataType: "jsonp",
                        data: {
                                query: request.term
                        },
                        success: function( data ) {
                            response( $.map( data.city, function( item ) {
                                    return {label: item.name}
                            }));
                        }
                });
            },
            minLength: 1,
            delay: 200,
            open: function() {
                    $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
            },
            close: function() {
                    $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
            }
        });

    $('#Entrant_citizenship').change(function(){
        var val = $('#Entrant_citizenship').val();
        if (val!=1 && val!=4 && val!=7 && val!=5 && val!=6){
            $('input[type=radio]').each(function(){
                id = $(this).attr('id').split('_');
                if (id[0]=='type' && id[2]=='0')
                    $(this).attr('disabled', 'disabled');
            });            
        }else{
            $('input[type=radio]').each(function(){
                id = $(this).attr('id').split('_');
                if (id[0]=='type' && id[2]=='0')
                    $(this).removeAttr('disabled');
            });
        }
    });

    $("#Entrant_home_region").combobox();
    $('#gradeduble').click(function(){
        var gradedoc=$('#Entrant_grade_rus_doc').val();
        var gradedoctype=$('#Entrant_grade_rus_doctype').val();
        $('#Entrant_grade_him_doc').val(gradedoc);
        $('#Entrant_grade_bio_doc').val(gradedoc);        
        $('#Entrant_grade_him_doctype').val(gradedoctype);
        $('#Entrant_grade_bio_doctype').val(gradedoctype);
        return false;
    });
    
    <?php 
    $errors=$model->getErrors();
    foreach ($errors as $field=>$error){
        echo '$("#Entrant_'.$field.'").addClass("error");'."\n";
    }
    $errors=$entrantSpecErrors->getErrors();
    foreach ($errors as $field=>$error){
        echo '$("#'.$field.'").addClass("error");'."\n";
    }
    if ($model->sex==2)
        echo '$(".military").addClass("hide");';
    ?>
    
});

</script>

<div id="entrantform">
    <?php
    if ($model->id) $action='/pkentrant/update/id/'.$model->id;
    else $action='/pkentrant/create';
    ?>
    <form id="entrant-form" action="<?php echo $action; ?>" method="post">
    <div class="errorSummary">
        <?php echo CHtml::errorSummary(array($model,$entrantDocErrors,$entrantSpecErrors)); ?>
    </div>
    <div class="leftblock">
        <!--<div class="title"><h1>Добавление нового абитуриента</h1></div>-->

	<div class="maindoc block">
            <?php 
            $options['ajax']=array('update'=>'#maindocinput', 'url' => $this->createUrl('pkentrant/getDocFields'),'data' => 'js:\'id=\'+this.value+\'&entrantid='.$model->id.'&data='.CJSON::encode($docsdata).'&docserrors='.CJSON::encode($entrantDocErrors->getErrors()).'\'', 'cache' => false, 'beforeSend'=>'function(){$("#maindocinput").addClass("loading");}', 'complete'=>'function(){$("#maindocinput").removeClass("loading");}', 'success'=>'function(html){$("#maindocinput").html(html); inputformat("#maindocinput");}'); 
            if (empty($model->maindoc)) $model->maindoc=21;
            ?>
            <div class="blockinfo"><h2><label for="Entrant_maindoc">Основной документ</label></h2> <?php echo CHtml::dropDownList('Entrant[maindoc]',$model->maindoc, CHtml::listData($docs->findAll('categoryid=1'), 'id', 'name'), $options); ?></div>
            <div class="row">
                <label for="fam" class="required">ФИО <span class="required">*</span></label>
                <?php echo CHtml::textField('Entrant[fam]', $model->fam,array('size'=>60, 'maxlength'=>250, 'placeholder'=>'Фамилия', 'class'=>'s-input')); ?>
		<?php echo CHtml::textField('Entrant[name]', $model->name,array('size'=>60,'maxlength'=>250, 'placeholder'=>'Имя', 'class'=>'s-input')); ?>
		<?php echo CHtml::textField('Entrant[m_name]', $model->m_name,array('size'=>60,'maxlength'=>250, 'placeholder'=>'Отчество', 'class'=>'s-input')); ?>
            </div>
            <div id="maindocinput">
            </div>
            <div class="row">
                <label for="Entrant_birth_date" class="required">Родился <span class="required">*</span></label>
                <?php echo CHtml::textField('Entrant[birth_date]', $model->birth_date,array('placeholder'=>'__.__.____', 'class'=>'datepicker')); ?>
                <?php echo CHtml::textField('Entrant[birth_place_id]', $model->birth_city->name,array('size'=>60,'placeholder'=>'Населенный пункт')); ?>
            </div>
            <div class="row">
                <label for="Entrant_citizenship" class="required">Гражданство <span class="required">*</span></label>
                <?php echo CHtml::dropDownList('Entrant[citizenship]', 1, CHtml::listData($citizenship, 'id', 'name')); ?>
                <label for="Entrant_sex" class="required">Пол <span class="required">*</span></label>
                <?php echo CHtml::radioButtonList('Entrant[sex]', $model->sex, array(1=>'Муж', 2=>'Жен'), array('separator'=>'', 'onclick'=>'js:hidemilitary(this);')); ?>
            </div>
	</div>
	<div class="spec block">
            <div class="blockinfo"><h2>Приоритет специальностей</h2></div>
            <div class="row">
                <?php
                if (count($model->specs)>0){
                    $i=1;
                    foreach ($model->specs as $key=>$spec){
                        if (empty($spec->specid)) continue;
                        $specid=$spec->id;
                        if (empty($specid)) $specid=$key;
                ?>
                        <div class="spec-item">
                            <span class="spec-itemnumber rownumber"><?php echo $i++; ?>.</span>
                            <label for="specid_<?php echo $specid; ?>" class="required">Факультет <span class="required">*</span></label>
                            <?php echo CHtml::dropDownList('specid['.$specid.']', $spec->specid, CHtml::listData($department, 'id', 'name')); ?>
                            <label for="type_<?php echo $spec->id; ?>" class="required">Условия <span class="required">*</span></label>
                            <?php echo CHtml::radioButtonList('type['.$specid.']', $spec->type, array(1=>'Бюджет', 2=>'Договор'), array('separator'=>'')); ?>
                        </div>
                        <?php 
                        if ($spec->id)
                        echo CHtml::hiddenField('spec_change['.$spec->id.']', 1);
                        ?>
                <?php
                    }
                    $deleteclass='';
                }else{
                    $deleteclass='hidden';
                ?>
                        <div class="spec-item">
                            <span class="spec-itemnumber rownumber">1.</span>
                            <label for="specid_<?php echo $key; ?>" class="required">Факультет <span class="required">*</span></label>
                            <?php echo CHtml::dropDownList('specid[0]', '', CHtml::listData($department, 'id', 'name')); ?>
                            <label for="type_<?php echo $key; ?>" class="required">Условия <span class="required">*</span></label>
                            <?php echo CHtml::radioButtonList('type[0]', '', array(1=>'Бюджет', 2=>'Договор'), array('separator'=>'')); ?>
                        </div>
                <?php } ?>
                <div class="more"></div>
                <div class="row">
                    <?php echo CHtml::button('Добавить' ,array('onclick'=>'js:dublicaterow(".spec-item", ".more"); $("#removebuttom").removeClass("hide")')); ?>
                    <?php echo CHtml::button('Удалить' ,array('onclick'=>'js:removerow(".spec-item");', 'id'=>'removebuttom', 'class'=>$deleteclass)); ?>
                </div>
            </div>
	</div>
	<div class="home block">
            <div class="blockinfo"><h2>Контактные данные, проживание</h2></div>
            <div class="row">
                <label for="Entrant_phone">Мобильный телефон</label>
                <?php echo CHtml::textField('Entrant[phone]', $model->phone,array('placeholder'=>'8 123 456 7890', 'alt'=>'8 999 999 9999')); ?>
                <label for="Entrant_need_hostel">Нуждается в общежитии</label>
                <?php echo CHtml::checkBox('Entrant[need_hostel]', $model->need_hostel); ?>
            </div>
            <div class="row">
                <label for="Entrant_home_city">Адрес по прописке (регистрация)</label>
            </div>
            <div class="row">
                <?php echo CHtml::dropDownList('Entrant[home_type]', $model->home_type, $model->hometypes); ?>
                <?php echo CHtml::textField('Entrant[home_city]', $model->home_city_db->name, array('size'=>30,'placeholder'=>'Населенный пункт')); ?>
                <?php 
                $array=CHtml::listData(DirectRegions::model()->findAll(), 'id', 'name');
                array_unshift($array, '');
                echo CHtml::dropDownList('Entrant[home_region]', $model->home_region, $array);
                ?>
            </div>
            <div class="row">
                <?php echo CHtml::textField('Entrant[home_street]', $model->home_street,array('size'=>43,'placeholder'=>'Улица', 'class'=>'home-st')); ?>
                <?php echo CHtml::textField('Entrant[home_build]', $model->home_build,array('size'=>10,'placeholder'=>'Дом/Корпус', 'class'=>'home-st')); ?>
                <?php echo CHtml::textField('Entrant[home_kv]', $model->home_kv,array('size'=>10,'placeholder'=>'Квартира', 'class'=>'home-st')); ?>
            </div>
            <div class="row">
                <label for="Entrant_home_fact">Фактическое проживание</label>
            </div>
            <div class="row">
                <?php echo CHtml::textField('Entrant[home_fact]', $model->home_fact, array('size'=>80,'placeholder'=>'Индекс, Город, улица, дом/корпус, квартира')); ?>
            </div>
	</div>
	<div class="education block">
            <div class="blockinfo"><h2>Окончил учебное заведение</h2></div>
            <div class="edu-row">
                <div class="row">
                    <div class="leftinputlimit">
                    <?php
                    $options=array('size'=>60,'placeholder'=>'Населенный пункт');
                    if ($model->edu->cityid){
                        $options['class']='hide';
                        $text=$model->edu->city->name.' <a href="#" class="change-educity">Изменить</a>';
                        $disable='';
                    }
                    else{
                        $text='';
                        $disable='disabled';
                    }
                    echo CHtml::textField('Edu[cityid]', $model->edu->cityid, $options); ?>
                    <span id="educity-text" class="city-text"><?php echo $text; ?></span>
                    </div>
                    <?php echo CHtml::textField('Edu[name]', $model->edu->name,array('size'=>40,'placeholder'=>'Название учебного заведения', 'disabled'=>$disable)); ?>
                </div>
                <div class="row"><label for="Edu_exp_date">Окончил</label>
                    <?php echo CHtml::textField('Edu[exp_date]', $model->edu->exp_date,array('placeholder'=>'__.__.____', 'class'=>'datepicker')); ?>
                    <label for="Edu_grade">Средний балл</label>
                    <?php echo CHtml::textField('Edu[grade]', $model->edu->grade,array('size'=>2,'placeholder'=>'_._', 'alt'=>'5.9')); ?>
                </div>
                <?php echo CHtml::hiddenField('Edu[id]', $model->edu->id); ?>
            </div>
	</div>
	<div class="military block">
            <div class="blockinfo">
                <h2>Отношение к военной службе</h2>
                <?php echo CHtml::radioButtonList('Entrant[service]', $model->service, array(1=>'Военнообязанный', 2=>'Невоеннообязанный'), array('separator'=>' ')); ?>
            </div>
	</div>
	<div class="parents block">
            <div class="blockinfo"><h2>Родители</h2></div>
            <?php
            if (count($model->parents)>0){
                foreach ($model->parents as $key=>$parent){
            ?>
                <div class="parants-row">
                    <div class="row">
                        <label for="parent_fio">ФИО</label>
                        <?php echo CHtml::textField('parent_fio['.$parent->id.']', $parent['fio'],array('placeholder'=>'Фамилия Имя Отчество', 'size'=>'60')); ?>
                    </div>
                    <div class="row">
                        <label for="uparent_addr">Адрес родителя</label>
                        <?php echo CHtml::textField('parent_addr['.$parent->id.']', $parent['addr'],array('placeholder'=>'Город, улица, дом/корпус, квартира', 'size'=>'60')); ?>
                    </div>
                    <div class="row">
                        <?php echo CHtml::textField('parent_work['.$parent->id.']', $parent['work'],array('placeholder'=>'Место работы и должность', 'size'=>'50')); ?>
                        <?php echo CHtml::textField('parent_phone['.$parent->id.']', $parent['phone'],array('placeholder'=>'8 123 456 7890', 'alt'=>'8 999 999 9999')); ?>
                    </div>
                    <?php echo CHtml::hiddenField('parent_change['.$parent->id.']', 1) ?>
                </div>
            <?php
                }
            }else{
            ?>
                <div class="parants-row">
                    <div class="row">
                        <label for="parent_fio_0">ФИО</label>
                        <?php echo CHtml::textField('parent_fio[0]', '',array('placeholder'=>'Фамилия Имя Отчество', 'size'=>'60')); ?>
                    </div>
                    <div class="row">
                        <label for="parent_addr_0">Адрес родителя</label>
                        <?php echo CHtml::textField('parent_addr[0]', '',array('placeholder'=>'Город, улица, дом/корпус, квартира', 'size'=>'60')); ?>
                    </div>
                    <div class="row">
                        <?php echo CHtml::textField('parent_work[0]', '',array('placeholder'=>'Место работы и должность', 'size'=>'50')); ?>
                        <?php echo CHtml::textField('parent_phone[0]', '',array('placeholder'=>'8 123 456 7890', 'alt'=>'8 999 999 9999')); ?>
                    </div>
                </div>
                <div class="parants-row">
                    <div class="row">
                        <label for="parent_fio_1">ФИО</label>
                        <?php echo CHtml::textField('parent_fio[1]', '',array('placeholder'=>'Фамилия Имя Отчество', 'size'=>'60')); ?>
                    </div>
                    <div class="row">
                        <label for="parent_addr_1">Адрес родителя</label>
                        <?php echo CHtml::textField('parent_addr[1]', '',array('placeholder'=>'Город, улица, дом/корпус, квартира', 'size'=>'60')); ?>
                    </div>
                    <div class="row">
                        <?php echo CHtml::textField('parent_work[1]', '',array('placeholder'=>'Место работы и должность', 'size'=>'50')); ?>
                        <?php echo CHtml::textField('parent_phone[1]', '',array('placeholder'=>'8 123 456 7890', 'alt'=>'8 999 999 9999')); ?>
                    </div>
                </div>
            <?php
            }
            ?>
	</div>
    </div>
    <div class="rightblock">
	<div class="dopdocs block">
            <div class="blockinfo"><h2>Информация о сданных документах</h2></div>
            <?php
            $dopdocs=$docs->findAll(array('condition'=>'categoryid=2', 'order'=>'action'));
            foreach ($dopdocs as $docinfo){ 
                if ($docinfo->action=='input'){
                    $docfields= new DirectDocsFields;
                    $fields=$docfields->findAll(array('condition'=>'docid='.$docinfo->id, 'order'=>'id ASC'));
                    $id=$docinfo->id;
                    echo '<div class="row"><b>'.$docinfo->name.'</b></div>';
                    include Yii::app()->getBasePath().'/views/pkentrant/doc-with-fields.php';
                }else{
                    include Yii::app()->getBasePath().'/views/pkentrant/doc-without-fields.php';
                }
            }
            ?>
	</div>
	<div class="grades block">
            <div class="blockinfo"><h2>Данные об экзаменах и ЕГЭ</h2></div>
            <div class="row"><b>Русский язык</b></div>
            <div class="row">
                <label for="Entrant_grade_rus">Оценка</label>
                <?php echo CHtml::textField('Entrant[grade_rus]', $model->grade_rus,array('alt'=>'999', 'size'=>'2')); ?>
                <label for="Entrant_grade_rus_doctype">Документ</label> <?php echo CHtml::dropDownList('Entrant[grade_rus_doctype]',$model->grade_rus_doctype, $model->gradedoctypes); ?>
            </div>
            <div class="row">
                <label for="Entrant_grade_rus_doc">Номер</label>
                <?php echo CHtml::textField('Entrant[grade_rus_doc]', $model->grade_rus_doc,array('alt'=>'99-999999999-99', 'placeholder'=>'__-_________-__')); ?>
                <a href="#" id="gradeduble">Продублировать</a>
            </div>
            <div class="row"><b>Химия</b></div>
            <div class="row">
                <label for="Entrant_grade_him">Оценка</label>
                <?php echo CHtml::textField('Entrant[grade_him]', $model->grade_him,array('alt'=>'999', 'size'=>'2')); ?>
                <label for="Entrant_grade_him_doctype">Документ</label> <?php echo CHtml::dropDownList('Entrant[grade_him_doctype]',$model->grade_him_doctype, $model->gradedoctypes); ?>
            </div>
            <div class="row">
                <label for="Entrant_grade_him_doc">Номер</label>
                <?php echo CHtml::textField('Entrant[grade_him_doc]', $model->grade_him_doc,array('alt'=>'99-999999999-99', 'placeholder'=>'__-_________-__')); ?>
            </div>
            <div class="row"><b>Биология</b></div>
            <div class="row">
                <label for="Entrant_grade_bio">Оценка</label>
                <?php echo CHtml::textField('Entrant[grade_bio]', $model->grade_bio,array('alt'=>'999', 'size'=>'2')); ?>
                <label for="Entrant_grade_bio_doctype">Документ</label> <?php echo CHtml::dropDownList('Entrant[grade_bio_doctype]',$model->grade_bio_doctype, $model->gradedoctypes); ?>
            </div>
            <div class="row">
                <label for="Entrant_grade_bio_doc">Номер</label>
                <?php echo CHtml::textField('Entrant[grade_bio_doc]', $model->grade_bio_doc,array('alt'=>'99-999999999-99', 'placeholder'=>'__-_________-__')); ?>
            </div>
	</div>
        
	<div class="lang block">
            <div class="blockinfo"><h2>Иностранный язык</h2></div>
            <div class="row">
                <label for="Entrant_foreignid">Язык</label>
                <?php echo CHtml::dropDownList('Entrant[foreignid]',$model->foreignid, CHtml::listData($lang, 'id', 'name')); ?>
            </div>
            <div class="row">
                <label for="Entrant_foreign_exp">Изучал</label>
                <?php echo CHtml::textField('Entrant[foreign_exp]', $model->foreign_exp,array('alt'=>'99', 'placeholder'=>'Лет', 'size'=>'2')); ?>
                <label for="Entrant_foreign_grade">Оценка в аттестате/дипломе</label>
                <?php echo CHtml::textField('Entrant[foreign_grade]', $model->foreign_grade,array('alt'=>'5', 'size'=>'1')); ?>
            </div>
	</div>

        <div class="dop block">
            <div class="blockinfo"><h2>Дополнительно</h2></div>
            <div class="row"><label for="Entrant_social_work">Общественная работа до поступления в ВУЗ</label></div>
            <div class="row"><?php echo CHtml::textArea('Entrant[social_work]', $model->social_work, array('class'=>'doptextarea')); ?></div>
            <div class="row"><label for="Entrant_talent">Участие в художественное самодеятельности</label></div>
            <div class="row"><?php echo CHtml::textArea('Entrant[talent]', $model->talent, array('class'=>'doptextarea')); ?></div>
            <div class="row"><label for="Entrant_sport">Занятие спортом</label></div>
            <div class="row"><?php echo CHtml::textArea('Entrant[sport]', $model->sport, array('class'=>'doptextarea')); ?></div>
	</div>

    </div>
    <br class="clear">
    <div class="row">
        <b><label for="Entrant_vk">Вне конкурса</label></b>
        <?php echo CHtml::checkBox('Entrant[vk]', $model->vk); ?><br />
        <b><label for="Entrant_took">Забор</label></b>
        <?php echo CHtml::checkBox('Entrant[took]', $model->took); ?><br />
        <b><label for="Entrant_orig">Оригиналы</label></b>
        <?php echo CHtml::checkBox('Entrant[orig]', $model->orig); ?><br />
    </div>
    <div class="row buttons">
            <?php echo CHtml::button($model->isNewRecord ? 'Добавить' : 'Сохранить', array('onclick'=>'js:beforesend();')); ?>
    </div>
</form>
</div><!-- entantform -->