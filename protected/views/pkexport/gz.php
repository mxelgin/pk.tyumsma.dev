<?php
$pdf = false;
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mPDF('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Приказ о зачислении</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
        border-collapse: collapse;
}
table td.center{
    text-align: center;
}
table td{
    border: 1px solid black;
    padding: 4px;
}
table th{
    border: 1px solid black;
    padding: 4px;
    text-align: center;
}
@media print{
    .break{
        page-break-after: always;
    }
}
</style>
</head>
<?php
	$arr[] = array();

	$specs = $wave1[specs];
   	$entrants = $wave1[entrants];
   	$entrantsrcm = $wave1[entrantsrcm];
   	$regname = $wave1[regname];

    if ($specs != null) {
        foreach ($specs as $spec){
		    foreach ($entrants[$spec->id] as $ent){
			    if (!empty($enrol[$ent->id])) continue;
			    if (!empty($ent)) $arr[] = $ent;
			    $enrol[$ent->id]=$spec->id;
		    }
   		    foreach ($entrantsrcm[$spec->id] as $entr){
			    foreach ($entr as $ent){
				    if (!empty($enrol[$ent->id])) continue;
				    if (!empty($ent)) $arr[] = $ent;
				    $enrol[$ent->id]=$spec->id;
			    }
		    }
	    }
    }

	$specs = $wave2[specs];
   	$entrants = $wave2[entrants];
   	$entrantsrcm = $wave2[entrantsrcm];
   	$regname = $wave2[regname];

    if ($specs != null) {
        foreach ($specs as $spec){
            foreach ($entrants[$spec->id] as $ent){
                if (!empty($enrol[$ent->id])) continue;
                if (!empty($ent)) $arr[] = $ent;
                $enrol[$ent->id]=$spec->id;
            }
            foreach ($entrantsrcm[$spec->id] as $entr){
                foreach ($entr as $ent){
                    if (!empty($enrol[$ent->id])) continue;
                    if (!empty($ent)) $arr[] = $ent;
                    $enrol[$ent->id]=$spec->id;
                }
            }
        }
    }

    $specs = $wave3[specs];
    $entrants = $wave3[entrants];
    $entrantsrcm = $wave3[entrantsrcm];
    $regname = $wave3[regname];

    if ($specs != null) {
        foreach ($specs as $spec){
            foreach ($entrants[$spec->id] as $ent){
                if (!empty($enrol[$ent->id])) continue;
                if (!empty($ent)) $arr[] = $ent;
                $enrol[$ent->id]=$spec->id;
            }
            foreach ($entrantsrcm[$spec->id] as $entr){
                foreach ($entr as $ent){
                    if (!empty($enrol[$ent->id])) continue;
                    if (!empty($ent)) $arr[] = $ent;
                    $enrol[$ent->id]=$spec->id;
                }
            }
        }
    }

    $specs = $wave4[specs];
    $entrants = $wave4[entrants];
    $entrantsrcm = $wave4[entrantsrcm];
    $regname = $wave4[regname];

    if ($specs != null) {
        foreach ($specs as $spec){
            foreach ($entrants[$spec->id] as $ent){
                if (!empty($enrol[$ent->id])) continue;
                if (!empty($ent)) $arr[] = $ent;
                $enrol[$ent->id]=$spec->id;
            }
            foreach ($entrantsrcm[$spec->id] as $entr){
                foreach ($entr as $ent){
                    if (!empty($enrol[$ent->id])) continue;
                    if (!empty($ent)) $arr[] = $ent;
                    $enrol[$ent->id]=$spec->id;
                }
            }
        }
    }

	foreach ($arr as $key => $row) {
	    $fam[$key]  = $row['fam'];
	    $name[$key] = $row['name'];
	    $m_name[$key] = $row['m_name'];
	}
	array_multisort($fam, SORT_ASC, $name, SORT_ASC, $m_name, SORT_ASC, $arr);
?>
<?php
if ($_GET['type'] == 'xls') {
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
} elseif ($_GET['type'] == 'pdf') {
    $mpdf = new mPDF('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10); /*задаем формат, отступы и.т.д.*/
    ob_start();
}
?>
<body>
<div class="section">
<span class="right">Приложение №1</span>
<div class="clear"></div>
<p class="title">Форма для Газпрома <?php echo date('Y') ?> года<br></p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="2%" valign="bottom">№ пп</td>
    <td width="5%" valign="bottom">Код организации</td>
    <td width="12%" valign="bottom">Фамилия</td>
    <td width="6%" valign="bottom">Имя</td>
    <td width="12%" valign="bottom">Отчество</td>
    <td width="2%" valign="bottom">Пол</td>
    <td width="2%" valign="bottom">Индекс</td>
    <td width="2%" valign="bottom">Регион</td>
    <td width="2%" valign="bottom">Район</td>
    <td width="2%" valign="bottom">Тип</td>
    <td width="2%" valign="bottom">Город</td>
    <td width="2%" valign="bottom">Населенный пункт</td>
    <td width="2%" valign="bottom">Улица</td>
    <td width="2%" valign="bottom">Дом</td>
    <td width="2%" valign="bottom">Корпус</td>
    <td width="2%" valign="bottom">Квартира</td>
    <td width="2%" valign="bottom">Тип паспорта</td>
    <td width="2%" valign="bottom">Серия паспорта</td>
    <td width="2%" valign="bottom">Номер паспорта</td>
    <td width="2%" valign="bottom">Когда выдан</td>
    <td width="2%" valign="bottom">Кем выдан</td>
    <td width="6%" valign="bottom">Дата рождения</td>
    <td width="6%" valign="bottom">Место рождения</td>
    <td width="6%" valign="bottom">Гражданство</td>
    <td width="6%" valign="bottom">Телефон домашний</td>
    <td width="6%" valign="bottom">Телефон служебный</td>
    <td width="6%" valign="bottom">Контрольная информация</td>
    <td width="6%" valign="bottom">Имя в латинской транскрипции</td>
    <td width="6%" valign="bottom">Фамилия в латинской транскрипции</td>
  </tr>
<?php
	$i = 1;
  	foreach ($arr as $key=>$ent){
		if (!empty($ent)){
//            Education::model()->
?>
	<tr>
		<td><?php echo $i++; ?></td>
		<td><?php echo "{$ent->department->specname}"; ?></td>
		<td><?php echo "{$ent->fam}"; ?></td>
		<td><?php echo "{$ent->name}"; ?></td>
		<td><?php echo "{$ent->m_name}"; ?></td>
		<td><?php {if ($ent->sex == 2) echo "Ж"; elseif ($ent->sex == 1) echo "М"; }; ?></td>
		<td><?php echo "{$ent->street->parent->index}"; ?></td>
		<td><?php echo "{$ent->street->parent->region->name}"; ?></td>
		<td><?php echo "{$ent->street->parent->raion->name}"; ?></td>
		<td><?php echo "{$ent->street->parent->type->fullname}"; ?></td>
		<td><?php if ($ent->street->parent->type->name == 'г') echo "{$ent->street->parent->name}"; ?></td>
		<td><?php if ($ent->street->parent->type->name != 'г') echo "{$ent->street->parent->name}"; ?></td>
		<td><?php echo "{$ent->street->name}"; ?></td>
		<td><?php echo "'{$ent->home_build}"; ?></td>
		<td><?php echo ""; ?></td>
		<td><?php echo "{$ent->home_kv}"; ?></td>
        <td><?php echo "{$ent->mdoc->name}"; ?></td>
        <td><?php
            foreach ($ent->docs as $key=>$value){
                if (($value->docid == 21) AND ($value->fieldid == 13)){
                    echo $value->value;
                }
                if (($value->docid == 47) AND ($value->fieldid == 34)){
                    echo $value->value;
                }
            }
            ?>
        </td>
        <td><?php echo ""; ?></td>
        <td><?php foreach ($ent->docs as $key=>$value){
                if (($value->docid == 21) AND ($value->fieldid == 14)){
                    echo $value->value;
                }
                if (($value->docid == 47) AND ($value->fieldid == 38)){
                    echo $value->value;
                }

            }
            ?>
        </td>
        <td><?php foreach ($ent->docs as $key=>$value){
                if (($value->docid == 21) AND ($value->fieldid == 15)){
                    echo $value->value;
                }
                if (($value->docid == 47) AND ($value->fieldid == 37)){
                    echo $value->value;
                }

            }
            ?>
        </td>
		<td><?php echo "{$ent->birth_date}"; ?></td>
        <td><?php echo "{$ent->birth_city->name}"; ?></td>
        <td><?php echo "{$ent->citizen->name}"; ?></td>
        <td><?php echo ""; ?></td>
        <td><?php echo ""; ?></td>
        <td><?php echo ""; ?></td>
        <td><?php echo ""; ?></td>
        <td><?php echo ""; ?></td>
    </tr>
<?php
	}}
?>
</table>
<br/>
* Виды документов<br>
Паспорт РФ<br>
Иностранный паспорт<br>
Вид на жительство<br>
Миграционная карта<br>
<br/><br/>
</div>
</body>
</html>
<?php
if ($_GET['type'] == 'pdf') {
    $mpdf->WriteHTML(ob_get_contents());
    ob_end_clean();
    $mpdf->Output('mpdf.pdf', 'I');
}
?>