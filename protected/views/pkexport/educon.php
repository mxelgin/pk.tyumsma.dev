<?php
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-type: application/csv; charset=utf-8');
header('Content-Disposition: attachment; filename="export.csv"');
header('Cache-Control: max-age=0');
echo 'lastname,firstname'."\n";
echo $groupname."\n";
foreach ($entrants as $entrant){
    echo $entrant->fam.','.$entrant->name.' '.$entrant->m_name."\n";
}
?>