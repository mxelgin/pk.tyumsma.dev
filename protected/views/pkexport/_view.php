<div class="view">
    <p align="center">
        <?php echo CHtml::button('Распечатать заявление', array('onclick'=>'js: window.open("'.$this->createUrl('pkentrant/printStatement', array('id'=>$data->id)).'", "Statement", "menubar=no,location=no,resizable=no,status=no,width=710")')); ?>
    </p>
    <table width="100%">
        <tr><td width="50%" valign="top">
	<b><?php echo $data->getAttributeLabel('id'); ?>:</b>
	<?php echo CHtml::link($data->id, array('view', 'id'=>$data->id)); ?>
	<br />
        
	<b>ФИО:</b>
	<?php echo $data->fam.' '.$data->name.' '.$data->m_name; ?>
	<br />
        
        <?php if ($data->phone){ ?>
        <b><?php echo $data->getAttributeLabel('phone'); ?>:</b>
	<?php echo $data->phone; ?>
	<br />
        <?php } ?>

        <?php if ($data->birth_date){ ?>
        <b><?php echo $data->getAttributeLabel('birth_date'); ?>:</b>
	<?php echo $data->birth_date; ?>
	<br />
        <?php } ?>

        <?php if ($data->birth_place_id){ ?>
        <b><?php echo $data->getAttributeLabel('birth_place_id'); ?>:</b>
	<?php echo $data->birth_city->name; ?>
	<br />
        <?php } ?>

        <?php if ($data->citizenship){ ?>
        <b><?php echo $data->getAttributeLabel('citizenship'); ?>:</b>
	<?php echo $data->citizen->name; ?>
	<br />
        <?php } ?>

        <?php if ($data->home_city){ ?>
        <b>Домашний адрес:</b>
        <?php echo $data->home_city_db->name.', '.$data->home_street.' '.$data->home_build.', '.$data->home_kv;?>
	<br />        
        <?php } ?>

        <?php if ($data->sex){ ?>
        <b><?php echo $data->getAttributeLabel('sex'); ?>:</b>
	<?php echo $data->sextypes[$data->sex]; ?>
	<br />
        <?php } ?>

        <?php if ($data->need_hostel){
            echo '<b>Нуждается в общежитии</b></br>';
        } ?>
        
        <?php if ($data->sex==1){ ?>
        <b><?php echo $data->getAttributeLabel('service'); ?>:</b>
	<?php echo $data->servicetypes[$data->service]; ?>
	<br />
        <?php } ?>
        
        <?php if ($data->foreignid){ ?>
        <b><?php echo $data->getAttributeLabel('foreignid'); ?>:</b>
	<?php echo $data->foreign->name; ?>
	<br />
        <?php } ?>

        <?php if ($data->foreign_exp){ ?>
        <b><?php echo $data->getAttributeLabel('foreign_exp'); ?>:</b>
	<?php echo $data->foreign_exp; ?>
	<br />
        <?php } ?>
        
        <?php if ($data->foreign_grade){ ?>
        <b><?php echo $data->getAttributeLabel('foreign_grade'); ?>:</b>
	<?php echo $data->foreign_grade; ?>
	<br />
        <?php } ?>
        
        <?php if ($data->edu){ ?>
        <b>Окончил учебное заведение:</b> <?php echo $data->edu->name; ?><br>
	Город: <?php echo $data->edu->city->name; ?><br>
        Дата окончания: <?php echo $data->edu->exp_date; ?><br>
        Средний балл: <?php echo $data->edu->grade; ?>
	<br />
        <?php } ?>        

        <?php if ($data->social_work){ ?>
        <b><?php echo $data->getAttributeLabel('social_work'); ?>:</b>
	<?php echo $data->social_work; ?>
	<br />
        <?php } ?>

        <?php if ($data->talent){ ?>
        <b><?php echo $data->getAttributeLabel('talent'); ?>:</b>
	<?php echo $data->talent; ?>
	<br />
        <?php } ?>

        <?php if ($data->sport){ ?>
        <b><?php echo $data->getAttributeLabel('sport'); ?>:</b>
	<?php echo $data->sport; ?>
	<br />        
        <?php } ?>
        </td>
        <td width="50%" valign="top">
            <table>
                <tr><th>Предмет</th><th>Оцена</th><th>Документ</th><th>Номер</th></tr>
                <tr><td>Русский язык</td><td><?php echo $data->grade_rus; ?></td><td><?php echo $data->gradedoctypes[$data->grade_rus_doctype]; ?></td><td><?php echo $data->grade_rus_doc; ?></td></tr>
                <tr><td>Химия</td><td><?php echo $data->grade_him; ?></td><td><?php echo $data->gradedoctypes[$data->grade_him_doctype]; ?></td><td><?php echo $data->grade_him_doc; ?></td></tr>
                <tr><td>Биология</td><td><?php echo $data->grade_bio; ?></td><td><?php echo $data->gradedoctypes[$data->grade_bio_doctype]; ?></td><td><?php echo $data->grade_bio_doc; ?></td></tr>
            </table>
            <b><?php echo $data->mdoc->name; ?></b><br />
            <?php
            foreach ($mdocfields as $field){
                echo $field->field->name.': '.$field->value.'<br>';
            }
            ?>
            <b>Сдал документы:</b><br />
            <?php
            foreach ($dopdocs as $doc){
                if ($doc->doc->action=='input'){
                    echo $doc->doc->name.' (';
                    echo $doc->field->name.': '.$doc->value.')<br>';
                }else{
                    echo $doc->doc->name.'<br>';
                }
            }
            ?>
        </td></tr>
        <tr>
            <td>
                <?php if ($data->specs){ ?>
                    <b>Приоритет специальностей:</b><br />
                    <?php
                    foreach ($data->specs as $spec){
                        echo $spec->level.' '.$spec->spec->name.' '.$spec->types[$spec->type].'<br>';
                    }
                }
                ?>
                    <br />
                    <?php
                    if ($data->took){
                        echo '<b>Забрал документы</b><br />';
                    }
                    if ($data->vk){
                        echo '<b>Вне конкурса</b><br />';
                    }
                    if ($data->orig){
                        echo '<b>Сдал оригиналы</b><br />';
                    }
                    ?>
            </td>
            <td>
                <?php if ($data->parents){ ?>
                    <b>Информация о родителях:</b><br />
                    <?php
                    foreach ($data->parents as $parent){
                        echo 'ФИО: '.$parent->fio.'<br>';
                        echo 'Адрес: '.$parent->addr.'<br>';
                        echo 'Работа: '.$parent->work.'<br>';
                        echo 'Телефон: '.$parent->phone.'<br>';
                    }
                }
                ?>        
            </td>
        </tr>
    </table>
</div>