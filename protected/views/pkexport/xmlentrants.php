<?php
header("Content-type: text/xml;charset=utf-8");
$s = "<?xml version='1.0' encoding='utf-8'?>";
$s .= "<rows>";
$departments=  Departments::model()->findAll();
$dp=array();
foreach ($departments as $department){
    $dp[$department->id]=array(0=>$department->name, 1=>$department->specname);
}
foreach ($entrants as $entrant){
    $pass=Docs::model()->find('docid=21 and fieldid=13 and entrantid='.$entrant->id);
    $pass=explode('-', $pass->value);
    $pass_series=$pass[0];
    $pass_number=$pass[1];
    $pass_who=Docs::model()->find('docid=21 and fieldid=15 and entrantid='.$entrant->id);
    $pass_date=Docs::model()->find('docid=21 and fieldid=14 and entrantid='.$entrant->id);
    $address = $model->getFullAddressArray($entrant->id);
    $s .= "<row id='". $entrant->id."'>";
    $s .= "<prn>". $entrant->id."</prn>";
    $s .= "<lastname>". $entrant->fam."</lastname>";
    $s .= "<firstname>". $entrant->name."</firstname>";
    $s .= "<middlename>". $entrant->m_name."</middlename>";
    $s .= "<sex>". $entrant->sex."</sex>";
    $s .= "<birthdate>". $entrant->birth_date."</birthdate>";
    $s .= "<pens_number></pens_number>";
    $s .= "<inn></inn>";
    $s .= "<pass_series>". $pass_series."</pass_series>";
    $s .= "<pass_num>". $pass_number."</pass_num>";
    $s .= "<pass_who>". $pass_who->value."</pass_who>";
    $s .= "<pass_date>". $pass_date->value."</pass_date>";
    $specid=0;
    if ($entrant->wave_first>0) $specid=$entrant->wave_first;
    if ($entrant->wave_second>0) $specid=$entrant->wave_second;
    if ($entrant->wave_third>0) $specid=$entrant->wave_third;
    $s .= "<faculty>".$dp[$specid][0]."</faculty>";
    $s .= "<spec>".$dp[$specid][1]."</spec>";
    $s .= "<kind>".$entrant->enrol_type."</kind>";
    $s .= "<form></form>";
    $s .= "<pact_num></pact_num>";
    $s .= "<pact_date></pact_date>";
    $s .= "<pact_payer></pact_payer>";
    $s .= "<pact_datefrom></pact_datefrom>";
    $s .= "<pact_dateto></pact_dateto>";
    $s .= "<cost></cost>";
    $s .= "<country>". $entrant->citizen->name."</country>";
    $s .= "<region>". $address['region']."</region>";
    $s .= "<region_type></region_type>";
    $s .= "<rayon></rayon>";
    $s .= "<city>". $address['raion']."</city>";
    $s .= "<town>". $address['city']."</town>";
    $s .= "<town_type>". $entrant->hometypes[$entrant->home_type]."</town_type>";
    $s .= "<street>". $address['street']."</street>";
    $s .= "<street_type></street_type>";
    $s .= "<house>". $entrant->home_build."</house>";
    $s .= "<block></block>";
    $s .= "<building></building>";
    $s .= "<flat>". $entrant->home_kv."</flat>";
    $s .= "<postindex></postindex>";
    $s .= "<is_base></is_base>";
    $s .= "<is_legal>1</is_legal>";
    $s .= "<is_fact></is_fact>";
    $s .= "<is_birth></is_birth>";
    $s .= "</row>";
}
$s .= "</rows>";
header("Content-Type: application/xml");
header("Content-Length: " . strlen($s));
echo $s;
?>
