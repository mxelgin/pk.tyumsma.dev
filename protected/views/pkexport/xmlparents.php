<?php
header("Content-type: text/xml;charset=utf-8");
$s = "<?xml version='1.0' encoding='utf-8'?>";
$s .= "<rows>";
foreach ($parents as $parent){
    $s .= "<row>";
    $s .= "<prn>". $parent->entrantid."</prn>";
    $mn=explode(' ', $parent->fio);
    //if (!empty($mn))
    $s .= "<lastname>". $mn[0]."</lastname>";
    $s .= "<firstname>". $mn[1]."</firstname>";
    $s .= "<middlename>". $mn[2] ."</middlename>";
    $s .= "<fio>". $parent->fio ."</fio>";
    $s .= "<birthdate></birthdate>";
    $s .= "<relation></relation>";
    $s .= "</row>";
}
$s .= "</rows>";
header("Content-Type: application/xml");
header("Content-Length: " . strlen($s));
echo $s;
?>