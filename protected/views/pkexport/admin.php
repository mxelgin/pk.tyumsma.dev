<h1>Экспорт абитуриентов:</h1>

<div style="width:50%;float:left;border-right:1px solid;">
<h3>в программу "ДЕКАНАТ"</h3>


<p align='left'><b>Введите номер группы!!! </b>
    <br><i>*Номер группы берется из программы "Деканат"
    <br>**Экспортируются только принятые абитуриенты</i>
    <form action='/pkexport/mssqlexport' method="get">
        Группа: <input type='text' name='group' value=''><br />
        Факультет: <input type='text' name='kod' value=''>
        <input type='submit' value='Экспорт в Деканат'>
    </form>
</p>
</div>
<div style="width:49%;float:left;">
    <h3 align='right'>в *.CSV файл</h3><br>
    <p align="right"><i>*Файл предназначен для программы "Приемная комиссия" (Excel)</i></p>
    <br>
<p align="right">Экспорт в:<a href="/pkexport/csvexport"> CSV</a></p>
</div>       
<br style="clear: both;" /><br /><br /><br />
<div style="width:50%;float:left;border-right:1px solid;">
<h3>Экспортировать в программу "Парус"</h3>
<ul>
    <li><a href="/pkexport/xmlentrants">Выгрузка информации о поступивших</a></li>
    <li><a href="/pkexport/xmlparents">Выгрузка информации о родителях</a></li>
</ul>
<p><b>Другое</b></p>
<ul>
    <li><a href="/pkexport/csvBank">Выгрузка информации о бюджетниках в банк</a></li>
    <li><a href="/pkexport/csvClinik">Выгрузка информации о поступивших в клинику</a></li>
</ul>
</div>
<div style="width:49%;float:left;">
    <h3 align='right'>Модуль взаимодействия с ЕИС</h3><br>
    <p align="right"><i>Все зачисленные абитуриенты переносятся в ЕИС как студенты первого курса группы "Абитуриенты" соответствующей кафедры.</i></p>
    <p align="right"><a href="/pkexport/modulEIS" target="_blank">Экспортировать данные</a></p>
</div>
<br style="clear: both;" /><br /><br /><br />
<div style="width:50%;float:left;border-right:1px solid;">
    <h3 align='left'>Модуль экспорта в программу Едукон</h3><br>
    <ul>
        <?php
        $specs=Departments::model()->findAll();
        foreach ($specs as $spec){
            echo '<li><a href="/pkexport/educon/?spec='.$spec->id.'">'.$spec->name.'</a></li>';
        }
        ?>
    </ul>
    <br/>
    <h3 align='left'>Модуль экспорта для карточек Газпром</h3><br>
    <ul>
        <?php
            echo '<li><a href="/pkexport/gz?type=xls">Экспортировать данные</a></li>';
        ?>
    </ul>
    <br/>
    <h3 align='left'>Модуль экспорта в программу "ДЕКАНАТ"</h3><br>
    <ul>
        <?php
        echo '<li><a href="/pkexport/dekanat?type=xls">Экспортировать данные</a></li>';
        ?>
    </ul>
</div>
<div style="width:49%;float:left;">
    <h3 align='right'>Модуль выгрузки в ФИС ЕГЭ</h3><br>
    <p align="right"><i>Все поданные заявления экспортируются в ФИС ЕГЭ</i></p>
    <p align="right"><a href="/pkexport/modulFIS" target="_blank">Экспортировать данные</a></p>
</div>