<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Заявление</title>
<style>
body {
	margin:0;
	padding:0;
}
.section2 {
	width:710px;
	font-size:12pt;
	font-family:"Times New Roman","serif";
	line-height:1.0em;
}
.head_title {
	text-align:center;
	font-size:14pt;
}
.head_title1 {
	text-align:justify;
}
.head_title span{
	text-transform:uppercase;
}
.small {
	font-size:10pt;
}
/*p {
	text-align:justify;
	line-height:1.0em;
}*/
.just {
	text-align:justify;
}
.right{
	line-height:1.5em;
	text-align:right;
}
.left_float{
	float:left;
	line-height:1.5em;
}
.right_float{
	float:right;
	right:0;
	line-height:1.5em;
}
.nor{
	line-height:1.5em;
}
</style>
<script>
window.onload = function () {
    window.print() ;
};
</script>
</head>

<body>
<div class="section2">
<div class="left_float">
К вступительным экзаменам допущен<br />
протокол № ______________________<br />
от «_____»_______________<?php echo date('Y'); ?> г.<br />
Подпись
</div>
<div class="right_float">

Зачислен на 1 курс<br />
__________________________________факультета<br />
протокол № _____ от «___»_____________<?php echo date('Y'); ?> г.<br />
Подпись
</div>
<div style="overflow:hidden;"></div>
<p>&nbsp;</p>
<div>
<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
<p class="nor">
Ректору ГОУ ВПО ТюмГМА Минздравсоцразвития России от гр.<br />
ФИО: <u><?php echo $model->fam.' '.$model->name.' '.$model->m_name; ?></u><br />
проживающего (ей):<u> <?php echo $model->home_city_db->name.', '.$model->home_street.' '.$model->home_build.', '.$model->home_kv; ?></u><br />
окончившего: <u>дневная школа, диплом с отличием</u><br />
<?php
if ($model->maindoc==21)
echo 'паспортные данные (серия, №, когда и кем выдан): <u>';
foreach ($mdocfields as $mfield)
    echo $mfield->value.' ';
echo '</u></p>';
?>
<b><center>З А Я В Л Е Н И Е</center></b>
<p class="nor">Прошу допустить меня к
<?php
if ($model->grade_rus==0 || $model->grade_him==0 || $model->grade_bio==0)
    echo '<s> участию в конкурсе</s>, вступительным экзаменам ';
else
    echo 'Прошу допустить меня к участию в конкурсе, <s>вступительным экзаменам</s>';
?>
для поступления на дневное<br />
заочное обучение<br />
На <s>целевое место</s>, 
<?php
if ($specfirst->type==1)
    echo 'финансируемое из федерального бюджета, <s>на место с оплатой стоимости обучения</s>';
else
    echo '<s>финансируемое из федерального бюджета</s>, на место с оплатой стоимости обучения';
?>
<br />
<small>ненужное зачеркнуть</small><br />
факультета: <u><?php echo $specfirst->spec->name; ?></u><br />
по специальности ________________________________________________________________________<br />
В случае непрохождения по конкурсу прошу передать мои документы на специальность в соответствии
с приведенным ниже списком приоритетов:</p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="8%" rowspan="2" align="center">№ пп</td>
    <td width="49%" rowspan="2" align="center">Название факультета</td>
    <td colspan="2" align="center">Условия</td>
  </tr>
  <tr>
    <td width="23%" align="center">Бюджет</td>
    <td width="20%" align="center">Внебюджет</td>
  </tr>
<?php
if (count($specother)){
    foreach ($specother as $spec){
        echo '
        <tr>
            <td>'.($spec->level-1).'</td>
            <td>'.$spec->spec->name.'</td>
            <td colspan="2" align="center">'.$spec->types[$spec->type].'</td>
        </tr>';
    }
}else{
    echo '
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
    </tr>';
}
?>
</table>
<p>Прошу засчитать в качестве результатов вступительных испытаний следующее:</p>
<table width="100%" border="1" cellspacing="0" cellpadding="4">
  <tr>
    <td width="32%" height="34" valign="top">Наименование предмета</td>
    <td width="33%" valign="top">Балл</td>
    <td width="35%" valign="top">Подтверждающий документ
      Наименование  &nbsp;&nbsp;&nbsp;&nbsp;  номер</td>
  </tr>
    <tr><td>Русский язык</td><td align="center"><?php echo $model->grade_rus; ?></td><td><?php echo $model->gradedoctypes[$model->grade_rus_doctype]; ?> <?php echo $model->grade_rus_doc; ?></td></tr>
    <tr><td>Химия</td><td align="center"><?php echo $model->grade_him; ?></td><td><?php echo $model->gradedoctypes[$model->grade_him_doctype]; ?> <?php echo $model->grade_him_doc; ?></td></tr>
    <tr><td>Биология</td><td align="center"><?php echo $model->grade_bio; ?></td><td><?php echo $model->gradedoctypes[$model->grade_bio_doctype]; ?> <?php echo $model->grade_bio_doc; ?></td></tr>
</table>
<p class="nor">
<?php
if ($model->need_hostel)
    echo 'В общежитии <u>нуждаюсь</u>, не нуждаюсь ';
else
    echo 'В общежитии нуждаюсь, <u>не нуждаюсь</u> ';
?>
  (подчеркнуть)<br />
  О себе сообщаю следующие сведения:<br />
  Пол <u><?php echo $model->sextypes[$model->sex]; ?></u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Гражданство <u><?php echo $model->citizen->name; ?></u><br />
  Год и место рождения <u><?php echo $model->birth_date.' '.$model->birth_city->name; ?></u> <br />
  Фамилия, имя, отчество родителей, их местожительство, телефон, кем и где работают (наименование и местонахождение предприятия, организации, занимаемая должность):<br />
<?php
if ($model->parents){
    foreach ($model->parents as $parent){
        echo '<u>';
        if ($parent->fio) echo $parent->fio.', ';
        if ($parent->addr) echo $parent->addr.', ';
        if ($parent->work) echo $parent->work.', ';
        if ($parent->phone) echo $parent->phone;
        echo '</u><br />';
    }
}else{
    echo '________________________________________________________________________________________<br />';
}
?>
  О себе дополнительно сообщаю ____________________________________________________________<br />
  ________________________________________________________________________________________<br />
  С правилами приема на 2011 г., порядком приема, уставом ТюмГМА, лицензией, свидетельством об аккредитации и приложениями к ним ознакомлен(а).___________________________________________<br />
  Высшее образование данного уровня получаю впервые ________________________________________<br />
  Подтверждаю подачу заявления не более чем в пять вузов ______________________________________<br />
  С датой представления оригинала документа об образовании и свидетельства о результатах ЕГЭ ознакомлен (а) ___________________________________________________________________________<br />
  С правилами подачи апелляции ознакомлен (а) _______________________________________________<br />
  Согласен(сна) на обработку своих персональных данных в порядке, установленном ФЗ от 27 июля 2006 № 152-ФЗ «О персональных данных» 
  ________________________________________________________<br />
  <br />
  «<?php echo date('d'); ?>» <?php echo date('m'); ?> <?php echo date('Y'); ?> г.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Подпись ____________________
</p>
</div>
</div>
</body>
</html>
