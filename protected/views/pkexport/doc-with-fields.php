<?php 
foreach ($fields as $field){ 
    if ($field->editonly && $entrantid==0) continue;
?>

    <div class="row">
            <?php 
            echo CHtml::label($field->name,'docs['.$id.']['.$field->id.']', array('required'=>$field->required)).' ';
            $options=array();
            if ($field->type=='serialnumber')
                $options=array('size'=>'10', 'class'=>$field->type,'alt'=>'9999-999999', 'placeholder'=>'____-______');
            if ($field->type=='date'){
                $options=array('class'=>'datepicker', 'placeholder'=>'__.__.____');
            }
            if ($field->type=='foreign')
                $options=array('class'=>$field->type,'alt'=>'99-9999999', 'placeholder'=>'__-_______');
            if ($field->type=='egenumber')
                $options=array('class'=>$field->type,'alt'=>'99-999999999-99', 'placeholder'=>'__-_________-__');
            if ($field->type=='text')
                $options=array('size'=>'40');
            echo CHtml::textField('docs['.$id.']['.$field->id.']',$docsdata[$id][$field->id], $options);
            ?>
        
    </div>
<?php } ?>

<?php 
if ($docserrors){
    echo '<script>';
    foreach ($docserrors as $field=>$error){
        echo '$("#'.$field.'").addClass("error");'."\n";
    }
    echo '</script>';
}
?>
