<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contract4-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->errorSummary($model); ?>
        <?php if ($entrantid) $model->entrantid=$entrantid;
        echo $form->hiddenField($model,'entrantid'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'org_name'); ?>
		<?php echo $form->textField($model,'org_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'org_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org_person'); ?>
		<?php echo $form->textField($model,'org_person',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'org_person'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org_osn'); ?>
		<?php echo $form->textField($model,'org_osn',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'org_osn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org_addr'); ?>
		<?php echo $form->textField($model,'org_addr',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'org_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org_phone'); ?>
		<?php echo $form->textField($model,'org_phone',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'org_phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org_rekv'); ?>
		<?php echo $form->textField($model,'org_rekv',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'org_rekv'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fiz_fio'); ?>
		<?php echo $form->textField($model,'fiz_fio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fiz_fio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fiz_pass_number'); ?>
		<?php echo $form->textField($model,'fiz_pass_number',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fiz_pass_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fiz_pass_date'); ?>
		<?php echo $form->textField($model,'fiz_pass_date'); ?>
		<?php echo $form->error($model,'fiz_pass_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fiz_pass_place'); ?>
		<?php echo $form->textField($model,'fiz_pass_place',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fiz_pass_place'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fiz_addr'); ?>
		<?php echo $form->textField($model,'fiz_addr',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fiz_addr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fiz_phone'); ?>
		<?php echo $form->textField($model,'fiz_phone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fiz_phone'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->