<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entrantid'); ?>
		<?php echo $form->textField($model,'entrantid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'org_name'); ?>
		<?php echo $form->textField($model,'org_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'org_person'); ?>
		<?php echo $form->textField($model,'org_person',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'org_osn'); ?>
		<?php echo $form->textField($model,'org_osn',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'org_addr'); ?>
		<?php echo $form->textField($model,'org_addr',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'org_phone'); ?>
		<?php echo $form->textField($model,'org_phone',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'org_rekv'); ?>
		<?php echo $form->textField($model,'org_rekv',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fiz_fio'); ?>
		<?php echo $form->textField($model,'fiz_fio',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fiz_pass_number'); ?>
		<?php echo $form->textField($model,'fiz_pass_number',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fiz_pass_date'); ?>
		<?php echo $form->textField($model,'fiz_pass_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fiz_pass_place'); ?>
		<?php echo $form->textField($model,'fiz_pass_place',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fiz_addt'); ?>
		<?php echo $form->textField($model,'fiz_addt',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fiz_phone'); ?>
		<?php echo $form->textField($model,'fiz_phone',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->