<?php
$this->breadcrumbs=array(
	'Contract4s',
);

$this->menu=array(
	array('label'=>'Create Contract4', 'url'=>array('create')),
	array('label'=>'Manage Contract4', 'url'=>array('admin')),
);
?>

<h1>Contract4s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
