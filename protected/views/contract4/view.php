<?php
$this->breadcrumbs=array(
	'Contract4s'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Contract4', 'url'=>array('index')),
	array('label'=>'Create Contract4', 'url'=>array('create')),
	array('label'=>'Update Contract4', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Contract4', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Contract4', 'url'=>array('admin')),
);
?>

<h1>View Contract4 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'entrantid',
		'org_name',
		'org_person',
		'org_osn',
		'org_addr',
		'org_phone',
		'org_rekv',
		'fiz_fio',
		'fiz_pass_number',
		'fiz_pass_date',
		'fiz_pass_place',
		'fiz_addt',
		'fiz_phone',
	),
)); ?>
