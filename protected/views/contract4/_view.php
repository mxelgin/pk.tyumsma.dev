<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrantid')); ?>:</b>
	<?php echo CHtml::encode($data->entrantid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_name')); ?>:</b>
	<?php echo CHtml::encode($data->org_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_person')); ?>:</b>
	<?php echo CHtml::encode($data->org_person); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_osn')); ?>:</b>
	<?php echo CHtml::encode($data->org_osn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_addr')); ?>:</b>
	<?php echo CHtml::encode($data->org_addr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('org_phone')); ?>:</b>
	<?php echo CHtml::encode($data->org_phone); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('org_rekv')); ?>:</b>
	<?php echo CHtml::encode($data->org_rekv); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fiz_fio')); ?>:</b>
	<?php echo CHtml::encode($data->fiz_fio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fiz_pass_number')); ?>:</b>
	<?php echo CHtml::encode($data->fiz_pass_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fiz_pass_date')); ?>:</b>
	<?php echo CHtml::encode($data->fiz_pass_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fiz_pass_place')); ?>:</b>
	<?php echo CHtml::encode($data->fiz_pass_place); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fiz_addt')); ?>:</b>
	<?php echo CHtml::encode($data->fiz_addt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fiz_phone')); ?>:</b>
	<?php echo CHtml::encode($data->fiz_phone); ?>
	<br />

	*/ ?>

</div>