<?php
$this->menu=array(
	array('label'=>'Создать документ', 'url'=>array('create')),
	array('label'=>'Редактировать документ', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Удалить документ', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены что хотите удалить этот документ?')),
	array('label'=>'Управление документами', 'url'=>array('admin')),
        array('label'=>'Управление категориями', 'url'=>'directDocsCategories'),
);
?>

<h1>Просмотр документа #<?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_view', array('data'=>$model,'acttypes'=>$acttypes, 'categories'=>$categories, 'fields'=>$fields,'fieldtypes'=>$fieldtypes,)); ?>

<?php
/*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'action',
		'categoryid',
		'required',
	),
));
*/
 ?>
