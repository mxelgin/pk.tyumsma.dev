<?if ($model->id){?>

<script>

$(document).ready(function($) {
    selected=$('#DirectDocs_action').attr('value');
    if (selected=='input'){
        $.ajax({'url':'/directDocsFields/update/id/<?php echo $model->id;?>','data':"type=input",'cache':false,'success':function(html){$("#direct-docs-fields").html(html)}});
    }
});

</script>

<?php } ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'direct-docs-form',
	'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
)); ?>

	<p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'action'); ?>
                <?php $url='directDocsFields/create';
                if ($model->id) $url='/directDocsFields/update/id/'.$model->id;
                $options['ajax']=array('update'=>'#direct-docs-fields', 'url' => $this->createUrl($url),'data' => 'js:"type="+this.value', 'cache' => false);?>
		<?php echo $form->dropDownList($model, 'action', $acttypes, $options); ?>
		<?php echo $form->error($model,'action'); ?>
	</div>
        <div id="direct-docs-fields">
            
        </div>

	<div class="row">
		<?php echo $form->labelEx($model,'categoryid'); ?>
		<?php echo $form->dropDownList($model, 'categoryid', CHtml::listData($categories, 'id', 'name')); ?>
		<?php echo $form->error($model,'categoryid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'required'); ?>
		<?php echo $form->checkBox($model,'required'); ?>
		<?php echo $form->error($model,'required'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->