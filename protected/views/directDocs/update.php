<?php

$this->menu=array(
	array('label'=>'Управление документами', 'url'=>array('admin')),
        array('label'=>'Управление категориями', 'url'=>'directDocsCategories'),
);
?>

<h1>Изменение документа</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'acttypes'=>$acttypes, 'categories'=>$categories)); ?>