<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action')); ?>:</b>
	<?php echo $acttypes[$data->action]; ?>
	<br />
        
        <?php
        if ($fields){
            echo '<p><br>Документ должен содержать следующие поля (обязательные поля помечены *):<br>';
            foreach ($fields as $field){
                echo $field->name. ' ('.$fieldtypes[$field->type].')';
                if ($field->required) echo '*';
                echo '<br>';
            }
            echo '</p>';
        }
        
        ?>

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoryid')); ?>:</b>
	<?php
        $cat=CHtml::listData($categories, 'id', 'name');
        echo $cat[$data->categoryid]; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('required')); ?>:</b>
	<?php echo $data->reqtypes[$data->required]; ?>
	<br />


</div>