<?php

$this->menu=array(
	array('label'=>'Добавить документ', 'url'=>array('create')),
        array('label'=>'Добавить категорию', 'url'=>'directDocsCategories/create'),
        array('label'=>'Управление категориями', 'url'=>'directDocsCategories'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('direct-docs-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление документами</h1>

<p>
Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) для поиска нужных позиций.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'direct-docs-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
                array('name'=>'action', 'value'=>'$data->acttypes[$data->action]', 'filter'=>$model->acttypes),
		array('name'=>'categoryid', 'value'=>'$data->category->name', 'filter'=>CHtml::listData(DirectDocsCategories::model()->findAll(),'id','name')),
                array('name'=>'required', 'value'=>'$data->reqtypes[$data->required]', 'filter'=>$model->reqtypes),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
