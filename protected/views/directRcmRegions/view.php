<?php
$this->breadcrumbs=array(
	'Direct Rcm Regions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectRcmRegions', 'url'=>array('index')),
	array('label'=>'Create DirectRcmRegions', 'url'=>array('create')),
	array('label'=>'Update DirectRcmRegions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectRcmRegions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectRcmRegions', 'url'=>array('admin')),
);
?>

<h1>View DirectRcmRegions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
