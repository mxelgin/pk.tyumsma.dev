<?php
$this->breadcrumbs=array(
	'Direct Rcm Regions',
);

$this->menu=array(
	array('label'=>'Create DirectRcmRegions', 'url'=>array('create')),
	array('label'=>'Manage DirectRcmRegions', 'url'=>array('admin')),
);
?>

<h1>Direct Rcm Regions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
