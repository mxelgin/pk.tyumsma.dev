<?php
$this->breadcrumbs=array(
	'Direct Cities',
);

$this->menu=array(
	array('label'=>'Create DirectCity', 'url'=>array('create')),
	array('label'=>'Manage DirectCity', 'url'=>array('admin')),
);
?>

<h1>Direct Cities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
