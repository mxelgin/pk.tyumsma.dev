<?php
$this->menu=array(
	array('label'=>'Добавить город', 'url'=>array('create')),
	array('label'=>'Управление городами', 'url'=>array('admin')),
);
?>

<h1>Изменение города</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>