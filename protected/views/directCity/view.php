<?php
$this->breadcrumbs=array(
	'Direct Cities'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectCity', 'url'=>array('index')),
	array('label'=>'Create DirectCity', 'url'=>array('create')),
	array('label'=>'Update DirectCity', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectCity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectCity', 'url'=>array('admin')),
);
?>

<h1>View DirectCity #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'useradd',
	),
)); ?>
