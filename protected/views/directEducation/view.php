<?php
$this->breadcrumbs=array(
	'Direct Educations'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectEducation', 'url'=>array('index')),
	array('label'=>'Create DirectEducation', 'url'=>array('create')),
	array('label'=>'Update DirectEducation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectEducation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectEducation', 'url'=>array('admin')),
);
?>

<h1>View DirectEducation #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
