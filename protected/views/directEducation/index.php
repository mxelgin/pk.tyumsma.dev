<?php
$this->breadcrumbs=array(
	'Direct Educations',
);

$this->menu=array(
	array('label'=>'Create DirectEducation', 'url'=>array('create')),
	array('label'=>'Manage DirectEducation', 'url'=>array('admin')),
);
?>

<h1>Direct Educations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
