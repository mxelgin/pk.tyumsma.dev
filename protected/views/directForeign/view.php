<?php
$this->breadcrumbs=array(
	'Direct Foreigns'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectForeign', 'url'=>array('index')),
	array('label'=>'Create DirectForeign', 'url'=>array('create')),
	array('label'=>'Update DirectForeign', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectForeign', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectForeign', 'url'=>array('admin')),
);
?>

<h1>View DirectForeign #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
