<?php
$this->menu=array(
	array('label'=>'Добавить факультет', 'url'=>array('create')),
	array('label'=>'Управление факультетами', 'url'=>array('admin')),
);
?>

<h1>Изменение факультета</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>