<?php
$this->menu=array(
	array('label'=>'Создать пользователя', 'url'=>array('create')),
	array('label'=>'Права доступа', 'url'=>array('admin')),
);
?>

<h1>Изменить пользователя</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'auth'=>$auth)); ?>