<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model, $auth); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'middle_name'); ?>
		<?php echo $form->textField($model,'middle_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'middle_name'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($auth,'login'); ?>
		<?php echo $form->textField($auth,'login',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($auth,'login'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($auth,'password'); ?>
		<?php echo $form->passwordField($auth,'password',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($auth,'password'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($auth,'role'); ?>
		<?php echo $form->dropDownList($auth,'role',$auth->roles); ?>
		<?php echo $form->error($auth,'role'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->