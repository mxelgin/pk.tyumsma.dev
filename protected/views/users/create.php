<?php
$this->menu=array(
	array('label'=>'Права доступа', 'url'=>array('admin')),
);
?>

<h1>Создать пользователя</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'auth'=>$auth)); ?>