<?php include_once('headers.php'); ?>
<?php
if ($_GET['type'] == 'csv') {
    $i = 1;
    echo "№;№ дела;ФИО абитуриента;Причина внеконкурса;Копия;Сумма баллов ЕГЭ;\r\n";
    if (!empty($entrants))
        foreach ($entrants as $e){
            echo $i++.';';
            echo $e->id.';';
            echo $e->fam.' '.$e->name.' '.$e->m_name.';';
            echo $e->vk_notice.';';
            if (!$e->orig) echo 'Да';
            echo ";";
            echo $e->rating.';';
            echo "\r\n";
        }
    die;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cписок абитуриентов.</title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
    border-collapse: collapse; 
    width: 70%;
}
table td.center{
    text-align: center;
}
table td{
    border: 1px solid black;
    padding: 4px;
}
table th{
    border: 1px solid black;
    padding: 4px;
    text-align: center;
}
@media print{
    .break{
        page-break-after: always;
    }
}
</style>
</head>

<body>

<h1>Список абитуриентов для зачисления на внеконкурсной основе</h1>

    <?php
        echo '<table class="bordertable">
                <tr>
                    <th>№</th>
                    <th>№ дела</th>
                    <th>ФИО абитуриента</th>
                    <th>Причина внеконкурса</th>
                    <th>Копия</th>
                    <th>Сумма баллов ЕГЭ</th>
                </tr>';
        $i = 1;
        if (!empty($entrants))
            foreach ($entrants as $e){
                echo '<tr>';
                    echo '<td class="center">'.$i++.'</td>';
                    echo '<td class="center">'.$e->id.'</td>';
                    echo '<td>'.$e->fam.' '.$e->name.' '.$e->m_name.'</td>';
                    echo '<td>'.$e->vk_notice.'</td>';
                    echo '<td class="center">';
                    if (!$e->orig) echo 'Да';
                    echo '</td>';
                    echo '<td>'.$e->rating.'</td>';
                echo '</tr>';
            }
        echo '</table><br />';
    ?>
<br />
<br />

Согласовано:        
<br />
<br />
        
Ответственный секретарь приёмной комиссии         ________________          / И.А. Трошина /        
<br />
<br />
        
Заместитель
<br />
ответственного секретаря приёмной комиссии        ________________          / Е.А. Бабакин /
</body>
</html>
<?php include_once('footer.php'); ?>
