<?php
$type = $_GET['type'];
switch ($type) {
case 'xls':
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/xls; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.xls"');
    header('Cache-Control: max-age=0');
    break;
case 'pdf':
    ob_start();
    break;
case 'csv':
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-type: application/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename="export.csv"');
    header('Cache-Control: max-age=0');
    break;
}
?>
