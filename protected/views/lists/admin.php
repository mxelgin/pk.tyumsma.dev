<?php $this->breadcrumbs[] = 'Списки абитуриентов'; ?>
<h1>Формирование списков для приказа</h1>

<ul>
<li><a target=_blank href=/lists/alpha>Алфавитный список</a> (<a target=_blank href=/lists/alpha?type=csv>CSV</a> <a target=_blank href=/lists/alpha?type=pdf>PDF</a>)</li>
</ul>

<h3>Список абитуриентов для зачисления на внеконкурсной основе</h3>
<ul>
<?php foreach ($specs as $s) {
$link = "/lists/vk?spec=$s->id";
$csv = $link . '&type=csv';
$pdf = $link . '&type=pdf';
echo "<li><a target=_blank href=$link>$s->name</a> (<a target=_blank href=$csv>CSV</a> <a target=_blank href=$pdf>PDF</a>)</li>";
}
?>
</ul>

<h3>Основной список абитуриентов для зачисления на бюджетные места</h3>
<ul>
<?php foreach ($specs as $s) {
$link = "/lists/budget?spec=$s->id";
$csv = $link . '&type=csv';
$pdf = $link . '&type=pdf';
echo "<li><a target=_blank href=$link>$s->name</a> (<a target=_blank href=$csv>CSV</a> <a target=_blank href=$pdf>PDF</a>)</li>";
}
?>
</ul>

<h3>Список абитуриентов рекомендованных для зачисления на бюджетные места</h3>
<ul>
<?php foreach ($specs as $s) {
$link = "/lists/budgetr?spec=$s->id";
$csv = $link . '&type=csv';
$pdf = $link . '&type=pdf';
echo "<li><a target=_blank href=$link>$s->name</a> (<a target=_blank href=$csv>CSV</a> <a target=_blank href=$pdf>PDF</a>)</li>";
}
?>
</ul>

<h3>Список абитуриентов для зачисления на внебюджетные места</h3>
<ul>
<?php foreach ($specs as $s) {
$link = "/lists/nebudgetr?spec=$s->id";
$csv = $link . '&type=csv';
$pdf = $link . '&type=pdf';
echo "<li><a target=_blank href=$link>$s->name</a> (<a target=_blank href=$csv>CSV</a> <a target=_blank href=$pdf>PDF</a>)</li>";
}
?>
</ul>

<h3>Список абитуриентов имеющих нероссийское гражданство</h3>
<ul>
<?php
$link = "/lists/noru?budget=1";
$csv = $link . '&type=csv';
$pdf = $link . '&type=pdf';
echo "<li><a target=_blank href=$link>Имеющие право на бюджет</a> (<a target=_blank href=$csv>CSV</a> <a target=_blank href=$pdf>PDF</a>)</li>";
$link = "/lists/noru?budget=0";
$csv = $link . '&type=csv';
$pdf = $link . '&type=pdf';
echo "<li><a target=_blank href=$link>Не имеющие право на бюджет</a> (<a target=_blank href=$csv>CSV</a> <a target=_blank href=$pdf>PDF</a>)</li>";
?>
</ul>

<h3>Список абитуриентов, имеющих регионально-целевые направления</h3>
<ul>
<?php foreach ($specs as $s) {
$link = "/lists/rcm?spec=$s->id";
$csv = $link . '&type=csv';
$pdf = $link . '&type=pdf';
echo "<li><a target=_blank href=$link>$s->name</a> (<a target=_blank href=$csv>CSV</a> <a target=_blank href=$pdf>PDF</a>)</li>";
}
?>
</ul>

<h3>Приказ по регионально-целевому набору</h3>
<ul>
<?php foreach ($specs as $s) {
$link = "/lists/prcm?spec=$s->id";
$csv = $link . '&type=csv';
$pdf = $link . '&type=pdf';
echo "<li><a target=_blank href=$link>$s->name</a> (<a target=_blank href=$csv>CSV</a> <a target=_blank href=$pdf>PDF</a>)</li>";
}
?>
</ul>
