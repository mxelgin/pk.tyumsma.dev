<?php include_once('headers.php'); ?>
<?php
if ($_GET['type'] == 'csv') {
    foreach ($head as $h)
        echo $h . ';';
    echo "\r\n";

    foreach ($data as $row) {
        foreach ($row as $d)
            echo $d . ';';
        echo "\r\n";
    }
    die;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title ?></title>
<style>
body{
	font-size:12pt;
	font-family:"Times New Roman","serif";
	margin:0;
	padding:0;
}
.section{
	width:1000px;
	margin: 0 auto;
	padding: 0 5px 0 5px;
}
.title{
	text-align:center;
}
table{
	font-size:12pt;
    border-collapse: collapse; 
    width: 70%;
}
table td.center{
    text-align: center;
}
table td{
    border: 1px solid black;
    padding: 4px;
}
table th{
    border: 1px solid black;
    padding: 4px;
    text-align: center;
}
@media print{
    .break{
        page-break-after: always;
    }
}
</style>
</head>

<body>
<h1><?php echo $title ?></h1>

    <?php
        echo '<table class="bordertable">';
        echo '<tr>';
        foreach ($head as $h)
            echo "<th>$h</th>";
        echo '</tr>';

        foreach ($data as $row) {
            echo '<tr>';
            foreach ($row as $d)
                echo "<td>$d</td>";
            echo '</tr>';
        }
        echo '</table><br />';
    ?>
<br />
<br />

Согласовано:        
<br />
<br />
        
Ответственный секретарь приёмной комиссии         ________________          / И.А. Трошина /        
<br />
<br />
        
Заместитель
<br />
ответственного секретаря приёмной комиссии        ________________          / Е.А. Бабакин /
</body>
</html>
<?php include_once('footer.php'); ?>
