<?php
$this->breadcrumbs=array(
	'Kladr Statuses'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Создать KladrStatus', 'url'=>array('create')),
	array('label'=>'Изменить KladrStatus', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить KladrStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление KladrStatus', 'url'=>array('admin')),
);
?>

<h1>Просмотр KladrStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
