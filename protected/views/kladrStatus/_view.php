<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo $data->id; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

<?php echo CHtml::link("Изменить", array("update", "id"=>$data->id)); ?>
  <?php echo CHtml::link("Удалить", array("delete", "id"=>$data->id),array("confirm"=>"Вы уверены, что хотите удалить данный элемент?")); ?>
</div>