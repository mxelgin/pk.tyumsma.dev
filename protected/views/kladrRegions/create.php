<?php
$this->breadcrumbs=array(
	'Kladr Regions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List KladrRegions', 'url'=>array('index')),
	array('label'=>'Manage KladrRegions', 'url'=>array('admin')),
);
?>

<h1>Create KladrRegions</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>