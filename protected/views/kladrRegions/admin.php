<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kladr-regions-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Регионы</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'kladr-regions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'index',
		array('header'=>'Тип','name'=>'type.fullname'),
		'name',            
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{update}',
		),
	),
)); ?>
