<?php

$this->menu=array(
	array('label'=>'Create KladrRegions', 'url'=>array('create')),
	array('label'=>'Manage KladrRegions', 'url'=>array('admin')),
);
?>

<h1>Kladr Regions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
