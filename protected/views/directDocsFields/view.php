<?php
$this->breadcrumbs=array(
	'Direct Docs Fields'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectDocsFields', 'url'=>array('index')),
	array('label'=>'Create DirectDocsFields', 'url'=>array('create')),
	array('label'=>'Update DirectDocsFields', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectDocsFields', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectDocsFields', 'url'=>array('admin')),
);
?>

<h1>View DirectDocsFields #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'docid',
		'name',
		'type',
		'required',
	),
)); ?>
