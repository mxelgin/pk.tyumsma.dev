<?php
$this->breadcrumbs=array(
	'Direct Docs Fields',
);

$this->menu=array(
	array('label'=>'Create DirectDocsFields', 'url'=>array('create')),
	array('label'=>'Manage DirectDocsFields', 'url'=>array('admin')),
);
?>

<h1>Direct Docs Fields</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
