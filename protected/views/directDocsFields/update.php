<div class="updatefields">
    <?php foreach ($fields as $field){ ?>
    <div class="field-row">
        <div class="row">
		<?php echo CHtml::label('Название поля','ufield-name_'.$field->id); ?>
		<?php echo CHtml::textField('ufield-name['.$field->id.']', $field->name,array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Тип поля','ufield-type_'.$field->id);?>
		<?php echo CHtml::dropDownList('ufield-type['.$field->id.']', $field->type, $fieldtypes); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Обязательное','ufield-required_'.$field->id); ?>
		<?php echo CHtml::checkBox('ufield-required['.$field->id.']' ,$field->required); ?>
	</div>

        <div class="row">
		<?php echo CHtml::label('Только для редактирования','field-editonly_'.$field->id); ?>
		<?php echo CHtml::checkBox('ufield-editonly['.$field->id.']' ,$field->editonly); ?>
	</div>
    </div>        
    <?php } ?>
</div>
<?php 
if (count($fields)>0) $hidden='hidden';
else $hidden='';
?>
<div class="first <?php echo $hidden; ?>">
    <div class="field-row">
        <div class="row">
		<?php echo CHtml::label('Название поля','field-name_0'); ?>
		<?php echo CHtml::textField('field-name[0]', '',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Тип поля','field-type_0');?>
		<?php echo CHtml::dropDownList('field-type[0]', 'text', $fieldtypes); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label('Обязательное','field-required_0'); ?>
		<?php echo CHtml::checkBox('field-required[0]' ,''); ?>
	</div>
        
        <div class="row">
		<?php echo CHtml::label('Только для редактирования','field-editonly_0'); ?>
		<?php echo CHtml::checkBox('field-editonly[0]' ,''); ?>
	</div>
    </div>
</div>
<div class="more">
</div>
        <div class="row">
		<?php echo CHtml::button('Добавить еще поле' ,array('onclick'=>'js:dublicaterow(".field-row", ".more");')); ?>
	</div>