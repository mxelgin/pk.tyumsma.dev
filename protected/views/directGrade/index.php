<?php
$this->breadcrumbs=array(
	'Direct Grades',
);

$this->menu=array(
	array('label'=>'Create DirectGrade', 'url'=>array('create')),
	array('label'=>'Manage DirectGrade', 'url'=>array('admin')),
);
?>

<h1>Direct Grades</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
