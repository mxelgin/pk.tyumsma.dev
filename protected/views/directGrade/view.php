<?php
$this->breadcrumbs=array(
	'Direct Grades'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectGrade', 'url'=>array('index')),
	array('label'=>'Create DirectGrade', 'url'=>array('create')),
	array('label'=>'Update DirectGrade', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectGrade', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectGrade', 'url'=>array('admin')),
);
?>

<h1>View DirectGrade #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
