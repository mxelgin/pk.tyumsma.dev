<?php
$this->breadcrumbs=array(
	'Config Rcms',
);

$this->menu=array(
	array('label'=>'Create ConfigRcm', 'url'=>array('create')),
	array('label'=>'Manage ConfigRcm', 'url'=>array('admin')),
);
?>

<h1>Config Rcms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
