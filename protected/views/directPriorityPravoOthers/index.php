<?php
$this->breadcrumbs=array(
	'Direct Priority Pravo Others',
);

$this->menu=array(
	array('label'=>'Create DirectPriorityPravoOthers', 'url'=>array('create')),
	array('label'=>'Manage DirectPriorityPravoOthers', 'url'=>array('admin')),
);
?>

<h1>Direct Priority Pravo Others</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
