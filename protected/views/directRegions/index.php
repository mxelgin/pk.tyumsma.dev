<?php
$this->breadcrumbs=array(
	'Direct Regions',
);

$this->menu=array(
	array('label'=>'Create DirectRegions', 'url'=>array('create')),
	array('label'=>'Manage DirectRegions', 'url'=>array('admin')),
);
?>

<h1>Direct Regions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
