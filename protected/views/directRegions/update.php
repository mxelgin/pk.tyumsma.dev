<?php
$this->menu=array(
	array('label'=>'Добавить область', 'url'=>array('create')),
	array('label'=>'Управление областями', 'url'=>array('admin')),
);
?>

<h1>Изменение области</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>