<?php
$this->menu=array(
//	array('label'=>'List Log', 'url'=>array('index')),
//	array('label'=>'Create Log', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('log-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Журнал изменений</h1>

<p>
Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) для поиска нужных позиций.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'log-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'dt',
		'ip',
		'userid',
        /*
        array(
            'class' => 'CLinkColumn',
            'labelExpression' => '$data->userid',
            'urlExpression'=>'"users/view?id=".$data->userid',
            'header' => 'Пользователь'
        ),
        */
		'model',
		'action',
        'comment',
        array(
            'class' => 'CLinkColumn',
            //'labelExpression' => '$data->comment',
            'label' => '-->',
            'urlExpression'=>'$data->model."/view?id=".$data->objectid',
            'header' => 'Ссылка',
            //'filterHtmlOptions' => array( '',)
        ),
		/*
		array(
			'class'=>'CButtonColumn',
            'template'=>'{update}',
		),
		*/
	),
)); ?>
