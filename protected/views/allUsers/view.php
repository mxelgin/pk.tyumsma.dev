<?php
$this->breadcrumbs=array(
	'All Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AllUsers', 'url'=>array('index')),
	array('label'=>'Create AllUsers', 'url'=>array('create')),
	array('label'=>'Update AllUsers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AllUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AllUsers', 'url'=>array('admin')),
);
?>

<h1>View AllUsers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'fio',
		'id',
		'podr',
		'appointment',
	),
)); ?>
