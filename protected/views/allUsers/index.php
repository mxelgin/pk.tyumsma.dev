<?php
$this->breadcrumbs=array(
	'All Users',
);

$this->menu=array(
	array('label'=>'Create AllUsers', 'url'=>array('create')),
	array('label'=>'Manage AllUsers', 'url'=>array('admin')),
);
?>

<h1>All Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
