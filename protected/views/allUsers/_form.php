<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'all-users-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fio'); ?>
		<?php echo $form->textField($model,'fio',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'fio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'podr'); ?>
		<?php echo $form->textField($model,'podr',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'podr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'appointment'); ?>
		<?php echo $form->textField($model,'appointment',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'appointment'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->