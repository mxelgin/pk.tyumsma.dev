<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fio')); ?>:</b>
	<?php echo CHtml::encode($data->fio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('podr')); ?>:</b>
	<?php echo CHtml::encode($data->podr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('appointment')); ?>:</b>
	<?php echo CHtml::encode($data->appointment); ?>
	<br />


</div>