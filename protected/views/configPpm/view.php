<?php
$this->breadcrumbs=array(
	'Config Ppms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ConfigPpm', 'url'=>array('index')),
	array('label'=>'Create ConfigPpm', 'url'=>array('create')),
	array('label'=>'Update ConfigPpm', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ConfigPpm', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ConfigPpm', 'url'=>array('admin')),
);
?>

<h1>View ConfigPpm #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'specid',
		'value',
	),
)); ?>
