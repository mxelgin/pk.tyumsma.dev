<?php
$this->breadcrumbs=array(
	'Config Ppms',
);

$this->menu=array(
	array('label'=>'Create ConfigPpm', 'url'=>array('create')),
	array('label'=>'Manage ConfigPpm', 'url'=>array('admin')),
);
?>

<h1>Config Ppms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
