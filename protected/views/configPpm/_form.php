<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'config-ppm-form',
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля отмеченнные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'specid'); ?>
        <?php echo $form->dropDownList($model, 'specid', CHtml::listData(Departments::model()->findAll(), 'id', 'name')); ?>
        <?php echo $form->error($model,'specid'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value'); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->