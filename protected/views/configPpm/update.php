<?php
$this->menu=array(
    array('label'=>'Добавить значение', 'url'=>array('create')),
    array('label'=>'Управление значениями', 'url'=>array('admin')),
);
?>

    <h1>Изменение значения</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>