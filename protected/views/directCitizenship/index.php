<?php
$this->breadcrumbs=array(
	'Direct Citizenships',
);

$this->menu=array(
	array('label'=>'Create DirectCitizenship', 'url'=>array('create')),
	array('label'=>'Manage DirectCitizenship', 'url'=>array('admin')),
);
?>

<h1>Direct Citizenships</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
