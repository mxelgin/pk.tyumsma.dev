<?php
$this->breadcrumbs=array(
	'Direct Citizenships'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectCitizenship', 'url'=>array('index')),
	array('label'=>'Create DirectCitizenship', 'url'=>array('create')),
	array('label'=>'Update DirectCitizenship', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectCitizenship', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectCitizenship', 'url'=>array('admin')),
);
?>

<h1>View DirectCitizenship #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'required',
	),
)); ?>
