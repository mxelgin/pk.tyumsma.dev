<?php
$this->menu=array(
	array('label'=>'Добавить гражданство', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('direct-citizenship-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление гражданствами</h1>

<p>
Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) для поиска нужных позиций.
</p>

<?php echo CHtml::link('Расширенный поиск','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'direct-citizenship-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
        array(
            'name' => 'is_budget',
            'header' => "Право на бюджет",
            'value' => '$data->is_budget?Yii::t(\'app\',\'Есть\'):Yii::t(\'app\', \'\')',
            'filter' => array('0' => Yii::t('app', 'Нет'), '1' => Yii::t('app', 'Есть')),
            'htmlOptions' => array('style' => "text-align:center;"),
        ),
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{update}{delete}',
                        'buttons'=>array('delete'=>array('visible'=>'($data->required)?false:true;')),
		),
	),
)); ?>
