<?php
$this->menu=array(
	array('label'=>'Добавить гражданство', 'url'=>array('create')),
	array('label'=>'Управление гражданствами', 'url'=>array('admin')),
);
?>

<h1>Изменение гражданства</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>