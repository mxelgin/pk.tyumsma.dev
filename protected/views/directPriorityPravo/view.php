<?php
$this->breadcrumbs=array(
	'Direct Priority Pravos'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DirectPriorityPravo', 'url'=>array('index')),
	array('label'=>'Create DirectPriorityPravo', 'url'=>array('create')),
	array('label'=>'Update DirectPriorityPravo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DirectPriorityPravo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DirectPriorityPravo', 'url'=>array('admin')),
);
?>

<h1>View DirectPriorityPravo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'weight',
	),
)); ?>
