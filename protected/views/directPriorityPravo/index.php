<?php
$this->breadcrumbs=array(
	'Direct Priority Pravos',
);

$this->menu=array(
	array('label'=>'Create DirectPriorityPravo', 'url'=>array('create')),
	array('label'=>'Manage DirectPriorityPravo', 'url'=>array('admin')),
);
?>

<h1>Direct Priority Pravos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
