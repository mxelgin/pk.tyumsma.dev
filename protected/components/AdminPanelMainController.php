<?php

class AdminPanelMainController extends Controller
{
	public $layout='//layouts/column2';
	public $menu=array();
	public $breadcrumbs=array('Панель управления'=>'/adminPanel');
        
    public $submenu =  array(
        "Общее"=>"/pkconfig",
        "Права доступа"=>"/users",
        "Факультеты"=>"/departments",
        "Специальности"=>"/specialty",
        "Заявленнные РЦМ"=>"/configRcm",
        "Бюджетные места"=>"/configBm",
        "Договорные места"=>"/configDm",
        "Места вне конкурса"=>"/configPpm",
        "Места по предметам"=>"/configGm",
        "Журнал изменений"=>"/log",
//        "Страны"=>"/countries",
//        "Сессии"=>"/pkConfig",
        );
}

?>
