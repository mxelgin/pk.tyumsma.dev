<?php

class DirectoryMainController extends Controller
{
	public $layout='//layouts/column2';
	public $menu=array();
	public $breadcrumbs=array('Справочники'=>'/directory');
        
    public $submenu =  array(
        //"Города"=>"/directCity",
        //"Области"=>"/directRegions", 
        'Документы'=>'/directDocs',
        'Гражданство'=>'/directCitizenship',
        'Уч. заведения'=>'/directEducation',
        'Иностранные языки'=>'/directForeign',
        'Медали'=>'/directMedal',
        'Регионы РЦМ'=>'/directRcmRegions',
        'Регионы' => '/kladrRegions',
        'Районы' => '/kladrRaions',
        'Города' => '/kladr',
        'Улицы' => '/kladrStreet',
        'Преимущественное право' => '/directPriorityPravo',
        'Преимущественное право при прочих равных условиях' => '/directPriorityPravoOthers',
        'Предметы' => '/directGrade'
    );
}

?>
