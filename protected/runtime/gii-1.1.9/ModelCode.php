<?php
return array (
  'template' => 'default',
  'tablePrefix' => 'pk_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
);
