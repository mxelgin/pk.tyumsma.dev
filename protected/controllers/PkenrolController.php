<?php

class PkenrolController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction='admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','list', 'preview1', 'preview2', 'print', 'previewAlph','printAlph', 'preview3', 'preview33'),
				'roles'=>array('operator'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				//'actions'=>array('enrol1', 'enrol2', 'enrol22', 'import'),
                                'actions'=>array('enrol1', 'enrol2', 'enrol22', 'enrol3', 'enrol33', 'unenrol'),
				'roles'=>array('administrator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        public function actionPreview1(){
 			$this->render('preview',$this->arrView());
        }	
        public function actionPreviewAlph(){
        	$this->render('previewAlph',$this->arrView());
        }
        
		private function arrView(){
                $this->layout=false;
                $model= new Entrant;
                
                $minb=array(
                    1=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    2=>array('him'=>60, 'bio'=>50, 'rus'=>50),
                    3=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    4=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    5=>array('him'=>40, 'bio'=>38, 'rus'=>38),
                    6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                    7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                );
                $mind=array(
                    1=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    2=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    3=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    4=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                    5=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                    6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                    7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                );                
                
                $specs=Departments::model()->findAll();
                $regions=DirectRcmRegions::model()->findAll(array('order'=>'ord'));
                $regname=array();
                foreach ($regions as $reg) $regname[$reg->id]=$reg->name;
                $entrantsvkb=array();
                $entrantsvkd=array();
                $entrantsrcm=array();
                $entrants=array();
                foreach ($specs as $spec){
                    //вне конкурса
                    $entrantsvkb[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and wave_first=0 and wave_second=0 and wave_third=0 and specs.specid=".$spec->id." and specs.type_b=1 and specs.level=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>=".$minb[$spec->id]['rus']." and grade_bio>=".$minb[$spec->id]['bio']." and grade_him>=".$minb[$spec->id]['him']." and vk=1 and rcm.entrantid is null", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                    $entrantsvkd[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and wave_first=0 and wave_second=0 and wave_third=0 and specs.specid=".$spec->id." and specs.type_d=1 and specs.level=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>=".$mind[$spec->id]['rus']." and grade_bio>=".$mind[$spec->id]['bio']." and grade_him>=".$mind[$spec->id]['him']." and vk=1 and rcm.entrantid is null", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                    //рцм
                    foreach ($regions as $rrcm){
                        $limit=ConfigRcm::model()->find('regionid='.$rrcm->id.' and specid='.$spec->id);
                        $entrantsrcm[$spec->id][$rrcm->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and wave_first=0 and wave_second=0 and wave_third=0 and specs.specid=".$spec->id." and specs.level=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>=".$minb[$spec->id]['rus']." and grade_bio>=".$minb[$spec->id]['bio']." and grade_him>=".$minb[$spec->id]['him']." and rcm.entrantid is not null and rcm.regionid=".$rrcm->id, 'order'=>'vk DESC, rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC', 'together'=>true, 'limit'=>$limit->value));
                    }
                    
                    //$entrantsb_first[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and wave_first=0 and wave_second=0 and wave_third=0 and specs.specid=".$spec->id." and specs.level=1 and vk=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>=".$minb[$spec->id]['rus']." and grade_bio>=".$minb[$spec->id]['bio']." and grade_him>=".$minb[$spec->id]['him']." and rcm.entrantid is null", 'order'=>'rating DESC, grade_him DESC, edu_grade'));
                }
			return array(
					'specs'=>$specs, 
					'entrants'=>$entrants, 
					'entrantsvkb'=>$entrantsvkb, 
					'entrantsvkd'=>$entrantsvkd,
					'entrantsrcm'=>$entrantsrcm, 
					'minb'=>$minb, 
					'mind'=>$mind, 
					'regname'=>$regname,
			);
        }
        
        private function arrPrint(){
        	$this->layout=false;
        	$model= new Entrant;
        	$wave=(int)$_GET['wave'];
        	$type=(int)$_GET['type'];
        	if (empty($type)) $type=1;
        	if (empty($wave)) $wave=1;
        	if ($wave==1) $wave_sql='wave_first';
        	elseif ($wave==2) $wave_sql='wave_second';
        	elseif ($wave==3) $wave_sql='wave_third';
        	
        	if ($model->exists($wave_sql.'>0 and enrol_type='.$type)){
        		$specs=Departments::model()->findAll();
        		$regions=DirectRcmRegions::model()->findAll(array('order'=>'ord'));
        		$regname=array();
        		foreach ($regions as $reg) $regname[$reg->id]=$reg->name;
        		//$entrantsvkb=array();
        		//$entrantsvkd=array();
        		$entrantsrcm=array();
        		$entrants=array();
        		foreach ($specs as $spec){
        			foreach ($regions as $rrcm){
        				$entrantsrcm[$spec->id][$rrcm->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and ".$wave_sql."=".$spec->id." and enrol_type=".$type." and rcm.entrantid is not null and rcm.regionid=".$rrcm->id, 'order'=>'vk DESC, rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC, fam ASC', 'together'=>true));
        			}
        	
        			$entrants[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and ".$wave_sql."=".$spec->id." and enrol_type=".$type." and rcm.entrantid is null", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC, fam ASC'));
        		}
        	}
        	return array(
        			'specs'=>$specs,
        			'entrants'=>$entrants,
        			'entrantsrcm'=>$entrantsrcm,
        			'regname'=>$regname,
        	);
        	 
        }

        public function actionUnenrol(){
            $model= new Entrant;
            if ($model->exists('wave_first>0')){
                if (!$model->exists('wave_second>0 and enrol_type=1')){
                    $model-> updateAll(array('enrol_type' => 0, 'wave_first' => 0),'wave_first>0');
                }
            }
            if ($model->exists('wave_second>0 and enrol_type=1')){
                if (!$model->exists('wave_third>0 and enrol_type=1')){
                    $model-> updateAll(array('enrol_type' => 0, 'wave_second' => 0),'wave_second>0');
                }
            }
            if ($model->exists('wave_third>0 and enrol_type=1')){
                if (!$model->exists('wave_third>0 and enrol_type=2')){
                    $model-> updateAll(array('enrol_type' => 0, 'wave_third' => 0),'wave_third>0 and enrol_type=1');
                }
            }
           if ($model->exists('wave_third>0 and enrol_type=2')){
               $model-> updateAll(array('enrol_type' => 0, 'wave_third' => 0),'wave_third>0 and enrol_type=2');
            }
            $this->actionAdmin();
        }

        public function actionEnrol1(){
                $this->layout=false;
                $model= new Entrant;
                $exist=0;
                
                if (!$model->exists('wave_first>0')){
                    $minb=array(
                        1=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        2=>array('him'=>60, 'bio'=>50, 'rus'=>50),
                        3=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        4=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        5=>array('him'=>40, 'bio'=>38, 'rus'=>38),
                        6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                        7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                    );
                    $mind=array(
                        1=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                        2=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                        3=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                        4=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                        5=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                        6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                        7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                    );                

                    $specs=Departments::model()->findAll();
                    $regions=DirectRcmRegions::model()->findAll(array('order'=>'ord'));
                    $regname=array();
                    foreach ($regions as $reg) $regname[$reg->id]=$reg->name;
                    $entrantsvkb=array();
                    $entrantsvkd=array();
                    $entrantsrcm=array();
                    $entrants=array();
                    $fail = array();
                    foreach ($specs as $spec){
                        //вне конкурса
                        $entrantsvkb[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and wave_first=0 and wave_second=0 and wave_third=0 and specs.specid=".$spec->id." and specs.type_b=1 and specs.level=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>=".$minb[$spec->id]['rus']." and grade_bio>=".$minb[$spec->id]['bio']." and grade_him>=".$minb[$spec->id]['him']." and vk=1 and rcm.entrantid is null", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, grade_rus DESC, edu_grade DESC'));
                        //рцм
                        foreach ($regions as $rrcm){
                            $limit=ConfigRcm::model()->find('regionid='.$rrcm->id.' and specid='.$spec->id);
                            $entrantsrcm[$spec->id][$rrcm->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and wave_first=0 and wave_second=0 and wave_third=0 and specs.specid=".$spec->id." and specs.level=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>=".$minb[$spec->id]['rus']." and grade_bio>=".$minb[$spec->id]['bio']." and grade_him>=".$minb[$spec->id]['him']." and rcm.entrantid is not null and rcm.regionid=".$rrcm->id, 'order'=>'vk DESC, rating DESC, grade_him DESC, grade_bio DESC, grade_rus DESC, achv DESC, edu_grade, medblock, t.id', 'together'=>true, 'limit'=>$limit->value));

                            $last_entrantsrcm = $entrantsrcm[$spec->id][$rrcm->id][$limit->value-1];
                            $entrantsrcm_double[$spec->id][$rrcm->id]=$model->findAll(array('with'=>array('rcm', 'specs'),
                                'condition'=>"deleted=0 and took=0
                                and wave_first=0 and wave_second=0 and wave_third=0
                                and specs.specid=".$spec->id." and specs.level=1
                                and rating    = ". intval($last_entrantsrcm->rating)."
                                and grade_rus = ". intval($last_entrantsrcm->grade_rus)."
                                and grade_him = ". intval($last_entrantsrcm->grade_him)."
                                and grade_bio = ". intval($last_entrantsrcm->grade_bio)."
                                and achv      = ". intval($last_entrantsrcm->achv)."
                                and edu_grade = ". intval($last_entrantsrcm->edu_grade)."
                                and medblock  = ". intval($last_entrantsrcm->medblock)."
                                and rcm.entrantid is not null
                                and rcm.regionid=".$rrcm->id."
                                and t.id > ". intval($last_entrantsrcm->id),
                                'order'=>'vk DESC, rating DESC, grade_him DESC, grade_bio DESC, grade_rus DESC,
                                achv DESC, edu_grade, medblock'));
                            {
                                $e = $model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and orig=1 and wave_first=0 and wave_second=0 and wave_third=0 and specs.specid=".$spec->id." and specs.level=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>=".$minb[$spec->id]['rus']." and grade_bio>=".$minb[$spec->id]['bio']." and grade_him>=".$minb[$spec->id]['him']." and rcm.entrantid is not null and rcm.regionid=".$rrcm->id, 'order'=>'vk DESC, rating DESC, grade_him DESC, edu_grade DESC', 'together'=>true, 'limit'=>$limit->value + 10));
                                $l = $limit->value - 1;
                                if (count($e) > $l)
                                    $r = $e[$l]->rating;
                                else
                                    $r = 0;

                                $c = count($e);

                                //echo "<pre>$r\n\n";
                                //echo "$c\n";

                                //foreach ($e as $ee) print_r($ee->fam . ' ' . $ee->rating . "\n");

                                for ($i = 0; $i < $c; $i++) if ($r != $e[$i]->rating) unset($e[$i]);

                                $ok = false;
                                foreach ($e as $k => $v) if ($k > $l) $ok = true;
                                if (!$ok) unset($e);

                                if (count($e) == 1) unset($e);

                                //echo count($e) . "\n";
                                //foreach ($e as $ee) print_r($ee->fam . ' ' . $ee->rating . "\n");

                                //$fail[$spec->id][$rrcm->id] = $e;
                            }
                        }

                        //$entrantsb_first[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and orig=1 and wave_first=0 and wave_second=0 and wave_third=0 and specs.specid=".$spec->id." and specs.level=1 and vk=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>=".$minb[$spec->id]['rus']." and grade_bio>=".$minb[$spec->id]['bio']." and grade_him>=".$minb[$spec->id]['him']." and rcm.entrantid is null", 'order'=>'rating DESC, grade_him DESC, edu_grade'));
                    }
                    $ok = false;
                    if (!empty($_GET['yes'])) {
                        /*
                        echo '<pre>'; print_r($fail);
                        die;
                        */
                        /*foreach ($fail as $f) 
                            foreach ($f as $s) 
                                if (!empty($s)) {
                                    $ok = true;
                                    break;
                                }
                        if ($ok){
                            $entrantsrcm = $fail;
                            unset($entrantsvkb);
                        }
                        unset($_GET['yes']);*/
                    }
                }
                $this->render('enrol1',array(
                        'specs'=>$specs, 'yes'=>$_GET['yes'], 'fail'=>$ok, 'exist'=>$exist, 'entrants'=>$entrants, 'entrantsvkb'=>$entrantsvkb, 'entrantsvkd'=>$entrantsvkd,'entrantsrcm'=>$entrantsrcm,'entrantsrcm_double'=>$entrantsrcm_double, 'minb'=>$minb, 'mind'=>$mind, 'regname'=>$regname,
                ));
        }
        
        public function actionPreview2(){
                $this->layout=false;
                $model= new Entrant;
                $exist=0;
                                
                if ($model->exists('wave_second>0')){
                    $exist=1;
                }else{
                    $minb=array(
                        1=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        2=>array('him'=>60, 'bio'=>50, 'rus'=>50),
                        3=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        4=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        5=>array('him'=>40, 'bio'=>38, 'rus'=>38),
                        6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                        7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                    );

                    $specsBM=ConfigBm::model()->findAll();
                    $bm=array();
                    foreach ($specsBM as $sbm) $bm[$sbm->specid]=$sbm->value;
                    $specs=Departments::model()->findAll();
                    $fwcounts=array();
                    foreach ($specs as $spec) $fwcounts[$spec->id]=Entrant::model()->count('wave_first='.$spec->id);
                    
                    $list_b=array();
                    $rekomend_b=array();
                    $hvost_b=array();
                    
                    $entrants=$model->findAll(array('condition'=>"deleted=0 and took=0 and wave_first=0 and wave_second=0 and wave_third=0 and vk=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>0 and grade_bio>0 and grade_him>0", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                    $emptyplace=array(1=>0, 2=>0, 3=>0, 4=>0, 5=>0, 6=>0);
                    foreach ($entrants as $entrant){
                        $in=0;
                        foreach ($entrant->specs as $spec){
                            if ($entrant->grade_him<$minb[$spec->specid]['him'] || $entrant->grade_bio<$minb[$spec->specid]['bio'] || $entrant->grade_rus<$minb[$spec->specid]['rus']) continue;
                            if ($spec->type_b==1 && count($list_b[$spec->specid])<($bm[$spec->specid]-$fwcounts[$spec->specid])){
                                $list_b[$spec->specid][]=$entrant;
                                $in=1;
                                if (empty($entrant->orig)) $emptyplace[$spec->specid]++;
                                break;
                            }
                        }
                        if ($in==0){
                            foreach ($entrant->specs as $spec){
                                if ($entrant->grade_him<$minb[$spec->specid]['him'] || $entrant->grade_bio<$minb[$spec->specid]['bio'] || $entrant->grade_rus<$minb[$spec->specid]['rus']) continue;
                                if ($emptyplace[$spec->specid]>0 && $spec->type_b==1){
                                    $rekomend_b[$spec->specid][]=$entrant;
                                    $emptyplace[$spec->specid]--;
                                    $in=1;
                                    break;
                                }
                            }
                        }
                        if ($in==0){
                            foreach ($entrant->specs as $spec){
                                if ($entrant->grade_him<$minb[$spec->specid]['him'] || $entrant->grade_bio<$minb[$spec->specid]['bio'] || $entrant->grade_rus<$minb[$spec->specid]['rus']) continue;
                                if ($spec->type_b==1){
                                    $hvost_b[$spec->specid][]=$entrant;
                                }
                            }
                        }
                    }
                }
                $this->render('preview2',array(
                        'list_b'=>$list_b, 'specs'=>$specs, 'hvost_b'=>$hvost_b, 'rekomend_b'=>$rekomend_b
                ));
        }
        
        public function actionPreview3(){
                $this->layout=false;
                $model= new Entrant;
                $exist=0;
                                
                if ($model->exists('wave_third>0')){
                    $exist=1;
                }else{
                    $minb=array(
                        1=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        2=>array('him'=>60, 'bio'=>50, 'rus'=>50),
                        3=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        4=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                        5=>array('him'=>40, 'bio'=>38, 'rus'=>38),
                        6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                        7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                    );

                    $specsBM=ConfigBm::model()->findAll();
                    $bm=array();
                    foreach ($specsBM as $sbm) $bm[$sbm->specid]=$sbm->value;
                    $specs=Departments::model()->findAll();
                    $fwcounts=array();
                    foreach ($specs as $spec) $fwcounts[$spec->id]=Entrant::model()->count('wave_first='.$spec->id.' or wave_second='.$spec->id);
                    
                    $list_b=array();
                    
                    $entrants=$model->findAll(array('condition'=>"deleted=0 and took=0 and orig>0 and wave_first=0 and wave_second=0 and wave_third=0 and vk=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>0 and grade_bio>0 and grade_him>0", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                    foreach ($entrants as $entrant){
                        foreach ($entrant->specs as $spec){
                            if ($entrant->grade_him<$minb[$spec->specid]['him'] || $entrant->grade_bio<$minb[$spec->specid]['bio'] || $entrant->grade_rus<$minb[$spec->specid]['rus']) continue;
                            if ($spec->type_b==1 && count($list_b[$spec->specid])<($bm[$spec->specid]-$fwcounts[$spec->specid])){
                                $list_b[$spec->specid][]=$entrant;
                                break;
                            }
                        }
                    }
                }
                $this->render('preview3',array(
                        'list_b'=>$list_b, 'specs'=>$specs
                ));
        }
        
        public function actionPreview33(){
                $this->layout=false;
                $model= new Entrant;
                $exist=0;
                                
                if ($model->exists('wave_third>0 and enrol_type=2')){
                    $exist=1;
                }else{
                    $mind=array(
                        1=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                        2=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                        3=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                        4=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                        5=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                        6=>array('him'=>0, 'bio'=>0, 'rus'=>0),
                        7=>array('him'=>0, 'bio'=>0, 'rus'=>0)
                    );                

                    $specsDM=ConfigDm::model()->findAll();
                    $dm=array();
                    foreach ($specsDM as $sdm) $dm[$sdm->specid]=$sdm->value;
                    $specs=Departments::model()->findAll();
                    $fwcounts=array();
                    foreach ($specs as $spec) $fwcounts[$spec->id]=Entrant::model()->count('(wave_first='.$spec->id.' or wave_second='.$spec->id.') and enrol_type=2');
                    
                    $list_d=array();
                    
                    $entrants=$model->findAll(array('condition'=>"deleted=0 and took=0 and orig>0 and wave_first=0 and wave_second=0 and wave_third=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>0 and grade_bio>0 and grade_him>0", 'order'=>'vk DESC, rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                    foreach ($entrants as $entrant){
                        foreach ($entrant->specs as $spec){
                            if ($entrant->grade_him<$mind[$spec->specid]['him'] || $entrant->grade_bio<$mind[$spec->specid]['bio'] || $entrant->grade_rus<$mind[$spec->specid]['rus']) continue;
                            if ($spec->type_d==1 && count($list_d[$spec->specid])<($dm[$spec->specid]-$fwcounts[$spec->specid])){
                                $list_d[$spec->specid][]=$entrant;
                                break;
                            }
                        }
                    }
                }
                $this->render('preview33',array(
                        'list_d'=>$list_d, 'specs'=>$specs
                ));
        }
        
        public function actionPrint(){
            $this->render('print',$this->arrPrint());
                    }

        public function actionPrintAlph(){
            $this->render('print',$this->arrPrint());
                }
        
        public function actionEnrol2(){
                $this->layout=false;
                $model= new Entrant;
                $enrol=$_POST['entrant'];
                $showspec=(int)$_POST['spec'];
                
                if (!empty($enrol)){
                    foreach ($enrol as $entrantid=>$specid)
                        $model->updateByPk($entrantid, array('wave_second'=>$specid, 'enrol_type'=>1));
                }
                                
                $minb=array(
                    1=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    2=>array('him'=>60, 'bio'=>50, 'rus'=>50),
                    3=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    4=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    5=>array('him'=>40, 'bio'=>38, 'rus'=>38),
                    6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                    7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                );

                $specsBM=ConfigBm::model()->findAll();
                $bm=array();
                foreach ($specsBM as $sbm) $bm[$sbm->specid]=$sbm->value;
                $specs=Departments::model()->findAll();
                $fwcounts=array();
                foreach ($specs as $spec) $fwcounts[$spec->id]=Entrant::model()->count('wave_first='.$spec->id);

                $prikaz=array();
                $list_b=array();
                $rekomend_b=array();
                $hvost_b=array();

                $p_entrants=$model->findAll(array('condition'=>'wave_second>0 and enrol_type=1', 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                foreach ($p_entrants as $p_entant){
                    $prikaz[$p_entant->wave_second][]=$p_entant;
                }
                $entrants=$model->findAll(array('condition'=>"deleted=0 and took=0 and wave_first=0 and wave_third=0 and vk=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>0 and grade_bio>0 and grade_him>0", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, grade_rus DESC, achv DESC, edu_grade DESC, medblock'));
                $emptyplace=array(1=>0, 2=>0, 3=>0, 4=>0, 5=>0, 6=>0);
                foreach ($entrants as $entrant){
                    $in=0;
                    foreach ($entrant->specs as $spec){
                        if (!empty($prikaz[$spec->specid])) break;
                        if ($entrant->grade_him<$minb[$spec->specid]['him'] || $entrant->grade_bio<$minb[$spec->specid]['bio'] || $entrant->grade_rus<$minb[$spec->specid]['rus']) continue;
                        if ($spec->type_b==1 && count($list_b[$spec->specid])<($bm[$spec->specid]-$fwcounts[$spec->specid])){
                            $list_b[$spec->specid][]=$entrant;
                            $in=1;
                            if (empty($entrant->orig)) $emptyplace[$spec->specid]++;
                            break;
                        }
                    }
                    if ($in==0){
                        foreach ($entrant->specs as $spec){
                            if (!empty($prikaz[$spec->specid])) break;
                            if ($entrant->grade_him<$minb[$spec->specid]['him'] || $entrant->grade_bio<$minb[$spec->specid]['bio'] || $entrant->grade_rus<$minb[$spec->specid]['rus']) continue;
                            if ($emptyplace[$spec->specid]>0 && $spec->type_b==1){
                                $rekomend_b[$spec->specid][]=$entrant;
                                $emptyplace[$spec->specid]--;
                                $in=1;
                                break;
                            }
                        }
                    }
                    if ($in==0){
                        foreach ($entrant->specs as $spec){
                            if (!empty($prikaz[$spec->specid])) break;
                            if (count($hvost_b[$spec->specid])>=10) continue;
                            if ($entrant->grade_him<$minb[$spec->specid]['him'] || $entrant->grade_bio<$minb[$spec->specid]['bio'] || $entrant->grade_rus<$minb[$spec->specid]['rus']) continue;
                            if ($spec->type_b==1){
                                $hvost_b[$spec->specid][]=$entrant;
                            }
                        }
                    }
                }
                $this->render('enrol2',array(
                        'list_b'=>$list_b, 'specs'=>$specs, 'hvost_b'=>$hvost_b, 'rekomend_b'=>$rekomend_b, 'showspec'=>$showspec, 'prikaz'=>$prikaz,
                ));
        }
        
        public function actionEnrol3(){
                $this->layout=false;
                $model= new Entrant;
                $enrol=$_POST['entrant'];
                $showspec=(int)$_POST['spec'];
                
                if (!empty($enrol)){
                    foreach ($enrol as $entrantid=>$specid)
                        $model->updateByPk($entrantid, array('wave_third'=>$specid, 'enrol_type'=>1));
                }
                                
                $minb=array(
                    1=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    2=>array('him'=>60, 'bio'=>50, 'rus'=>50),
                    3=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    4=>array('him'=>50, 'bio'=>40, 'rus'=>40),
                    5=>array('him'=>40, 'bio'=>38, 'rus'=>38),
                    6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                    7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                );

                $specsBM=ConfigBm::model()->findAll();
                $bm=array();
                foreach ($specsBM as $sbm) $bm[$sbm->specid]=$sbm->value;
                $specs=Departments::model()->findAll();
                $fwcounts=array();
                foreach ($specs as $spec) $fwcounts[$spec->id]=Entrant::model()->count('wave_first='.$spec->id.' or wave_second='.$spec->id);

                $prikaz=array();
                $list_b=array();

                $p_entrants=$model->findAll(array('condition'=>'wave_third>0 and enrol_type=1', 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                foreach ($p_entrants as $p_entant){
                    $prikaz[$p_entant->wave_third][]=$p_entant;
                }
                $entrants=$model->findAll(array('condition'=>"deleted=0 and took=0 and orig>0 and wave_first=0 and wave_second=0 and vk=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>0 and grade_bio>0 and grade_him>0", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, grade_rus DESC, achv DESC, edu_grade DESC, medblock'));
                foreach ($entrants as $entrant){
                    foreach ($entrant->specs as $spec){
                        if (!empty($prikaz[$spec->specid])) break;
                        if ($entrant->grade_him<$minb[$spec->specid]['him'] || $entrant->grade_bio<$minb[$spec->specid]['bio'] || $entrant->grade_rus<$minb[$spec->specid]['rus']) continue;
                        if ($spec->type_b==1 && count($list_b[$spec->specid])<($bm[$spec->specid]-$fwcounts[$spec->specid])){
                            $list_b[$spec->specid][]=$entrant;
                            break;
                        }
                    }
                }
                $this->render('enrol3',array(
                        'list_b'=>$list_b, 'specs'=>$specs, 'prikaz'=>$prikaz, 'showspec'=>$showspec
                ));
        }
        
        public function actionEnrol33(){
                $this->layout=false;
                $model= new Entrant;
                $enrol=$_POST['entrant'];
                $showspec=(int)$_POST['spec'];
                
                if (!empty($enrol)){
                    foreach ($enrol as $entrantid=>$specid)
                        $model->updateByPk($entrantid, array('wave_third'=>$specid, 'enrol_type'=>2));
                }
                                
                $mind=array(
                    1=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    2=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    3=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    4=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                    5=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                    6=>array('him'=>0, 'bio'=>0, 'rus'=>0),
                    7=>array('him'=>0, 'bio'=>0, 'rus'=>0)
                );                

                $specsDM=ConfigDm::model()->findAll();
                $dm=array();
                foreach ($specsDM as $sdm) $dm[$sdm->specid]=$sdm->value;
                $specs=Departments::model()->findAll();
                $fwcounts=array();
                foreach ($specs as $spec) $fwcounts[$spec->id]=Entrant::model()->count('(wave_first='.$spec->id.' or wave_second='.$spec->id.') and enrol_type=2');

                $prikaz=array();
                $list_d=array();

                $p_entrants=$model->findAll(array('condition'=>'wave_third>0 and enrol_type=2', 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                foreach ($p_entrants as $p_entant){
                    $prikaz[$p_entant->wave_third][]=$p_entant;
                }
                $entrants=$model->findAll(array('condition'=>"deleted=0 and took=0 and orig>0 and wave_first=0 and wave_second=0 and wave_third=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>0 and grade_bio>0 and grade_him>0", 'order'=>'vk DESC, rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
                foreach ($entrants as $entrant){
                    foreach ($entrant->specs as $spec){
                        if (!empty($prikaz[$spec->specid])) break;
                        if ($entrant->grade_him<$mind[$spec->specid]['him'] || $entrant->grade_bio<$mind[$spec->specid]['bio'] || $entrant->grade_rus<$mind[$spec->specid]['rus']) continue;
                        if ($spec->type_d==1 && count($list_d[$spec->specid])<($dm[$spec->specid]-$fwcounts[$spec->specid])){
                            $list_d[$spec->specid][]=$entrant;
                            break;
                        }
                    }
                }
                $this->render('enrol33',array(
                        'list_d'=>$list_d, 'specs'=>$specs, 'prikaz'=>$prikaz, 'showspec'=>$showspec
                ));
        }
        
        public function actionEnrol22(){
                $this->layout=false;
                $model= new Entrant;
              
                $mind=array(
                    1=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    2=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    3=>array('him'=>40, 'bio'=>40, 'rus'=>40),
                    4=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                    5=>array('him'=>38, 'bio'=>38, 'rus'=>38),
                    6=>array('him'=>100, 'bio'=>100, 'rus'=>100),
                    7=>array('him'=>100, 'bio'=>100, 'rus'=>100)
                );                
                
                $specsDM=ConfigDm::model()->findAll();
                $dm=array();
                foreach ($specsDM as $sdm) $dm[$sdm->specid]=$sdm->value;
                $specs=Departments::model()->findAll();
                //$fwcounts=array();
                //foreach ($specs as $spec) $fwcounts[$spec->id]=Entrant::model()->count('wave_first='.$spec->id);

                $list_d=array();

                $entrants=$model->findAll(array('condition'=>"deleted=0 and took=0 and wave_first=0 and wave_second=0 and wave_third=0 and vk=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>0 and grade_bio>0 and grade_him>0", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, grade_rus DESC, achv DESC, edu_grade DESC, medblock'));
                //$emptyplace=array(1=>0, 2=>0, 3=>0, 4=>0, 5=>0, 6=>0);
                foreach ($entrants as $entrant){
                    foreach ($entrant->specs as $spec){
                        if ($entrant->grade_him<$mind[$spec->specid]['him'] || $entrant->grade_bio<$mind[$spec->specid]['bio'] || $entrant->grade_rus<$mind[$spec->specid]['rus']) continue;
                        if ($spec->type_d==1 && count($list_b[$spec->specid])<=($bm[$spec->specid]-$fwcounts[$spec->specid])){
                            $list_d[$spec->specid][]=$entrant;
                            break;
                        }
                    }
                }
                //зачисляем вторую волну - договор
                /*if ($yes){
                    foreach ($list_b as $specid=>$entrants){
                        foreach ($entrants as $entrant){
                            Entrant::model()->updateByPk($entrant->id, array('wave_second'=>$specid));
                        }
                    }
                }*/
                    
                $this->render('enrol22',array(
                        'list_d'=>$list_d, 'specs'=>$specs
                ));
        }
        
	public function actionImport()
	{
                $model= new Entrant;
                $specs=Departments::model()->findAll();
                
                $entrants=array();
                if ($_POST['importdata']){
                    $data=explode("\n", $_POST['importdata']);
                    foreach ($data as $d){
                        if (!empty($d)){
                            $d=explode(' ', $d);
                            $fam=trim($d[0]);
                            $name=trim($d[1]);
                            $m_name=trim($d[2]);
                            if (!empty($fam) && !empty($name) && !empty($m_name)){
                                $find=$model->find("fam like '%".$fam."%' and name like '%".$name."%' and m_name like'%".$m_name."%'");
                                if (!$find) echo '<p>'.$fam.' '.$name.' '.$m_name.'</p>';
                                $entrants[]=$find;
                            }
                        }
                    }
                    if (count($entrants)>0){
                        foreach ($entrants as $entrant){
                            $specid=intval($_POST['spec']);
                            $wave='';
                            if ($_POST['wave']==1) $wave='wave_first';
                            if ($_POST['wave']==2) $wave='wave_second';
                            if ($_POST['wave']==3) $wave='wave_third';
                            $enrol_type=intval($_POST['enrol_type']);
                            Entrant::model()->updateByPk($entrant->id, array($wave=>$specid,'enrol_type'=>$enrol_type));
                        }
                    }
                }
                
                $this->render('import',array(
			'entrants'=>$entrants, 'specs'=>$specs,
		));
	}
                
	public function actionAdmin()
	{
                $model= new Entrant;
                $specs=Departments::model()->findAll();
                
                $this->render('admin',array(
			'model'=>$model, 'specs'=>$specs,
		));
	}
}
