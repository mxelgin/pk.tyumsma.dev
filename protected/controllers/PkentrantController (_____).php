<?php

class PkentrantController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        public $defaultAction='admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'getDocFields', 'getKladrItems', 'getKladrStreets'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $model=$this->loadModel($id);
                $docfields= new Docs();
                $mdocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->maindoc);
                $gradedocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->gradedoc);
                $dopdocs=$docfields->findAll('entrantid='.$id.' and docid<>'.$model->gradedoc.' and docid<>'.$model->maindoc);
		$this->render('view',array(
			'model'=>$model, 'mdocfields'=>$mdocfields, 'gradedocfields'=>$gradedocfields, 'dopdocs'=>$dopdocs
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                $this->layout='//layouts/column1';
		$model=new Entrant;
                $docs=new DirectDocs;
                $citizen=new DirectCitizenship;
                $departmentModel=new Departments;
                $foreignModel=new DirectForeign;
                $docsfields= new DirectDocsFields;
                $entrantDoc= new Docs;

                $citizenship=$citizen->findAll();
                $department=$departmentModel->findAll();
                //иностранная модель показывает язык
                $lang=$foreignModel->findAll();
                $dataspec=array();
                $dataprt=array();

		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);

		if(isset($_POST['Entrant']))
		{
                        if ($_POST['docs']){
                            
                            $model->setAttributes($_POST['Entrant'],false);
                            $valid=true;
                            
                            $docsdata=$_POST['docs'];
                            foreach ($_POST['docs'] as $docid=>$fields){
                                $adddoc=array();
                                $adddoc['entrantid']=1;
                                $adddoc['docid']=$docid;
                                $docinfo=$docs->findByPk($docid);
                                if ($docinfo->action=='input'){
                                    foreach ($fields as $fieldid=>$field){
                                        $fieldinfo=$docsfields->findByPk($fieldid);
                                        if ($fieldinfo->required){
                                            if ($field==''){
                                                $errors=array();
                                                $errors['docs_'.$docid.'_'.$fieldid] = 'Необходимо заполнить поле '.$fieldinfo->name;
                                                $entrantDoc->addErrors($errors);
                                                $valid=false;
                                            }
                                        }
                                        if (!empty($field)){
                                            $adddoc['fieldid']=$fieldid;
                                            $adddoc['value']=$field;
                                            $inserDoc= new Docs;
                                            $inserDoc->attributes=$adddoc;
                                            //$model->docs[]=$inserDoc;
                                            $model->addRelatedRecord('docs',$inserDoc,count($model->docs));
                                        }
                                    }
                                }else{
                                    if ($docinfo->required && $fields==''){
                                        $errors=array();
                                        $errors['docs_'.$docid] = 'Необходимо заполнить поле '.$docinfo->name;
                                        $entrantDoc->addErrors($errors);
                                        $valid=false;
                                    }
                                    if (!empty($fields)){
                                        $adddoc['value']=$field;
                                        $inserDoc= new Docs;
                                        $inserDoc->attributes=$adddoc;
                                        //$model->docs[]=$inserDoc;
                                        $model->addRelatedRecord('docs',$inserDoc,count($model->docs));
                                    }
                                }
                            }
                        }
                        
                        $level=1;
                        foreach ($_POST['specid'] as $i=>$spec){
                            if ($spec){
                                $entrantSpec= new EntrantSpec;
                                $dataspec[$i]['specid']=$spec;
                                $dataspec[$i]['type']=$_POST['type'][$i];
                                $dataspec[$i]['level']=$level;
                                $dataspec[$i]['entrantid']=1;
                                $entrantSpec->attributes=$dataspec[$i];
                                $valid=$entrantSpec->validate() && $valid;
                                //$model->specs[]=$entrantSpec;
                                $model->addRelatedRecord('specs',$entrantSpec,count($model->specs));
                                $level++;
                            }
                        }

                        $entrantEdu= new Education;
                        $_POST['Edu']['entrantid']=1;
                        $entrantEdu->setAttributes($_POST['Edu'], false);
                        $valid=$entrantEdu->validate() && $valid;
                        $model->edu=$entrantEdu;

                        if (is_array($_POST['parent_fio'])){
                            foreach ($_POST['parent_fio'] as $i=>$parent){
                                if ($parent){
                                    $entrantParents= new Parents;
                                    $dataprt[$i]['fio']=$parent;
                                    $dataprt[$i]['addr']=$_POST['parent_addr'][$i];
                                    $dataprt[$i]['work']=$_POST['parent_work'][$i];
                                    $dataprt[$i]['phone']=$_POST['parent_phone'][$i];
                                    $dataprt[$i]['entrantid']=1;
                                    $entrantParents->attributes=$dataprt[$i];
                                    $valid=$entrantParents->validate() && $valid;
                                    //$model->parents=$entrantParents;
                                    $model->addRelatedRecord('parents',$entrantParents,count($model->parents));
                                }
                            }
                        }
                        if ($valid){
                            if($model->save()){
                                    $this->redirect(array('view','id'=>$model->id));
                            }
                        }
		}
                
                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.meio.mask.min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.placeholder.min.js');
                $cs->registerCoreScript('jquery.ui');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.ui.datepicker-ru.js');
                //$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.autocomplete-min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
                //$cs->registerCssFile(Yii::app()->baseUrl.'/css/autocomplete.css');

		$this->render('create',array(
			'model'=>$model, 'docs'=>$docs, 'citizenship'=>$citizenship, 'department'=>$department, 'lang'=>$lang, 'entrantDoc'=>$entrantDoc, 'docsdata'=>$docsdata, 'dataspec'=>$dataspec, 'entrantEdu'=>$entrantEdu, 'dataprt'=>$dataprt,
		));
	}
        
        public function actionGetDocFields()
        {
            if(isset($_GET['id']))
            {
                $docsdata=CJSON::decode($_GET['data']);
                $docserrors=CJSON::decode($_GET['docserrors']);
                $id=intval($_GET['id']);
                $doc=new DirectDocs;
                $docinfo=$doc->findByPk($id);
                
                if ($docinfo->action=='input'){
                    $docfields= new DirectDocsFields;
                    $fields=$docfields->findAll(array('condition'=>'docid='.$id, 'order'=>'id ASC'));
                    include Yii::app()->getBasePath().'/views/pkentrant/doc-with-fields.php';
                }else{
                    include Yii::app()->getBasePath().'/views/pkentrant/doc-without-fields.php';
                }
            }
        }
        
        public function actionGetKladrItems()
        {
                if ($_GET['query']){
                    $query=mysql_escape_string($_GET['query']);
                    if (strpos($query, '. ')){
                        $queryarray=explode('. ', $query);
                        $query=$queryarray[1];
                    }
                    /*$kladr= new Kladr;
                    $kladrtype= new KladrType;
                    $types=CHtml::listData($kladrtype->findAll(), 'id', 'fullname');
                    $items=$kladr->findAll(array('with'=>array('kladrregion'),'condition'=>"t.id_type in (103, 301, 302, 303, 304, 314, 315, 317, 401, 402, 405, 406, 416, 419, 421, 423, 429, 430, 431, 433, 434, 435, 436, 536, 544, 548, 551, 555, 556, 558) and t.name like '".$query."%'", 'order'=>'t.id_raion ASC, t.id_center DESC, t.gninmb DESC, t.id_type ASC'));
                    $result=array();
                    $data=array();
                    foreach ($items as $item){
                        $result[]=$types[$item->id_type].' '.$item->name.' ('.$item->kladrregion->name.' '.$types[$item->kladrregion->id_type].')';
                        $data[]=$item->code;
                    }*/
                    $city= new DirectCity;
                    $items=$city->findAll("name like '".$query."%'");
                    $result=array();
                    foreach ($items as $item){
                        $result[]=$item->name;
                    }
                    $array['query']=$_GET['query'];
                    $array['suggestions']=$result;
                    //$array['data']=$data;
                    echo CJSON::encode($array);
            }
        }

        public function actionGetKladrStreets()
        {
                if ($_GET['query'] && $_GET['city']){
                    $query=  mysql_escape_string($_GET['query']);
                    $city=$_GET['city'];
                    if (strpos($query, '. ')){
                        $queryarray=explode('. ', $query);
                        $query=$queryarray[1];
                    }
                    $kladr= new KladrStreet;
                    $items=$kladr->findAll(array('condition'=>"id_parent=:parentID and name like '".$query."%'", 'order'=>'`index` ASC', 'params'=>array(':parentID'=>$city)));
                    $result=array();
                    foreach ($items as $item){
                        $result[]=$item->type_text.' '.$item->name;
                    }
                    $array['query']=$_GET['query'];
                    $array['suggestions']=$result;
                    echo CJSON::encode($array);
            }
        }

        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                $this->layout='//layouts/column1';
		$model=$this->loadModel($id);
                $docs=new DirectDocs;
                $citizen=new DirectCitizenship;
                $departmentModel=new Departments;
                $foreignModel=new DirectForeign;
                $docsfields= new DirectDocsFields;
                $entrantDoc= new Docs;

                $citizenship=$citizen->findAll();
                $department=$departmentModel->findAll();
                //иностранная модель показывает язык
                $lang=$foreignModel->findAll();
                $dataprt=array();

		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);

		if(isset($_POST['Entrant']))
		{
                        if ($_POST['docs']){
                            
                            $model->setAttributes($_POST['Entrant'],false);
                            
                            $docsdata=$_POST['docs'];
                            foreach ($_POST['docs'] as $docid=>$fields){
                                $adddoc=array();
                                $adddoc['entrantid']=1;
                                $adddoc['docid']=$docid;
                                $docinfo=$docs->findByPk($docid);
                                if ($docinfo->action=='input'){
                                    foreach ($fields as $fieldid=>$field){
                                        $fieldinfo=$docsfields->findByPk($fieldid);
                                        if ($fieldinfo->required){
                                            if ($field==''){
                                                $errors=array();
                                                $errors['docs_'.$docid.'_'.$fieldid] = 'Необходимо заполнить поле '.$fieldinfo->name;
                                                $entrantDoc->addErrors($errors);
                                            }
                                        }
                                        if (!empty($field)){
                                            $adddoc['fieldid']=$fieldid;
                                            $adddoc['value']=$field;
                                            $updatedoc=Docs::model()->find(array('condition'=>'entrantid=:entrantID and docid=:docID and fieldid=:fieldID', 'params'=>array(':entrantID'=>$model->id, ':docID'=>$docid, ':fieldID'=>$fieldid)));
                                            if ($updatedoc){
                                                $model->docs[$updatedoc->id]->value=$field;
                                            }else{
                                                $inserDoc= new Docs;
                                                $inserDoc->attributes=$adddoc;
                                                $model->addRelatedRecord('docs',$inserDoc,count($model->docs));
                                            }
                                        }
                                    }
                                }else{
                                    if ($docinfo->required && $fields==''){
                                        $errors=array();
                                        $errors['docs_'.$docid] = 'Необходимо заполнить поле '.$docinfo->name;
                                        $entrantDoc->addErrors($errors);
                                    }
                                    if (!empty($fields)){
                                        $adddoc['value']=$field;
                                        $updatedoc=Docs::model()->find(array('condition'=>'entrantid=:entrantID and docid=:docID', 'params'=>array(':entrantID'=>$model->id, ':docID'=>$docid)));
                                        if ($updatedoc){
                                            $model->docs[$updatedoc->id]->value=$field;
                                        }else{
                                            $inserDoc= new Docs;
                                            $inserDoc->attributes=$adddoc;
                                            $model->addRelatedRecord('docs',$inserDoc,count($model->docs));
                                        }
                                    }
                                }
                            }
                        }
                        
                        $valid=true;
                        $level=1;
                        foreach ($_POST['specid'] as $i=>$spec){
                            if ($spec){
                                $dataspec=array();
                                $dataspec['specid']=$spec;
                                $dataspec['type']=$_POST['type'][$i];
                                $dataspec['level']=$level;
                                if ($_POST['spec_change'][$i]){
                                   // $entrantSpec=$entrantSpec->findByPk ($i);
                                    foreach ($model->specs as $k=>$spec){
                                        if ($spec->id==$i)
                                            $model->specs[$k]->attributes=$dataspec;
                                    }
                                }else{
                                    $entrantSpec= new EntrantSpec;
                                    $dataspec['entrantid']=1;
                                    $entrantSpec->attributes=$dataspec;
                                    $valid=$entrantSpec->validate() && $valid;
                                    $model->addRelatedRecord('specs',$entrantSpec,count($model->specs));
                                }
                                $level++;
                            }
                        }

                        //$entrantEdu= new Education;
                        //$_POST['Edu']['entrantid']=1;
                        $model->edu->attributes=$_POST['Edu'];
                        $valid=$model->edu->validate() && $valid;
                        //$model->edu=$entrantEdu;

                        if (is_array($_POST['parent_fio'])){
                            foreach ($_POST['parent_fio'] as $i=>$parent){
                                if ($parent){
                                    $dataprt[$i]['fio']=$parent;
                                    $dataprt[$i]['addr']=$_POST['parent_addr'][$i];
                                    $dataprt[$i]['work']=$_POST['parent_work'][$i];
                                    $dataprt[$i]['phone']=$_POST['parent_phone'][$i];
                                    if ($_POST['parent_change'][$i]){
                                        //$entrantParents=$entrantParents->findByPk ($i);
                                        foreach ($model->parents as $k=>$parent){
                                            if ($parent->id==$i)
                                                $model->parents[$k]->attributes=$dataprt;
                                        }
                                    }else{
                                        $entrantParents= new Parents;
                                        $dataprt[$i]['entrantid']=1;
                                        $entrantParents->attributes=$dataprt[$i];
                                        $valid=$entrantParents->validate() && $valid;
                                        $model->addRelatedRecord('parents',$entrantParents,count($model->parents));
                                    }
                                }
                            }
                        }
                        
			if($model->save() && $valid){
				$this->redirect(array('view','id'=>$model->id));
                        }
		}else{
                    foreach ($model->docs as $ddocs){
                        if ($ddocs->fieldid)
                            $docsdata[$ddocs->docid][$ddocs->fieldid]=$ddocs->value;
                        else
                            $docsdata[$ddocs->docid]=$ddocs->value;
                    }
                }
                
                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.meio.mask.min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.placeholder.min.js');
                $cs->registerCoreScript('jquery.ui');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.ui.datepicker-ru.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.autocomplete-min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
                $cs->registerCssFile(Yii::app()->baseUrl.'/css/autocomplete.css');

		$this->render('create',array(
			'model'=>$model, 'docs'=>$docs, 'citizenship'=>$citizenship, 'department'=>$department, 'lang'=>$lang, 'entrantDoc'=>$entrantDoc, 'docsdata'=>$docsdata, 'entrantEdu'=>$entrantEdu,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=$this->loadModel($id);
                        $model->deleted=1;
                        $model->save();
                        

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Entrant();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Entrant']))
			$model->attributes=$_GET['Entrant'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Entrant::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='departments-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
