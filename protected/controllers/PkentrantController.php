<?php

class PkentrantController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    public $defaultAction='admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin', 'create', 'index', 'view', 'update', 'check', 'getDocFields', 'getFullAddress', 'getFullAddressArray', 'getKladr', 'getKladrItems', 'getKladrStreets', 'printStatement', 'printForm9', 'printOpis', 'dog1', 'dog2', 'dog3', 'dog4', 'dateList', 'ratingList'),
				'roles'=>array('operator', 'secretary', 'administrator'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete', 'examList', 'egeList', 'unchecked'),
				'roles'=>array('secretary', 'administrator'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $model=$this->loadModel($id);
                $docfields= new Docs();
                $mdocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->maindoc);
                //$gradedocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->gradedoc);
                $dopdocs=$docfields->findAll('entrantid='.$id.' and docid<>'.$model->maindoc);
                $address = $model->getFullAddress($id);
		$this->render('view',array(
			'model'=>$model, 'mdocfields'=>$mdocfields, 'dopdocs'=>$dopdocs, 'address' => $address
		));
	}

        public function actionCheck($id)
	{
                $model=$this->loadModel($id);
                //$model->setAttribute('checkedbyuser', Yii::app()->user->id);
                $model->saveAttributes(array('checkedbyuser'=>Yii::app()->user->id));
                $this->redirect(array('view','id'=>$model->id));
	}
	
        public function actionExamList()
	{
                $model= new Entrant;
                $entrants=$model->findAll(array('with'=>'checked','condition'=>'deleted=0 and (grade_him is NUll or grade_rus is NUll or grade_bio is NULL) and wave_first=0 and wave_second=0 and wave_third=0 and (edu_spec<>1 or edu_spec is NULL)'));
		$this->render('examlist',array(
			'entrants'=>$entrants
		));
	}

        public function actionEgeList()
	{
                $model= new Entrant;
                $entrants=$model->findAll('deleted=0 and (grade_him is NUll or grade_rus is NUll or grade_bio is NULL) and wave_first=0 and wave_second=0 and wave_third=0 and edu_spec=1');
		$this->render('egelist',array(
			'entrants'=>$entrants
		));
	}

        public function actionDateList()
	{
                $model= new Entrant;
                if(isset($_POST['day'])){
                    $entrants=$model->findAll("deleted=0 and wave_first=0 and wave_second=0 and wave_third=0 and dateadd>'".addslashes($_POST['day'])." 00:00:00' and dateadd<'".addslashes($_POST['day'])." 00:00:00' + interval 1 day");
                }
                $days=$model->findAll(array('select'=>"id, DATE_FORMAT(dateadd, '%Y-%m-%d') as dateadd",'condition'=>"deleted=0 and wave_first=0 and wave_second=0 and wave_third=0", 'group'=>"DATE_FORMAT(dateadd, '%Y-%m-d')"));
		$this->render('datelist',array(
			'entrants'=>$entrants, 'days'=>$days,
		));
	}
        
        public function actionRatingList()
	{
                $this->layout=false;
                $model= new Entrant;
                
                $specs=Departments::model()->findAll();
                $entrantsvk=array();
                $entrantsrcm=array();
                $entrants=array();
                foreach ($specs as $spec){
                    $entrantsvk[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and specs.specid=".$spec->id." and specs.level=1 /*and grade_rus is not null and grade_him is not null and grade_bio is not null and grade_rus>0 and grade_bio>0 and grade_him>0 */ and vk=1 and rcm.entrantid is null and wave_first=0 and wave_second=0 and wave_third=0 and took=0", 'order'=>'rating DESC'));
                    $entrantsrcm[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and specs.specid=".$spec->id." and specs.level=1 and wave_first=0 and wave_second=0 and wave_third=0 and rcm.entrantid is not null and took=0", 'order'=>'vk DESC, rating DESC'));
                    $entrants[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and specs.specid=".$spec->id." and vk=0 and rcm.entrantid is null and wave_first=0 and wave_second=0 and wave_third=0 and took=0", 'order'=>'rating DESC'));
                }
		$this->render('ratinglist',array(
			'specs'=>$specs, 'entrants'=>$entrants, 'entrantsvk'=>$entrantsvk,'entrantsrcm'=>$entrantsrcm,
		));
	}

        public function actionDog1($id)
	{
                $this->layout=false;
                $model=$this->loadModel($id);
                $pass_number=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=13')->value;
                if (empty($pass_number)) $pass_number=Docs::model()->find('entrantid='.$model->id.' and docid=47 and fieldid=34')->value;
                $pass_date=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=14')->value;
                $pass_place=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=15')->value;
                //if (empty($pass_number) || empty($pass_date) || empty($pass_place)) throw new CHttpException(404,'Не указаны паспортные данные');
                $this->render('dog_1',array(
			'model'=>$model, 'pass_number'=>$pass_number,
            'pass_date'=>$pass_date, 'pass_place'=>$pass_place,
            'address' => $model->getFullAddress($id),
		));
	}

        public function actionDog2($id)
	{
                $this->layout=false;
                $model=$this->loadModel($id);
                $pass_number=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=13')->value;
                if (empty($pass_number)) $pass_number=Docs::model()->find('entrantid='.$model->id.' and docid=47 and fieldid=34')->value;
                $pass_date=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=14')->value;
                $pass_place=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=15')->value;
                //if (empty($pass_number) || empty($pass_date) || empty($pass_place)) throw new CHttpException(404,'Не указаны паспортные данные');
                $dog2=Contract2::model()->find('entrantid='.$model->id);
                if (!$dog2) throw new CHttpException(404,'Не указаны данные плательщика');
                $this->render('dog_2',array(
			'model'=>$model, 'pass_number'=>$pass_number, 'pass_date'=>$pass_date, 'pass_place'=>$pass_place, 'dog2'=>$dog2,
            'address' => $model->getFullAddress($id),
		));
	}

        public function actionDog3($id)
	{
                $this->layout=false;
                $model=$this->loadModel($id);
                $pass_number=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=13')->value;
                if (empty($pass_number)) $pass_number=Docs::model()->find('entrantid='.$model->id.' and docid=47 and fieldid=34')->value;
                $pass_date=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=14')->value;
                $pass_place=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=15')->value;
                //if (empty($pass_number) || empty($pass_date) || empty($pass_place)) throw new CHttpException(404,'Не указаны паспортные данные');
                $dog3=Contract3::model()->find('entrantid='.$model->id);
                if (!$dog3) throw new CHttpException(404,'Не указаны данные плательщика');
                $this->render('dog_3',array(
			'model'=>$model, 'pass_number'=>$pass_number, 'pass_date'=>$pass_date, 'pass_place'=>$pass_place, 'dog3'=>$dog3,
            'address' => $model->getFullAddress($id),
		));
	}

        public function actionDog4($id)
	{
                $this->layout=false;
                $model=$this->loadModel($id);
                $pass_number=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=13')->value;
                if (empty($pass_number)) $pass_number=Docs::model()->find('entrantid='.$model->id.' and docid=47 and fieldid=34')->value;
                $pass_date=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=14')->value;
                $pass_place=Docs::model()->find('entrantid='.$model->id.' and docid=21 and fieldid=15')->value;
                //if (empty($pass_number) || empty($pass_date) || empty($pass_place)) throw new CHttpException(404,'Не указаны паспортные данные');
                $dog4=Contract4::model()->find('entrantid='.$model->id);
                if (!$dog4) throw new CHttpException(404,'Не указаны данные плательщика');
                $this->render('dog_4',array(
			'model'=>$model, 'pass_number'=>$pass_number, 'pass_date'=>$pass_date, 'pass_place'=>$pass_place, 'dog4'=>$dog4,
            'address' => $model->getFullAddress($id),
		));
	}

        public function actionPrintStatement($id)
	{
                $this->layout=false;
                $model=$this->loadModel($id);
                $docfields= new Docs();
                $mdocfields=$docfields->findAll(array('with'=>'field','condition'=>'t.entrantid='.$id.' and t.docid='.$model->maindoc, 'order'=>'field.required DESC'));
                $specfirst=EntrantSpec::model()->find(array('with'=>'spec', 'condition'=>'t.entrantid='.$id.' and t.level=1'));
                $specother=EntrantSpec::model()->findAll(array('with'=>'spec', 'condition'=>'t.entrantid='.$id.' and t.level>1'));
                //$gradedocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->gradedoc);
                $dopdocs=$docfields->findAll('entrantid='.$id.' and docid<>'.$model->maindoc);
		$this->render('statement',array(
			'model'=>$model, 'mdocfields'=>$mdocfields, 'specfirst'=>$specfirst, 'specother'=>$specother, 'dopdocs'=>$dopdocs,
            'address' => $model->getFullAddress($id),
		));
	}

        public function actionPrintForm9($id)
	{
                $this->layout=false;
                $model=$this->loadModel($id);
		$this->render('form9',array(
			'model'=>$model,
            'address' => $model->getFullAddress($id),
		));
	}

        public function actionPrintOpis($id)
	{
                $this->layout=false;
                $model=$this->loadModel($id);
                $docfields= new Docs();
                $mdocfields=$docfields->findAll(array('with'=>'field','condition'=>'t.entrantid='.$id.' and t.docid='.$model->maindoc, 'order'=>'field.required DESC'));
                $specfirst=EntrantSpec::model()->find(array('with'=>'spec', 'condition'=>'t.entrantid='.$id.' and t.level=1'));
                $specother=EntrantSpec::model()->findAll(array('with'=>'spec', 'condition'=>'t.entrantid='.$id.' and t.level>1'));
                //$gradedocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->gradedoc);
                $dopdocs=$docfields->findAll('entrantid='.$id.' and docid<>'.$model->maindoc);
		$this->render('opis',array(
			'model'=>$model, 'mdocfields'=>$mdocfields, 'specfirst'=>$specfirst, 'specother'=>$specother, 'dopdocs'=>$dopdocs,
            'address' => $model->getFullAddress($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $this->layout='//layouts/column1';
		$model=new Entrant;
        $docs=new DirectDocs;
        $citizen=new DirectCitizenship;
        $departmentModel=new Departments;
        $docsfields= new DirectDocsFields;
        $entrantDocErrors= new Docs;
        $entrantSpecErrors= new EntrantSpec;
        $docsDate = new DocsDate;

        $citizenship=$citizen->findAll();
        $department=$departmentModel->findAll();
        $dataspec=array();
        $dataprt=array();

		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);

		if(isset($_POST['Entrant']))
		{
                        $_POST['Entrant']['vk']=intval($_POST['Entrant']['vk']);
                        $_POST['Entrant']['achv']=count($_POST['Entrant']['achv']);
                        $_POST['Entrant']['medblock']=intval($_POST['Entrant']['medblock']);
                        $_POST['Entrant']['orig']=intval($_POST['Entrant']['orig']);
                        $_POST['Entrant']['crimea']=intval($_POST['Entrant']['crimea']);
                        $_POST['Entrant']['took']=intval($_POST['Entrant']['took']);
                        $_POST['Entrant']['orphan1'] = $_POST['orphan1'];
                        $_POST['Entrant']['orphan2'] = $_POST['orphan2'];
                        $_POST['Entrant']['orphan1_type'] = $_POST['orphan1_type'];
                        $_POST['Entrant']['orphan2_type'] = $_POST['orphan2_type'];
                        $_POST['Entrant']['need_hostel'] = intval($_POST['Entrant']['need_hostel']);

                        $model->setAttributes($_POST['Entrant'],false);
                        $model->home_type = $this->getHT($_POST['Entrant']['kladr_street']);
                        $valid=true;
                        $alldocs=$docs->findAll('(categoryid=1 and id='.$model->maindoc.' ) or categoryid=2');
                        foreach ($alldocs as $dc){
                            $adddoc=array();
                            $adddoc['entrantid']=1;
                            $adddoc['docid']=$dc->id;
                            if ($dc->action=='input'){
                                $fields=$docsfields->findAll('docid='.$dc->id);
                                foreach ($fields as $fl){
                                    if ($_POST['docs'][$dc->id][$fl->id]){
                                        $adddoc['fieldid']=$fl->id;
                                        $adddoc['value']=$_POST['docs'][$dc->id][$fl->id];
                                        $doctype=DirectDocsFields::model()->findByPk($fl->id);
                                        if ($doctype->type=='date'){
                                            $date=explode('.', $adddoc['value']);
                                            if (!checkdate($date[1], $date[0], $date[2])){
                                                $errors=array();
                                                $errors['docs_'.$dc->id.'_'.$fl->id] = 'Неверный формат даты';
                                                $entrantDocErrors->addErrors($errors);
                                                $valid=false;                                                
                                            }
                                        }
                                        if (($adddoc['docid']==21 && $adddoc['fieldid']==13 && Docs::model()->exists("docid=21 and fieldid=13 && value='".$adddoc['value']."'")) || ($adddoc['docid']==26 && $adddoc['fieldid']==18 && Docs::model()->exists("docid=26 and fieldid=18 && value='".$adddoc['value']."'")) || ($adddoc['docid']==47 && $adddoc['fieldid']==34 && Docs::model()->exists("docid=47 and fieldid=34 && value='".$adddoc['value']."'"))){
                                            $errors=array();
                                            $errors['docs_'.$dc->id.'_'.$fl->id] = 'Абитуриент с такими данными уже присутствует в базе';
                                            $entrantDocErrors->addErrors($errors);
                                            $valid=false;                                            
                                        }else{
                                            $inserDoc= new Docs;
                                            $inserDoc->attributes=$adddoc;
                                            $model->addRelatedRecord('docs',$inserDoc,count($model->docs));
                                            $docsDate->docid = $dc->id;
                                            $docsDate->date = date("Y-m-d H:i:s");
                                            $docsDate->save();
                                        }
                                    }elseif (empty($_POST['docs'][$dc->id][$fl->id]) && $fl->required){
                                        $errors=array();
                                        $errors['docs_'.$dc->id.'_'.$fl->id] = 'Необходимо заполнить поле '.$fl->name;
                                        $entrantDocErrors->addErrors($errors);
                                        $valid=false;
                                    }
                                }
                            }else{
                                if ($_POST['docs'][$dc->id]){
                                    $adddoc['value']=$_POST['docs'][$dc->id];
                                    $inserDoc= new Docs;
                                    $inserDoc->attributes=$adddoc;
                                    $model->addRelatedRecord('docs',$inserDoc,count($model->docs));
                                }elseif (empty($_POST['docs'][$dc->id]) && $dc->required){
                                    $errors=array();
                                    $errors['docs_'.$dc->id] = 'Необходимо заполнить поле '.$dc->name;
                                    $entrantDocErrors->addErrors($errors);
                                    $valid=false;
                                }
                            }
                        }

                        $level=1;
                        foreach ($_POST['specid'] as $i=>$spec){
                            $entrantSpec= new EntrantSpec;
                            $dataspec=array();
                            $dataspec['specid']=$spec;
                            $dataspec['type_b']=$_POST['type_b'][$i];
                            $dataspec['type_d']=$_POST['type_d'][$i];
                            $dataspec['level']=$level;
                            $dataspec['entrantid']=1;
                            $entrantSpec->attributes=$dataspec;
                            $valid=$entrantSpec->validate() && $valid;

                            if (!empty($spec) && empty($dataspec['type_b']) && empty($dataspec['type_d'])){
                                $errors=array();
                                $errors['type_b_'.$i] = 'Необходимо указать условие обучения';
                                $entrantSpecErrors->addErrors($errors);
                                $valid=false;
                            }

                            $model->addRelatedRecord('specs',$entrantSpec,count($model->specs));
                            $level++;
                        }
                        
                        if (empty($_POST['specid'][0]) || (empty($_POST['type_b'][0]) && empty($_POST['type_d'][0]))){
                            $errors=array();
                            $errors['specid_0'] = 'Необходимо указать приоритетное место';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;
                        }

                        if ($_POST['specid'][0]==$_POST['specid'][1] && $_POST['specid'][0]!=0){
                            $errors=array();
                            $errors['specid_1'] = 'Нельзя допускать дублирования специальностей';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;
                        }
                        if ($_POST['specid'][1]==$_POST['specid'][2] && $_POST['specid'][1]!=0){
                            $errors=array();
                            $errors['specid_2'] = 'Нельзя допускать дублирования специальностей';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;
                        }
                        if ($_POST['specid'][0]==$_POST['specid'][2] && $_POST['specid'][0]!=0){
                            $errors=array();
                            $errors['specid_2'] = 'Нельзя допускать дублирования специальностей';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;
                        }
                        if (empty($_POST['specid'][1]) && !empty($_POST['specid'][2])){
                            $errors=array();
                            $errors['specid_1'] = 'Нельзя оставлять пропущенную специальность';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;                            
                        }


                        //if (is_array($_POST['parent_fio'])){
                            //foreach ($_POST['parent_fio'] as $i=>$parent){
                            for ($i = 1; $i < 3; $i++) {
                                //$parent = $_POST['parent_fio'][$i];
                                //if (empty($_POST['parent_fio'][$i])) $valid = false;
                                $entrantParents= new Parents;
                                $dataprt[$i]['fio']=$_POST['parent_fio'][$i];
                                $dataprt[$i]['addr']=$_POST['parent_addr'][$i];
                                $dataprt[$i]['work']=$_POST['parent_work'][$i];
                                $dataprt[$i]['phone']=$_POST['parent_phone'][$i];
                                $dataprt[$i]['entrantid']=1;
                                $entrantParents->attributes=$dataprt[$i];
                                $valid=$entrantParents->validate() && $valid;
                                //$model->parents=$entrantParents;
                         //       echo '<pre>'; print_r($entrantParents->attributes); 
                                $model->addRelatedRecord('parents',$entrantParents,$i);//count($model->parents));
                            }
                        //}
                        //die;
                        if ($model->edu_type==0) $model->edu_type='';
                        if ($model->edu_spec==0) $model->edu_spec='';
                        $model->sentToFIS = false;
                        
                        $valid=$model->validate() && $valid;
                        
                        if ($valid){
                            if($model->save()){
                                $id = $model->getAttribute('id');

                                LinkEntrantPravo::model()->deleteAll('entrantid='.$id);
                                $checkPravos = $_POST['Entrant']['pravos'];
                                if (!is_null($checkPravos)){
                                    foreach($checkPravos as $pravoId=>$pravo){
                                        $p = new LinkEntrantPravo;
                                        $p->entrantid = $id;
                                        $p->pravoid = $pravoId;
                                        $p->save();
                                    }
                                }

// achvs
                                LinkEntrantAchv::model()->deleteAll('entrantid='.$id);
                                $checkAchvs = $_POST['Entrant']['achvs'];
                                if (!is_null($checkAchvs)){
                                    foreach($checkAchvs as $achvId=>$achv){
                                        $a= new LinkEntrantAchv;
                                        $a->entrantid = $id;
                                        $a->achvid = $achvId;
                                        $a->save();
                                    }
                                }


                                LogController::addLog($this, 'create', $model, $model->fam . ' '. $model->name . ' ' . $model->m_name);
                                PkexportController::fisCreate($model->getPrimaryKey());
                                $this->redirect(array('view','id'=>$model->id));
                            }
                        }
		}
                
                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.meio.mask.min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.placeholder.min2.js');
                $cs->registerCoreScript('jquery.ui');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.ui.datepicker-ru.js');
                //$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.autocomplete-min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-ui-combobox.js');
                //$cs->registerCssFile(Yii::app()->baseUrl.'/css/autocomplete.css');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/kladr.js');
                //$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.maskedinput.js');
                //$cs->registerScriptFile('http://code.jquery.com/jquery-1.9.1.js');
                //$cs->registerScriptFile('http://code.jquery.com/ui/1.10.3/jquery-ui.js');
                
                $docsdata=$_POST['docs'];
        //echo '<pre>'; print_r($_POST); die;

		$this->render('create',array(
			'model'=>$model, 'docs'=>$docs,
            'citizenship'=>$citizenship, 'department'=>$department,
            'entrantDocErrors'=>$entrantDocErrors, 'docsdata'=>$docsdata,
            'dataspec'=>$dataspec, 'dataprt'=>$dataprt,
            'entrantSpecErrors'=>$entrantSpecErrors,
            'create' => 'create',
		));
	}
        
        public function actionGetDocFields()
        {
            if(isset($_GET['id']))
            {
                $docsdata=CJSON::decode($_GET['data']);
                $docserrors=CJSON::decode($_GET['docserrors']);
                $id=intval($_GET['id']);
                $doc=new DirectDocs;
                $docinfo=$doc->findByPk($id);
                $entrantid=intval($_GET['entrantid']);
                
                if ($docinfo->action=='input'){
                    $docfields= new DirectDocsFields;
                    $fields=$docfields->findAll(array('condition'=>'docid='.$id, 'order'=>'id ASC'));
                    include Yii::app()->getBasePath().'/views/pkentrant/doc-with-fields.php';
                }else{
                    include Yii::app()->getBasePath().'/views/pkentrant/doc-without-fields.php';
                }
            }
        }

        public function actionGetFullAddressArray() {
            if (!$_GET['id']) return;
            $id = $_GET['id'];

            $model=new Entrant();
            $r = $model->getFullAddressArray($id);

            echo '<pre>'; print_r($r);
        }
        
        public function actionGetFullAddress() {
            //$strt = new KladrStreet;
            if (!$_GET['id']) return;
            $id = $_GET['id'];

            $model=new Entrant();
            $r = $model->getFullAddress($id);

            echo '<pre>'; print_r($r);
        }
        
        public function actionGetKladr() {
            //if (!$_GET['query']) return;
            if (!$_GET['type']) $kladr = new KladrRegions;
            elseif ($_GET['type'] == 'raion') $kladr = new KladrRaions;
            elseif (in_array($_GET['type'], array('city', 'parent'))) $kladr = new Kladr;
            elseif ($_GET['type'] == 'street') $kladr = new KladrStreet;
            else $kladr = new KladrRegions;

            $query=mysql_escape_string($_GET['query']);
            $parent = mysql_escape_string($_GET['_parent']);
            $more=mysql_escape_string($_GET['more']);

            if (strpos($query, '. ')){
                $queryarray=explode('. ', $query);
                $query=$queryarray[1];
            }

            if ($_GET['type'] == 'parent') {
                $query = "
                 SELECT
                    id,
                    code,
                    region,
                    raion,
                    city,
                    concat(region, raion, ', ', city) address
                 FROM
                 (
                     SELECT 
                         city.id id,
                         city.code code,
                         concat(regn.name, ' ', regn.type_text) region,
                         case
                             when left(city.id, 5) <> concat(left(city.id, 2), '000')
                             then concat(', ', rain.name, ' ', rain.type_text)
                             else ''
                         end raion,
                         concat(city.type_text, ' ', city.name) city
                     FROM
                         kladr city
                         left join kladr rain
                         on rain.id = concat(left(city.id, 5), '00000000')
                         left join kladr regn
                         on regn.id = concat(left(city.id, 2), '00000000000')
                     where city.code = '".$_GET['_parent']."'
                        param1
                 ) sub";

                 //echo '<pre>' . $query; 
                 if (isset($_GET['query'])) $query = str_replace('param1', "or city.name like '%".$_GET['query']."%'", $query);
                 else $query = str_replace('param1', '', $query);
                 //echo '<pre>' . $query; die;

                 $cmd = Yii::app()->db->createCommand($query);
                 $items= $cmd->queryAll();
            } else {
                $items=$kladr->findAll("name like '%".$query."%' and id like '".$parent."%' ".$more);
            }

            $result=array();
            $code = "";
            $type = "";
            $name = "";

            foreach ($items as $item){
                if (isset($item['code'])) $code = $item['code'];
                if (isset($item->type_text)) $type = $item->type_text;
                if ($_GET['type'] == 'parent') $name = $item['address'];
                else $name = $item->name;


                $result[]=array(
                    'id' => $item['id'],
                    'code' => $code,
                    'name'=> $name,
                    'sfx' => $type,
                );
                /*
                echo '<pre>'; print_r($item);
                echo '<pre>'; print_r($result);
                */
            }
            $array['data']=$result;
            echo $_GET['callback'].'('.CJSON::encode($array).');';
            //echo '<pre>'; print_r($array);
        }

        public function actionGetKladrItems()
        {
                if ($_GET['query']){
                    $query=mysql_escape_string($_GET['query']);
                    if (strpos($query, '. ')){
                        $queryarray=explode('. ', $query);
                        $query=$queryarray[1];
                    }
                    $city= new DirectCity;
                    $items=$city->findAll("name like '".$query."%'");
                    $result=array();
                    foreach ($items as $item){
                        $result[]=array('name'=>$item->name);
                    }
                    $array['city']=$result;
                    echo $_GET['callback'].'('.CJSON::encode($array).');';
            }
        }

        public function actionGetKladrStreets()
        {
                if ($_GET['query'] && $_GET['city']){
                    $query=  mysql_escape_string($_GET['query']);
                    $city=$_GET['city'];
                    if (strpos($query, '. ')){
                        $queryarray=explode('. ', $query);
                        $query=$queryarray[1];
                    }
                    $kladr= new KladrStreet;
                    $items=$kladr->findAll(array('condition'=>"id_parent=:parentID and name like '".$query."%'", 'order'=>'`index` ASC', 'params'=>array(':parentID'=>$city)));
                    $result=array();
                    foreach ($items as $item){
                        $result[]=$item->type_text.' '.$item->name;
                    }
                    $array['query']=$_GET['query'];
                    $array['suggestions']=$result;
                    echo CJSON::encode($array);
            }
        }

        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $this->layout='//layouts/column1';
		$model=$this->loadModel($id);
        if (!Yii::app()->user->checkAccess('administrator')){
                    if ($model->checkedbyuser){
                        if (Yii::app()->user->id!=$model->checkedbyuser){
                            throw new CHttpException(403,'С этим абитуриентом может работать только секретарь '.$model->checkedbyuser);
                        }
                    }
                    if (!Yii::app()->user->checkAccess('secretary')){
                        if ($model->useradd!=Yii::app()->user->id)
                            throw new CHttpException(403,'С этим абитуриентом может работать только оператор '.$model->useradd);
                    }
                }
                $docs=new DirectDocs;
                $citizen=new DirectCitizenship;
                $departmentModel=new Departments;
                $docsfields= new DirectDocsFields;
                $entrantDocErrors= new Docs;
                $entrantSpecErrors= new EntrantSpec;
                $docsDate = new DocsDate;
                $priorityPravos = new DirectPriorityPravo;

                $citizenship=$citizen->findAll();
                $department=$departmentModel->findAll();
                //иностранная модель показывает язык
                $dataprt=array();
                $docsdata=array();

		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);

		if(isset($_POST['Entrant']))
		{
                        $_POST['Entrant']['vk']=intval($_POST['Entrant']['vk']);
                        $_POST['Entrant']['achv']=count($_POST['Entrant']['achvs']);
                        $_POST['Entrant']['medblock']=intval($_POST['Entrant']['medblock']);
                        $_POST['Entrant']['orig']=intval($_POST['Entrant']['orig']);
                        $_POST['Entrant']['crimea']=intval($_POST['Entrant']['crimea']);
                        $_POST['Entrant']['took']=intval($_POST['Entrant']['took']);
                        $_POST['Entrant']['orphan1'] = $_POST['orphan1'];
                        $_POST['Entrant']['orphan2'] = $_POST['orphan2'];
                        $_POST['Entrant']['orphan1_type'] = $_POST['orphan1_type'];
                        $_POST['Entrant']['orphan2_type'] = $_POST['orphan2_type'];
                        $_POST['Entrant']['need_hostel'] = intval($_POST['Entrant']['need_hostel']);
                        //echo '<pre>'; print_r($_POST); die();
                        $model->setAttributes($_POST['Entrant'],false);
                        $model->home_type = $this->getHT($_POST['Entrant']['kladr_street']);
                        $valid=true;
// docs
                        $alldocs=$docs->findAll('(categoryid=1 and id='.$model->maindoc.' ) or categoryid=2');
                        foreach ($alldocs as $dc){
                            $adddoc=array();
                            $adddoc['entrantid']=1;
                            $adddoc['docid']=$dc->id;
                            if ($dc->action=='input'){
                                $fields=$docsfields->findAll('docid='.$dc->id);
                                foreach ($fields as $fl){
                                    if ($_POST['docs'][$dc->id][$fl->id]){
                                        $adddoc['fieldid']=$fl->id;
                                        $adddoc['value']=$_POST['docs'][$dc->id][$fl->id];
                                        $doctype=DirectDocsFields::model()->findByPk($fl->id);
                                        if ($doctype->type=='date'){
                                            $date=explode('.', $adddoc['value']);
                                            if (!checkdate($date[1], $date[0], $date[2])){
                                                $errors=array();
                                                $errors['docs_'.$dc->id.'_'.$fl->id] = 'Неверный формат даты';
                                                $entrantDocErrors->addErrors($errors);
                                                $valid=false;                                                
                                            }
                                        }
                                        if (($adddoc['docid']==21 && $adddoc['fieldid']==13 && Docs::model()->exists("docid=21 and fieldid=13 && entrantid!=".$model->id." && value='".$adddoc['value']."'")) || ($adddoc['docid']==26 && $adddoc['fieldid']==18 && Docs::model()->exists("docid=26 and fieldid=18 && entrantid!=".$model->id." && value='".$adddoc['value']."'")) || ($adddoc['docid']==47 && $adddoc['fieldid']==34 && Docs::model()->exists("docid=47 and fieldid=34 && entrantid!=".$model->id." && value='".$adddoc['value']."'"))){
                                            $errors=array();
                                            $errors['docs_'.$dc->id.'_'.$fl->id] = 'Абитуриент с такими данными уже присутствует в базе';
                                            $entrantDocErrors->addErrors($errors);
                                            $valid=false;                                            
                                        }else{
                                            $updatedoc=false;
                                            foreach ($model->docs as $k=>$doc){
                                                if ($doc->docid==$dc->id && $doc->fieldid==$fl->id){
                                                    $model->docs[$k]->attributes=$adddoc;
                                                    $updatedoc=true;
                                                }
                                            }
                                            if (!$updatedoc){
                                                $inserDoc= new Docs;
                                                $inserDoc->attributes=$adddoc;
                                                $model->addRelatedRecord('docs',$inserDoc,count($model->docs));
                                            }
                                        }
                                    }elseif (empty($_POST['docs'][$dc->id][$fl->id]) && $fl->required){
                                        $errors=array();
                                        $errors['docs_'.$dc->id.'_'.$fl->id] = 'Необходимо заполнить поле '.$fl->name;
                                        $entrantDocErrors->addErrors($errors);
                                        $valid=false;
                                    }else{
                                        Docs::model()->deleteAll('entrantid='.$id.' and docid='.$dc->id.' and fieldid='.$fl->id);
                                    }
                                }
                            }else{
                                if ($_POST['docs'][$dc->id]){
                                    $adddoc['value']=$_POST['docs'][$dc->id];
                                    $updatedoc=false;
                                    foreach ($model->docs as $k=>$doc){
                                        if ($doc->docid==$dc->id){
                                            $model->docs[$k]->attributes=$adddoc;
                                            $updatedoc=true;
                                        }
                                    }
                                    if (!$updatedoc){
                                        $inserDoc= new Docs;
                                        $inserDoc->attributes=$adddoc;
                                        $model->addRelatedRecord('docs',$inserDoc,count($model->docs));
                                        $docsDate->docid = $dc->id;
                                        $docsDate->date = date("Y-m-d H:i:s");
                                        $docsDate->save();
                                    }

                                }elseif (empty($_POST['docs'][$dc->id]) && $dc->required){
                                    $errors=array();
                                    $errors['docs_'.$dc->id] = 'Необходимо заполнить поле '.$dc->name;
                                    $entrantDocErrors->addErrors($errors);
                                    $valid=false;
                                }else{
                                    //echo '<pre>'; print_r($dc->attributes); 
                                    Docs::model()->deleteAll('entrantid='.$id.' and docid='.$dc->id);
                                    $docsDate->deleteAll('docid=' . $dc->id);
                                }
                            }
                        }


// specs
                        foreach ($model->specs as $k=>$spec){
                            $dataspec=array();
                            $dataspec['specid']=$_POST['specid'][$spec->id];
                            $dataspec['type_b']=$_POST['type_b'][$spec->id];
                            $dataspec['type_d']=$_POST['type_d'][$spec->id];
                            $model->specs[$k]->attributes=$dataspec;
                            if (!empty($dataspec['specid']) && empty($dataspec['type_b']) && empty($dataspec['type_d'])){
                                $errors=array();
                                $errors['type_b_'.$spec->id] = 'Необходимо указать условие обучения';
                                $entrantSpecErrors->addErrors($errors);
                                $valid=false;
                            }
                            if ($spec->level==1){
                                if (empty($dataspec['specid']) || (empty($dataspec['type_b']) && empty($dataspec['type_d']))){
                                    $errors=array();
                                    $errors['specid_'.$spec->id] = 'Необходимо указать приоритетное место';
                                    $entrantSpecErrors->addErrors($errors);
                                    $valid=false;
                                }
                            }
                        }

                        if ($model->specs[0]->specid==$model->specs[1]->specid && $model->specs[0]->specid){
                            $errors=array();
                            $errors['specid_'.$model->specs[1]->id] = 'Нельзя допускать дублирования специальностей';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;
                        }
                        if ($model->specs[1]->specid==$model->specs[2]->specid && $model->specs[1]->specid){
                            $errors=array();
                            $errors['specid_'.$model->specs[2]->id] = 'Нельзя допускать дублирования специальностей';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;
                        }
                        if ($model->specs[0]->specid==$model->specs[2]->specid && $model->specs[0]->specid){
                            $errors=array();
                            $errors['specid_'.$model->specs[2]->id] = 'Нельзя допускать дублирования специальностей';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;
                        }
                        if (empty($model->specs[1]->specid) && !empty($model->specs[2]->specid)){
                            $errors=array();
                            $errors['specid_'.$model->specs[1]->id] = 'Нельзя оставлять пропущенную специальность';
                            $entrantSpecErrors->addErrors($errors);
                            $valid=false;                            
                        }

// parents
                        foreach ($model->parents as $k=>$parent){
                            $dataprt['fio']=$_POST['parent_fio'][$parent->id];
                            $dataprt['addr']=$_POST['parent_addr'][$parent->id];
                            $dataprt['work']=$_POST['parent_work'][$parent->id];
                            $dataprt['phone']=$_POST['parent_phone'][$parent->id];
                            $model->parents[$k]->attributes=$dataprt;
                        }

                        if ($model->edu_type==0) $model->edu_type='';
                        if ($model->edu_spec==0) $model->edu_spec='';
                        $model->sentToFIS = false;


            $valid=$model->validate() && $valid;
                        if ($valid){
                            if($model->save()){
// pravos
                                LinkEntrantPravo::model()->deleteAll('entrantid='.$id);
                                $checkPravos = $_POST['Entrant']['pravos'];
                                if (!is_null($checkPravos)){
                                    foreach($checkPravos as $pravoId=>$pravo){
                                        $p = new LinkEntrantPravo;
                                        $p->entrantid = $id;
                                        $p->pravoid = $pravoId;
                                        $p->save();
                                    }
                                }
// achvs
                                LinkEntrantAchv::model()->deleteAll('entrantid='.$id);
                                $checkAchvs = $_POST['Entrant']['achvs'];
                                if (!is_null($checkAchvs)){
                                    foreach($checkAchvs as $achvId=>$achv){
                                        $a = new LinkEntrantAchv;
                                        $a->entrantid = $id;
                                        $a->achvid = $achvId;
                                        $a->save();
                                    }
                                }
                                LogController::addLog($this, 'update', $model, $model->fam . ' '. $model->name . ' ' . $model->m_name);
                                PkexportController::fisUpdate($model->id);
                                $this->redirect(array('view','id'=>$model->id));
                            }
                        }
                        $docsdata=$_POST['docs'];

		} else {
                    foreach ($model->docs as $ddocs){
                        if ($ddocs->fieldid)
                            $docsdata[$ddocs->docid][$ddocs->fieldid]=$ddocs->value;
                        else
                            $docsdata[$ddocs->docid]=$ddocs->value;
                    }
                }
                
                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.meio.mask.min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.placeholder.min.js');
                $cs->registerCoreScript('jquery.ui');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.ui.datepicker-ru.js');
                //$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.autocomplete-min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
                //$cs->registerCssFile(Yii::app()->baseUrl.'/css/autocomplete.css');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-ui-combobox.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/kladr.js');
                //$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.maskedinput.js');
/*
                $cs->registerScriptFile('http://code.jquery.com/jquery-1.9.1.js');
                $cs->registerScriptFile('http://code.jquery.com/ui/1.10.3/jquery-ui.js');
*/


		$this->render('create',array(
			'model'=>$model, 'docs'=>$docs, 'citizenship'=>$citizenship, 'department'=>$department, 'entrantDocErrors'=>$entrantDocErrors, 'docsdata'=>$docsdata, 'entrantSpecErrors'=>$entrantSpecErrors,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model=$this->loadModel($id);
            $model->deleted=1;
            $model->sentToFIS = false;
            $model->save();
                        
            LogController::addLog($this, 'delete', $model, $model->fam . ' '. $model->name . ' ' . $model->m_name);
            PkexportController::fisDelete($model->id);

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        //echo '<pre>'; print_r(date("Y-m-d H:i:s", yii::app()->user->getState('userSessionTimeout'))); echo '</pre>';// die();
		$model=new Entrant();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Entrant']))
			$model->attributes=$_GET['Entrant'];
            
        $cs = Yii::app()->getClientScript();
        //$cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
        $cs->registerCoreScript('jquery.ui');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/kladr.js');

		$this->render('admin',array(
			'model'=>$model,
		));
	}

        public function actionUnchecked()
	{
		$model=new Entrant();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Entrant']))
			$model->attributes=$_GET['Entrant'];

		$this->render('unchecked',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Entrant::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='departments-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    function getHT($id) {
        //if (strlen($id) < 17) $id = '0'.$id;
        if (empty($id)) return 0;
        //echo '<pre>'; print_r($_POST);die;
        $model = Kladr::model()->find('code = '.substr($id, 0, 11));
        //print_r($model->attributes); die;
        if (in_array($model->id_type,
                array('103', '301', '405',)
            ))
            return 1;
        return 0;
    }
}
