<?php


class DirectoryController extends DirectoryMainController {
    
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
        
    public function actionIndex()
    {
        $this->render('index');
    }
    
}

?>
