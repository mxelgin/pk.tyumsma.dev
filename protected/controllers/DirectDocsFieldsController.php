<?php

class DirectDocsFieldsController extends DirectoryMainController
{
    
        function init() {
            $this->breadcrumbs[]='Поля документов';
            parent::init();
        }
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create'),
				'roles'=>array('secretary'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('delete','update'),
				'roles'=>array('administrator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DirectDocsFields;

		// Uncomment the following line if AJAX validation is needed
		//$this->performAjaxValidation($model);

		if(isset($_POST['DirectDocsFields']))
		{
			$model->attributes=$_POST['DirectDocsFields'];
                        $model->save();
			//if($model->save())
				//$this->redirect(array('view','id'=>$model->id));
		}else{
                    if(Yii::app()->request->isAjaxRequest){
                        if ($_GET['type']=='input'){
                            $fieldtypes=$model->fieldtypes;
                            include Yii::app()->getBasePath().'/views/directDocsFields/create.php';
                        }
                    }else{
                        $this->render('create',array(
                                'model'=>$model,
                        ));                    
                    }
                }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            if(Yii::app()->request->isAjaxRequest){
                if ($_GET['type']=='input'){
                    $model=new DirectDocsFields;
                    $fields=$model->findAll('docid='.$id);
                    $fieldtypes=$model->fieldtypes;
                    include Yii::app()->getBasePath().'/views/directDocsFields/update.php';
                }
            }else{
                    $model=$this->loadModel($id);
                    $model->attributes=$_POST['DirectDocsFields'];
                    $model->save();
            }
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DirectDocsFields');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DirectDocsFields('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DirectDocsFields']))
			$model->attributes=$_GET['DirectDocsFields'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DirectDocsFields::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='direct-docs-fields-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
