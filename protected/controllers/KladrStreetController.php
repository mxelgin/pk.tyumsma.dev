<?php

class KladrStreetController extends DirectoryMainController
{
    function init() {
        $this->breadcrumbs[]='Улицы';
        parent::init();
    }
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    public $defaultAction='admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','view', 'getIndex'),
				'roles'=>array('secretary', 'operator'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('delete', 'create', 'update'),
				'roles'=>array('administrator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        return $this->actionUpdate($id);

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new KladrStreet;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KladrStreet']))
		{
			$model->attributes=$_POST['KladrStreet'];
            $model->id_parent = $_POST['parent-id'];
            $model->parent = $_POST['parent'];
			if($model->save()){
                LogController::addLog($this, 'create', $model, $model->name);
				$this->redirect(array('view','id'=>$model->id));
            }
		}

        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery.ui');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-ui-combobox.js');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/kladr.js');

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KladrStreet']))
		{
			$model->attributes=$_POST['KladrStreet'];
            $model->id_parent = $_POST['parent-id'];
            $model->parent = $_POST['parent'];
			if($model->save()){
                LogController::addLog($this, 'update', $model, $model->name);
				$this->redirect(array('view','id'=>$model->id));
            }
		}

        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery.ui');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-ui-combobox.js');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/kladr.js');

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();
            LogController::addLog($this, 'delete', $model, $model->name);

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        return $this->actionAdmin();

		$dataProvider=new CActiveDataProvider('KladrStreet');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KladrStreet('search');
		$model->unsetAttributes();  // clear any default values
        $_GET['KladrStreet']['id_parent'] = $_GET['id_parent'];
		if(isset($_GET['KladrStreet']))
			$model->attributes=$_GET['KladrStreet'];

        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery.ui');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-ui-combobox.js');
        $cs->registerScriptFile(Yii::app()->baseUrl.'/js/kladr.js');

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    public function actionGetIndex() {
        $id = $_GET['id'];
        $model = $this->loadModel($id);
        if ($model->index != '000000'){
            echo $model->index;
            return;
        }
        $model = Kladr::model()->find('code = '.$model->id_parent);
        if (!empty($model->index)) echo $model->index;
        else echo '000000';
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KladrStreet::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kladr-street-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
