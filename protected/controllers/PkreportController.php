<?php

class PkreportController extends ExportMainController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
        public $defaultAction='admin';
        function init() {
            $this->breadcrumbs[]='Отчеты';
            parent::init();
        }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', 
				'actions'=>array('admin', 'report1', 'report2', 'report3', 'report4', 'report5', 'report6'),
				'roles'=>array('operator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionAdmin()
	{
		$this->render('admin');
	}
        
        public function actionReport1()
        {
                $this->layout=false;
                $bm=ConfigBm::model()->findAll('specid in (1,2,3,4)');
                $dm=ConfigDm::model()->findAll('specid in (1,2,3,4)');
                $rcm=ConfigRcm::model()->findAll(array('select'=>'t.*, sum(t.value) as value', 'condition'=>'t.specid in (1,2,3,4)','group'=>'t.specid'));
                $rcmReg=ConfigRcm::model()->findAll(array('order'=>'regionid'));
                $rcmvalues=array();
                $counts=array();
                foreach ($rcm as $r){
                    $rcmvalues[$r->specid]=$r->value;
                    $counts['all'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_sum']+=$counts['all'][$r->specid];
                    $counts['all_ent_sum']+=$counts['all_ent'][$r->specid];
                    $counts['bm'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_b=1 and specs.specid='.$r->specid));
                    $counts['bm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_b=1 and specs.specid='.$r->specid));
                    $counts['bm_sum']+=$counts['bm'][$r->specid];
                    $counts['bm_ent_sum']+=$counts['bm_ent'][$r->specid];
                    $counts['dm'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_d=1 and specs.specid='.$r->specid));
                    $counts['dm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_d=1 and specs.specid='.$r->specid));
                    $counts['dm_sum']+=$counts['dm'][$r->specid];
                    $counts['dm_ent_sum']+=$counts['dm_ent'][$r->specid];
                    $counts['rcm'][$r->specid]=Rcm::model()->count(array('condition'=>'entrantid>0 and specid='.$r->specid));
                    $counts['rcm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'rcm','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and rcm.entrantid>0 and rcm.specid='.$r->specid));
                    $counts['rcm_sum']+=$counts['rcm'][$r->specid];
                    $counts['rcm_ent_sum']+=$counts['rcm_ent'][$r->specid];
                    $counts['vk'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and vk=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vk_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and vk=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vk_sum']+=$counts['vk'][$r->specid];
                    $counts['vk_ent_sum']+=$counts['vk_ent'][$r->specid];
                    $counts['ege'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and grade_rus_doctype=1 and grade_him_doctype=1 and grade_bio_doctype=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['ege_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and grade_rus_doctype=1 and grade_him_doctype=1 and grade_bio_doctype=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['ege_sum']+=$counts['ege'][$r->specid];
                    $counts['ege_ent_sum']+=$counts['ege_ent'][$r->specid];
                    $counts['exam'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (grade_rus_doctype=2 or grade_him_doctype=2 or grade_bio_doctype=2) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['exam_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and (grade_rus_doctype=2 or grade_him_doctype=2 or grade_bio_doctype=2) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['exam_sum']+=$counts['exam'][$r->specid];
                    $counts['exam_ent_sum']+=$counts['exam_ent'][$r->specid];
                    $counts['vuz'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=3 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vuz_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and edu_type=3 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vuz_sum']+=$counts['vuz'][$r->specid];
                    $counts['vuz_ent_sum']+=$counts['vuz_ent'][$r->specid];
                    $counts['spo'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=2 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['spo_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and edu_type=2 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['spo_sum']+=$counts['spo'][$r->specid];
                    $counts['spo_ent_sum']+=$counts['spo_ent'][$r->specid];
                }
                $dmvalues=array();
                foreach ($dm as $d)
                    $dmvalues[$d->specid]=$d->value;
                
                $this->render('report1',array(
                    'bm'=>$bm, 
                    'rcmvalues'=>$rcmvalues, 
                    'dmvalues'=>$dmvalues,
                    'counts'=>$counts,
                    'rcmReg'=>$rcmReg,
                ));
        }
        
        public function actionReport2()
        {
                $this->layout=false;
                $bm=ConfigBm::model()->findAll('specid in (6)');
                $dm=ConfigDm::model()->findAll('specid in (6)');
                $rcm=ConfigRcm::model()->findAll(array('select'=>'t.*, sum(t.value) as value', 'condition'=>'t.specid in (6)','group'=>'t.specid'));
                $rcmvalues=array();
                $counts=array();
                foreach ($rcm as $r){
                    $rcmvalues[$r->specid]=$r->value;
                    $counts['all'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_sum']+=$counts['all'][$r->specid];
                    $counts['all_ent_sum']+=$counts['all_ent'][$r->specid];
                    $counts['bm'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_b=1 and specs.specid='.$r->specid));
                    $counts['bm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_b=1 and specs.specid='.$r->specid));
                    $counts['bm_sum']+=$counts['bm'][$r->specid];
                    $counts['bm_ent_sum']+=$counts['bm_ent'][$r->specid];
                    $counts['dm'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_d=1 and specs.specid='.$r->specid));
                    $counts['dm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_d=1 and specs.specid='.$r->specid));
                    $counts['dm_sum']+=$counts['dm'][$r->specid];
                    $counts['dm_ent_sum']+=$counts['dm_ent'][$r->specid];
                    $counts['rcm'][$r->specid]=Rcm::model()->count(array('condition'=>'entrantid>0 and specid='.$r->specid));
                    $counts['rcm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'rcm','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and rcm.entrantid>0 and rcm.specid='.$r->specid));
                    $counts['rcm_sum']+=$counts['rcm'][$r->specid];
                    $counts['rcm_ent_sum']+=$counts['rcm_ent'][$r->specid];
                    $counts['vk'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and vk=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vk_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and vk=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vk_sum']+=$counts['vk'][$r->specid];
                    $counts['vk_ent_sum']+=$counts['vk_ent'][$r->specid];
                    $counts['ege'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and grade_rus_doctype=1 and grade_him_doctype=1 and grade_bio_doctype=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['ege_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and grade_rus_doctype=1 and grade_him_doctype=1 and grade_bio_doctype=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['ege_sum']+=$counts['ege'][$r->specid];
                    $counts['ege_ent_sum']+=$counts['ege_ent'][$r->specid];
                    $counts['exam'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (grade_rus_doctype=2 or grade_him_doctype=2 or grade_bio_doctype=2) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['exam_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and (grade_rus_doctype=2 or grade_him_doctype=2 or grade_bio_doctype=2) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['exam_sum']+=$counts['exam'][$r->specid];
                    $counts['exam_ent_sum']+=$counts['exam_ent'][$r->specid];
                    $counts['vuz'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=3 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vuz_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and edu_type=3 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vuz_sum']+=$counts['vuz'][$r->specid];
                    $counts['vuz_ent_sum']+=$counts['vuz_ent'][$r->specid];
                    $counts['spo'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=2 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['spo_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and edu_type=2 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['spo_sum']+=$counts['spo'][$r->specid];
                    $counts['spo_ent_sum']+=$counts['spo_ent'][$r->specid];
                }
                $dmvalues=array();
                foreach ($dm as $d)
                    $dmvalues[$d->specid]=$d->value;
                
                $this->render('report2',array(
                    'bm'=>$bm, 
                    'rcmvalues'=>$rcmvalues, 
                    'dmvalues'=>$dmvalues,
                    'counts'=>$counts,
                ));
        }
        
        public function actionReport3()
        {
                $this->layout=false;
                $count['all']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (1,2,3,4)'));
                $count['all_ent']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (1,2,3,4) and (wave_first>0 or wave_second>0 or wave_third>0)'));
                $count['dm']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_d=1 and specs.specid in (1,2,3,4)'));
                $count['dm_ent']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_d=1 and specs.specid in (1,2,3,4)'));
                $count['sng']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (1,2,3,4) and citizenship>1'));
                $count['sng_ent']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (1,2,3,4) and citizenship>1 and (wave_first>0 or wave_second>0 or wave_third>0)'));
                $count['male']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (1,2,3,4) and sex=1'));
                $count['male_ent']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (1,2,3,4) and sex=1 and (wave_first>0 or wave_second>0 or wave_third>0)'));
                $count['exam']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (1,2,3,4) and grade_rus is not null and grade_him is not null and grade_bio is not null'));
                $count['summ_b']=ConfigBm::model()->find(array('select'=>'*, sum(value) as value'))->value;
                $this->render('report3',array(
                    'count'=>$count,
                ));
        }
        
        public function actionReport4()
        {
                $this->layout=false;
                $count['all']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (6)'));
                $count['all_ent']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (6) and (wave_first>0 or wave_second>0 or wave_third>0)'));
                $count['dm']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_d=1 and specs.specid in (6)'));
                $count['dm_ent']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_d=1 and specs.specid in (1,2,3,4)'));
                $count['sng']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (6) and citizenship>1'));
                $count['sng_ent']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (6) and citizenship>1 and (wave_first>0 or wave_second>0 or wave_third>0)'));
                $count['male']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (6) and sex=1'));
                $count['male_ent']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (6) and sex=1 and (wave_first>0 or wave_second>0 or wave_third>0)'));
                $count['exam']=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid in (6) and grade_rus is not null and grade_him is not null and grade_bio is not null'));
                $count['summ_b']=ConfigBm::model()->find(array('select'=>'*, sum(value) as value', 'condition'=>'specid>5'))->value;
                $this->render('report4',array(
                    'count'=>$count,
                ));
        }
        
        public function actionReport5()
        {
                $this->layout=false;
                $bm=ConfigBm::model()->findAll('specid in (1,2,3,4)');
                $dm=ConfigDm::model()->findAll('specid in (1,2,3,4)');
                $counts=array();
                foreach ($bm as $r){
                    $counts['all'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['bm'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_b=1 and specs.specid='.$r->specid));
                    $counts['bm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_b=1 and specs.specid='.$r->specid));
                    $counts['dm'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_d=1 and specs.specid='.$r->specid));
                    $counts['dm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_d=1 and specs.specid='.$r->specid));
                    $counts['vk'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and vk=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vk_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and vk=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_tmn'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72000%" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_hmao'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "86%" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_yanao'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "89%" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_ug'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street regexp "^720[01-22]" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_other'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street not regexp "^72|^86|^89" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                }
                $dmvalues=array();
                foreach ($dm as $d)
                    $dmvalues[$d->specid]=$d->value;
                
                $this->render('report5',array(
                    'bm'=>$bm, 
                    'dmvalues'=>$dmvalues,
                    'counts'=>$counts,
                ));
        }
        
        public function actionReport6()
        {
                $this->layout=false;
                $bm=ConfigBm::model()->findAll('specid in (6)');
                $dm=ConfigDm::model()->findAll('specid in (6)');
                $counts=array();
                foreach ($bm as $r){
                    $counts['all'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['bm'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_b=1 and specs.specid='.$r->specid));
                    $counts['bm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_b=1 and specs.specid='.$r->specid));
                    $counts['dm'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and specs.level=1 and specs.type_d=1 and specs.specid='.$r->specid));
                    $counts['dm_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.type_d=1 and specs.specid='.$r->specid));
                    $counts['vk'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and vk=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['vk_ent'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (wave_first>0 or wave_second>0 or wave_third>0) and vk=1 and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_tmn'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72000%" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_hmao'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "86%" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_yanao'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "89%" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_ug'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street regexp "^720[01-22]" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                    $counts['all_ent_other'][$r->specid]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street not regexp "^72|^86|^89" and (wave_first>0 or wave_second>0 or wave_third>0) and specs.level=1 and specs.specid='.$r->specid));
                }
                $dmvalues=array();
                foreach ($dm as $d)
                    $dmvalues[$d->specid]=$d->value;
                
                $this->render('report6',array(
                    'bm'=>$bm, 
                    'dmvalues'=>$dmvalues,
                    'counts'=>$counts,
                ));
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Entrant::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='departments-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
