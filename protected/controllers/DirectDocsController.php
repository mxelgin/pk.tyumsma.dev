<?php

class DirectDocsController extends DirectoryMainController
{
        public $defaultAction='admin';
    
        function init() {
            $this->breadcrumbs[]='Документы';
            parent::init();
        }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create', 'view'),
				'roles'=>array('secretary'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('delete','update'),
				'roles'=>array('administrator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $category=new DirectDocsCategories;
                $categories=$category->findAll();
                
                $fieldModel= new DirectDocsFields;
                $fields=$fieldModel->findAll('docid='.$id);
                
                $model=$this->loadModel($id);

		$this->render('view',array(
			'model'=>$model, 'acttypes'=>$model->acttypes, 'categories'=>$categories, 'fields'=>$fields, 'fieldtypes'=>$fieldModel->fieldtypes,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DirectDocs;
		//Uncomment the following line if AJAX validation is needed
                $this->performAjaxValidation($model);

                $category=new DirectDocsCategories;
                $categories=$category->findAll();

		if(isset($_POST['DirectDocs']))
		{
			$model->attributes=$_POST['DirectDocs'];
			if($model->save()){
                                if ($model->attributes['action']=='input'){
                                    $field = Yii::app()->createController('DirectDocsFields');
                                    foreach ($_POST['field-name'] as $key=>$fieldsName){
                                        $_POST['DirectDocsFields']['docid']=$model->id;
                                        $_POST['DirectDocsFields']['name']=$fieldsName;
                                        $_POST['DirectDocsFields']['type']=$_POST['field-type'][$key];
                                        $_POST['DirectDocsFields']['required']=intval($_POST['field-required'][$key]);
                                        $_POST['DirectDocsFields']['editonly']=intval($_POST['field-editonly'][$key]);
                                        $field[0]->actionCreate();
                                    }
                                }
                LogController::addLog($this, 'create', $model, $model->name);
				$this->redirect(array('view','id'=>$model->id));
                        }
		}

                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');

		$this->render('create',array(
			'model'=>$model, 'acttypes'=>$model->acttypes, 'categories'=>$categories,
		));
	}
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		//Uncomment the following line if AJAX validation is needed
                $this->performAjaxValidation($model);

                $category=new DirectDocsCategories;
                $categories=$category->findAll();

		if(isset($_POST['DirectDocs']))
		{
                        if ($model->action=='input' && $_POST['DirectDocs']['action']=='checkbox'){
                            $fieldModel= new DirectDocsFields;
                            $fields=$fieldModel->deleteAll('docid='.$id);
                        }
                        $model->attributes=$_POST['DirectDocs'];
			if($model->save()){
                                if ($model->attributes['action']=='input'){
                                    $fieldupdate = Yii::app()->createController('DirectDocsFields');
                                    if (count($_POST['ufield-name'])){
                                        foreach ($_POST['ufield-name'] as $key=>$fieldsName){
                                            $_POST['DirectDocsFields']['docid']=$model->id;
                                            $_POST['DirectDocsFields']['name']=$fieldsName;
                                            $_POST['DirectDocsFields']['type']=$_POST['ufield-type'][$key];
                                            $_POST['DirectDocsFields']['required']=intval($_POST['ufield-required'][$key]);
                                            $_POST['DirectDocsFields']['editonly']=intval($_POST['ufield-editonly'][$key]);
                                            $fieldupdate[0]->actionUpdate($key);
                                        }
                                    }
                                    $fieldcreate = Yii::app()->createController('DirectDocsFields');
                                    foreach ($_POST['field-name'] as $key=>$fieldsName){
                                        $_POST['DirectDocsFields']['docid']=$model->id;
                                        $_POST['DirectDocsFields']['name']=$fieldsName;
                                        $_POST['DirectDocsFields']['type']=$_POST['field-type'][$key];
                                        $_POST['DirectDocsFields']['required']=intval($_POST['field-required'][$key]);
                                        $_POST['DirectDocsFields']['editonly']=intval($_POST['field-editonly'][$key]);
                                        $fieldcreate[0]->actionCreate();
                                    }
                                }
                LogController::addLog($this, 'update', $model, $model->name);
				$this->redirect(array('view','id'=>$model->id));
                        }
		}

                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');

		$this->render('update',array(
			'model'=>$model, 'acttypes'=>$model->acttypes, 'categories'=>$categories,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
                    // we only allow deletion via POST request
                        $fieldModel= new DirectDocsFields;
                        $fields=$fieldModel->deleteAll('docid='.$id);
            LogController::addLog($this, 'delete', $model, $model->name);
                        
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DirectDocs('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DirectDocs']))
			$model->attributes=$_GET['DirectDocs'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DirectDocs::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='direct-docs-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
