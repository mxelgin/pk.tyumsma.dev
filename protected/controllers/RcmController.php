<?php

class RcmController extends Controller
{
        public $defaultAction='admin';
        /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin', 'delete','create','update', 'setrcm', 'stat'),
				'roles'=>array('secretary', 'administrator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionStat()
	{
		$this->render('stat');
	}

	public function actionSetrcm($id, $rcm)
	{
            $model=$this->loadModel($rcm);
            $pass_number=Docs::model()->find('entrantid='.$id.' and docid=21 and fieldid=13');
            $entrant=Entrant::model()->findByPk($id);
            $rcmok=true;
            if (empty($entrant->orig)){
                $rcmok=false;
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
            if ($entrant->specs[0]->specid!=$model->specid){
                $rcmok=false;
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
            if (empty($entrant->specs[0]->type_b)){
                $rcmok=false;
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }

            if ($model->pass_number!=$pass_number->value){
                $rcmok=false;
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            }
            
            if ($rcmok){
                $model->setAttribute('entrantid', $id);
                $model->save();
                $this->redirect('/pkentrant/view/id/'.$id);
            }
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Rcm;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Rcm']))
		{
			$model->attributes=$_POST['Rcm'];
			if($model->save()){
                LogController::addLog($this, 'create', $model, $model->name);
				$this->redirect(array('admin'));
            }
		}

                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.meio.mask.min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.placeholder.min2.js');
                $cs->registerCoreScript('jquery.ui');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
                
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Rcm']))
		{
			$model->attributes=$_POST['Rcm'];
			if($model->save()){
                LogController::addLog($this, 'update', $model, $model->name);
				$this->redirect(array('admin'));
            }
		}

                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.meio.mask.min.js');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.placeholder.min2.js');
                $cs->registerCoreScript('jquery.ui');
                $cs->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
            LogController::addLog($this, 'delete', $model, $model->name);
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Rcm');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Rcm('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Rcm']))
			$model->attributes=$_GET['Rcm'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Rcm::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rcm-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
