<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
                $model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm'])) {
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {

                $model_conf = Config::model()->find('name=:descr', array(':descr'=>'sessionTime'));
                $h = $model_conf->value;
                $s = $h * 60 * 60;
                $s = $s ? $s : 31536000 * 5;
                //echo $s;
                yii::app()->user->setState('userSessionTimeout', time() + $s);
                //echo '<pre>'; print_r(date("Y-m-d H:i:s", yii::app()->user->getState('userSessionTimeout'))); echo '</pre>'; die();

				$this->redirect('/pkentrant');
            }
		}

        // display the login form
        if (Yii::app()->user->isGuest)
            $frm_login = $this->renderFile($this->getViewFile ('application.views.site.login'),array('model'=>$model),true);
    
        $model_conf = Config::model()->find('name=:descr', array(':descr'=>'description'));
        $descr_txt = $model_conf->value;

        $this->render('index',array('descr_txt'=>$descr_txt,'frm_login'=>$frm_login));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
        
}
