<?php
define('test', true);
if (test) define('SERVER', 'http://priem.edu.ru:8000/import/importservice.svc');
else define('SERVER', 'http://10.0.3.1:8080/import/importservice.svc');

//define('USER', 'iritro@inbox.ru');
//define('PASS', 'j1Ud8au');
define('USER', 'priem_tgma@mail.ru');
define('PASS', '1qaZXsw2');

define('SHIFT', '    ');

class XML2Array {

    private static $xml = null;
    private static $encoding = 'UTF-8';

    /**
     * Initialize the root XML node [optional]
     * @param $version
     * @param $encoding
     * @param $format_output
     */
    public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {
        self::$xml = new DOMDocument($version, $encoding);
        self::$xml->formatOutput = $format_output;
        self::$encoding = $encoding;
    }

    /**
     * Convert an XML to Array
     * @param string $node_name - name of the root node to be converted
     * @param array $arr - aray to be converterd
     * @return DOMDocument
     */
    public static function &createArray($input_xml) {
        //echo '<pre>'; print_r($input_xml); die;
        $xml = self::getXMLRoot();
        if(is_string($input_xml)) {
            $parsed = $xml->loadXML($input_xml);
            if(!$parsed) {
                throw new Exception('[XML2Array] Error parsing the XML string.');
            }
        } else {
            if(get_class($input_xml) != 'DOMDocument') {
                throw new Exception('[XML2Array] The input XML object should be of type: DOMDocument.');
            }
            $xml = self::$xml = $input_xml;
        }
        $array[$xml->documentElement->tagName] = self::convert($xml->documentElement);
        self::$xml = null;    // clear the xml node in the class for 2nd time use.
        return $array;
    }

    /**
     * Convert an Array to XML
     * @param mixed $node - XML as a string or as an object of DOMDocument
     * @return mixed
     */
    private static function &convert($node) {
        $output = array();

        switch ($node->nodeType) {
            case XML_CDATA_SECTION_NODE:
                $output['@cdata'] = trim($node->textContent);
                break;

            case XML_TEXT_NODE:
                $output = trim($node->textContent);
                break;

            case XML_ELEMENT_NODE:

                // for each child node, call the covert function recursively
                for ($i=0, $m=$node->childNodes->length; $i<$m; $i++) {
                    $child = $node->childNodes->item($i);
                    $v = self::convert($child);
                    if(isset($child->tagName)) {
                        $t = $child->tagName;

                        // assume more nodes of same kind are coming
                        if(!isset($output[$t])) {
                            $output[$t] = array();
                        }
                        $output[$t][] = $v;
                    } else {
                        //check if it is not an empty text node
                        if($v !== '') {
                            $output = $v;
                        }
                    }
                }

                if(is_array($output)) {
                    // if only one node of its kind, assign it directly instead if array($value);
                    foreach ($output as $t => $v) {
                        if(is_array($v) && count($v)==1) {
                            $output[$t] = $v[0];
                        }
                    }
                    if(empty($output)) {
                        //for empty nodes
                        $output = '';
                    }
                }

                // loop through the attributes and collect them
                if($node->attributes->length) {
                    $a = array();
                    foreach($node->attributes as $attrName => $attrNode) {
                        $a[$attrName] = (string) $attrNode->value;
                    }
                    // if its an leaf node, store the value in @value instead of directly storing it.
                    if(!is_array($output)) {
                        $output = array('@value' => $output);
                    }
                    $output['@attributes'] = $a;
                }
                break;
        }
        return $output;
    }

    /*
     * Get the root XML node, if there isn't one, create it.
     */
    private static function getXMLRoot(){
        if(empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }
}

class MechanicalBear{
     
    private $writer;
    private $version = '1.0';
    private $encoding = 'UTF-8';
    private $rootName = 'Root';
    private $shift  = 1;
    private $curl;
    public  $server = SERVER;
    public  $dicts;
    public  $prefix = '2014';
        // 10 - з/о, 11 - о, 12 - вечер
        // 14 бюджет 15 с оплатой 16 целевой
    public  $edulevels = array(
        array(
            2,//бакалавриат
            array(
                array(10, 15, '2013-06-20', '2013-07-25', '2013-08-12'),
                array(11, 14, '2013-06-20', '2013-07-25', '2013-08-05', 1),
                array(11, 14, '2013-06-20', '2013-07-25', '2013-08-10', 2),
                array(11, 15, '2013-06-20', '2013-07-25', '2013-08-12'),
            ),
            array(
                array('060500', 15,   0,  15,  40),
            )
        ),
        array(
            5,//специалитет
            array(
                array(10, 14, '2013-06-20', '2013-07-25', '2013-08-12'),
                array(10, 15, '2013-06-20', '2013-07-25', '2013-08-12'),
                array(11, 14, '2013-06-20', '2013-07-25', '2013-08-05', 1),
                array(11, 14, '2013-06-20', '2013-07-25', '2013-08-10', 2),
                array(11, 15, '2013-06-20', '2013-07-25', '2013-08-12'),
            ),
            array(
                array('060101', 245,   0, 120,   0),
                array('060103',  75,   0,  60,   0),
                array('060201',  15,   0,  60,   0),
                array('060301',  32,   0,  85,  50),
            )
        ),
    );
     
 
    function __construct() {
        $this->writer = new XMLWriter();
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($this->curl, CURLOPT_POST, true);
    }

    function writeRaw($text) {
        if (test) $this->writer->writeRaw($text);
    }
     
    public function convertArrayToXML($data) {
        $this->writer->openMemory();
        $this->writer->startDocument($this->version, $this->encoding);
        $this->writer->startElement($this->rootName);
        if (is_array($data)) {
            $this->getXML($data);
        }
        $this->writeRaw("\n");
        $this->writer->endElement();
        return $this->writer->outputMemory();
    }

    public function setVersion($version) {
        $this->version = $version;
    }

    public function setEncoding($encoding) {
        $this->encoding = $encoding;
    }

    public function setRootName($rootName) {
        $this->rootName = $rootName;
    }

    private function getXML($data) {
        foreach ($data as $key => $val) {
            if ($val === null || $val === array()) continue;
            /*
            if (is_numeric($key)) {
                $key = 'key'.$key;
            }
            */
            if (is_array($val)) {
                if (is_numeric($key)) {
                    $this->getXML($val);
                    continue;
                }

                $this->writeRaw("\n");
                for ($i = 0; $i < $this->shift; $i++)
                    $this->writeRaw(SHIFT);
                $this->writer->startElement(ucfirst($key));

                $this->shift++;
                $this->getXML($val);
                $this->shift--;

                $this->writeRaw("\n");
                for ($i = 0; $i < $this->shift; $i++)
                    $this->writeRaw(SHIFT);
                $this->writer->endElement();
            }
            else {
                $this->writeRaw("\n");
                for ($i = 0; $i < $this->shift; $i++)
                    $this->writeRaw(SHIFT);
                $this->writer->writeElement(ucfirst($key), $val);
            }
        }
    }

    /*
    Convert date format ??.??.???? to ????-??-??
    */
    public static function convertDateToSQL($s) {
        if (mb_strpos($s, '.', 0, 'utf-8') !== false) {
            $a = explode('.', $s);

            if (count($a) != 3) return $s;

            if     (strlen($a[2]) == 4) $s = $a[2];
            elseif (strlen($a[2]) == 2)
                if ($a[2] + 0 > 15) $s = '19' . $a[2];
                else $s = '20' . $a[2];
            else return $s;

            $s .= '-';

            if   (strlen($a[1]) == 1) $s .= '0'.$a[1];
            else $s .= $a[1];

            $s .= '-';

            if   (strlen($a[0]) == 1) $s .= '0'.$a[0];
            else $s .= $a[0];

            return $s;
        }
        elseif (mb_strpos($s, '-', 0, 'utf-8') !== false) {
            return trim(mb_substr($s, 0, 10, 'utf-8'));
        }
        return $s;
    }

    public function sendXML($path, $xml){
        if ($this->server == '') return 'Не указан сервер отправки данных.';

        curl_setopt($this->curl, CURLOPT_URL, $this->server . $path);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $xml);

        $result = curl_exec($this->curl);
        
        if($result === false)
        {
            echo 'Ошибка curl: ' . curl_error($this->curl);
        }

        $result = XML2Array::createArray($result);

        if (!isset($result['Error'])) return $result;

        switch ($result['Error']) {
            case 'No access to system':
                $result = "Ошибка доступа к системе.\nПроверьте имя пользователя и пароль.";
                break;
            default:
                $result = "Неизвестная ошибка!\n" . print_r($result['Error'], true);
        }

        return $result;
    }

    public function deleteData() {
        $auth = array(
            'authData' => array(
                'login' => USER,
                'pass' => PASS,
            ),
        );
        $xml = $this->convertArrayToXML($auth);

        $result = $this->sendXML('/delete', $xml);
    }

    public function getDictionaries() {
        $auth = array(
            'authData' => array(
                'login' => USER,
                'pass' => PASS,
            ),
        );

        $xml = $this->convertArrayToXML($auth);

        $result = $this->sendXML('/dictionary', $xml);
        $this->dicts = array();

        foreach ($result['Dictionaries']['Dictionary'] as $k => $v) {
            if ($v['Code'] == 3 ) continue;
            $req = $auth;
            $req['getDictionaryContent'] = array(
                'dictionaryCode' => $v['Code'],
            );

            $xml = $this->convertArrayToXML($req);
            $xml = $this->sendXML('/dictionaryDetails', $xml);
            $this->dicts[$xml['DictionaryData']['Code']] = array(
                'name' => $xml['DictionaryData']['Name'],
                'data' => $xml['DictionaryData']['DictionaryItems']['DictionaryItem'],
            );
        }
    }
}


class PkexportController extends ExportMainController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $defaultAction='admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','mssqlexport','csvexport', 'xmlentrants', 'xmlparents', 'csvBank', 'csvClinik', 'educon'),
				'roles'=>array('operator'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('modulEIS', 'modulFIS', 'modulFISnew', 'modulFISorder', 'modulFISresult', 'modulFISdelete', 'modulFISdeleteResult', 'gz', 'dekanat'),
				'roles'=>array('administrator'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('modulEIS'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
                            'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $model=$this->loadModel($id);
                $docfields= new Docs();
                $mdocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->maindoc);
                //$gradedocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->gradedoc);
                $dopdocs=$docfields->findAll('entrantid='.$id.' and docid<>'.$model->maindoc);
		$this->render('view',array(
			'model'=>$model, 'mdocfields'=>$mdocfields, 'dopdocs'=>$dopdocs
		));
	}
        
        public function actionCsvBank()
        {
                $this->layout=false;
                $model=new Entrant;
                $entrants=$model->findAll('(wave_first>0 or wave_second>0 or wave_third>0) and enrol_type=1');
                $this->render('csvbank',array(
                    'model'=>$model, 'entrants'=>$entrants,
                ));
        }
        
        public function actionEducon()
        {
                $spec=(int)$_GET['spec'];
                $this->layout=false;
                $model=new Entrant;
                $entrants=$model->findAll('(wave_first='.$spec.' or wave_second='.$spec.' or wave_third='.$spec.')');
                $groupname='';
                if ($spec==1) $groupname='Л-2012-01';
                if ($spec==2) $groupname='С-2012-01';
                if ($spec==3) $groupname='П-2012-01';
                if ($spec==4) $groupname='Ф-2012-01';
                if ($spec==5) $groupname='СД-2012-01';
                if ($spec==6) $groupname='ФЗ-2012-01';
                if ($spec==7) $groupname='СДЗ-2012-01';
                $this->render('educon',array(
                    'model'=>$model, 'entrants'=>$entrants, 'groupname'=>$groupname,
                ));
        }

        public function actionCsvClinik()
        {
                $this->layout=false;
                $model=new Entrant;
                $entrants=$model->findAll('wave_first>0 or wave_second>0 or wave_third>0');
                $this->render('csvclinik',array(
                    'model'=>$model, 'entrants'=>$entrants,
                ));
        }

        public function actionXmlentrants()
        {
                $this->layout=false;
                $model=new Entrant;
                $entrants=$model->findAll('wave_first>0 or wave_second>0 or wave_third>0');
                $this->render('xmlentrants',array(
                    'model'=>$model, 'entrants'=>$entrants,
                ));
        }

        public function actionXmlparents()
        {
                $this->layout=false;
                $model=new Entrant;
                $parents=Parents::model()->findAll(array('with'=>'entrant', 'condition'=>'entrant.wave_first>0 or entrant.wave_second>0 or entrant.wave_third>0'));
                $this->render('xmlparents',array(
                    'model'=>$model, 'parents'=>$parents,
                ));
        }
        
        public function actionModulEIS()
        {
                $departments=Departments::model()->findAll();
                $dirFaculty=new SoapClient('http://eis.tyumsma.ru/DirFaculty/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'pk', 'password'=>'vasiliev'));
                $dirSpecialty=new SoapClient('http://eis.tyumsma.ru/DirSpecialty/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'pk', 'password'=>'vasiliev'));
                $dirCourse=new SoapClient('http://eis.tyumsma.ru/DirCourse/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'pk', 'password'=>'vasiliev'));
                $stdGroups=new SoapClient('http://eis.tyumsma.ru/StdGroups/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'pk', 'password'=>'vasiliev'));
                $eisEvents=new SoapClient('http://eis.tyumsma.ru/EisEvents/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'pk', 'password'=>'vasiliev'));
                $stdEdutypes=new SoapClient('http://eis.tyumsma.ru/StdEdutypes/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'pk', 'password'=>'vasiliev'));
                $dirCitizenships=new SoapClient('http://eis.tyumsma.ru/DirCitizenship/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'pk', 'password'=>'vasiliev'));
                $citizenships=DirectCitizenship::model()->findAll();
                //print_r($dirSpecialty->add_record(2, array('name'=>'test3', 'code'=>'321', 'faculty_id'=>7)));
                //print_r($dirSpecialty->get_record(1));
                
                foreach ($departments as $dp){
                    $eisdepartment=$dirFaculty->get_record(array('uid'=>$dp->id));
                    if (!$eisdepartment){
                        $eisdepartment=$dirFaculty->add_record($dp->id, array('name'=>$dp->name));
                    }
                    $eisspec=$dirSpecialty->get_record(array('code'=>$dp->code));
                    if (!$eisspec){
                        $spar=explode(' ',trim($dp->specname));
                        $code=$spar[count($spar)-1];
                        unset($spar[count($spar)-1]);
                        $name=implode(' ', $spar);
                        $eisspec=$dirSpecialty->add_record($dp->id, array('name'=>$name, 'code'=>$code, 'faculty_id'=>$eisdepartment[uid]));
                    }
                    $coursearray=array(1=>'Первый');
                    $eiscourse=$dirCourse->get_record(array('uid'=>1));
                    if (!$eiscourse){
                        $eiscourse=$dirCourse->add_record(1, array('name'=>$coursearray[1]));
                    }
                    $groupsarray=array(1=>'Л-2014-01', 2=>'С-2014-01', 3=>'П-2014-01', 4=>'Ф-2014-01', 5=>'СД-2014-01', 6=>'ФЗ-2014-01', 7=>'СДЗ-2014-01');
                    $eisgroups=$stdGroups->get_record(array('name'=>$groupsarray[$dp->id]));
                    if (!$eisgroups){
                        $eisgroups=$stdGroups->add_record($dp->id, array('name'=>$groupsarray[$dp->id], 'faculty_id'=>$eisdepartment[uid], 'specialty_id'=>$eisspec[uid], 'course_id'=>$eiscourse[uid]));
                    }   
                }
                $stdtypesarray=array(1=>'Очно', 2=>'Заочно');
                foreach ($stdtypesarray as $k=>$v){
                    $record=$stdEdutypes->get_record(array('name'=>$v));
                    if (!$record){
                        $record=$stdEdutypes->add_record($k, array('name'=>$v));
                    }   
                }
                
                foreach ($citizenships as $ctz){
                    $record=$dirCitizenships->get_record(array('id'=>$ctz->id));
                    if (!$record){
                        $recname = $dirCitizenships->get_record(array('name'=>$ctz->name));
                        if ($recname){
                            $dirCitizenships->addInternalID($ctz->id, $recname[uid]);
                        }
                        else{
                            $record=$dirCitizenships->add_record($ctz->id, array('name'=>$ctz->name));
                        }
                    }
                }
            
                $model=new Entrant;
                $entrants=$model->findAll('wave_first>0 or wave_second>0 or wave_third>0');
                $stdStudents=new SoapClient('http://eis.tyumsma.ru/StdStudents/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'pk', 'password'=>'vasiliev'));
                foreach ($entrants as $ent){
                    $atr=array();
                    $atr['lastname']=$ent->fam;
                    $atr['name']=$ent->name;
                    $atr['m_name']=$ent->m_name;

                    $entspec=$dirSpecialty->get_record(array('code'=>$ent->department->code));
                    for ($k = 0 ; $k < count($groupsarray); $k++){
                        $entgroups=$stdGroups->get_record(array('specialty_id'=>$entspec[uid], 'name'=>$groupsarray[$k]));
                        if ($entgroups) {
                            $atr['group']=$entgroups[uid];
                            break;
                        }
                    }

                    $entEdu=$stdEdutypes->get_record(array('name'=>'Очно'));
                    $atr['edu_type'] = $entEdu[uid];


                    if ($ent->sex==1) $atr['sex']='male';
                    elseif ($ent->sex==2) $atr['sex']='female';
                    $atr['birth_date']=date('Y-m-d',strtotime($ent->birth_date));
//                    if (!empty($ent->wave_first)) $atr['group']=$ent->wave_first;
//                    if (!empty($ent->wave_second)) $atr['group']=$ent->wave_second;
//                    if (!empty($ent->wave_third)) $atr['group']=$ent->wave_third;
//                    if ($atr['group']<6) $atr['edu_type']=1;
//                    else $atr['edu_type']=2;
                    $atr['address_official']=$ent->street->parent->type_text .'. '. $ent->street->parent->name.', ул. '.$ent->street->name.',  д. '.$ent->home_build.' кв. '.$ent->home_kv;
                    $atr['address_fias_official']=$ent->kladr_street;
//                    $atr['address_official']=$ent->home_city_db->name.', ул. '.$ent->home_street.',  д. '.$ent->home_build.' кв. '.$ent->home_kv;
                    $atr['address_actual']=$ent->home_fact;
                    //$atr['contract']=0;
                    $recCitizenship = $dirCitizenships->get_record(array('id'=>$ent->citizenship));
                    $atr['citizenship']=$recCitizenship[uid];
                    $pass=Docs::model()->find('docid=21 and fieldid=13 and entrantid='.$ent->id);
                    $pass_who=Docs::model()->find('docid=21 and fieldid=15 and entrantid='.$ent->id);
                    $pass_date=Docs::model()->find('docid=21 and fieldid=14 and entrantid='.$ent->id);
                    $atr['passport']=$pass->value;
                    $atr['passport_org']=$pass_who->value;
                    $atr['passport_date']=date('Y-m-d',strtotime($pass_date->value));
                    //$atr['passport_org_code']=$pass_date;
                    $atr['phone']=$ent->phone;
                    //$atr['nationality']=$ent->phone;
                    if ($ent->service==1) $atr['army']='yes';
                    if ($ent->service==2) $atr['army']='no';
                    $atr['sport']=$ent->sport;
                    $atr['talents']=$ent->talent;
                    $atr['community']=0;
                    //print_r($atr);
                    $atr['nationality'] = 0;
                    if ($ent->wave_third >0 && $ent->enrol_type == 2) $atr['typeKBK'] = 1;

                    if (!$stdStudents->get_record(array('id'=>20140000+$ent->id))){
                        try{
                            $record=$stdStudents->add_record(20140000+$ent->id, $atr);
                            //print_r($record);
                        }catch (SoapFault $e){
                            print_r($stdStudents->__getLastResponse().'<br />!fail! <br />');
                        }
                    }
                }
            //$stdStudents=new SoapClient('http://eis.tyumsma.ru/StdStudents/mv', array('cache_wsdl' => WSDL_CACHE_NONE, 'trace' => 1, 'login'=>'educon', 'password'=>'test'));
            //print_r($stdStudents->get_record(1458));

                //print_r($dirFaculty->addInternalID(10,1));

                   /* $events=$eisEvents->get_events(0);
                    //print_r($events);
                    foreach ($events as $ev){
                        print_r($ev);
                    }*/
                /*try{
                    $record=$stdGroups->get_record('',7);
                    print_r($record);
                }catch (SoapFault $e){
                    print_r($dirCourse->__getLastResponse().'<br />!fail! <br />');
                }*/
                echo '<br />done<br />';
        }

    public function actionModulFISresult($n) {
        $mb = new MechanicalBear();
		$dataProvider=new CActiveDataProvider('Entrant');

        $req = array(
            'authData' => array(
                'login' => USER,
                'pass' => PASS,
            ),
            'GetResultImportApplication' => array(
                'PackageGUID' => $n,
            ),
        );

        $xml = $mb->convertArrayToXML($req);

        $result = "dummy";
        $result = $mb->sendXML('/import/result', $xml);

		$this->render('fis',array(
			'dataProvider'=>$dataProvider,
            'server' => $mb->server,
            'xml' => $xml,
            'result' => $result,
            'path' => '/import/result',
		));
    }

    public function actionModulFISdeleteResult($n) {
        $mb = new MechanicalBear();
		$dataProvider=new CActiveDataProvider('Entrant');

        $req = array(
            'authData' => array(
                'login' => USER,
                'pass' => PASS,
            ),
            'GetResultDeleteApplication' => array(
                'PackageGUID' => $n,
            ),
        );

        $xml = $mb->convertArrayToXML($req);

        //$result = "dummy";
        $result = $mb->sendXML('/delete/result', $xml);

		$this->render('fis',array(
			'dataProvider'=>$dataProvider,
            'server' => $mb->server,
            'xml' => $xml,
            'result' => $result,
            'path' => '/delete/result',
		));
    }

    public function actionModulFISdelete() {
        $mb = new MechanicalBear();
		$dataProvider=new CActiveDataProvider('Entrant');
        $data = $dataProvider->model->findAll();

        $req = array(
            'authData' => array(
                'login' => USER,
                'pass' => PASS,
            ),
        );

        $xml = $mb->convertArrayToXML($req);

        //$result = "dummy";
        $d = $mb->sendXML('/institutioninfo', $xml);

/*
        echo '<pre>';
        print_r($d);
        die;
*/

        $req['dataForDelete'] = array( 'applications' => array());

        $k = 0;
        $result = "";
        $d = $d['InstitutionExports'];
/*
echo '<pre>';
print_r($d);
die();
*/
        if (empty($d['InstitutionExport']['Applications']['Application'][0])) {
            $v = $d['InstitutionExport']['Applications']['Application'];
            if (!empty($v['ApplicationNumber'])) {
                $a = array(
                    'application' => array(
                        'applicationNumber' => $v['ApplicationNumber'],
                        'registrationDate' => $v['RegistrationDate'],
                    ),
                );
                $req['dataForDelete']['applications'][] = $a;
                $req['dataForDelete']['ordersOfAdmission'][] = $a;
            }

        } else {
            foreach ($d['InstitutionExport']['Applications']['Application'] as $v) {
                //if ($v['ApplicationNumber'] == 0) continue;
                if ($k++ == 500) {
                    $xml = $mb->convertArrayToXML($req);
                    $r = $mb->sendXML('/delete', $xml);
                    $result .= print_r($r, true);

                    $req['dataForDelete'] = array( 'applications' => array());
                    $k = 0;
                }

                $a = array(
                    'application' => array(
                        'applicationNumber' => $v['ApplicationNumber'],
                        'registrationDate' => $v['RegistrationDate'],
                    ),
                );
                $req['dataForDelete']['applications'][] = $a;
                $req['dataForDelete']['ordersOfAdmission'][] = $a;
            }
        }
        /*
        foreach ($d['InstitutionExport']['Campaigns']['Campaign'] as $v) {
            $req['dataForDelete']['campaigns'][] = array(
                'campaign' => array(
                ),
            );
        }
        */
        /*
        echo '<pre>';
        print_r($req);
        die;
        */

        $xml = $mb->convertArrayToXML($req);
        $r = $mb->sendXML('/delete', $xml);
        $result .= print_r($r, true);

		$this->render('fis',array(
			'dataProvider'=>$dataProvider,
            'server' => $mb->server,
            'xml' => $xml,
            'result' => $result,
            'dicts' => $d,
            'path' => '/delete',
		));
    }

    public function actionModulFISnew() {
        return $this->actionModulFIS(true);
    }
        
    public function actionModulFIS($new = false) {
        $mb = new MechanicalBear();
		$dataProvider=new CActiveDataProvider('Entrant');
        /*
        $criteria = new CDbCriteria();
        //$criteria->alias = 'spec';
        $criteria->join = 'JOIN pk_entrant_spec 
                        ON pk_entrant_spec.entrantid = t.id
                        AND pk_entrant_spec.specid <> 0';
        $data = $dataProvider->model->findAll($criteria);
        */
        $data = $dataProvider->model->findAll('sentToFIS = 0');
        $mb->getDictionaries();
        $prefix = $mb->prefix;

/*
        echo '<pre>';
        print_r(count($data));
        die();
        if (test) $edulevels = array(5);
        else 
*/
        // 10 - з/о, 11 - о, 12 - вечер
        // 14 бюджет 15 с оплатой 16 целевой
        $edulevels = $mb->edulevels;

        $m = array();

        foreach ($mb->dicts as $s) {
            if ($s['name'] == 'Направления подготовки')
                break;
        }

        for ($i = 0; $i < count($edulevels); $i++) {
            $e = $edulevels[$i];
            for ($j = 0; $j < count($e[2]); $j++) {
                $a = $e[2][$j];
                foreach ($s['data'] as $k) {
                    if ($k['Code'] == $a[0] && $k['QualificationCode'] == '6'. $e[0]) {
                        $a['id'] = $k['ID'];
                        break;
                    }
                }
                $e[2][$j] = $a;
            }
            $edulevels[$i] = $e;
        }
/*
        echo '<pre>';
        print_r($directions);
        die();
*/

        $xml = array(
            'authData' => array(
                'login' => USER,
                'pass' => PASS,
            ),
            'packageData' => array(),
        );

        if ($new) {
            $campaignInfo = array(
                'campaigns' => array(
                    'campaign' => array(
                        'UID' => $prefix.'1',
                        'name' => 'Приемная кампания 2014',
                        'yearStart' => 2014,
                        'yearEnd' => 2014,
                        'educationForms' => array(
                            array(
                                'educationFormID' => 10,// 10 - з/о, 11 - о, 12 - вечер
                            ),
                            array(
                                'educationFormID' => 11,
                            ),
                            /*
                            array(
                                'educationFormID' => 12,
                            ),
                            */
                        ),
                        'statusID' => 1, // 0 - не начался, 1 - идет, 2 - окончен
    /*
                        'educationLevels' => array(
                            array(
                                'educationLevel' => array(
                                    'course' => 1, //'TODO',
                                    'educationLevelID' => $edulevel,
                                ),
                            ),
                        ),
                        'campaignDates' => array(),
    */
                    ),
                ),
            );
            foreach ($edulevels as $e) {
                $campaignInfo['campaigns']['campaign']['educationLevels'][]['educationLevel'] = array(
                    'course' => 1, //'TODO',
                    'educationLevelID' => $e[0],
                );
            }
        

            $campaignDate = array( 'campaignDate' => array(),);

            $m = 1;
            foreach ($edulevels as $e) {
                foreach ($e[1] as $a) {
                    $campaignDate = array();
                    $campaignDate['UID'] = $prefix.$m++;
                    $campaignDate['course'] = 1;
                    $campaignDate['educationLevelID'] = $e[0];
                    $campaignDate['educationFormID'] = $a[0];
                    $campaignDate['educationSourceID'] = $a[1];
                    if (isset($a[5])) $campaignDate['stage'] = $a[5];
                    $campaignDate['dateStart']  = $a[2];
                    $campaignDate['dateEnd']    = $a[3];
                    $campaignDate['dateOrder']  = $a[4];

                    $campaignInfo['campaigns']['campaign']['campaignDates'][]['campaignDate'] = $campaignDate;
                }
            }
            $xml['packageData']['campaignInfo'] = $campaignInfo;
            
            $admissionInfo = array(
                'admissionVolume' => array(),
                    'competitiveGroups' => array(
                        array(
                            'competitiveGroup' => array(
                                'UID' => $prefix.'1',
                                'campaignUID' => $prefix.'1',
                                'course' => 1,//'TODO',
                                'name' => 'Конкурсная группа №1',
                                'items' => array(),
                                /*
                                'targetOrganizations' => array(),
                                'commonBenefit' => array(),
                                */
                                'entranceTestItems' => array(
                                    array(
                                        'entranceTestItem' => array(
                                            'UID' => $prefix.'1',
                                            'entranceTestTypeID' => 1,
                                            'form' => 1, // TODO
                                            'entranceTestSubject' => array(
                                                'subjectID' => 1,
                                            ),
                                        ),
                                    ),
                                    array(
                                        'entranceTestItem' => array(
                                            'UID' => $prefix.'2',
                                            'entranceTestTypeID' => 1,
                                            'form' => 1, // TODO
                                            'entranceTestSubject' => array(
                                                'subjectID' => 4,
                                            ),
                                        ),
                                    ),
                                    array(
                                    'entranceTestItem' => array(
                                        'UID' => $prefix.'3',
                                        'entranceTestTypeID' => 1,
                                        'form' => 1, // TODO
                                        'entranceTestSubject' => array(
                                            'subjectID' => 11,
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            );
        }

        $id = 1;

        foreach ($edulevels as $e) {
            foreach ($e[2] as $a) {
                $item = array();
                $item['UID'] = $prefix.$id++;
                $item['campaignUID'] = $prefix.'1';
                $item['educationLevelID'] = $e[0];
                $item['course'] = 1;
                $item['directionID'] = $a['id'];
                $item['numberBudgetO']  = $a[1];
                $item['numberBudgetZ']  = $a[2];
                $item['numberPaidO']    = $a[3];
                $item['numberPaidZ']    = $a[4];

                $admissionInfo['admissionVolume'][] = array('item' => $item);
                $item['campaignUID'] = null;
                $item['course'] = null;
                $admissionInfo['competitiveGroups'][0]['competitiveGroup']['items'][] = array('competitiveGroupItem' => $item);
            }
        }

        $xml['packageData']['admissionInfo'] = $admissionInfo;

        //$j = 1;
        $t = 1;
        $apporder = array();

        for ($i = 0; !$new && $i < count($data); $i++) {
            //if ($data[$i]->sentToFIS) continue;

            // 14 - бюджет
            // 15 - с оплатой
            // 16 - целевой 
            $finform = $this->getFinForm($data[$i]->id);
            //echo '<pre>';print_r($finform); 
	    //
            for ($k = 0; $k < count($finform); $k++) {
                $f = $finform[$k];
                foreach ($edulevels as $e) {
                    foreach ($e[2] as $a) {
                        //echo $a[0] . '|' . $f['spec'] . "|\n";
                        if ($a[0] == $f['spec']) {
                            $f['id'] = $a['id'];
                            $f['eid'] = $e[0];
                            foreach ($e[1] as $b) {
                                if ($b[0] != $f['eduform']) continue;
                                if ($b[1] != $f['type']) continue;
                                //echo "<pre>"; print_r($b);
                                if (!empty($b[5])) {
                                    $f['stage'] = $b[5];
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    if (isset($f['id'])) break;
                }
                foreach ($xml['packageData']['admissionInfo']['admissionVolume'] as $x) {
                    $x = $x['item'];
                    if ($x['directionID'] == $f['id']) {
                        $f['cid'] = $x['UID'];
                        break;
                    }
                }
                $finform[$k] = $f;
            }

            //if ($data[$i]->id == 1206) { echo '<pre>'; print_r($finform); die(); }
            // 10 - з/о, 11 - о, 12 - вечер

            $status = $data[$i]->took ? 6 : 4;

	    $dir = $data[$i]->wave_first;
	    if (empty($dir)) $dir = $data[$i]->wave_second;
	    if (empty($dir)) $dir = $data[$i]->wave_third;

	    if (!empty($dir)
		//$data[$i]->wave_first
                //|| $data[$i]->wave_second
                //|| $data[$i]->wave_third
                ) {
                $a = array(
                    'application' => array(
                        'applicationNumber' => $prefix.$data[$i]->id,
                        'registrationDate' => str_replace(' ', 'T', $data[$i]->dateadd),
                    ),
                );
                for ($k = 0; $k < count($finform); $k++) {
                    if ($finform[$k]['specid'] == $dir && $data[$i]->wave_third == 0) break;
                    if ($finform[$k]['specid'] == $dir && $data[$i]->wave_third != 0){
                        if ($data[$i]->enrol_type == 1 && $finform[$k]['type'] == 14) break;
                        if ($data[$i]->enrol_type == 2 && $finform[$k]['type'] == 15) break;
                    }
                }
                if ($k != count($finform)) {
                    $a['financeSourceID'] = $finform[$k]['type'];
                    $a['educationFormID'] = $finform[$k]['eduform'];
                    $a['directionID'] = $finform[$k]['id'];
                    $a['educationLevelID'] = $finform[$k]['eid'];
                    if (isset($finform[$k]['stage'])) $a['stage'] = $finform[$k]['stage'];
                    //$a['isBeneficiary'] = 0;
                    $apporder[]= array('orderOfAdmission' => $a);
                    //$status = 8;
                }
            }

            $application = array(
                'UID' => $prefix.$data[$i]->id,
                'applicationNumber' => $prefix.$data[$i]->id,
                'entrant' => array(
                    'UID' => $prefix.$data[$i]->id,
                    'firstName' => $data[$i]->name,
                    'middleName' => $data[$i]->m_name,
                    'lastName' => $data[$i]->fam,
                    'genderID' => $data[$i]->sex,
                    /*
                    'customInformation' => 0,
                    'snils' => 0,
                    */
                ),
                'registrationDate' => str_replace(' ', 'T', $data[$i]->dateadd),
                //'registrationDate' => MechanicalBear::convertDateToSQL($data[$i]->dateadd),
                'needHostel' => $data[$i]->need_hostel,
                'statusID' => $status, // 4 принято 6 отозвано 8 приказ
                'selectedCompetitiveGroups' => array(),
                'finSourceAndEduForms' => array(),
                /*
                'applicationCommonBenefit' => array(),
                'entranceTestResults' => array(),
                */
                'selectedCompetitiveGroupItems' => array(),
                'applicationDocuments' => array(
                    $this->getEge($data[$i]->id),
                    $this->getPassport($data[$i]->id),
                    $this->getEduDoc($data[$i]->id),
                    /*
                    'militaryCardDocument' => array(
                        //'UID' => 0,
                        'originalReceived' => 0,
                        'originalReceivedDate' => 0,
                        'documentSeries' => 0,
                        'documentNumber' => 0,
                        'documentDate' => 0,
                        'documentOrganization' => 0,
                    ),
                    'customDocuments' => array(
                        array(
                            'customDocument' => array(
                                //'UID' => 0,
                                'originalReceived' => 0,
                                'originalReceivedDate' => 0,
                                'documentSeries' => 0,
                                'documentNumber' => 0,
                                'documentDate' => 0,
                                'documentOrganization' => 0,
                                'additionalInfo' => 0,
                                'documentTypeNameText' => 0,
                            ),
                        ),
                    ),
                    'giaDocuments' => array(
                        array(
                            'giaDocument' => array(
                                'UID' => $data[$i]->id,
                                'originalReceived' => 0,
                                'originalReceivedDate' => 0,
                                'documentNumber' => 0,
                                'documentDate' => 0,
                                'documentOrganization' => 0,
                                'subjects' => array(
                                    array(
                                        'subjectData' => array(
                                            'subjectID' => 0,
                                            'value' => 0,
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    */
                ),
            );
            $spec=EntrantSpec::model()->findAll(array('with'=>'spec', 'condition'=>'t.entrantid='.$data[$i]->id));
            $compGroups=array();
            foreach ($spec as $s){
                if (empty($s->specid)) continue;
                //если регионально-целевой
                $rcm= Rcm::model()->find("entrantid=".$data[$i]->id." and specid=".$s->specid);
                if ($rcm){
                    $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.'001',);
                    $compGroups[]=$prefix.'_'.$s->specid.'001';
                    //Элементы Конкурсной группы
                    $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.'001',);
                    continue;
                }
                if ($data[$i]->crimea){
                    $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.'kr',);
                    $compGroups[]=$prefix.'_'.$s->specid.'kr';
                    //Элементы Конкурсной группы
                    $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.'kr',);
                    continue;                    
                }
                //конкурсные группы
                if ($s->type_b && $s->type_d){
                        $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.$s->type_b.'0',);
                        $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.'0'.$s->type_d,);
                        $compGroups[]=$prefix.'_'.$s->specid.$s->type_b.'0';
                        $compGroups[]=$prefix.'_'.$s->specid.'0'.$s->type_d;
                        //Элементы Конкурсной группы
                        $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.$s->type_b.'0',);
                        $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.'0'.$s->type_d,);
                 }else{
                    $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.$s->type_b.$s->type_d,);
                    $compGroups[]=$prefix.'_'.$s->specid.$s->type_b.$s->type_d;
                    //Элементы Конкурсной группы
                    $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.$s->type_b.$s->type_d,);
                }
            }


            $res = $this->getResults($data[$i]->id);
            $testResults = array();
            $subs = array(1, 4, 11);
            foreach ($compGroups as $cG){
                foreach ($subs as $s) {
                    if (empty($res[$s])) continue;

                    $r = array(
                        'UID' => $prefix.$data[$i]->id.str_pad ($s, 2,"0", STR_PAD_LEFT).$cG,
                        //'UID' => $prefix.$t++,
                        'resultValue' => $res[$s],
                        'resultSourceTypeID' => 1,
                        'entranceTestSubject' => array(
                            'subjectID' => $s,
                        ),
                        'entranceTestTypeID' => 1,
                        'competitiveGroupID' => $cG,
                    );
                    switch ($s) {
                        case  1: $v = $data[$i]->grade_rus_doctype; break;
                        case  4: $v = $data[$i]->grade_bio_doctype; break;
                        case 11: $v = $data[$i]->grade_him_doctype; break;
                        default: $v = 0;
                    }
                    $r['resultSourceTypeID'] = $v;
                    $testResults[] = array(
                        'entranceTestResult' => $r,
                    );
                }
            }

            if (!empty($testResults)) 
                $application['entranceTestResults'] = $testResults;


            // форма обучения
            //$f = array_unique($finform, SORT_REGULAR);

            foreach ($finform as $v) {
                $application['finSourceAndEduForms'][] = array(
                    'finSourceEduForm' => array(
                        'financeSourceID' => $v['type'],
                        'educationFormID' => $v['eduform'],
                        'targetOrganizationUID' => $v['targetuid'],
                    ),
                );
                //$application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => '2014_210',);
            }
            //$application['applicationNumber'] = $j++;
            $xml['packageData']['applications'][] = array('application' => $application);
            $xml['packageData']['ordersOfAdmission'] = $apporder;
            
            //if ($i >= 5) break; // dummy
            $sent[] = $data[$i];
        }
        if ($new) {
            unset($xml['packageData']['applications']);
            unset($xml['packageData']['ordersOfAdmission']);
        } else {
            unset($xml['packageData']['campaignInfo']);
            unset($xml['packageData']['admissionInfo']);

            if (sizeof($sent) > 0)
                foreach ($sent as $ent) { // а вот и хз приняли его там или нет
                    $ent->sentToFIS = true;
                    $ent->save();
                }
        }
        /*
        */
/*
        echo '<pre>';
        print_r($xml);
        die();
*/

        $xml = $mb->convertArrayToXML($xml);

        $result = "dummy";
        $result = $mb->sendXML('/import', $xml);

		$this->render('fis',array(
			'dataProvider'=>$dataProvider,
            'server' => $mb->server,
            'xml' => $xml,
            'result' => $result,
            'dicts' => $mb->dicts,
            'path' => '/import',
		));
        }

	/**
	 * Lists all models.
	 */
	public function actionIndex() {
            $this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Entrant();
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Entrant']))
			$model->attributes=$_GET['Entrant'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionMssqlexport()
        {
                $this->layout=false;
                $model=new Entrant;
                if($_GET['group']){ 
                //$docfields= new Docs();
                ////$mdocfields=$docfields->findAll('entrantid=2 and docid=21');
                //$gradedocfields=$docfields->findAll('entrantid='.$id.' and docid='.$model->gradedoc);
                //$dopdocs=$docfields->findAll('entrantid='.$id.' and docid<>'.$model->maindoc);*/
                //$this->renderPartial('view', array('data'=>$model, 'mdocfields'=>$mdocfields, 'dopdocs'=>$dopdocs));
                    $data=$model->findAll('wave_first>0 or wave_second>0 or wave_third>0');
                    $this->render('view',array(
			'model'=>$model, 'data'=>$data, 'group'=>$_GET['group'], 'kod'=>$_GET['kod'],
                    ));
               } else $this->redirect(array('admin'));
        }
        
    public function actionCsvexport()
    {
                $this->layout=false;
                $model=new Entrant;
                //$docfields= new Docs();
                $data=$model->findAll();
                $this->render('list',array(
			'model'=>$model, 'data'=>$data,
                ));
    }

    private function arrPrintParam($wave, $enrolType){
        $this->layout=false;
        $model= new Entrant;
        if (empty($enrolType)) $enrolType=1;
        if (empty($wave)) $wave=1;
        if ($wave==1) $wave_sql='wave_first';
        elseif ($wave==2) $wave_sql='wave_second';
        elseif ($wave==3) $wave_sql='wave_third';

        if ($model->exists($wave_sql.'>0 and enrol_type='.$enrolType)){
            $specs=Departments::model()->findAll();
            $regions=DirectRcmRegions::model()->findAll(array('order'=>'ord'));
            $regname=array();
            foreach ($regions as $reg) $regname[$reg->id]=$reg->name;
            //$entrantsvkb=array();
            //$entrantsvkd=array();
            $entrantsrcm=array();
            $entrants=array();
            foreach ($specs as $spec){
                foreach ($regions as $rrcm){
                    $entrantsrcm[$spec->id][$rrcm->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and ".$wave_sql."=".$spec->id." and enrol_type=".$enrolType." and rcm.entrantid is not null and rcm.regionid=".$rrcm->id, 'order'=>'vk DESC, rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC', 'together'=>true));
                }

                $entrants[$spec->id]=$model->findAll(array('with'=>array('rcm', 'specs'), 'condition'=>"deleted=0 and took=0 and ".$wave_sql."=".$spec->id." and enrol_type=".$enrolType." and rcm.entrantid is null", 'order'=>'rating DESC, grade_him DESC, grade_bio DESC, edu_grade DESC'));
            }
        }
        return array(
            'specs'=>$specs,
            'entrants'=>$entrants,
            'entrantsrcm'=>$entrantsrcm,
            'regname'=>$regname,
        );

    }

    public function actionGz(){
        $this->render('gz',
            array(  'wave1'=> $this->arrPrintParam(1,1),
                    'wave2'=> $this->arrPrintParam(2,1),
                    'wave3'=> $this->arrPrintParam(3,1),
                    'wave4'=> $this->arrPrintParam(3,2)
            ));
    }
    public function actionDekanat(){
        $this->render('dekanat',
            array(  'wave1'=> $this->arrPrintParam(1,1),
                'wave2'=> $this->arrPrintParam(2,1),
                'wave3'=> $this->arrPrintParam(3,1),
                'wave4'=> $this->arrPrintParam(3,2)
            ));
    }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Entrant::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='departments-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    static function getPassport($entrantid) {
        $rf = true;
        $docs =new CActiveDataProvider('Docs');
        $data = $docs->model->findAllByAttributes(
            array(
                'entrantid' => $entrantid,
                'docid' => 21,
            )
        );
        if (empty($data)) {
            $data = $docs->model->findAllByAttributes(
                array(
                    'entrantid' => $entrantid,
                    'docid' => 47,
                )
            );
            $rf = false;
        }

        $doc = array();
        $doc['originalReceived']   = 1;
        $doc['documentSeries']     = 'n/a';
        $doc['documentNumber']     = 'n/a';
        $doc['documentDate']       = '1900-01-01';
        $doc['IdentityDocumentTypeID']  = $rf ? 1 : 3;
        $doc['nationalityTypeID']       = 1;    // РФ как определить страну по паспорту?
        $doc['birthDate']          = '1900-01-01';

        if (count($data) == 0) return array('identityDocument' => $doc);

        for ($i = 0; $i < count($data); $i++) {
            $d = $data[$i];
            switch ($d->fieldid) {
                case 0:
                    $doc['originalReceived'] = $d->value;
                    break;
                case 13:
                    $s = explode('-', $d->value);
                    if (sizeof($s) == 1) $s = explode(' ', $d->value);
                    $n = $s[1];
                    $s = $s[0];
                    if (!empty($s)) $doc['documentSeries'] = $s;
                    if (!empty($n)) $doc['documentNumber'] = $n;
                    break;
                case 34:
                    //$s = explode(' ', $d->value, 2);
                    $n = explode(' ', $d->value);
                    $s = '';
                    for ($i = 0; $i < count($n) - 1; $i++) $s .= $n[$i];
                    $n = $n[count($n) - 1];

                    $n = str_replace(' ', '', $n);
                    $s = str_replace(' ', '', $s);
                    if (empty($n)) { $n = $s; $s = null; }

                    if (!empty($s)) $doc['documentSeries'] = $s;
                    if (!empty($n)) $doc['documentNumber'] = $n;
                    break;
                case 14:
                    $doc['documentDate'] = MechanicalBear::convertDateToSQL($d->value);
                    break;
                case 15:
                    $doc['documentOrganization'] = $d->value;
                    break;
            }
        }
                $dataProvider=new CActiveDataProvider('Entrant');
        $data = $dataProvider->model->findByPK($entrantid);
        if ($data != null) {
            $doc['birthDate'] = MechanicalBear::convertDateToSQL($data->birth_date);
        }

        return array('identityDocument' => $doc);
    }

    static function getEduDoc($entrantid) {
        $docs =new CActiveDataProvider('Docs');
        $data = $docs->model->findAllByAttributes(
            array(
                'entrantid' => $entrantid,
                'docid' => 29,
            )
        );

        $type = 'basicDiplomaDocument';
        $doc = array();
        $doc['originalReceived']       =  1;
        $doc['documentSeries']         = 'n/a';
        $doc['documentNumber']         = 'n/a';
        $doc['documentDate']           = '1900-01-01';
        $doc['documentOrganization']   = 'n/a';

        if (count($data) == 0)
            return array('eduDocuments' => array(
                    'eduDocument' => array(
                        $type => $doc
                    ),
                ),
            );

        for ($i = 0; $i < count($data); $i++) {
            $d = $data[$i];
            switch ($d->fieldid) {
                case 0:
                    $doc['originalReceived'] = $d->value;
                    break;
                case 20:
                    if (!empty($d->value)) $doc['documentNumber'] = $d->value;
                    break;
                case 21:
                    $doc['documentDate'] = MechanicalBear::convertDateToSQL($d->value);
                    break;
                case 19: // TYPE
                    $type = mb_strtolower($d->value, 'utf-8');
                    //$type = str_ireplace('a', 'а', $type);
                    if (mb_strpos($type, 'аттестат', 0, 'utf-8') !== false || mb_strpos($type, 'среднем образовании', 0, 'utf-8') !== false)   $type = 1;
                    elseif (mb_strpos($type, 'ысш', 0, 'utf-8') !== false)    $type = 2;
                    elseif (mb_strpos($type, 'диплом', 0, 'utf-8') !== false) $type = 3;
                    break;
            }
        }

        $s = str_replace(' ', '', $doc['documentNumber']);
        $s = str_replace('№', '', $s);

        if (!empty($s))
            switch ($type) {
                case 1:
                    $type = 'schoolCertificateDocument';
                    $doc['documentSeries'] = trim(mb_substr($s, -7, 10, 'utf-8'));
                    $doc['documentNumber'] = trim(mb_substr($s, 0, -7, 'utf-8'));
//                    $doc['OriginalReceived'] = 1;
                    break;
                case 2:
                    $type = 'highEduDiplomaDocument';
                    $doc['documentSeries'] = trim(mb_substr($s, 0, 3, 'utf-8'));
                    $doc['documentNumber'] = trim(mb_substr($s, 3, 10, 'utf-8'));
//                    $doc['OriginalReceived'] = 1;
                    break;
                case 3:
                    $type = 'middleEduDiplomaDocument';
                    $doc['documentSeries'] = trim(mb_substr($s, 0, 4, 'utf-8'));
                    $doc['documentNumber'] = trim(mb_substr($s, 4, 10, 'utf-8'));
//                    $doc['OriginalReceived'] = 1;
                    break;
                default:
                    if (!empty($type)) $doc['documentSeries'] = $type;
                    $type = 'basicDiplomaDocument';
//                    $doc['OriginalReceived'] = 1;
            }
        else
            $type = 'basicDiplomaDocument';

        if (empty($doc['documentNumber'])) $doc['documentNumber'] = 'n/a';

        return array('eduDocuments' => array(
                'eduDocument' => array(
                    $type => $doc
                ),
            ),
        );
    }

    static function getFinForm($entrantid) {
        $e = EntrantSpec::model()->findAllByAttributes(array('entrantid' => $entrantid,));
        $data = new CActiveDataProvider('Departments');
        $res = array();
        for ($i = 0; $i < count($e); $i++) {
            if ($e[$i]->specid == 0) continue;

            $s = $data->model->findByAttributes( array( 'id' => $e[$i]->specid,));
            $s = explode(' ', $s->specname);
            $spec = $s[count($s) - 1];
            $eduf = $e[$i]->specid > 5 ? 10 : 11;
            
            $rcm=Rcm::model()->find("entrantid=".$entrantid." and specid=".$e[$i]->specid);
            if ($rcm){
                $res[] = array( 'spec' => $spec, 'type' => 16, 'eduform' => $eduf, 'specid' => $e[$i]->specid, 'targetuid'=>'2014_'.$rcm->regionid );
                continue;
            }

            if ($e[$i]->type_b) $res[] = array( 'spec' => $spec, 'type' => 14, 'eduform' => $eduf, 'specid' => $e[$i]->specid );
            if ($e[$i]->type_d) $res[] = array( 'spec' => $spec, 'type' => 15, 'eduform' => $eduf, 'specid' => $e[$i]->specid );
        }
        return $res;
    }

    static function addSub($s, $subs, $i) {
        if (!empty($s[$i])) {
            $subs[] = array(
                'subjectData' => array(
                    'subjectID' => $i,
                    'value' => $s[$i],
                ),
            );
        }
    }

    static function getEge($entrantid) {
        $s = PkexportController::getResults($entrantid, true);
        if (empty($s)) return null;

        $subs = array();
        PkexportController::addSub($s, $subs, 1);
        PkexportController::addSub($s, $subs, 4);
        PkexportController::addSub($s, $subs, 11);

        if (empty($subs)) return null;

        return array(
            'egeDocuments' => array(
                array(
                    'egeDocument' => array(
                        'UID' => $entrantid,
                        'originalReceived' => $s['orig'],
                        //'originalReceivedDate' => 'TODO',
                        'documentNumber' => $s['docNum'],
                        'documentYear' => $s['docYear'],
                        'subjects' => $subs,
                    ),
                ),
            ),
        );
    }

    static function getResults($entrantid, $ege = false) {
		$data = new CActiveDataProvider('Entrant');
        $e = $data->model->findByPk($entrantid);

        if ($ege
            & $e->grade_rus_doctype == 2
            & $e->grade_him_doctype == 2
            & $e->grade_bio_doctype == 2
            ) return null;

        $s = array();
        if (!empty($e->grade_rus)) $s[1] = $e->grade_rus;
        if (!empty($e->grade_him)) $s[11] = $e->grade_him;
        if (!empty($e->grade_bio)) $s[4] = $e->grade_bio;

        $docYear = '20' . substr($e->grade_rus_doc, -2);
        if ($docYear == '20' || $docYear == '20-2') {
            $docYear = '20' . substr($e->grade_him_doc, -2);
            if ($docYear == '20' || $docYear == '20-2') {
                $docYear = '20' . substr($e->grade_bio_doc, -2);
                if ($docYear == '20' || $docYear == '20-2')
                    $docYear = '1900-01-01';
            }
        }
        $docNum = $e->grade_rus_doc;
        if (empty($docNum)) $docNum = 'n/a';

        $s['docYear'] = $docYear;
        $s['docNum'] = $docNum;
        $s['orig'] = $e->orig;

        return $s;
    }


    static function fisDelete($id) {
        $conf = new Config;
        $k = $conf->find('name = "onlineDataUpload"')->value;
        if ($k != 1) return;
        
        //echo "DELETE: " . $i . "<br />";
        $mb = new MechanicalBear();
        $prefix = $mb->prefix;

        $model = new Entrant;
        $model = $model->findByPk($id);

        $req = array(
            'authData' => array(
                'login' => USER,
                'pass' => PASS,
            ),
        );
        $req['dataForDelete'] = array(
            'applications' => array(
                    'application' => array(
                        'applicationNumber' => $prefix.$id,
                        'registrationDate' => str_replace(' ', 'T', $model->dateadd),
                    ),
            )
        );
        $xml = $mb->convertArrayToXML($req);
        $r = $mb->sendXML('/delete', $xml);

        //print_r($r); die;
    }
    static function fisCreate($id) {
        $conf = new Config;
        $k = $conf->find('name = "onlineDataUpload"')->value;
        if ($k != 1) return;
        
        //echo "CREATE: " . $id . "<br />";

        $mb = new MechanicalBear();
        $prefix = $mb->prefix;

        $model = new Entrant;
        $model = $model->findByPk($id);

        $req = array(
            'authData' => array(
                'login' => USER,
                'pass' => PASS,
            ),
        );

        $data = array($model);
        $i = 1;
        $status = 4;
        $edulevels = $mb->edulevels;

        $admissionInfo = array();

        foreach ($edulevels as $e) {
            foreach ($e[2] as $a) {
                $item = array();
                $item['UID'] = $prefix.$i++;
                $item['campaignUID'] = $prefix.'1';
                $item['educationLevelID'] = $e[0];
                $item['course'] = 1;
                $item['directionID'] = $a['id'];
                $item['numberBudgetO']  = $a[1];
                $item['numberBudgetZ']  = $a[2];
                $item['numberPaidO']    = $a[3];
                $item['numberPaidZ']    = $a[4];

                $admissionInfo['admissionVolume'][] = array('item' => $item);
                $item['campaignUID'] = null;
                $item['course'] = null;
                $admissionInfo['competitiveGroups'][0]['competitiveGroup']['items'][] = array('competitiveGroupItem' => $item);
            }
        }
        //echo '<pre>';print_r($admissionInfo); die;
        $i = 0;

        $finform = PkexportController::getFinForm($data[$i]->id);
        for ($k = 0; $k < count($finform); $k++) {
            $f = $finform[$k];
            foreach ($edulevels as $e) {
                foreach ($e[2] as $a) {
                    //echo $a[0] . '|' . $f['spec'] . "|\n";
                    if ($a[0] == $f['spec']) {
                        $f['id'] = $a['id'];
                        $f['eid'] = $e[0];
                        foreach ($e[1] as $b) {
                            if ($b[0] != $f['eduform']) continue;
                            if ($b[1] != $f['type']) continue;
                            //echo "<pre>"; print_r($b);
                            if (!empty($b[5])) {
                                $f['stage'] = $b[5];
                                break;
                            }
                        }
                        break;
                    }
                }
                if (isset($f['id'])) break;
            }
            foreach ($admissionInfo['admissionVolume'] as $x) {
                $x = $x['item'];
                if ($x['directionID'] == $f['id']) {
                    $f['cid'] = $x['UID'];
                    break;
                }
            }
            $finform[$k] = $f;
        }

        //echo '<pre>';print_r($finform); die;

        $application = array(
            'UID' => $prefix.$id,
            'applicationNumber' => $prefix.$data[$i]->id,
            'entrant' => array(
                'UID' => $prefix.$id,
                'firstName' => $data[$i]->name,
                'middleName' => $data[$i]->m_name,
                'lastName' => $data[$i]->fam,
                'genderID' => $data[$i]->sex,
            ),
            'registrationDate' => str_replace(' ', 'T', $data[$i]->dateadd),
            'needHostel' => $data[$i]->need_hostel,
            'statusID' => $status, // 4 принято 6 отозвано 8 приказ
            'selectedCompetitiveGroups' => array(),
            'selectedCompetitiveGroupItems' => array(),
            'finSourceAndEduForms' => array(),
            'applicationDocuments' => array(
                PkexportController::getEge($data[$i]->id),
                PkexportController::getPassport($data[$i]->id),
                PkexportController::getEduDoc($data[$i]->id),
            ),
        );
        
        
        $spec=EntrantSpec::model()->findAll(array('with'=>'spec', 'condition'=>'t.entrantid='.$id));
        $compGroups=array();
        foreach ($spec as $s){
            if (empty($s->specid)) continue;
            //если регионально-целевой
            $rcm=  Rcm::model()->find("entrantid=".$id." and specid=".$s->specid);
            if ($rcm){
                $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.'001',);
                $compGroups[]=$prefix.'_'.$s->specid.'001';
                //Элементы Конкурсной группы
                $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.'001',);
                continue;
            }
            if ($data[$i]->crimea){
                $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.'kr',);
                $compGroups[]=$prefix.'_'.$s->specid.'kr';
                //Элементы Конкурсной группы
                $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.'kr',);
                continue;                    
            }
            //конкурсные группы
            if ($s->type_b && $s->type_d){
                $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.$s->type_b.'0',);
                $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.'0'.$s->type_d,);
                $compGroups[]=$prefix.'_'.$s->specid.$s->type_b.'0';
                $compGroups[]=$prefix.'_'.$s->specid.'0'.$s->type_d;
                //Элементы Конкурсной группы
                $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.$s->type_b.'0',);
                $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.'0'.$s->type_d,);
            }else{
                $application['selectedCompetitiveGroups'][]=array('competitiveGroupID' => $prefix.'_'.$s->specid.$s->type_b.$s->type_d,);
                $compGroups[]=$prefix.'_'.$s->specid.$s->type_b.$s->type_d;
                //Элементы Конкурсной группы
                $application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $prefix.'_'.$s->specid.$s->type_b.$s->type_d,);
            }
        }

        $res = PkexportController::getResults($data[$i]->id);
        $testResults = array();
        $t = $id;
        $subs = array(1, 4, 11);
        foreach ($compGroups as $cG){
            foreach ($subs as $s) {
                if (empty($res[$s])) continue;
                $r = array(
                    'UID' => $prefix.$data[$i]->id.str_pad ($s, 2,"0", STR_PAD_LEFT).$cG,
                    //'UID' => $prefix.$t++,
                    'resultValue' => $res[$s],
                    'resultSourceTypeID' => 1,
                    'entranceTestSubject' => array(
                        'subjectID' => $s,
                    ),
                    'entranceTestTypeID' => 1,
                    'competitiveGroupID' => $cG,
                );
                switch ($s) {
                    case  1: $v = $data[$i]->grade_rus_doctype; break;
                    case  4: $v = $data[$i]->grade_bio_doctype; break;
                    case 11: $v = $data[$i]->grade_him_doctype; break;
                    default: $v = 0;
                }
                $r['resultSourceTypeID'] = $v;
                $testResults[] = array(
                    'entranceTestResult' => $r,
                );
            }
        }

        if (!empty($testResults)) 
            $application['entranceTestResults'] = $testResults;

        foreach ($finform as $v) {
            $application['finSourceAndEduForms'][] = array(
                'finSourceEduForm' => array(
                    'financeSourceID' => $v['type'],
                    'educationFormID' => $v['eduform'],
                    'targetOrganizationUID' => $v['targetuid'],
                ),
            );
            //$application['selectedCompetitiveGroupItems'][] = array('competitiveGroupItemID' => $v['cid'],);
        }
        $req['packageData']['applications'][] = array('application' => $application);
        //echo '<pre>'; print_r($req); die;
        
        $xml = $mb->convertArrayToXML($req);
        //echo '<pre>'; print_r($xml); die;
        $r = $mb->sendXML('/import', $xml);

        $model->sentToFIS = true;
        $model->save();

        //print_r($r); die;
    }
    static function fisUpdate($id) {
        $conf = new Config;
        $k = $conf->find('name = "onlineDataUpload"')->value;
        if ($k != 1) return;

        PkexportController::fisDelete($id);
        PkexportController::fisCreate($id);
    }
}
