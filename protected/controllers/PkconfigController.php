<?php

class PkconfigController extends AdminPanelMainController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
        function init() {
            $this->breadcrumbs[]='Общее';
            parent::init();
        }

	/**
	 * @return array action filters
	 */
	public function filters()
        {
            return array(
               'accessControl',
            );
        }
        
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','update', 'create', 'delete', 'index', 'view', 'bagfixcity'),
				'roles'=>array('administrator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Config;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Config']))
		{
			$model->attributes=$_POST['Config'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id=0)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Config']))
		{
			$model->attributes=$_POST['Config'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $saved=false;
		if(isset($_POST['Config']))
		{
                    $saved=true;
                    foreach ($_POST['Config'] as $key=>$config){
                        $model=Config::model()->findByPk($key);
                        $model->value=$config['value'];
			if (!$model->save())
                            $saved=false;
                    }
		}
                Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );
                $records=Config::model()->findAll();
		$this->render('update',array(
			'records'=>$records, 'saved'=>$saved,
		));
        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Config('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Config']))
			$model->attributes=$_GET['Config'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Config::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='config-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function get_real_address($cityid){
            $realcity=DirectCity::model()->findByPk((int)$cityid);
            if (is_numeric($realcity->name) && $realcity->name!=0) return $this->get_real_address($realcity->name);
            else return $realcity;
        }
        
        /*
         * fix bug on exportController
         */
        public function actionBagfixcity(){
            $city=DirectCity::model()->findAll();
            $i=0;
            foreach ($city as $c){
                if (is_numeric($c->name)){
                    $realcity=$this->get_real_address($c->name);
                    echo '<p>'.$c->id.' '.$c->name.' -> '.$realcity->name.'<br />';
                    $ents=Entrant::model()->findAll('birth_place_id='.$c->id);
                    foreach ($ents as $e){
                        echo $e->fam.' '.$e->name.' '.$e->m_name.'<br />';
                        $e->birth_place_id=$realcity->name;
                        //echo '!'.$e->birth_place_id.'!';
                        $e->save();
                        $i++;
                    }
                    //$c->delete();
                    echo '</p>';
                }
            }
            echo '<p>'.$i.'</p>';
            $city=DirectCity::model()->findAll();
            foreach ($city as $c){
                if (is_numeric($c->name)){
                    $c->delete();
                }
            }
            
            /*$ents=Entrant::model()->findAll();
            $i=0;
            foreach ($ents as $e){
                $city=DirectCity::model()->findByPk($e->birth_place_id);
                if (is_numeric($city->name)){
                    $i++;
                }
            }
            echo '<p>'.$i.'</p>';*/
        }
}
