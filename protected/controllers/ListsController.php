<?php

class ListsController extends ExportMainController
{
    public $defaultAction='admin';

	public function actionAdmin() {
        $specs=Departments::model()->findAll();

		$this->render('admin', array(
            'specs' => $specs,
            )
        );
	}

	public function actionAlpha() {
        $this->layout=false;
                
        if (!empty($_GET['spec'])) $spec = "and specs.specid = " . $_GET['spec'];
        $entrants = Entrant::model()->findAll(
            array(
                'with'=>array('specs'),
                'condition'=>"deleted=0 and wave_first=0 and wave_second=0 and wave_third=0 and took=0 $spec",
                'order'=>'fam ASC, name ASC, m_name ASC',
                //'limit'=>30,
            )
        );

        $title = "Алфавитный список абитуриентов";

        $head = array(
            '№',
            'ФИО абитуриента',
            'Копия',
        );
        $data = array();
        $i = 1;
        foreach ($entrants as $e) {
            $data[] = array(
                $i++,
                $e->fam . ' ' . $e->name . ' ' . $e->m_name,
                $e->orig ? '': 'Да'
            );
        }

		$this->render('report',array(
            'title' => $title,
			'head'=>$head,
			'data'=>$data,
                        'watermark'=>1,
		));
	}

	public function actionPRcm() {
        $this->layout=false;
        $nrcm = array();
        if (!empty($_GET['spec'])) {
            $rcms=ConfigRcm::model()->findAll('specid = ' . $_GET['spec']);
            if (!empty($rcms))
                foreach ($rcms as $rcm)
                    $nrcm[$rcm->regionid] = $rcm->value;
        }
                
        if (!empty($_GET['spec'])) $spec = "and specs.specid = " . $_GET['spec'];
        foreach ($nrcm as $k => $n)
            $entrants[$n] = Entrant::model()->findAll(
                array(
                    'with'=>array('rcm', 'specs'),
                    'condition'=>"vk = 0 and rcm.regionid = $k and rcm.entrantid is not null and deleted=0 and wave_first=0 and wave_second=0 and wave_third=0 and took=0 $spec",
                    'order'=>'rcm.regionid ASC, rating DESC',
                    'limit'=>$n,
                    'together' => true,
                )
            );
        $e = Entrant::model()->findAll(
            array(
                'select' => 'fam, name, m_name, rating',
                'with'=>array('rcm', 'specs'),
                'condition'=>"vk = 1 and deleted=0 and wave_first=0 and wave_second=0 and wave_third=0 and took=0 $spec",
                'order'=>'rating DESC',
                'together' => true,
            )
        );
        /*
        echo sizeof($e);
        foreach ($e as $i){
            echo '<pre>'; print_r($i->attributes); echo '</pre>';
        }
        die;
        */
        $entrants[] = $e;

        $title = "Приказ по регионально-целевому набору";

        $head = array(
            'РЦМ',
            '№',
            'ФИО абитуриента',
            'Сумма баллов',
        );
        $data = array();
        $i = 1;
        $r = -1;
        $j = 0;
        $k = sizeof($entrants) - 1;
        foreach ($entrants as $ent)
            foreach ($ent as $e) {
                if ($j < $k) {
                    if ($r != $e->rcm->region) {
                        $j++;
                        $r = $e->rcm->region;
                    }
                    $rcm = $e->rcm->region;
                    $rcm = empty($rcm->whom) ? $rcm->name : $rcm->whom;
                }
                if ($j >= $k) $rcm = "на внеконкурсной основе";

                $s = "§$j<br />
                    Зачислить по результатам ЕГЭ и вступительных испытаний 
                    на места Федерального бюджета <b>по направлению подготовкии<br /> 
                    ".  Departments::model()->findByPk($_GET['spec'])->specname .",</b>
                    выделеные 
                    <b>".$rcm.",</b>
                    абитуриентов, учавствующих в конкурсе и набравших следующие баллы:";
                $data[] = array(
                    $s,
                    $i,
                    $e->fam . ' ' . $e->name . ' ' . $e->m_name,
                    $e->rating,
                );
                $i++;
            }
        //echo '<pre>'; print_r ($data); die;
        $group = array('РЦМ');

		$this->render('report',array(
            'title' => $title,
			'head'=>$head,
			'data'=>$data,
            'group'=>$group,
		));
	}

	public function actionRcm() {
        $this->layout=false;
        $nrcm = array();
        if (!empty($_GET['spec'])) {
            $rcms=ConfigRcm::model()->findAll('specid = ' . $_GET['spec']);
            if (!empty($rcms))
                foreach ($rcms as $rcm)
                    $nrcm[$rcm->regionid] = $rcm->value;
                //echo '<pre>'; print_r($nrcm); echo '</pre>';
        }
                
        if (!empty($_GET['spec'])) $spec = "and specs.specid = " . $_GET['spec'];
        $entrants = Entrant::model()->findAll(
            array(
                'with'=>array('rcm', 'specs'),
                'condition'=>"rcm.entrantid is not null and deleted=0 and wave_first=0 and wave_second=0 and wave_third=0 and took=0 $spec",
                'order'=>'rcm.regionid ASC, rating DESC',
                //'limit'=>30,
            )
        );

        $title = "Список абитуриентов, имеющих регионально-целевые направления";

        $head = array(
            'РЦМ',
            'Порядковый №',
            '№ дела абитуриента',
            'ФИО абитуриента',
            'Направление',
            'Копия',
            'Да/Нет',
            'Сумма баллов ЕГЭ',
        );
        $data = array();
        $i = 1;
        $r = -1;
        foreach ($entrants as $e) {
            if ($r != $e->rcm->region) {
                $j = 1;
                $r = $e->rcm->region;
            }
            $data[] = array(
                $e->rcm->region->name,
                $i,
                $e->id,
                $e->fam . ' ' . $e->name . ' ' . $e->m_name,
                '', // TODO //$e->specs->name,
                $e->orig ? 'Нет': '',
                $j > $nrcm[$e->rcm->regionid] ? 'Нет' : 'Да',
                $e->rating,
            );
            $i++;
            $j++;
        }
        $group = array('РЦМ');

		$this->render('report',array(
            'title' => $title,
			'head'=>$head,
			'data'=>$data,
            'group'=>$group,
		));
	}

	public function actionBudgetR() {
        $this->actionBudget(true);
    }
	public function actionBudget($recommend = false) {
        $this->layout=false;
        $nbm = 0;
        $nrcm= 0;
                
        if (!empty($_GET['spec'])) {
            $spec = "and specs.specid = " . $_GET['spec'] . " and specs.level = 1";
            $nbm=ConfigBm::model()->find('specid = ' . $_GET['spec'])->value;
            /*
            $rcms=ConfigRcm::model()->findAll('specid = ' . $_GET['spec']);
            if (!empty($rcms))
                foreach ($rcms as $rcm)
                    $nrcm += $rcm->value;
            */
            $nrcm=ConfigRcm::model()->find(
                array(
                    'select'=>'sum(t.value) as value',
                    'condition'=>'t.specid = ' . $_GET['spec'],
                    'group'=>'t.specid'
                )
            )->value;
            //echo '<pre>'; print_r ($rcm->value); die;

            $nvk = Entrant::model()->find(
                array(
                    'with'=>array('rcm', 'citizen', 'specs'),
                    'select'=>'sum(t.vk) as vk',
                    'condition'=>"vk = 1 and deleted=0 and rcm.entrantid is null and wave_first=0 and wave_second=0 and wave_third=0 and took=0 and citizen.is_budget = 1 $spec",
                    'order'=>'rating desc',
                    'together'=>true,
                    'group'=>'specs.specid'
                )
            )->vk;
            //echo '<pre>'; print_r ($nvk); die;
        }
        $limit = $nbm - $nrcm - $nvk;
        $offset = 0;

        if ($recommend) {
            $offset = $limit;
            $limit = 100; // TODO
        }

        //echo $nrcm; die;
        $entrants = Entrant::model()->findAll(
            array(
                'with'=>array('rcm', 'citizen', 'specs'),
                'condition'=>"vk = 0 and deleted=0 and rcm.entrantid is null and wave_first=0 and wave_second=0 and wave_third=0 and took=0 and citizen.is_budget = 1 $spec",
                'order'=>'rating desc',
                'limit'=>$limit,
                'offset'=>$offset,
                'together'=>true,
            )
        );

        if ($recommend) $r = 'рекомендованных';
        $title = "Предварительный список абитуриентов $r для зачисления на бюджетные места";

        $head = array(
            '№',
            'Сумма баллов ЕГЭ',
            'ФИО абитуриента',
            'Копия',
        );
        $data = array();
        $i = 1;
        foreach ($entrants as $e) {
            $data[] = array(
                $i++,
                $e->rating,
                $e->fam . ' ' . $e->name . ' ' . $e->m_name,
                $e->orig ? '': 'Да'
            );
        }

		$this->render('report',array(
            'title' => $title,
			'head'=>$head,
			'data'=>$data,
		));
	}
        
        public function actionNebudget($recommend = false) {
        $this->layout=false;
        $nbm = 0;
        $nrcm= 0;
                
        if (!empty($_GET['spec'])) {
            $spec = "and specs.specid = " . $_GET['spec'] . " and specs.level = 1";
            $nbm=ConfigBm::model()->find('specid = ' . $_GET['spec'])->value;
            $nrcm=ConfigRcm::model()->find(
                array(
                    'select'=>'sum(t.value) as value',
                    'condition'=>'t.specid = ' . $_GET['spec'],
                    'group'=>'t.specid'
                )
            )->value;
            //echo '<pre>'; print_r ($rcm->value); die;

            $nvk = Entrant::model()->find(
                array(
                    'with'=>array('rcm', 'citizen', 'specs'),
                    'select'=>'sum(t.vk) as vk',
                    'condition'=>"vk = 1 and deleted=0 and rcm.entrantid is null and wave_first=0 and wave_second=0 and wave_third=0 and took=0 and citizen.is_budget = 1 $spec",
                    'order'=>'rating desc',
                    'together'=>true,
                    'group'=>'specs.specid'
                )
            )->vk;
            //echo '<pre>'; print_r ($nvk); die;
        }
        $limit = $nbm - $nrcm - $nvk;
        $offset = 0;

        if ($recommend) {
            $offset = $limit;
            $limit = 100; // TODO
        }

        //echo $nrcm; die;
        $entrants = Entrant::model()->findAll(
            array(
                'with'=>array('rcm', 'citizen', 'specs'),
                'condition'=>"vk = 0 and deleted=0 and rcm.entrantid is null and wave_first=0 and wave_second=0 and wave_third=0 and took=0 and citizen.is_budget = 1 $spec",
                'order'=>'rating desc',
                'limit'=>$limit,
                'offset'=>$offset,
                'together'=>true,
            )
        );

        if ($recommend) $r = 'рекомендованных';
        $title = "Предварительный список абитуриентов $r для зачисления на внебюджетные места";

        $head = array(
            '№',
            'Сумма баллов ЕГЭ',
            'ФИО абитуриента',
            'Копия',
        );
        $data = array();
        $i = 1;
        foreach ($entrants as $e) {
            $data[] = array(
                $i++,
                $e->rating,
                $e->fam . ' ' . $e->name . ' ' . $e->m_name,
                $e->orig ? '': 'Да'
            );
        }

		$this->render('report',array(
                        'title' => $title,
			'head'=>$head,
			'data'=>$data,
		));
	}

	public function actionNoru() {
        $this->layout=false;

        $b = "and citizen.is_budget = ";
        if ($_GET['budget'] == 1) {
            $b .= 1;
        } else {
            $b .= 0;
            $has = "не";
        }

        $entrants = Entrant::model()->findAll(
            array(
                'with'=>array('rcm', 'citizen', 'specs'),
                'condition'=>"deleted=0 and rcm.entrantid is null 
                    and wave_first=0 and wave_second=0 and wave_third=0 
                    and took=0 and citizen.required = 0 $b",
                'order'=>'citizen.name asc',
                'together'=>true,
            )
        );

        $title = "Список иностранных абитуриентов $has имеющих право на бюджетные места";

        $head = array(
            '№',
            '№ дела',
            'ФИО абитуриента',
            'Гражданство',
            'Заявленные факультеты',
            'Сумма баллов ЕГЭ',
        );
        $data = array();
        $i = 1;
        foreach ($entrants as $e) {
            $specs = '';
            foreach ($e->specs as $spec)
                $specs .= $spec->spec->name. '<br />';
            $data[] = array(
                $i++,
                $e->id,
                $e->fam . ' ' . $e->name . ' ' . $e->m_name,
                $e->citizen->name,
                $specs,
                $e->rating,
            );
        }

		$this->render('report',array(
            'title' => $title,
			'head'=>$head,
			'data'=>$data,
		));
	}

	public function actionVk() {
        $this->layout=false;
                
        if (!empty($_GET['spec'])) {
            $spec = "and specs.specid = " . $_GET['spec'] . " and specs.level = 1";
        }

        $entrants = Entrant::model()->findAll(
            array(
                'with'=>array('rcm', 'citizen', 'specs'),
                'condition'=>"vk = 1 and deleted=0 and rcm.entrantid is null and wave_first=0 and wave_second=0 and wave_third=0 and took=0 and citizen.is_budget = 1 $spec",
                'order'=>'rating desc',
                'together'=>true,
            )
        );

        $title = "Список абитуриентов для зачисления на внеконкурсной основе";

        $head = array(
            '№',
            '№ дела',
            'ФИО абитуриента',
            'Причина внеконкурса',
            'Копия',
            'Сумма баллов ЕГЭ',
        );
        $data = array();
        $i = 1;
        foreach ($entrants as $e) {
            $data[] = array(
                $i++,
                $e->id,
                $e->fam . ' ' . $e->name . ' ' . $e->m_name,
                $e->vk_notice,
                $e->orig ? '': 'Да',
                $e->rating,
            );
        }

		$this->render('report',array(
            'title' => $title,
			'head'=>$head,
			'data'=>$data,
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
