<?php

class UsersController extends AdminPanelMainController
{
        public $defaultAction='admin';
        function init() {
            $this->breadcrumbs[]='Права доступа';
            parent::init();
        }
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
        
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin', 'delete', 'create','update'),
				'roles'=>array('administrator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Users;
                $auth=new Auth;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']) && isset($_POST['Auth']))
		{
			$model->attributes=$_POST['Users'];
                        $auth->attributes=$_POST['Auth'];
                        $valid=$model->validate();
                        $valid=$auth->validate() && $valid;
			if($valid){
                            $model->save();
                            $auth->userid=$model->userid;
                            $auth->save();
                            LogController::addLog($this, 'create', $model, $model->last_name);
                            $this->redirect(array('admin'));
                        }
		}

		$this->render('create',array(
			'model'=>$model, 'auth'=>$auth,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $auth=Auth::model()->findByPk($model->userid);
                unset($auth->password);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']) && isset($_POST['Auth']))
		{
			$model->attributes=$_POST['Users'];
                        if (empty($_POST['Auth']['password'])) unset($_POST['Auth']['password']);
                        $auth->attributes=$_POST['Auth'];
                        $valid=$model->validate();
                        $valid=$auth->validate() && $valid;
			if($valid){
                            $model->save();
                            $auth->userid=$model->userid;
                            $auth->save();
                            LogController::addLog($this, 'update', $model, $model->last_name);
                            $this->redirect(array('admin'));
                        }
		}

		$this->render('update',array(
			'model'=>$model, 'auth'=>$auth,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
                    if ($id==1) throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            LogController::addLog($this, 'delete', $model, $model->last_name);
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');

                $columns = array('userid','last_name','first_name','middle_name','auth.role');
                
                $dataProvider->setPagination( new EDTPagination());
                $dataProvider->setSort(new EDTSort('Users',$columns));
                
                $widget=$this->createWidget('ext.EDataTables.EDataTables', array(
                    'id'            => 'Users',
                    'dataProvider'  => $dataProvider,
                    'ajaxUrl'       => Yii::app()->getBaseUrl().'/Users/index',
                    'columns'       => $columns,
                    'options'=>array('bStateSave'    => true),
                ));

                if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                    $this->render('index', array('widget' => $widget,));
                    return;
                }

                $data = $widget->getFormattedData(intval($_REQUEST['sEcho']));
                Yii::app()->getClientScript()->reset();
                $this->renderPartial('ajax', array('dtData' => $data), false, true); 
            
                /*$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
