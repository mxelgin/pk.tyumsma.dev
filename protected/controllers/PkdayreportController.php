<?php

class PkdayreportController extends ExportMainController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
        public $defaultAction='admin';
        function init() {
            $this->breadcrumbs[]='Ежедневные отчеты';
            parent::init();
        }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', 
				'actions'=>array('admin', 'report1', 'report2'),
				'roles'=>array('operator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionAdmin()
	{
		$this->render('admin');
	}
        
        public function actionReport1()
        {
                $this->layout=false;
                $specs=Departments::model()->findAll();
                $countall_b=EntrantSpec::model()->count('specid>0 and type_b>0');
                $countall_d=EntrantSpec::model()->count('specid>0 and type_d>0');
                $countall=$countall_b+$countall_d;
                //$all=Entrant::model()->findAll(array('with'=>'specs', 'condition'=>'specs.specid=1 and specs.type_b=1', 'order'=>'fam'));
                $count=array();
                foreach ($specs as $spec){
                    $count[$spec->id]['all_b']=EntrantSpec::model()->count('specid='.$spec->id.' and type_b=1');
                    $count[$spec->id]['all_d']=EntrantSpec::model()->count('specid='.$spec->id.' and type_d=1');
                    $count[$spec->id]['all']=$count[$spec->id]['all_b']+$count[$spec->id]['all_d'];
                    $count[$spec->id]['max']=Entrant::model()->find(array('select'=>'max(rating) as rating','with'=>'specs','condition'=>'specs.specid='.$spec->id.' and deleted=0 and took=0'))->rating;
                    $count[$spec->id]['min']=Entrant::model()->find(array('select'=>'min(rating) as rating','with'=>'specs','condition'=>'specs.specid='.$spec->id.' and specs.type_b=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and deleted=0 and rating>0 and took=0'))->rating;
                    $count[$spec->id]['sum']=Entrant::model()->find(array('select'=>'sum(rating) as rating','with'=>'specs','condition'=>'specs.specid='.$spec->id.' and specs.type_b=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and deleted=0 and took=0'))->rating;
                    $count[$spec->id]['count']=Entrant::model()->find(array('select'=>'count(rating) as rating','with'=>'specs','condition'=>'specs.specid='.$spec->id.' and specs.type_b=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and deleted=0 and rating>0 and took=0'))->rating;
                }
                
                $this->render('report1',array(
                    'specs'=>$specs, 
                    'count'=>$count,
                    'countall'=>$countall,
                    //'alls'=>$all,
                ));
        }
        
        public function actionReport2()
        {
                $this->layout=false;
                $specs=Departments::model()->findAll(array('order'=>'name'));
                
                $countall_b=EntrantSpec::model()->count('specid>0 and type_b>0');
                $countall_d=EntrantSpec::model()->count('specid>0 and type_d>0');
                $countall=$countall_b+$countall_d;
                //$all=Entrant::model()->findAll(array('with'=>'specs', 'condition'=>'specs.specid=1 and specs.type_b=1', 'order'=>'fam'));
                $count=array();
                foreach ($specs as $spec){
                    $count[$spec->id]['all_b']=EntrantSpec::model()->count('specid='.$spec->id.' and type_b=1');
                    $count[$spec->id]['all_d']=EntrantSpec::model()->count('specid='.$spec->id.' and type_d=1');
                    $count[$spec->id]['all']=$count[$spec->id]['all_b']+$count[$spec->id]['all_d'];
                    $count[$spec->id]['max']=Entrant::model()->find(array('select'=>'max(rating) as rating','with'=>'specs','condition'=>'specs.specid='.$spec->id.' and deleted=0'))->rating;
                    $count[$spec->id]['min']=Entrant::model()->find(array('select'=>'min(rating) as rating','with'=>'specs','condition'=>'specs.specid='.$spec->id.' and specs.type_b=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and deleted=0 and rating>0'))->rating;
                    $count[$spec->id]['sum']=Entrant::model()->find(array('select'=>'sum(rating) as rating','with'=>'specs','condition'=>'specs.specid='.$spec->id.' and specs.type_b=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and deleted=0'))->rating;
                    $count[$spec->id]['count']=Entrant::model()->find(array('select'=>'count(rating) as rating','with'=>'specs','condition'=>'specs.specid='.$spec->id.' and specs.type_b=1 and grade_rus is not null and grade_him is not null and grade_bio is not null and deleted=0 and rating>0'))->rating;
                }

                $counts=array();
                foreach ($specs as $spec){
                    $type='specs.type_b=1';
                    if ($spec->id>5) $type='specs.type_d=1';
                    $counts['all'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and '.$type.' and specs.specid='.$spec->id));
                    $counts['type_city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['type_hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and '.$type.' and specs.specid='.$spec->id));
                    $counts['male'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and sex=1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['female'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and sex=2 and '.$type.' and specs.specid='.$spec->id));
                    $counts['rus-all'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and citizenship=1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['rus-male'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and sex=1 and citizenship=1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['rus-female'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and sex=2 and citizenship=1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['foreign-all'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and citizenship>1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['foreign-male'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and sex=1 and citizenship>1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['foreign-female'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and sex=2 and citizenship>1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['city_tyumen'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_city=4 and '.$type.' and specs.specid='.$spec->id));
                    $counts['ege-max'][$spec->id]=Entrant::model()->find(array('with'=>'specs','condition'=>'deleted=0 and '.$type.' and specs.specid='.$spec->id, 'select'=>'max(rating) as rating'))->rating;
                    $counts['ege-sum'][$spec->id]=Entrant::model()->find(array('with'=>'specs','condition'=>'deleted=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and '.$type.' and specs.specid='.$spec->id, 'select'=>'sum(rating) as rating'))->rating;
                    $counts['ege-count'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and '.$type.' and specs.specid='.$spec->id, 'select'=>'sum(rating) as rating'));
                    $counts['ege-min'][$spec->id]=Entrant::model()->find(array('with'=>'specs','condition'=>'deleted=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and '.$type.' and specs.specid='.$spec->id, 'select'=>'min(rating) as rating'))->rating;
                    $counts['school'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-tyumenobl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and (kladr_street like "72%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-tyumenobl-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and (kladr_street like "72%") and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-tyumencity'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and kladr_street like "72000%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-tyumencity-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and kladr_street like "72000%" and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-othercity'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and home_type=1 and (kladr_street like "72%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-othercity-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and home_type=1 and (kladr_street not like "72%") and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and home_type=2 and (kladr_street not like "72%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-hill-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and home_type=2 and (kladr_street like "72%") and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-otherobl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and kladr_street not like "72%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-otherobl-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and kladr_street not like "72%" and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-cold'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and edu_spec=1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-cold-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and edu_spec=1 and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-silver'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and edu_spec=2 and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-silver-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=1 and edu_spec=2 and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-vuz'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=3 and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-vuz-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=3 and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-med'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=4 and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-med-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=4 and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-med-otl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=4 and edu_spec=3 and '.$type.' and specs.specid='.$spec->id));
                    $counts['school-med-otl-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and edu_type=4 and edu_spec=3 and edu_date>\'2012-00-00 00:00:00\' and '.$type.' and specs.specid='.$spec->id));
                    $counts['demobil'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and service=1 and '.$type.' and specs.specid='.$spec->id));
                    $counts['demobil-year'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and service=1 and service_year>=\'2012\' and '.$type.' and specs.specid='.$spec->id));
                    //районы
                    $counts['tyumen'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (kladr_street like "72000%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['tyumen-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and (kladr_street like "72000%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['tyumen-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and (kladr_street like "72000%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['abatskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72002%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['abatskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72002%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['abatskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72002%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['armizonskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72003%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['armizonskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72003%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['armizonskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72003%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['aromashevskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72004%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['aromashevskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72004%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['aromashevskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72004%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['berdujskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72005%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['berdujskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72005%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['berdujskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72005%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['vagaiskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72006%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['vagaiskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72006%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['vagaiskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72006%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['vikulovskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72007%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['vikulovskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72007%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['vikulovskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72007%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['golishmanovskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72008%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['golishmanovskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72008%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['golishmanovskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72008%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['zavodoukovskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72009%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['zavodoukovskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72009%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['zavodoukovskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72009%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['isetskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72010%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['isetskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72010%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['isetskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72010%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['ishimskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72011%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['ishimskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72011%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['ishimskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72011%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['kazanskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72012%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['kazanskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72012%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['kazanskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72012%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['nizhnetavdinskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72013%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['nizhnetavdinskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72013%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['nizhnetavdinskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72013%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['omutinskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72014%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['omutinskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72014%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['omutinskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72014%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sladkovskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72015%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sladkovskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72015%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sladkovskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72015%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sorokinskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72016%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sorokinskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72016%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sorokinskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72016%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['tobolskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72017%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['tobolskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72017%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['tobolskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72017%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['uvatskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72018%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['uvatskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72018%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['uvatskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72018%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['uporovskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72019%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['uporovskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72019%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['uporovskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72019%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['urginskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72020%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['urginskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72020%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['urginskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72020%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['yalutorovskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72021%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['yalutorovskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72021%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['yalutorovskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72021%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['yarkovskii'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "72022%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['yarkovskii-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "72022%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['yarkovskii-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "72022%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['hmao'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (kladr_street like "86%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['hmao-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and (kladr_street like "86%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['hmao-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and (kladr_street like "86%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['yanao'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (kladr_street like "89%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['yanao-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and (kladr_street like "89%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['yanao-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and (kladr_street like "89%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['tyumen-obl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and (kladr_street like "72%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['tyumen-obl-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and (kladr_street like "72%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['tyumen-obl-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and (kladr_street like "72%") and '.$type.' and specs.specid='.$spec->id));
                    $counts['other-obl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street not regexp "^72|^86|^89" and '.$type.' and specs.specid='.$spec->id));
                    $counts['other-obl-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street not regexp "^72|^86|^89" and '.$type.' and specs.specid='.$spec->id));
                    $counts['other-obl-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street not regexp "^72|^86|^89" and '.$type.' and specs.specid='.$spec->id));
                    $counts['kurg-obl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "45%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['kurg-obl-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "45%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['kurg-obl-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "45%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sverdl-obl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "66%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sverdl-obl-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "66%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['sverdl-obl-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "66%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['chelyab-obl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "74%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['chelyab-obl-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "74%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['chelyab-obl-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "74%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['omskaya-obl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street like "55%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['omskaya-obl-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street like "55%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['omskaya-obl-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street like "55%" and '.$type.' and specs.specid='.$spec->id));
                    $counts['other2-obl'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and kladr_street not regexp "^72|^86|^89|^45|^66|^55|^74" and '.$type.' and specs.specid='.$spec->id));
                    $counts['other2-obl-city'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=1 and kladr_street not regexp "^72|^86|^89|^45|^66|^55|^74" and '.$type.' and specs.specid='.$spec->id));
                    $counts['other2-obl-hill'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and home_type=2 and kladr_street not regexp "^72|^86|^89|^45|^66|^55|^74" and '.$type.' and specs.specid='.$spec->id));
                }
                
                $dspecs=Departments::model()->findAll('id in (1,2,3,4)');
                $dcounts=array();
                $dsum=0;
                foreach ($dspecs as $spec){
                    $dcounts['all'][$spec->id]=Entrant::model()->count(array('with'=>'specs','condition'=>'deleted=0 and type_d=1 and specs.specid='.$spec->id));
                    $dcounts['ege-max'][$spec->id]=Entrant::model()->find(array('with'=>'specs','condition'=>'deleted=0 and type_d=1 and specs.specid='.$spec->id, 'select'=>'max(rating) as rating'))->rating;
                    $dcounts['ege-min'][$spec->id]=Entrant::model()->find(array('with'=>'specs','condition'=>'deleted=0 and grade_rus is not null and grade_him is not null and grade_bio is not null and type_d=1 and specs.specid='.$spec->id, 'select'=>'min(rating) as rating'))->rating;
                    $dsum+=$dcounts['all'][$spec->id];
                }
                
                $sum=array();
                foreach ($counts as $k=>$countb){
                    foreach ($countb as $specid=>$c){
                        //if ($specid<=5) $sum[$k]['b']+=$c;
                        //else $sum[$k]['d']+=$c;
                        $sum[$k]['b']+=$c;
                    }
                }
                $ege['sum']=Entrant::model()->find(array('condition'=>'deleted=0 and grade_rus is not null and grade_him is not null and grade_bio is not null', 'select'=>'sum(rating) as rating'))->rating;
                $ege['max']=Entrant::model()->find(array('condition'=>'deleted=0', 'select'=>'max(rating) as rating'))->rating;
                $ege['count']=Entrant::model()->count(array('condition'=>'deleted=0 and grade_rus is not null and grade_him is not null and grade_bio is not null', 'select'=>'sum(rating) as rating'));
                $ege['min']=Entrant::model()->find(array('condition'=>'deleted=0 and grade_rus is not null and grade_him is not null and grade_bio is not null', 'select'=>'min(rating) as rating'))->rating;
                
                $this->render('report2',array(
                    'specs'=>$specs, 
                    'counts'=>$counts,
                    'sum'=>$sum,
                    'ege'=>$ege,
                    'dcounts'=>$dcounts,
                    'dspecs'=>$dspecs,
                    'dsum'=>$dsum,
                    'count'=>$count,
                    'countall'=>$countall,
                ));
        }
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Entrant::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='departments-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
?>
