<?php

class DirectCitizenshipController extends DirectoryMainController
{
        public $defaultAction='admin';
        function init() {
            $this->breadcrumbs[]='Гражданство';
            parent::init();
        }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','create','isBudget'),
				'roles'=>array('secretary'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('delete','update'),
				'roles'=>array('administrator'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DirectCitizenship;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DirectCitizenship']))
		{
			$model->attributes=$_POST['DirectCitizenship'];
			if($model->save()) {
                LogController::addLog($this, 'create', $model, $model->name);
				$this->redirect(array('admin'));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DirectCitizenship']))
		{
			$model->attributes=$_POST['DirectCitizenship'];
            //echo '<pre>'; print_r($_POST['DirectCitizenship']);
            //echo '<pre>'; print_r($model->attributes); die;
			if($model->save()) {
                LogController::addLog($this, 'update', $model, $model->name);
				$this->redirect(array('admin'));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
            LogController::addLog($this, 'delete', $model, $model->name);

			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DirectCitizenship');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DirectCitizenship('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DirectCitizenship']))
			$model->attributes=$_GET['DirectCitizenship'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    public function actionIsBudget() {
        if (!$_GET['id']) {
            //echo $_GET['callback'].'('.CJSON::encode($result).');';
            echo false;
            return;
        }

		$model=$this->loadModel($_GET['id']);

        if ($model->is_budget) echo true;
        else echo false;

        //echo $result;
        //echo $_GET['callback'].'('.CJSON::encode($result).');';

        return;
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DirectCitizenship::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='direct-citizenship-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
