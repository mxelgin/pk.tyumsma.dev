<?php

/**
 * This is the model class for table "direct_docs".
 *
 * The followings are the available columns in table 'direct_docs':
 * @property integer $id
 * @property string $name
 * @property string $action
 * @property integer $categoryid
 * @property integer $required
 */
class DirectDocs extends CActiveRecord
{
        public $acttypes=array('checkbox'=>'Подать документ', 'input'=>'Заполнить форму');
        public $reqtypes=array(0=>'Нет', 1=>'Да');
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DirectDocs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'direct_docs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, action, categoryid', 'required'),
			array('categoryid, required', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('action', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, action, categoryid, required', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'category'=>array(self::HAS_ONE, 'DirectDocsCategories', array('id'=>'categoryid')),
                    'fields'=>array(self::HAS_MANY, 'DirectDocsFields', array('docid'=>'id')),
                    );
                
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'action' => 'Действие',
			'categoryid' => 'Категория',
			'required' => 'Обязательный',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('categoryid',$this->categoryid);
		$criteria->compare('required',$this->required);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}