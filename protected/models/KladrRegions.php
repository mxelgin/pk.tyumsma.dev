<?php

/**
 * This is the model class for table "kladr_regions".
 *
 * The followings are the available columns in table 'kladr_regions':
 * @property string $id
 * @property string $name
 * @property string $index
 * @property integer $id_type
 *
 * The followings are the available model relations:
 * @property KladrType $idType
 */
class KladrRegions extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KladrRegions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kladr_regions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, name, id_type', 'required'),
			array('id_type', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>2),
			array('name', 'length', 'max'=>40),
			array('index', 'length', 'max'=>6),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, index, idTtype', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'type' => array(self::BELONGS_TO, 'KladrType', 'id_type'),
            //'region'=>array(self::HAS_MANY, 'Kladr', 'id_region'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Код области',
			'name' => 'Регион',
			'index' => 'Индекс',
			'id_type' => 'Тип',
			'type' => 'Тип',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('`index`',$this->index,true);
		$criteria->compare('id_type',$this->id_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
