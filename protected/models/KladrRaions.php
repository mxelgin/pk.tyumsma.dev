<?php

/**
 * This is the model class for table "kladr_raions".
 *
 * The followings are the available columns in table 'kladr_raions':
 * @property integer $id
 * @property string $name
 * @property string $index
 * @property integer $id_type
 * @property integer $id_region
 */
class KladrRaions extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KladrRaions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kladr_raions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, index, id_type, id_region', 'required'),
			array('id_type, id_region', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>40),
			array('index', 'length', 'max'=>6),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, index, id_type, id_region', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'type' => array(self::BELONGS_TO, 'KladrType', 'id_type'),
            'region' => array(self::BELONGS_TO, 'KladrRegions', 'id_region'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'index' => 'Индекс',
			'id_type' => 'Тип',
			'id_region' => 'Регион',
			'type' => 'Тип',
			'region' => 'Регион',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id, true);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.`index`',$this->index,true);
		$criteria->compare('t.id_type',$this->id_type);
		$criteria->compare('t.id_region',$this->id_region);
                //$criteria->with = array('region', 'type');
		//$criteria->compare('region.name',$this->region->name);
                /*if ($_GET['KladrRaions']['id_regions']){
                    $criteria->with = array('region');
                    $criteria->addSearchCondition('specs.specid',$_GET['Entrant']['specs']);
                }*/

        $sort = new CSort();
        $sort->attributes = array(
            'id', 'name', 'index',
            /*'region.name' => array(
                'asc' => 'region.name',
                'desc' => 'region.name DESC',
            )
            */
        );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>$sort,
		));
	}
}
