<?php

/**
 * This is the model class for table "pk_rcm".
 *
 * The followings are the available columns in table 'pk_rcm':
 * @property integer $id
 * @property string $fio
 * @property string $pass_number
 * @property integer $useradd
 * @property integer $entrantid
 */
class Rcm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rcm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_rcm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fio, pass_number, regionid, specid', 'required'),
			array('useradd, entrantid, regionid, specid', 'numerical', 'integerOnly'=>true),
			array('fio', 'length', 'max'=>255),
			array('pass_number', 'length', 'max'=>20),
                        array('pass_number', 'unique'),
                        array('entrantid', 'exist', 'attributeName'=>'id', 'className'=>'Entrant', 'allowEmpty'=>true),
                        array('regionid', 'exist', 'attributeName'=>'id', 'className'=>'DirectRcmRegions'),
                        array('specid', 'exist', 'attributeName'=>'id', 'className'=>'Departments'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fio, regionid, specid, pass_number, useradd, entrantid', 'safe', 'on'=>'search'),
		);
	}
        
        public function beforeSave() {
            parent::beforeSave();
            $this->useradd=Yii::app()->user->id;
            return true;
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'region'=>array(self::HAS_ONE, 'DirectRcmRegions', array('id'=>'regionid')),
                    'spec'=>array(self::HAS_ONE, 'Departments', array('id'=>'specid')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fio' => 'ФИО',
			'pass_number' => 'Серия и номер паспорта',
			'useradd' => 'Добавил пользователь',
                        'entrantid' => 'Абитуриент #',
                        'regionid' => 'Регион',
                        'specid' => 'Специальность',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('pass_number',$this->pass_number,true);
		$criteria->compare('useradd',$this->useradd);
                $criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('regionid',$this->regionid);
                $criteria->compare('specid',$this->specid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination' => array('pageSize' => 50),
		));
	}
}