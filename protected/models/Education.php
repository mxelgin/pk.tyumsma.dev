<?php

/**
 * This is the model class for table "pk_education".
 *
 * The followings are the available columns in table 'pk_education':
 * @property integer $id
 * @property integer $entrantid
 * @property integer $cityid
 * @property string $name
 * @property string $exp_date
 * @property double $grade
 */
class Education extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Education the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_education';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entrantid', 'required'),
			array('entrantid, cityid', 'numerical', 'integerOnly'=>true),
			array('grade', 'numerical'),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entrantid, cityid, name, exp_date, grade', 'safe', 'on'=>'search'),
		);
	}

        public function beforeSave() {
            parent::beforeSave();
            if ($this->exp_date)
            $this->exp_date=date("Y-m-d",strtotime($this->exp_date));
            return true;
        }
        
        public function afterFind() {
            parent::afterFind();
            if ($this->exp_date!='0000-00-00')
                $this->exp_date=date("d.m.Y",strtotime($this->exp_date));
            else
                $this->exp_date='';
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'city'=>array(self::BELONGS_TO, 'Kladr', array('cityid'=>'code')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entrantid' => 'Entrantid',
			'cityid' => 'Город',
			'name' => 'Название',
			'exp_date' => 'Дата окончания',
			'grade' => 'Средний балл',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('cityid',$this->cityid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('exp_date',$this->exp_date,true);
		$criteria->compare('grade',$this->grade);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}