<?php

/**
 * This is the model class for table "kladr_street".
 *
 * The followings are the available columns in table 'kladr_street':
 * @property string $id
 * @property string $id_parent
 * @property string $name
 * @property string $type_text
 * @property string $code
 * @property string $index
 * @property integer $gninmb
 * @property integer $uno
 * @property string $ocatd
 * @property integer $id_type
 * @property integer $id_status
 */
class KladrStreet extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KladrStreet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kladr_street';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_parent, name, type_text, code, index, id_type', 'required'),
			array('gninmb, uno, id_type, id_status', 'numerical', 'integerOnly'=>true),
			array('id_parent, ocatd', 'length', 'max'=>20),
			array('name', 'length', 'max'=>40),
			array('type_text', 'length', 'max'=>10),
			array('code', 'length', 'max'=>15),
			array('index', 'length', 'max'=>6),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_parent, name, type_text, code, index, gninmb, uno, ocatd, id_type, id_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            //'center'=>array(self::BELONGS_TO, 'KladrCenter', 'id_center'),
            //'region'=>array(self::BELONGS_TO, 'KladrRegions', 'id_region'),
            //'raion'=>array(self::BELONGS_TO, 'KladrRaions', 'id_raion'),
            'type'=>array(self::BELONGS_TO, 'KladrType', 'id_type'),
            'status'=>array(self::BELONGS_TO, 'KladrStatus', 'id_status'),
            'parent'=>array(self::BELONGS_TO, 'Kladr', array('id_parent' => 'code')),
		);
	}

    public function beforeSave() {
        parent::beforeSave();
        //$this->id_parent = $this->parent-id;
        return true;
    }

    public function afterFind() {
        parent::afterFind();
        //$this['parent-id'] = $this->id_parent;
        return true;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Наименование',
			'type_text' => 'Тип',
			'code' => 'Код',
			'index' => 'Индекс',
			'gninmb' => 'GNINMB',
			'uno' => 'UNO',
			'ocatd' => 'OCATD',
			'id_type' => 'ID типа',
			'id_status' => 'Статус',
			'id_parent' => 'ID Родителя',
			'type' => 'Тип',
			'status' => 'Статус',
			'parent' => 'Родитель',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('id_parent',$this->id_parent,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type_text',$this->type_text,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('`index`',$this->index,true);
		$criteria->compare('gninmb',$this->gninmb);
		$criteria->compare('uno',$this->uno);
		$criteria->compare('ocatd',$this->ocatd,true);
		$criteria->compare('id_type',$this->id_type);
		$criteria->compare('id_status',$this->id_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
