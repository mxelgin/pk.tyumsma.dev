<?php

/**
 * This is the model class for table "pk_entrant".
 *
 */
class Entrant extends CActiveRecord
{
        public $sextypes=array(1=>'Мужской', 2=>'Женский');
        public $servicetypes=array(1=>'Служил', 2=>'Не служил');
        public $vktypes=array(0=>'Общий конкурс', 1=>'Вне конкурса');
        public $origtypes=array(0=>'Копии', 1=>'Оригиналы');
        public $tooktypes=array(0=>'В архиве', 1=>'Забрал');
        public $hometypes=array(1=>'Город/поселок', 2=>'Село/деревня');
        public $gradedoctypes=array(1=>'Сертификат ЕГЭ', 2=>'Экзаменационный лист', 3=>'Олимпиада');
        public $faculty=array(1=>'Лечебный', 2=>'Стоматологический', 3=>'Педиатрический', 4=>'Фармацевтический', 5=>'ВСО');

        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Entrant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_entrant';
	}
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fam, name, birth_date, citizenship, sex, maindoc', 'required'),
			array('id, rating, useradd, checkedbyuser, citizenship, sex, edu_type, edu_spec, need_hostel, grade_rus_doctype, grade_him_doctype, grade_bio_doctype, home_region, home_type, service, maindoc, foreignid, foreign_exp, foreign_grade, deleted, vk, took, orig', 'numerical', 'integerOnly'=>true),
			array('fam, grade_rus_doc, ege_dop, grade_him_doc, grade_bio_doc, name, m_name, home_street, home_fact', 'length', 'max'=>255),
                        array('grade_rus, grade_him, grade_bio', 'numerical', 'max'=>100, 'min'=>10),
                        array('edu_grade', 'numerical', 'max'=>5, 'min'=>0),
                        array('home_build', 'length', 'max'=>50),
                        array('home_kv', 'length', 'max'=>10),
			array('phone', 'length', 'max'=>20),
                        array('fam, name, m_name', 'match', 'pattern'=>'/^[А-яЁё\-\` ]+$/u'),
                        array('birth_date, edu_date, took_date, orig_date', 'date','format' => 'd.mm.yyyy'),
                        array('service_year', 'date','format' => 'yyyy'),
                        //array('birth_place_id', 'exist', 'attributeName'=>'id', 'className'=>'DirectCity'),
                        //array('home_city', 'exist', 'attributeName'=>'id', 'className'=>'DirectCity'),
                        array('citizenship', 'exist', 'attributeName'=>'id', 'className'=>'DirectCitizenship'),
                        array('edu_type', 'exist', 'attributeName'=>'id', 'className'=>'DirectEducation'),
                        array('edu_spec', 'exist', 'attributeName'=>'id', 'className'=>'DirectMedal'),
                        array('sex, service', 'in', 'range'=>array(1,2)),
                        array('vk, took, orig', 'boolean'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fam, useradd, checkedbyuser, name, m_name, service_year, phone, ege_dop, grade_rus_doc, grade_him_doc, grade_bio_doc, grade_rus_doctype, grade_him_doctype, grade_bio_doctype, birth_date, birth_place_id, citizenship, sex, home_city, home_region, home_fact, home_type, home_street, home_build, home_kv, need_hostel, service, social_work, talent, sport, maindoc, grade_rus, grade_him, grade_bio, rating, foreignid, foreign_exp, foreign_grade, deleted, vk, vk_notice, took, orig, specs, dateadd', 'safe', 'on'=>'search'),
                        //array('is_mail, mail_num, orphan1, orphan2, orphan1_type, orphan2_type, took_date, orig_date, specs', 'safe', 'on'=>'search'),
		);
	}
        
        public function beforeSave() {
            parent::beforeSave();
            $this->birth_date=date("Y-m-d",strtotime($this->birth_date));
            $this->took_date=date("Y-m-d",strtotime($this->took_date));
            $this->orig_date=date("Y-m-d",strtotime($this->orig_date));
            if (!empty($this->edu_date))
            $this->edu_date=date("Y-m-d",strtotime($this->edu_date));
            $this->rating=$this->grade_bio+$this->grade_him+$this->grade_rus;

            $cityModel=new DirectCity;
            if (!is_numeric($this->birth_place_id)){
                $this->birth_place_id=trim($this->birth_place_id);
                $city=$cityModel->find("name like '".mysql_escape_string($this->birth_place_id)."'");
                if ($city->id) $this->birth_place_id=$city->id;
                else{
                    $cityModel->name=$this->birth_place_id;
                    $cityModel->save();
                    $this->birth_place_id=$cityModel->id;
                }
            }
            if ($this->service==2) $this->service_year='';

            /*$homecityModel=new DirectCity;
            $this->home_city=trim($this->home_city);
            $city=$homecityModel->find("name like '".mysql_escape_string($this->home_city)."'");
            if ($city->id) $this->home_city=$city->id;
            else{
                $homecityModel->name=$this->home_city;
                $homecityModel->save();
                $this->home_city=$homecityModel->id;
            }*/
            if (empty($this->id)) $this->useradd=Yii::app()->user->id;
            if (!$this->vk) $this->vk_notice='';

            return true;
        }
        
        /*public function afterValidate() {
            parent::afterValidate();
            if (!count($this->specs))
                $this->addError ('specid_0', 'Необходимо выбрать хотябы 1 специальность');
        }*/


        public function afterSave() {
            parent::afterSave();
            $idbase='';
            foreach ($this->docs as $doc){
                $doc->entrantid=$this->id;
                $doc->save();
                $idbase.=$doc->id.',';
            }
            $idbase=substr($idbase, 0, -1);
            Docs::model()->deleteAll('entrantid='.$this->id.' and id not in ('.$idbase.')');
            
            //$idbase='';
            foreach ($this->specs as $spec){
                $spec->entrantid=$this->id;
                $spec->save();
                //$idbase.=$spec->id.',';
            }
            //$idbase=substr($idbase, 0, -1);
            //$deletespecs=EntrantSpec::model()->findAll('entrantid='.$this->id.' and id not in ('.$idbase.')');
            //foreach ($deletespecs as $dsp)
            //    $dsp->delete();

            foreach ($this->parents as $parent){
                $parent->entrantid=$this->id;
                $parent->save();
            }
        }

        public function afterFind() {
            parent::afterFind();
            $this->birth_date=date("d.m.Y",strtotime($this->birth_date));
            $this->took_date=date("d.m.Y",strtotime($this->took_date));
            if ($this->took_date == '01.01.1970') $this->took_date = '';
            $this->orig_date=date("d.m.Y",strtotime($this->orig_date));
            if ($this->orig_date == '01.01.1970') $this->orig_date = '';
            $this->edu_date=date("d.m.Y",strtotime($this->edu_date));
            if ($this->service_year=='0000') $this->service_year='';
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'citizen'=>array(self::BELONGS_TO, 'DirectCitizenship', 'citizenship'),
                    'birth_city'=>array(self::BELONGS_TO, 'DirectCity', 'birth_place_id'),
                    'home_city_db'=>array(self::BELONGS_TO, 'DirectCity', 'home_city'),
                    'home_region_db'=>array(self::BELONGS_TO, 'DirectRegions', 'home_region'),
                    'foreign'=>array(self::BELONGS_TO, 'DirectForeign', 'foreignid'),
                    'mdoc'=>array(self::BELONGS_TO, 'DirectDocs', 'maindoc'),
                    'specs'=>array(self::HAS_MANY, 'EntrantSpec', 'entrantid', 'order'=>'level ASC'),
                    //'edu'=>array(self::HAS_ONE, 'Education', 'entrantid'),
                    'docs'=>array(self::HAS_MANY, 'Docs', 'entrantid'),
                    'parents'=>array(self::HAS_MANY, 'Parents', 'entrantid'),
                    'rcm'=>array(self::HAS_ONE, 'Rcm', array('entrantid'=>'id')),
                    'education'=>array(self::HAS_ONE, 'DirectEducation', array('id'=>'edu_type')),
                    'education_spec'=>array(self::HAS_ONE, 'DirectMedal', array('id'=>'edu_spec')),
                    'user'=>array(self::HAS_ONE, 'Users', array('userid'=>'useradd')),
                    'checked'=>array(self::HAS_ONE, 'Users', array('userid'=>'checkedbyuser')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fam' => 'Фамилия',
			'name' => 'Имя',
			'm_name' => 'Отчество',
			'phone' => 'Телефон',
			'birth_date' => 'Дата рождения',
			'birth_place_id' => 'Место рождения',
			'citizenship' => 'Гражданство',
			'sex' => 'Пол',
			'need_hostel' => 'Нужно общежитие',
			'service' => 'Служба в армии',
                        'service_year' => 'Год демобилизации',
			'social_work' => 'Общественная работа',
			'talent' => 'Самодеятельность',
			'sport' => 'Спорт',
                        'maindoc' => 'Основной документ',
                        'grade_rus' => 'Оценка по русскому',
                        'grade_him' => 'Оценка по химии',
                        'grade_bio' => 'Оценка по биологии',
                        'rating' => 'Рейтинг',
                        'foreignid' => 'Иностранный язык',
                        'foreign_exp' => 'Опыт изучения языка',
                        'foreign_grade' => 'Оценка по языку',
                        'deleted' => 'Удален',
                        'vk'=>'Вне конкурса',
                        'vk_notice'=>'Примечание',
                        'took'=>'Забор',
                        'home_type'=>'Тип населенного пункта',
                        'home_city'=>'Населенный пункт',
                        'home_region'=>'Область',
                        'home_fact'=>'Фактический адрес',
                        'orig'=>'Оригиналы',
                        'grade_rus_doc'=>'Номер документа (русский)',
                        'grade_him_doc'=>'Номер документа (химия)',
                        'grade_bio_doc'=>'Номер документа (биология)',
                        'grade_rus_doctype'=>'Документ (русский)',
                        'grade_him_doctype'=>'Документ (химия)',
                        'grade_bio_doctype'=>'Документ (биология)',
                        'edu_type'=>'Что окончил',
                        'edu_grade'=>'Средний балл',
                        'edu_date'=>'Дата окончания',
                        'edu_spec'=>'Отличие',
                        'ege_dop'=>'Место сдачи ЕГЭ в доп. сроки',
                        'checkedbyuser'=>'Проверка'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
        //echo $this->dateadd;
        //echo '<pre>'; print_r($_GET); die;

		$criteria=new CDbCriteria;
		$criteria->compare('t.id',$this->id);
		$criteria->compare('fam',$this->fam,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('m_name',$this->m_name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('birth_date',$this->birth_date,true);
		//$criteria->compare('birth_place_id',$this->birth_place_id);
		$criteria->compare('citizenship',$this->citizenship);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('need_hostel',$this->need_hostel);
		//$criteria->compare('service',$this->service);

		$criteria->compare('social_work',$this->social_work,true);
		$criteria->compare('talent',$this->talent,true);
		$criteria->compare('sport',$this->sport,true);

        $criteria->compare('maindoc',$this->maindoc);
        $criteria->compare('grade_rus',$this->grade_rus);
        $criteria->compare('grade_him',$this->grade_him);
        $criteria->compare('grade_bio',$this->grade_bio);
        //$criteria->compare('foreignid',$this->foreignid);
        $criteria->compare('rating',$this->rating);
        $criteria->compare('edu_type',$this->edu_type);
        $criteria->compare('edu_date',$this->edu_date, true);
        $criteria->compare('edu_grade',$this->edu_grade);
        $criteria->compare('edu_spec',$this->edu_spec);
        //$criteria->compare('foreign_exp',$this->foreign_exp,true);
        //$criteria->compare('foreign_grade',$this->foreign_grade,true);
        //$criteria->compare('home_city',$this->home_city);
        //$criteria->compare('home_type',$this->home_type);
        //$criteria->compare('home_region',$this->home_region);
        //$criteria->compare('home_street',$this->home_street, true);
        //$criteria->compare('home_build',$this->home_build, true);
        //$criteria->compare('home_kv',$this->home_kv, true);
        $criteria->compare('home_fact',$this->home_fact, true);
        $criteria->compare('deleted', 0);
        //$criteria->compare('wave_first', 0);
        //$criteria->compare('wave_second', 0);
        //$criteria->compare('wave_third', 0);
        $criteria->compare('vk', $this->vk);
        $criteria->compare('took', $this->took);
        $criteria->compare('orig', $this->orig);
        $criteria->compare('checkedbyuser', $this->checkedbyuser);
        $criteria->compare('grade_rus_doc', $this->grade_rus_doc, true);
        $criteria->compare('grade_him_doc', $this->grade_him_doc, true);
        $criteria->compare('grade_bio_doc', $this->grade_bio_doc, true);
        $criteria->compare('grade_rus_doctype', $this->grade_rus_doctype);
        $criteria->compare('grade_him_doctype', $this->grade_him_doctype);
        $criteria->compare('grade_bio_doctype', $this->grade_bio_doctype);
        //$criteria->compare('rcm.entrantid', $this->rcm->entrantid);
        //$criteria->compare('specs.id', array(1,2,3,4));
        //$criteria->compare("DATE_FORMAT(dateadd, '%d.%m.%Y')", $_GET['Entrant']['dateadd'], true);
        $criteria->compare("dateadd", $_GET['Entrant']['dateadd'], true);

        if ($_GET['Entrant']['specs']){
            $criteria->together = true;
            $criteria->with = array('specs');
            $criteria->compare('specs.specid', $_GET['Entrant']['specs']);
        }
        if ($_GET['docType']){
            $criteria->together = true;
            $criteria->with = array('docs');
            $criteria->compare('docs.docid', $_GET['docType']);
        }
        if ($_GET['docNum']){
            $criteria->together = true;
            $criteria->with = array('docs');
            $criteria->compare('docs.value', $_GET['docNum'], true);
            $criteria->compare('docs.fieldid', array('13', '18', '34'));
        }
        if ($_GET['docDate']){
            $criteria->together = true;
            $criteria->with = array('docs');
            $criteria->compare('docs.value', $_GET['docDate']);
            $criteria->compare('docs.fieldid', array('14', '21', '36', '38'));
        }
        if ($_GET['docSrc']){
            $criteria->together = true;
            $criteria->with = array('docs');
            $criteria->compare('docs.value', $_GET['docSrc'], true);
            $criteria->compare('docs.fieldid', array('15', '35', '37'));
        }
        if ($_GET['birthYear']){
            $criteria->compare('birth_date', $_GET['birthYear'], true);
        }
        if ($_GET['fakType']){
            $criteria->together = true;
            $criteria->with = array('specs');

            if ($_GET['fakType'])
                $criteria->compare('specs.specid', '>=6');
            else
                $criteria->compare('specs.specid', '< 6');

        }
        if ($_GET['fakCode']){
            $criteria->together = true;
            $criteria->with = array('specs', 'specs.spec');
            $criteria->compare('spec.specname', $_GET['fakCode'], true);
        }
        if ($_GET['fakUsl']){
            $criteria->together = true;
            $criteria->with = array('specs');

            if ($_GET['fakUsl'] == 1)
                $criteria->compare('specs.type_b', 1);
            else
                $criteria->compare('specs.type_d', 1);

        }
        if ($_GET['invited']){
            $criteria->compare('wave_first',    1, false);
            $criteria->compare('wave_second',   1, false, 'OR');
            $criteria->compare('wave_third',    1, false, 'OR');
        }
        if ($_GET['parentFIO'])     
        if ($_GET['parentAddress']) $this->searchParent($criteria, 'addr', $_GET['parentAddress'], true);
        if ($_GET['parentPhone'])   $this->searchParent($criteria, 'phone', $_GET['parentPhone'], true);

        $dop = array();
        if ($_GET['dopPhoto'])      $dop[] = '20'; //$this->searchDop($criteria, 20);
        if ($_GET['dopTK'])         $dop[] = '27'; //$this->searchDop($criteria, 27);
        if ($_GET['dop86u'])        $dop[] = '28'; //$this->searchDop($criteria, 28);
        if ($_GET['dopEGE'])        $dop[] = '30'; //$this->searchDop($criteria, 30);
        if ($_GET['dopVB'])         $dop[] = '36'; //$this->searchDop($criteria, 36);
        if ($_GET['dopPMPK'])       $dop[] = '37'; //$this->searchDop($criteria, 37);
        if ($_GET['dopInv'])        $dop[] = '38'; //$this->searchDop($criteria, 38);
        if ($_GET['dopOpek'])       $dop[] = '39'; //$this->searchDop($criteria, 39);
        if ($_GET['dopOlimp'])      $dop[] = '40'; //$this->searchDop($criteria, 40);
        if ($_GET['dopDohod'])      $dop[] = '41'; //$this->searchDop($criteria, 41);
        if ($_GET['dopPens'])       $dop[] = '42'; //$this->searchDop($criteria, 42);
        if ($_GET['dopPBook'])      $dop[] = '43'; //$this->searchDop($criteria, 43);
        if ($_GET['dopSNILS'])      $dop[] = '44'; //$this->searchDop($criteria, 44);
        if ($_GET['dopSber'])       $dop[] = '45'; //$this->searchDop($criteria, 45);
        if ($_GET['dopOrphan'])     $dop[] = '46'; //$this->searchDop($criteria, 46);
        $this->searchDop($criteria, $dop);

        if ($_GET['eduName'])       $this->searchDocs($criteria, $_GET['eduName'], true, 19);
        if ($_GET['eduNum'])        $this->searchDocs($criteria, $_GET['eduNum'], true, 20);
        if ($_GET['eduDate'])       $this->searchDocs($criteria, $_GET['eduDate'], true, 21);

        /*if (isset($_GET['Entrant']['specs'])){
            $criteria->with = array('specs');
            $criteria->addSearchCondition('specs.specid',$_GET['Entrant']['specs']);
        }*/

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize' => 50),
		));
	}

    private function searchDocs($c, $value, $like = false, $fields = false) {
        $c->together = true;
        $c->with = array('docs');
        $c->compare("docs.value", $value, $like);
        if ($fields)
            $c->compare("docs.fieldid", $fields);
    }
    private function searchParent($c, $field, $value, $like = false) {
        $c->together = true;
        $c->with = array('parents');
        $c->compare("parents.$field", $value, $like);
    }
    private function searchDop($c, $type ) {
        if (empty($type)) return;
//echo '<pre>'; print_r($type); die;
        $c->together = true;
        $c->with = array('docs');
        $c->addInCondition("docs.docid", $type);
    }
        
	public function searchUncheck()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$_GET['Entrant']['id']);
		$criteria->compare('fam',$this->fam,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('m_name',$this->m_name,true);

                $criteria->compare('deleted', 0);
                $criteria->compare('wave_first', 0);
                $criteria->compare('wave_second', 0);
                $criteria->compare('wave_third', 0);
                $criteria->compare('checkedbyuser', 0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination' => array('pageSize' => 50),
		));
	}
        
        public function showallspec(){
            $all='';
            foreach ($this->specs as $spec){
                $all.=$spec->level.'.'.$spec->spec->name." ";
            }
            return $all;
        }
        
        public function getEge($entrantid) {
            $e = $this->findByPk($entrantid);

            if (  $e->grade_rus_doctype == 2
                & $e->grade_him_doctype == 2
                & $e->grade_bio_doctype == 2
                ) return null;

            $s = array();
            if ($e->grade_rus_doctype == 1) {
                $s[] = array(
                    'subjectData' => array(
                        'subjectID' => 'TODO',
                        'value' => $e->grade_rus,
                    ),
                );
            }

            if ($e->grade_him_doctype == 1) {
                $s[] = array(
                    'subjectData' => array(
                        'subjectID' => 'TODO',
                        'value' => $e->grade_him,
                    ),
                );
            }

            if ($e->grade_bio_doctype == 1) {
                $s[] = array(
                    'subjectData' => array(
                        'subjectID' => 'TODO',
                        'value' => $e->grade_bio,
                    ),
                );
            }

            return array(
                'egeDocuments' => array(
                    array(
                        'egeDocument' => array(
                            'UID' => $e->id,
                            'originalRecieved' => 0,
                            //'originalRecievedDate' => 'TODO',
                            'documentNumber' => $e->grade_rus_doc,
                            'documentYear' => '20' . substr($e->grade_rus_doc, -2),
                            'subjects' => $s,
                        ),
                    ),
                ),
            );
        }

        public function getFullAddressArray($id) {
            $e = $this->findByPk($id);
            //echo '<pre>'; print_r($e);

            $query = "
             SELECT
                `index`,
                region,
                raion,
                city,
                street,
                 concat(`index`, ', ', region, raion, ', ', city, ' ', street) address
             FROM
             (
                 SELECT 
                     case
                        when strt.index = '000000'
                        then city.index
                        else strt.index
                     end `index`,
                     concat(regn.name, ' ', regn.type_text) region,
                     case
                         when left(strt.id_parent, 5) <> concat(left(strt.id_parent, 2), '000')
                         then concat(', ', rain.name, ' ', rain.type_text)
                         else ''
                     end raion,
                     concat(city.type_text, ' ', city.name) city,
                     concat(strt.type_text, ' ', strt.name) street
                 FROM
                     kladr_street strt
                     left join kladr city
                     on city.code = strt.id_parent
                     left join kladr rain
                     on rain.id = concat(left(strt.id_parent, 5), '00000000')
                     left join kladr regn
                     on regn.id = concat(left(strt.id_parent, 2), '00000000000')
                 where strt.id = '".$e->kladr_street."'
             ) sub";
             $cmd = Yii::app()->db->createCommand($query);
             $result = $cmd->queryRow();
             return $result;
        }

        public function getFullAddress($id) {
            $result = $this->getFullAddressArray($id);
             //return $result['index'] . ', ', $result['region'] . $result['raion'] . ', '
             //   . $result['city'] . ' ' $result['street'];
            return $result['address'];
        }
}
