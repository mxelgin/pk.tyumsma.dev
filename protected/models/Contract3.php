<?php

/**
 * This is the model class for table "pk_contract_3".
 *
 * The followings are the available columns in table 'pk_contract_3':
 * @property integer $id
 * @property integer $entrantid
 * @property string $name
 * @property string $person
 * @property string $osn
 * @property string $addr
 * @property string $phone
 * @property string $rekv
 */
class Contract3 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contract3 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_contract_3';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entrantid, name, person, osn, addr, phone, rekv', 'required'),
			array('entrantid', 'numerical', 'integerOnly'=>true),
			array('name, person, osn, rekv', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entrantid, name, person, osn, addr, phone, rekv', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'entrant'=>array(self::HAS_ONE, 'Entrant', array('id'=>'entrantid')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entrantid' => 'Номер абитуриента',
			'name' => 'Название организации',
			'person' => 'В лице',
			'osn' => 'Действующая на основании',
			'addr' => 'Адрес',
			'phone' => 'Телефон',
			'rekv' => 'Реквизиты',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('person',$this->person,true);
		$criteria->compare('osn',$this->osn,true);
		$criteria->compare('addr',$this->addr,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('rekv',$this->rekv,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
