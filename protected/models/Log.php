<?php

/**
 * This is the model class for table "pk_log".
 *
 * The followings are the available columns in table 'pk_log':
 * @property integer $id
 * @property string $dt
 * @property string $ip
 * @property integer $userid
 * @property string $model
 * @property string $action
 * @property string $comment
 */
class Log extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Log the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dt, ip, userid, model, action, comment', 'required'),
			array('userid', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>15),
			array('model', 'length', 'max'=>32),
			array('action', 'length', 'max'=>20),
			array('comment', 'length', 'max'=>64),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dt, ip, userid, model, action, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dt' => 'Dt',
			'ip' => 'Ip',
			'userid' => 'Userid',
			'model' => 'Model',
			'action' => 'Action',
			'comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dt',$this->dt,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}