<?php

/**
 * This is the model class for table "pk_contract_2".
 *
 * The followings are the available columns in table 'pk_contract_2':
 * @property integer $id
 * @property integer $entrantid
 * @property string $fio
 * @property string $pass_number
 * @property string $pass_date
 * @property string $pass_place
 * @property string $addr
 * @property string $phone
 */
class Contract2 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contract2 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_contract_2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entrantid, fio, pass_number, pass_date, pass_place, addr, phone', 'required'),
			array('entrantid', 'numerical', 'integerOnly'=>true),
			array('fio, pass_number', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entrantid, fio, pass_number, pass_date, pass_place, addr, phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'entrant'=>array(self::HAS_ONE, 'Entrant', array('id'=>'entrantid')),
		);
	}
        public function beforeSave() {
            parent::beforeSave();
            $this->pass_date=date("Y-m-d",strtotime($this->pass_date));
            return true;
        }
        public function afterFind() {
            parent::afterFind();
            $this->pass_date=date("d.m.Y",strtotime($this->pass_date));
            return true;
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entrantid' => 'Номер абитуриента',
			'fio' => 'ФИО',
			'pass_number' => 'Серия и номер паспорта',
			'pass_date' => 'Дата выдачи паспорта',
			'pass_place' => 'Кем выдан паспорт',
			'addr' => 'Адрес',
			'phone' => 'Телефон',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('pass_number',$this->pass_number,true);
		$criteria->compare('pass_date',$this->pass_date,true);
		$criteria->compare('pass_place',$this->pass_place,true);
		$criteria->compare('addr',$this->addr,true);
		$criteria->compare('phone',$this->phone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
