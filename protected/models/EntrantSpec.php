<?php

/**
 * This is the model class for table "pk_entrant_spec".
 *
 * The followings are the available columns in table 'pk_entrant_spec':
 * @property integer $id
 * @property integer $entrantid
 * @property integer $specid
 * @property integer $type_b
 * @property integer $type_d
 * @property integer $level
 */
class EntrantSpec extends CActiveRecord
{
        //public $types=array(1=>'Бюджет', 2=>'Внебюджет');

        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EntrantSpec the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_entrant_spec';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entrantid, specid, level', 'required'),
			array('entrantid, specid, type_b, type_d, level', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entrantid, specid, type_b, type_d, level', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'spec'=>array(self::BELONGS_TO, 'Departments', 'specid'),
                    'entrant'=>array(self::BELONGS_TO, 'Entrant', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entrantid' => 'Абитуриент',
			'specid' => 'Специальность',
			'type_b' => 'Бюджет',
                        'type_d' => 'Договор',
			'level' => 'Приоритет',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('specid',$this->specid);
		$criteria->compare('type_b',$this->type_b);
                $criteria->compare('type_d',$this->type_d);
		$criteria->compare('level',$this->level);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}