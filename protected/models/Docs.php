<?php

/**
 * This is the model class for table "pk_docs".
 *
 * The followings are the available columns in table 'pk_docs':
 * @property integer $id
 * @property integer $entrantid
 * @property integer $docid
 * @property integer $fieldid
 * @property string $value
 */
class Docs extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Docs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_docs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entrantid, docid', 'required'),
                        array('value', 'required', 'on' => 'invite'),
			array('entrantid, docid, fieldid', 'numerical', 'integerOnly'=>true),
			array('value', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entrantid, docid, fieldid, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'doc'=>array(self::BELONGS_TO, 'DirectDocs', 'docid'),
                    'field'=>array(self::BELONGS_TO, 'DirectDocsFields', 'fieldid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entrantid' => 'Entrantid',
			'docid' => 'Docid',
			'fieldid' => 'Fieldid',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('docid',$this->docid);
		$criteria->compare('fieldid',$this->fieldid);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getPassport($entrantid) {
                    $data = $this->findAllByAttributes(
                array(
                    'entrantid' => $entrantid,
                    'docid' => 21,
                )
            );

            if (count($data) == 0) return null;

            $doc = array();
            $doc['originalRecieved']   = 0;
            $doc['documentSeries']     = '';
            $doc['documentNumber']     = '';
            $doc['documentDate']       = '';
            $doc['nationalityTypeID']  = 0; // default RF?
            $doc['birthDate']          = '';

            for ($i = 0; $i < count($data); $i++) {
                $d = $data[$i];
                switch ($d->fieldid) {
                    case 0:
                        $doc['originalRecieved'] = $d->value;
                        break;
                    case 13:
                        $s = explode('-', $d->value);
                        $n = $s[1];
                        $s = $s[0];
                        $doc['documentSeries'] = $s;
                        $doc['documentNumber'] = $n;
                        break;
                    case 14:
                        $doc['documentDate'] = MechanicalBear::convertDateToSQL($d->value);
                        break;
                    case 15:
                        $doc['documentOrganization'] = $d->value;
                        break;
                }
            }
                    $dataProvider=new CActiveDataProvider('Entrant');
            $data = $dataProvider->model->findByPK($entrantid);
            if ($data != null) {
                $doc['birthDate'] = $data->birth_date;
            }

            return array('identityDocument' => $doc);
        }

        public function getEduDoc($entrantid) {
                    $data = $this->findAllByAttributes(
                array(
                    'entrantid' => $entrantid,
                    'docid' => 29,
                )
            );

            if (count($data) == 0) return null;

            $doc = array();
            $doc['originalRecieved']       =  0;
            $doc['documentSeries']         = '';
            $doc['documentNumber']         = '';
            $doc['documentDate']           = '';
            $doc['documentOrganization']   = '';

            for ($i = 0; $i < count($data); $i++) {
                $d = $data[$i];
                switch ($d->fieldid) {
                    case 0:
                        $doc['originalRecieved'] = $d->value;
                        break;
                    case 20:
                        $doc['documentNumber'] = $d->value;
                        break;
                    case 21:
                        $doc['documentDate'] = MechanicalBear::convertDateToSQL($d->value);
                        break;
                    case 19: // TYPE
                        $type = mb_strtolower($d->value, 'utf-8');
                        //$type = str_ireplace('a', 'а', $type);
                        if (mb_strpos($type, 'аттестат', 0, 'utf-8') !== false)   $type = 1;
                        elseif (mb_strpos($type, 'ысш', 0, 'utf-8') !== false)    $type = 2;
                        elseif (mb_strpos($type, 'диплом', 0, 'utf-8') !== false) $type = 3;
                        break;
                }
            }

            $s = str_replace(' ', '', $doc['documentNumber']);
            $s = str_replace('№', '', $s);

            switch ($type) {
                case 1:
                    $type = 'schoolSertificateDocument';
                    $doc['documentSeries'] = trim(mb_substr($s, -7, 10, 'utf-8'));
                    $doc['documentNumber'] = trim(mb_substr($s, 0, -7, 'utf-8')); 
                    break;
                case 2:
                    $type = 'highEduDiplomaDocument';
                    $doc['documentSeries'] = trim(mb_substr($s, 0, 3, 'utf-8'));
                    $doc['documentNumber'] = trim(mb_substr($s, 3, 10, 'utf-8'));
                    break;
                case 3:
                    $type = 'middleEduDiplomaDocument';
                    $doc['documentSeries'] = trim(mb_substr($s, 0, 4, 'utf-8'));
                    $doc['documentNumber'] = trim(mb_substr($s, 4, 10, 'utf-8'));
                    break;
                default:
                    $doc['documentSeries'] = $type;
                    $type = 'basicDiplomaDocument';
            }

            return array('eduDocuments' => array(
                    'eduDocument' => array(
                        $type => $doc
                    ),
                ),
            );
        }
}