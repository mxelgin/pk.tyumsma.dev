<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $userid
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $create_date
 * @property string $modify_date
 */
class Users extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('last_name', 'required'),
			array('first_name', 'length', 'max'=>30),
			array('last_name, middle_name', 'length', 'max'=>50),
			array('create_date, modify_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, first_name, last_name, middle_name, create_date, modify_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'auth'=>array(self::HAS_ONE, 'Auth', 'userid'),
		);
	}
        
        public function beforeSave() {
            parent::beforeSave();
            $this->create_date=date('Y-m-d h:i:s', time());
            return true;
        }
        
        public function beforeDelete() {
            parent::beforeDelete();
            Auth::model()->deleteByPk($this->userid);
            return true;
        }

        /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'first_name' => 'Имя',
			'last_name' => 'Фамилия',
			'middle_name' => 'Отчество',
			'create_date' => 'Создан',
			'modify_date' => 'Модифицирован',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('modify_date',$this->modify_date,true);
                $criteria->with = array('auth');
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
?>