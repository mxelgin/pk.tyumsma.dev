<?php

/**
 * This is the model class for table "pk_entrant".
 *
 */
class Export extends CActiveRecord
{
        public $sextypes=array(1=>'Мужской', 2=>'Женский');
        public $servicetypes=array(1=>'Военнообязанный', 2=>'Невоеннообязанный');
        public $vktypes=array(0=>'Общий конкурс', 1=>'Вне конкурса');
        public $origtypes=array(0=>'Копии', 1=>'Оригиналы');
        public $tooktypes=array(0=>'В архиве', 1=>'Забрал');
        public $hometypes=array(1=>'Город/поселок', 2=>'Село/деревня');
        public $gradedoctypes=array(1=>'Сертификат ЕГЭ', 2=>'Экзаменационный лист', 3=>'Олимпиада');

        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Entrant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_entrant';
	}
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fam, name, birth_date, citizenship, sex, maindoc', 'required'),
			array('rating, citizenship, sex, need_hostel, grade_rus_doctype, grade_him_doctype, grade_bio_doctype, home_region, home_type, service, maindoc, foreignid, foreign_exp, foreign_grade, deleted, vk, took, orig', 'numerical', 'integerOnly'=>true),
			array('fam, grade_rus_doc, grade_him_doc, grade_bio_doc, name, m_name, home_street, home_fact', 'length', 'max'=>255),
                        array('grade_rus, grade_him, grade_bio', 'numerical', 'max'=>100, 'min'=>10),
                        array('home_build', 'length', 'max'=>50),
                        array('home_kv', 'length', 'max'=>10),
			array('phone', 'length', 'max'=>20),
                        array('fam, name, m_name', 'match', 'pattern'=>'/^[А-я- ]+$/u'),
                        array('birth_date', 'date','format' => 'dd.mm.yyyy'),
                        //array('birth_place_id', 'exist', 'attributeName'=>'id', 'className'=>'DirectCity'),
                        //array('home_city', 'exist', 'attributeName'=>'id', 'className'=>'DirectCity'),
                        array('citizenship', 'exist', 'attributeName'=>'id', 'className'=>'DirectCitizenship'),
                        array('sex, service', 'in', 'range'=>array(1,2)),
                        array('vk, took, orig', 'boolean'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fam, name, m_name, phone, grade_rus_doc, grade_him_doc, grade_bio_doc, grade_rus_doctype, grade_him_doctype, grade_bio_doctype, birth_date, birth_place_id, citizenship, sex, home_city, home_region, home_fact, home_type, home_street, home_build, home_kv, need_hostel, service, social_work, talent, sport, maindoc, grade_rus, grade_him, grade_bio, rating, foreignid, foreign_exp, foreign_grade, deleted, vk, took, orig', 'safe', 'on'=>'search'),
		);
	}
        
        public function beforeSave() {
            parent::beforeSave();
            $this->birth_date=date("Y-m-d",strtotime($this->birth_date));
            $this->rating=$this->grade_bio+$this->grade_him+$this->grade_rus;

            $cityModel=new DirectCity;
            $this->birth_place_id=trim($this->birth_place_id);
            $city=$cityModel->find("name like '".mysql_escape_string($this->birth_place_id)."'");
            if ($city->id) $this->birth_place_id=$city->id;
            else{
                $cityModel->name=$this->birth_place_id;
                $cityModel->save();
                $this->birth_place_id=$cityModel->id;
            }

            /*$homecityModel=new DirectCity;
            $this->home_city=trim($this->home_city);
            $city=$homecityModel->find("name like '".mysql_escape_string($this->home_city)."'");
            if ($city->id) $this->home_city=$city->id;
            else{
                $homecityModel->name=$this->home_city;
                $homecityModel->save();
                $this->home_city=$homecityModel->id;
            }*/

            return true;
        }

        public function afterValidate() {
            parent::afterValidate();
            if (!count($this->specs))
                $this->addError ('specs', 'Необходимо выбрать хотябы 1 специальность');
        }


        public function afterSave() {
            parent::afterSave();
            $idbase='';
            foreach ($this->docs as $doc){
                $doc->entrantid=$this->id;
                $doc->save();
                $idbase.=$doc->id.',';
            }
            $idbase=substr($idbase, 0, -1);
            Docs::model()->deleteAll('entrantid='.$this->id.' and id not in ('.$idbase.')');
            
            $idbase='';
            foreach ($this->specs as $spec){
                $spec->entrantid=$this->id;
                $spec->save();
                $idbase.=$spec->id.',';
            }
            $idbase=substr($idbase, 0, -1);
            $deletespecs=EntrantSpec::model()->findAll('entrantid='.$this->id.' and id not in ('.$idbase.')');
            foreach ($deletespecs as $dsp)
                $dsp->delete();

            foreach ($this->parents as $parent){
                $parent->entrantid=$this->id;
                $parent->save();
            }
            $this->edu->entrantid=$this->id;
            $this->edu->save();
        }

        public function afterFind() {
            parent::afterFind();
            $this->birth_date=date("d.m.Y",strtotime($this->birth_date));
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'citizen'=>array(self::BELONGS_TO, 'DirectCitizenship', 'citizenship'),
                    'birth_city'=>array(self::BELONGS_TO, 'DirectCity', 'birth_place_id'),
                    'home_city_db'=>array(self::BELONGS_TO, 'DirectCity', 'home_city'),
                    'home_region_db'=>array(self::BELONGS_TO, 'DirectRegions', 'home_region'),
                    'foreign'=>array(self::BELONGS_TO, 'DirectForeign', 'foreignid'),
                    'mdoc'=>array(self::BELONGS_TO, 'DirectDocs', 'maindoc'),
                    'specs'=>array(self::HAS_MANY, 'EntrantSpec', 'entrantid'),
                    'edu'=>array(self::HAS_ONE, 'Education', 'entrantid'),
                    'docs'=>array(self::HAS_MANY, 'Docs', 'entrantid'),
                    'parents'=>array(self::HAS_MANY, 'Parents', 'entrantid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fam' => 'Фамилия',
			'name' => 'Имя',
			'm_name' => 'Отчество',
			'phone' => 'Телефон',
			'birth_date' => 'Дата рождения',
			'birth_place_id' => 'Место рождения',
			'citizenship' => 'Гражданство',
			'sex' => 'Пол',
			'need_hostel' => 'Нужно общежитие',
			'service' => 'Служба в армии',
			'social_work' => 'Общественная работа',
			'talent' => 'Самодеятельность',
			'sport' => 'Спорт',
                        'maindoc' => 'Основной документ',
                        'grade_rus' => 'Оценка по русскому',
                        'grade_him' => 'Оценка по химии',
                        'grade_bio' => 'Оценка по биологии',
                        'rating' => 'Рейтинг',
                        'foreignid' => 'Иностранный язык',
                        'foreign_exp' => 'Опыт изучения языка',
                        'foreign_grade' => 'Оценка по языку',
                        'deleted' => 'Удален',
                        'vk'=>'Вне конкурса',
                        'took'=>'Забор',
                        'home_type'=>'Тип населенного пункта',
                        'home_city'=>'Населенный пункт',
                        'home_region'=>'Область',
                        'home_fact'=>'Фактический адрес',
                        'orig'=>'Оригиналы',
                        'grade_rus_doc'=>'Номер документа (русский)',
                        'grade_him_doc'=>'Номер документа (химия)',
                        'grade_bio_doc'=>'Номер документа (биология)',
                        'grade_rus_doctype'=>'Документ (русский)',
                        'grade_him_doctype'=>'Документ (химия)',
                        'grade_bio_doctype'=>'Документ (биология)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fam',$this->fam,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('m_name',$this->m_name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('birth_date',$this->birth_date,true);
		$criteria->compare('birth_place_id',$this->birth_place_id);
		$criteria->compare('citizenship',$this->citizenship,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('need_hostel',$this->need_hostel);
		$criteria->compare('service',$this->service);
		$criteria->compare('social_work',$this->social_work,true);
		$criteria->compare('talent',$this->talent,true);
		$criteria->compare('sport',$this->sport,true);
                $criteria->compare('maindoc',$this->maindoc);
                $criteria->compare('grade_rus',$this->grade_rus);
                $criteria->compare('grade_him',$this->grade_him);
                $criteria->compare('grade_bio',$this->grade_bio);
                $criteria->compare('foreignid',$this->foreignid);
                $criteria->compare('rating',$this->rating);
                $criteria->compare('foreign_exp',$this->foreign_exp,true);
                $criteria->compare('foreign_grade',$this->foreign_grade,true);
                $criteria->compare('home_city',$this->home_city);
                $criteria->compare('home_type',$this->home_type);
                $criteria->compare('home_region',$this->home_region);
                $criteria->compare('home_street',$this->home_street, true);
                $criteria->compare('home_build',$this->home_build, true);
                $criteria->compare('home_kv',$this->home_kv, true);
                $criteria->compare('home_fact',$this->home_fact, true);
                $criteria->compare('deleted', 0);
                $criteria->compare('vk', $this->vk);
                $criteria->compare('took', $this->took);
                $criteria->compare('orig', $this->orig);
                $criteria->compare('grade_rus_doc', $this->grade_rus_doc);
                $criteria->compare('grade_him_doc', $this->grade_him_doc);
                $criteria->compare('grade_bio_doc', $this->grade_bio_doc);
                $criteria->compare('grade_rus_doctype', $this->grade_rus_doctype);
                $criteria->compare('grade_him_doctype', $this->grade_him_doctype);
                $criteria->compare('grade_bio_doctype', $this->grade_bio_doctype);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination' => array('pageSize' => 50),
		));
	}
}