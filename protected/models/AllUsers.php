<?php

/**
 * This is the model class for table "all_users".
 *
 * The followings are the available columns in table 'all_users':
 * @property string $fio
 * @property string $id
 * @property string $podr
 * @property string $appointment
 */
class AllUsers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AllUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'all_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fio', 'length', 'max'=>250),
			array('podr, appointment', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('fio, id, podr, appointment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
                        'fio' => 'ФИО',
			'podr' => 'Подразделение',
			'appointment' => 'Должность',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('podr',$this->podr,true);
		$criteria->compare('appointment',$this->appointment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}