<?php

/**
 * This is the model class for table "pk_grades".
 *
 * The followings are the available columns in table 'pk_grades':
 * @property integer $id
 * @property integer $ab_id
 * @property string $number
 * @property string $receipt_date
 * @property string $grades
 */
class Grades extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Grades the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_grades';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ab_id, number, receipt_date, grades', 'required'),
			array('ab_id', 'numerical', 'integerOnly'=>true),
			array('number', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ab_id, number, receipt_date, grades', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ab_id' => 'Ab',
			'number' => 'Number',
			'receipt_date' => 'Receipt Date',
			'grades' => 'Grades',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ab_id',$this->ab_id);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('receipt_date',$this->receipt_date,true);
		$criteria->compare('grades',$this->grades,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}