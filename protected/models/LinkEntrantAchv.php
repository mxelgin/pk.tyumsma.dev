<?php

/**
 * This is the model class for table "link_entrant_achv".
 *
 * The followings are the available columns in table 'link_entrant_achv':
 * @property integer $id
 * @property integer $entrantid
 * @property integer $achvid
 */
class LinkEntrantAchv extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LinkEntrantAchv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'link_entrant_achv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entrantid, achvid', 'required'),
			array('entrantid, achvid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entrantid, achvid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'achv'=>array(self::BELONGS_TO, 'DirectPriorityPravoOthers', array('achvid'=>'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entrantid' => 'Entrantid',
			'achvid' => 'Achvid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('achvid',$this->achvid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}