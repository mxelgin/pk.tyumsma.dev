<?php

/**
 * This is the model class for table "pk_parents".
 *
 * The followings are the available columns in table 'pk_parents':
 * @property integer $id
 * @property integer $entrantid
 * @property string $fio
 * @property string $work
 * @property string $phone
 */
class Parents extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Parents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_parents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entrantid', 'required'),
			array('entrantid', 'numerical', 'integerOnly'=>true),
			array('fio, addr, work', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entrantid, fio, addr, work, phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'entrant'=>array(self::HAS_MANY, 'Entrant', array('id'=>'entrantid')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entrantid' => 'Абитуриент',
			'fio' => 'ФИО',
			'work' => 'Работа',
			'phone' => 'Телефон',
                        'addr' => 'Адрес',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('work',$this->work,true);
		$criteria->compare('phone',$this->phone,true);
                $criteria->compare('addr',$this->addr,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}