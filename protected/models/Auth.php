<?php

/**
 * This is the model class for table "auth".
 *
 * The followings are the available columns in table 'auth':
 * @property string $userid
 * @property string $login
 * @property string $password
 * @property string $modify_date
 * @property string $role
 */
class Auth extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Auth the static model class
	 */
        public $roles=array('operator'=>'Оператор','secretary'=>'Секретарь','administrator'=>'Администратор');
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'auth';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('role, login', 'required'),
			array('userid', 'length', 'max'=>10),
			array('login, password', 'length', 'max'=>50),
			array('role', 'length', 'max'=>13),
			array('modify_date', 'safe'),
                        array('login', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, login, password, modify_date, role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'users'=>array(self::HAS_ONE, 'Users', 'userid'),
		);
	}
        
        public function beforeSave() {
            parent::beforeSave();
            if (!empty($this->password)) $this->password=md5($this->password);
            return true;
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'login' => 'Логин',
			'password' => 'Пароль',
			'modify_date' => 'Модифицирован',
			'role' => 'Роль',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('modify_date',$this->modify_date,true);
		$criteria->compare('role',$this->role,true);
                 

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        /**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password)
	{
		return $this->hashPassword($password)===$this->password;
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @param string salt
	 * @return string hash
	 */
	public function hashPassword($password)
	{
		return md5($password);
	}
}