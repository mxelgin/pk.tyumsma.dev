<?php

/**
 * This is the model class for table "direct_docs_fields".
 *
 * The followings are the available columns in table 'direct_docs_fields':
 * @property integer $id
 * @property integer $docid
 * @property string $name
 * @property string $type
 * @property integer $required
 */
class DirectDocsFields extends CActiveRecord
{
        
        public $fieldtypes=array('text'=>'Текстовое поле', 'serialnumber'=>'Серия и номер паспорта', 'foreign'=>'Серия и номер загран. паспорта', 'date'=>'Дата', 'number'=>'Номер', 'egenumber'=>'Сертификат ЕГЭ');
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DirectDocsFields the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'direct_docs_fields';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('docid, name, type', 'required'),
			array('docid, required, editonly', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('type', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, docid, name, type, required, editonly', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'docid' => 'Docid',
			'name' => 'Название',
			'type' => 'Тип',
			'required' => 'Обязательное поле',
                        'editonly'=>'Только для редактирования',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('docid',$this->docid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('required',$this->required);
                $criteria->compare('editonly',$this->editonly);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}