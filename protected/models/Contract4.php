<?php

/**
 * This is the model class for table "pk_contract_4".
 *
 * The followings are the available columns in table 'pk_contract_4':
 * @property integer $id
 * @property integer $entrantid
 * @property string $org_name
 * @property string $org_person
 * @property string $org_osn
 * @property string $org_addr
 * @property string $org_phone
 * @property string $org_rekv
 * @property string $fiz_fio
 * @property string $fiz_pass_number
 * @property string $fiz_pass_date
 * @property string $fiz_pass_place
 * @property string $fiz_addr
 * @property string $fiz_phone
 */
class Contract4 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contract4 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pk_contract_4';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entrantid, org_name, org_person, org_osn, org_addr, org_phone, org_rekv, fiz_fio, fiz_pass_number, fiz_pass_date, fiz_pass_place, fiz_addr, fiz_phone', 'required'),
			array('entrantid', 'numerical', 'integerOnly'=>true),
			array('org_name, org_person, org_osn, org_addr, org_rekv, fiz_fio, fiz_pass_number, fiz_pass_place, fiz_addr, fiz_phone', 'length', 'max'=>255),
			array('org_phone', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, entrantid, org_name, org_person, org_osn, org_addr, org_phone, org_rekv, fiz_fio, fiz_pass_number, fiz_pass_date, fiz_pass_place, fiz_addr, fiz_phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'entrant'=>array(self::HAS_ONE, 'Entrant', array('id'=>'entrantid')),
		);
	}
        public function beforeSave() {
            parent::beforeSave();
            $this->fiz_pass_date=date("Y-m-d",strtotime($this->fiz_pass_date));
            return true;
        }
        public function afterFind() {
            parent::afterFind();
            $this->fiz_pass_date=date("d.m.Y",strtotime($this->fiz_pass_date));
            return true;
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'entrantid' => 'Номер абитуриента',
			'org_name' => 'Название организации',
			'org_person' => 'В лице',
			'org_osn' => 'На основании',
			'org_addr' => 'Адрес организации',
			'org_phone' => 'Телефон организации',
			'org_rekv' => 'Реквизиты организации',
			'fiz_fio' => 'ФИО',
			'fiz_pass_number' => 'Серия и номер паспорта',
			'fiz_pass_date' => 'Дата выдачи паспорта',
			'fiz_pass_place' => 'Место выдачи паспорта',
			'fiz_addr' => 'Адрес',
			'fiz_phone' => 'Телефон',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('entrantid',$this->entrantid);
		$criteria->compare('org_name',$this->org_name,true);
		$criteria->compare('org_person',$this->org_person,true);
		$criteria->compare('org_osn',$this->org_osn,true);
		$criteria->compare('org_addr',$this->org_addr,true);
		$criteria->compare('org_phone',$this->org_phone,true);
		$criteria->compare('org_rekv',$this->org_rekv,true);
		$criteria->compare('fiz_fio',$this->fiz_fio,true);
		$criteria->compare('fiz_pass_number',$this->fiz_pass_number,true);
		$criteria->compare('fiz_pass_date',$this->fiz_pass_date,true);
		$criteria->compare('fiz_pass_place',$this->fiz_pass_place,true);
		$criteria->compare('fiz_addr',$this->fiz_addr,true);
		$criteria->compare('fiz_phone',$this->fiz_phone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
